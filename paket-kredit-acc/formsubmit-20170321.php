<?php
error_reporting(0);
require_once 'includes/config.php';
require_once 'includes/PHPMailer/PHPMailerAutoload.php';
//require_once 'includes/autoload.php';
$pid = sanitize($_POST['paket']);
$pkv = sanitize($_POST['vtype']);
$nama = sanitize($_POST['nama']);
$notelp = sanitize($_POST['notelp']);
$emailaddr = sanitize($_POST['emailaddr']);
$kota = sanitize($_POST['kota']);
$cabang = sanitize($_POST['cabang']);

$areas = $database->select("areas", [
	"id",
	"description"
], [
	"id" => $kota
]);
$namakota = $areas[0]["description"];

$category = $database->select("paketkredit_vehicle_types", [
"[>]paketkredit_categories" => ["pkc_id" => "pkc_id"],
	], [
	"paketkredit_categories.pkc_name",
	"paketkredit_vehicle_types.pkv_vehicletype"
], [
	"paketkredit_vehicle_types.pkv_id" => $pkv,
	"LIMIT" => 1
]);

$packages = $database->select("paketkredit_packages", [
	"pkp_id",
	"pkv_id",
	"pkp_variant",
	"pkp_price",
	"pkp_term",
	"pkp_pdp",
	"pkp_dp",
	"pkp_installment"
], [
	"pkp_id" => $pid
]);

$jenispaket = $category[0]["pkc_name"];
$tipemobil = $category[0]["pkv_vehicletype"];
$jenismobil = $packages[0]["pkp_variant"];
$hargamobil = $packages[0]["pkp_price"];
$termmobil = $packages[0]["pkp_term"];
$persendp = $packages[0]["pkp_pdp"];
$dpmobil = $packages[0]["pkp_dp"];
$angsuranmobil = $packages[0]["pkp_installment"];

$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
	"ORDER" => "pkc_order"
]);

$siteKey = '6LdpsxETAAAAAOlPjhE-uQbe-8Y3Bs60vgy9axSJ';
$secret = '6LdpsxETAAAAAGB9VYRuOIZKOvVpAIEZnGs3vKsi';
?>

<!-- Conversion Pixel - Form Submit Astra - DO NOT MODIFY -->
<script src="https://secure.adnxs.com/px?id=642238&t=1" type="text/javascript"></script>
<!-- End of Conversion Pixel -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Paket Kredit VIP ACCESS - ACC</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
    <link href="css/jquery.fancybox.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script>
	       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	       })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	       ga('create', 'UA-48042405-1', 'acc.co.id');
	       ga('send', 'pageview');
	   </script>
	   </head>
  <body>
	<div class="container">
	    <div class="row">
				<div class="row">
	             <div class="col-md-2 hidden-xs"><div data-spy="affix" data-offset-top="0"><img src="images/banner-skyscrapper1.png" alt="Banner" /></div></div>
	             <div class="col-sm-12 col-md-8">
								 <div id="logo2" class="visible-xs"><a href="http://www.acc.co.id"><img src="images/logo.png" alt="ACC Logo" /></a></div>
	             <h1>PAKET VIP ACCESS</h1>
	             <div id="menuatas">
								 <?php
 		              foreach($categories as $kategori) {
 		                  $menuclass = "menu".strtolower($kategori["pkc_name"]);
 		              ?>
 									<a href="index.php?v=<?php echo $kategori["pkc_id"]?>" class="<?php echo $menuclass;?>"><?php echo $kategori["pkc_name"]?></a>
 								<?php }?>
	             </div>
							 <div class="entry-wrapper">
							 	<div class="row">
							 			<div class="col-sm-12 col-md-12 col-lg-12">
							 					<div class="box-orange box-toyota" id="pkheader">
							 							<table>
							 									<tr>
							 											<td width="15%" class="abottom" style="color:#fff">PAKET</td>
							 												<td width="1%" class="abottom" style="color:#fff">:</td>
							 												<td width="84%" style="color:#fff">KREDIT&nbsp;&nbsp; <span class="mantap">VIP ACCESS <?php echo strtoupper($vendor);?></span></td>
							 										</tr>
							 									<tr>
							 											<td width="20%" style="color:#fff">TENOR</td>
							 												<td width="1%" style="color:#fff">:</td>
							 												<td width="79%" style="color:#fff">1 - 5 TAHUN</td>
							 										</tr>
							 										<tr>
							 												<td width="20%" style="color:#fff">BERLAKU</td>
							 													<td width="1%" style="color:#fff">:</td>
							 													<td width="79%" style="color:#fff">FEBRUARI - DESEMBER 2017</td>
							 											</tr>
							 								</table>
							 								<div id="logo" class="hidden-xs"><a href="http://www.acc.co.id"><img src="images/logo.png" alt="ACC Logo" /></a></div>
							 						</div>
							 				</div>
							 		 </div>
							 	</div>
            <?php
            if (!isset($_POST['g-recaptcha-response'], $nama, $notelp, $emailaddr, $kota, $cabang)) {
			?>
             <div class="box-orange2 text-center">
			   Mohon lengkapi data diri Anda!
			    </div>
            <?php } else {
				$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
				if ($response.success == true) {

				//proses

      	$pkentry = $database->insert("paketkredit_applicants", [
				"pkp_id" => $pid,
				"pka_name" => $nama,
				"pka_phone" => $notelp,
				"pka_email" => $emailaddr,
				"pka_city" => $namakota,
				"pka_branch" => $cabang,
				"pka_category" => $jenispaket,
				"pka_type" => $tipemobil,
				"pka_variant" => $jenismobil,
				"pka_price" => $hargamobil,
				"pka_term" => $termmobil,
				"pka_pdp" => $persendp,
				"pka_dp" => $dpmobil,
				"pka_installment" => $angsuranmobil,
				"pka_created" => date('Y-m-d H:i:s')
			]);

				//send email

				$recipients = $database->select("paketkredit_recipients", [
				"name",
				"email"
				], [
				"active" => 'Yes'
				]);

				//$to="";
				//foreach($recipients as $rcpt_to){
				//	$to .= $rcpt_to["email"]."; ";
				 //}


				/*$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 $headers = 'From: '.$nama.' <'.$emailaddr.'>'."\r\n" .
				 'Bcc: benyfr@gmail.com\r\n' .
				'Reply-To: '.$emailaddr. "\r\n" .
				'X-Mailer: PHP/' . phpversion();*/
				$pesanh = "Nama : $nama <br />";
				$pesanh .= "Email : $emailaddr <br />";
				$pesanh .= "No. Telepon : $notelp <br />";
				$pesanh .= "Kota : $namakota <br />";
				$pesanh .= "Cabang Terdekat : $cabang <br />";
				$pesanh .= "Brand : $jenispaket <br />";
				$pesanh .= "Tipe Mobil : $tipemobil <br />";
				$pesanh .= "Jenis Mobil : $jenismobil <br />";
				$pesanh .= "Tenor : $termmobil bulan<br />";
				$pesanh .= "Persentase DP : ".$persendp."% <br />";
				$pesanh .= "Harga Mobil : ".number_format($hargamobil,0,',','.')." <br />";
				$pesanh .= "Uang Muka : ".number_format($dpmobil,0,',','.')." <br />";
				$pesanh .= "Angsuran : ".number_format($angsuranmobil,0,',','.')." <br />";

				$pesan = "Nama : $nama \r\n";
				$pesan .= "Email : $emailaddr \r\n";
				$pesan .= "No. Telepon : $notelp \r\n";
				$pesan .= "Kota : $namakota \r\n";
				$pesan .= "Cabang Terdekat : $cabang \r\n";
				$pesan .= "Brand : $jenispaket \r\n";
				$pesan .= "Tipe Mobil : $tipemobil \r\n";
				$pesan .= "Jenis Mobil : $jenismobil \r\n";
				$pesan .= "Tenor : $termmobil bulan\r\n";
				$pesan .= "Persentase DP : ".$persendp."% \r\n";
				$pesan .= "Harga Mobil : ".number_format($hargamobil,0,',','.')." \r\n";
				$pesan .= "Uang Muka : ".number_format($dpmobil,0,',','.')." \r\n";
				$pesan .= "Angsuran : ".number_format($angsuranmobil,0,',','.')." \r\n";

				//send mail
				$mail = new PHPMailer;
				$mail->isSendmail();
				$mail->setFrom("info@acc.co.id", "ACC Website");
				$mail->addReplyTo($emailaddr, $nama);
				foreach($recipients as $rcpt_to){
					$mail->addAddress($rcpt_to["email"],$rcpt_to["name"]);
				}
				$mail->isHTML(true);
				$mail->Subject = '[Paket Kredit ACC] Customer Apply';
				$mail->Body    = $pesanh;
				$mail->AltBody = $pesan;

				$pesannya = "Terima kasih, aplikasi kredit Anda akan segera kami proses. \r\n\r\n\r\n Astra Credit Companies";
				$pesannyah = "Terima kasih, aplikasi kredit Anda akan segera kami proses. <br><br> Astra Credit Companies";

				$mail2 = new PHPMailer;
				$mail2->isSendmail();
				$mail2->setFrom('info@acc.co.id','Astra Credit Companies');
				$mail2->addReplyTo('info@acc.co.id','Astra Credit Companies');
				$mail2->addAddress($emailaddr, $nama);
				$mail2->isHTML(true);
				$mail2->Subject = '[Paket Kredit ACC] Thank You';
				$mail2->Body    = $pesannyah;
				$mail2->AltBody = $pesannya;

				if($mail->send()||$mail2->send()) {
		?>
			<div id="maincontent">
    		  	<div class="box-orange2 text-center" style="margin-top:20px;">
			    Terima kasih, aplikasi Paket Kredit ACC Anda telah terkirim. Kami akan memberikan respon kepada Anda secepatnya!
			    </div>
				</div>
              <!--end proc-->
              <?php } else {?>
					<div id="maincontent">
				  <div class="box-orange2 text-center" style="margin-top:20px;">
			    Terima kasih, aplikasi Paket Kredit ACC Anda telah terkirim. Kami akan memberikan respon kepada Anda secepatnya!
			    </div>
					</div>
			<?php	}
			  } else { ?>
					<div id="maincontent">
               <div class="box-orange2 text-center">
			    Silahkan cek kembali kode keamanan Anda!
			    	</div>
					</div>
              <?php } }?>
 			</div>
			<div class="col-md-2 hidden-xs"><div data-spy="affix" data-offset-top="0"><img src="images/banner-skyscrapper2.png" alt="Banner" /></div></div>
          </div>
					<!--menubawah-->
					<div class="row">
						<div class="col-sm-12 col-md-8 col-md-offset-2">
					<div class="menu-wrapper">
								<a href="index.php">Home</a> | <a href="http://www.acc.co.id/simulasi-kredit-mobil" target="_blank">Cek Simulasi Kredit</a> | <a href="http://www.acc.co.id" target="_blank">Web ACC</a>
								</div>
					<div class="row mart-20">
										<div class="col-md-3 col-xs-6"><a href="http://www.acc.co.id" target="_blank"><img src="images/footer-web.jpg" alt="" class="img-responsive" /></a></div>
											<div class="col-md-3 col-xs-6"><a href="https://www.facebook.com/ACCKreditMobil" target="_blank"><img src="images/footer-fb.jpg" alt="" class="img-responsive" /></a></div>
											<div class="col-md-3 col-xs-6"><a href="https://www.twitter.com/ACCKreditMobil" target="_blank"><img src="images/footer-twitter.jpg" alt="" class="img-responsive" /></a></div>
											<div class="col-md-3 col-xs-6"><img src="images/footer-phone.jpg" alt="" class="img-responsive" /></div>
									</div>
							</div>
					</div>
					<!--end menubawah-->
					<div class="row">
						<div id="menubawah">
							<?php
	              foreach($categories as $kategori) {
	                  $menuclass = "menu".strtolower($kategori["pkc_name"]);
	              ?>
								<a href="index.php?v=<?php echo $kategori["pkc_id"]?>" class="<?php echo $menuclass;?>"><?php echo $kategori["pkc_name"]?></a>
							<?php }?>
						</div>
					</div>
		</div>
  </body>
</html>
