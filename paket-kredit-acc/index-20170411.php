<?php
error_reporting(0);
require_once 'includes/config.php';
$v = (empty($_GET['v'])) ? 1 : sanitize($_GET['v']);
$datas = $database->select("paketkredit_vehicle_types", [
"[>]paketkredit_categories" => ["pkc_id" => "pkc_id"],
	], [
	"paketkredit_categories.pkc_id",
	"paketkredit_categories.pkc_name",
	"paketkredit_categories.pkc_published",
	"paketkredit_vehicle_types.pkv_id",
	"paketkredit_vehicle_types.pkv_vehicletype",
	"paketkredit_vehicle_types.pkv_thumbnails"
], [
	"paketkredit_categories.pkc_id" => $v,
	"ORDER" => "paketkredit_vehicle_types.pkv_vehicletype ASC"
]);
if (!$datas) {
	$vendor = "toyota";
} else {
	$vendor = strtolower($datas[0]["pkc_name"]);
}

$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
	"ORDER" => "pkc_order"
]);
?>

  <?php include_once('header.php');?>
				<!--car info-->
         <?php if ($v<4) { ?>
            <div id="maincontent">
               <div class="row">
               <?php
      				   $i=0;
      				   foreach($datas as $data) {
      				   $i++;
      				   ?>
                  <div class="col-sm-6">
                      <div class="carmodel-wrapper">
                      <a href="detail.php?pid=<?php echo $data["pkv_id"] ;?>" class="carmodel-link">
                         		 <div class="text-center">
                           	   <img src="images/mobil/<?php echo $data["pkv_thumbnails"] ;?>" alt="paket kredit <?php echo $data["pkv_vehicletype"] ;?>" />
                        		  </div>
                          </a>
                      </div>
                  </div>
                    <?php if ($i==2) {?>
                    </div><div class="row">
                    <?php } ?>

                <!--end car-->
                <?php }?>
              </div>
            </div>
            <!--end entry -->
            <?php } else {?>
              <div id="maincontent">
                <div class="box-orange2 text-center">
			               MAAF, DATA YANG ANDA CARI TIDAK DITEMUKAN!
    			      </div>
              </div>
            <?php }?>
      <?php include_once('footer.php');?>
