<?php
error_reporting(0);
require_once 'includes/config.php';
$pid = (empty($_GET['pid'])) ? 1 : sanitize($_GET['pid']);
$pkv = (empty($_GET['pkv'])) ? 1 : sanitize($_GET['pkv']);

$areas = $database->select("areas", [
	"id",
	"description"
], [
	"ORDER" => "description"
]);
$branches = $database->select("branches", [
	"branch_id",
	"description"
], [
	"area_id" => 1,
	"ORDER" => "description"
]);
$packages = $database->select("paketkredit_packages", [
	"pkp_id",
	"pkv_id",
	"pkp_variant",
	"pkp_price",
	"pkp_term",
	"pkp_dp",
	"pkp_installment"
], [
	"pkp_id" => $pid
]);
$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
	"ORDER" => "pkc_order"
]);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Paket Kredit VIP ACCESS - ACC</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css?v=2.2" rel="stylesheet">
    <link href="css/jquery.fancybox.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script>
	       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	       })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	       ga('create', 'UA-48042405-1', 'acc.co.id');
	       ga('send', 'pageview');
	   </script>
		  <script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
 <!-- Segment Pixel - Form Submission - DO NOT MODIFY -->
<script src="https://secure.adnxs.com/seg?add=4185825&t=1" type="text/javascript"></script>
<!-- End of Segment Pixel -->
  </head>
  <body>
   <div class="container">
    	<div class="row">
				<div class="col-md-2 hidden-xs"><div data-spy="affix" data-offset-top="0"><img src="images/banner-skyscrapper1.png" alt="Banner" /></div></div>
				<div class="col-sm-12 col-md-8">
          <div id="logo2" class="visible-xs"><a href="http://www.acc.co.id"><img src="images/logo.png" alt="ACC Logo" /></a></div>
				<h1>PAKET VIP ACCESS</h1>
				<div id="menuatas">
					<?php
						foreach($categories as $kategori) {
								$menuclass = "menu".strtolower($kategori["pkc_name"]);
						?>
						<a href="index.php?v=<?php echo $kategori["pkc_id"]?>" class="<?php echo $menuclass;?>"><?php echo $kategori["pkc_name"]?></a>
					<?php }?>
				</div>
				<div class="entry-wrapper">
					<div class="row">
							<div class="col-sm-12 col-md-12 col-lg-12">
									<div class="box-orange box-toyota" id="pkheader">
											<table>
													<tr>
															<td width="15%" class="abottom">PAKET</td>
																<td width="1%" class="abottom">:</td>
																<td width="84%">KREDIT&nbsp;&nbsp; <span class="mantap">VIP ACCESS <?php echo strtoupper($vendor);?></span></td>
														</tr>
													<tr>
															<td width="20%">TENOR</td>
																<td width="1%">:</td>
																<td width="79%">1 - 5 TAHUN</td>
														</tr>
														<tr>
																<td width="20%">BERLAKU</td>
																	<td width="1%">:</td>
																	<td width="79%">FEBRUARI - DESEMBER 2017</td>
															</tr>
												</table>
												<div id="logo" class="hidden-xs"><a href="http://www.acc.co.id"><img src="images/logo.png" alt="ACC Logo" /></a></div>
										</div>
								</div>
						 </div>
					</div>
            <?php if (!$packages) {?>
            <div class="box-orange2 text-center col-md-8">
			    MAAF, PAKET KREDIT YANG ANDA CARI TIDAK DITEMUKAN!
			    </div>
            <?php } else {?>
            <div class="box-orange2 col-md-12">
            TERIMA KASIH ATAS KETERTARIKAN ANDA PADA PAKET KREDIT ACC. MOHON ISI DATA BERIKUT INI :
            </div>
            <div id="form-entry">
            <form id="accform" action="formsubmit.php" method="post">
                <input type="hidden" name="paket" value="<?php echo $pid;?>" />
                <input type="hidden" name="vtype" value="<?php echo $pkv;?>" />
                  <div class="form-group col-lg-12">
                    <label for="nama">NAMA</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required="required">
                  </div>
                  <div class="form-group col-lg-12">
                    <label for="notelp">NOMOR TELEPON</label>
                    <input type="tel" class="form-control" id="notelp" name="notelp" placeholder="Nomor Telepon" required="required">
                  </div>
                   <div class="form-group col-lg-12">
                    <label for="emailaddr">EMAIL</label>
                    <input type="email" class="form-control h5-email" id="emailaddr" name="emailaddr" placeholder="Email" required="required">
                  </div>

                  <div class="form-group col-lg-12">
                    <label for="kota">KOTA</label>
                    <select class="form-control" id="kota" name="kota" required="required">
                        <option value="">pilih kota</option>
                        <?php foreach($areas as $area) {?>
                        <option value="<?php echo $area["id"] ;?>"><?php echo $area["description"] ;?></option>
                        <?php } ?>
                    </select>
                  </div>

                   <div class="form-group col-lg-12 col">
                    <label for="cabang">KANTOR CABANG ACC</label>
                    <div id="cabang-loading">
                    <select class="form-control" name="cabang" id="cabang" required="required">
                        <option value="">pilih kantor cabang ACC terdekat</option>
                        <?php foreach($branches as $branch) {?>
                        <option value="<?php echo $branch["description"] ;?>"><?php echo $branch["description"] ;?></option>
                        <?php } ?>
                    </select>
                    </div>
                  </div>
                   <div class="form-group col-lg-12">
                    <label for="captcha">&nbsp;</label>
                    <div class="g-recaptcha" data-sitekey="6LdpsxETAAAAAOlPjhE-uQbe-8Y3Bs60vgy9axSJ"></div>
                  </div>
                  <div class="clearfix"></div>
                    <div class="col-md-12" style="margin:10px 0;font-size:12px;padding:0 20px;"><em><strong>Saya mengizinkan ACC dan mitranya untuk menghubungi Saya dalam membantu proses pembelian mobil. Dengan memberikan email dan nomor telepon, saya telah menyetujui untuk menerima semua pemberitahuan melalui ACC.</strong></em>

                  <div class="mart-20"><button type="submit" class="btn btn-primary">Kirim</button></div></div>
                </form>

            </div>

          </div>
         <?php } ?>
				 <div class="col-md-2 hidden-xs"><div data-spy="affix" data-offset-top="0"><img src="images/banner-skyscrapper2.png" alt="Banner" /></div></div>
        </div>
				<!--menubawah-->
				<div class="row">
					<div class="col-sm-12 col-md-8 col-md-offset-2">
				<div class="menu-wrapper">
							<a href="index.php">Home</a> | <a href="http://www.acc.co.id/simulasi-kredit-mobil" target="_blank">Cek Simulasi Kredit</a> | <a href="http://www.acc.co.id" target="_blank">Web ACC</a>
							</div>
				<div class="row mart-20">
									<div class="col-md-3 col-xs-6"><a href="http://www.acc.co.id" target="_blank"><img src="images/footer-web.jpg" alt="" class="img-responsive" /></a></div>
										<div class="col-md-3 col-xs-6"><a href="https://www.facebook.com/ACCKreditMobil" target="_blank"><img src="images/footer-fb.jpg" alt="" class="img-responsive" /></a></div>
										<div class="col-md-3 col-xs-6"><a href="https://www.twitter.com/ACCKreditMobil" target="_blank"><img src="images/footer-twitter.jpg" alt="" class="img-responsive" /></a></div>
										<div class="col-md-3 col-xs-6"><img src="images/footer-phone.jpg" alt="" class="img-responsive" /></div>
								</div>
						</div>
				</div>
				<!--end menubawah-->
				<div class="row">
					<div id="menubawah">
						<?php
							foreach($categories as $kategori) {
									$menuclass = "menu".strtolower($kategori["pkc_name"]);
							?>
							<a href="index.php?v=<?php echo $kategori["pkc_id"]?>" class="<?php echo $menuclass;?>"><?php echo $kategori["pkc_name"]?></a>
						<?php }?>
					</div>
				</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.h5validate.js"></script>
    <script src="js/jquery.fancybox.pack.js"></script>
     <script type="text/javascript">
		 $(document).ready(function () {
			$('#accform').h5Validate({
				errorClass:'errororange'
			});
			$("#menuslide").click(function(){
				$("#menuslidecontent").animate({
					width: "toggle"
				});
			});
		});
   		$("#kota").change(function() {
			areanya = $( "#kota" ).val();
			$("#cabang-loading").html('<img src="images/fancybox_loading.gif" />');
			$.ajax({
				type: "GET",
				data: {kota: areanya},
				url: "includes/load_query.php",
				success: function (data) {
					$("#cabang-loading").html(data);
				},
				});
			});
    </script>
  </body>

</html>
