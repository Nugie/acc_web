<?php
error_reporting(0);
session_start();
require_once 'includes/config.php';
$pid = (empty($_GET['pid'])) ? 1 : sanitize($_GET['pid']);
$v = (empty($_GET['v'])) ? 1 : sanitize($_GET['v']);

if ($v==2){
$pdp = (empty($_GET['pdp'])) ? 25 : sanitize($_GET['pdp']);
} else {
	$pdp = 25;
}

$datas = $database->select("paketkredit_vehicle_types", [
"[>]paketkredit_categories" => ["pkc_id" => "pkc_id"],
	], [
	"paketkredit_categories.pkc_id",
	"paketkredit_categories.pkc_name",
	"paketkredit_vehicle_types.pkv_id",
	"paketkredit_vehicle_types.pkv_vehicletype",
	"paketkredit_vehicle_types.pkv_thumbnails"
], [
	"paketkredit_vehicle_types.pkv_id" => $pid,
	"LIMIT" => 1
]);
$vendor = strtolower($datas[0]["pkc_name"]);

$packages = $database->select("paketkredit_packages", [
	"pkp_id",
	"pkv_id",
	"pkp_pdp",
	"pkp_variant",
	"pkp_price",
	"pkp_term",
	"pkp_dp",
	"pkp_installment",
	"pkp_dp2",
	"pkp_installment2"
], [
"AND" => [
	"pkv_id" => $datas[0]["pkv_id"],
	"pkp_packages" => 4,
	"pkp_pdp" => $pdp
	],
	"ORDER" => ["pkp_variant ASC", "pkp_term ASC"]
]);
$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
	"paketkredit_categories.pkc_id[<=]" => 2,
	"ORDER" => "pkc_order"
]);
?>
<?php include_once('header.php');?>

 <!--dp-->
   <div class="entry-wrapper">
		<div class="row">
    	<div class="col-sm-8 col-md-8 col-lg-8">
        	<div class="pilihdp">
            	Pilihan DP&nbsp;&nbsp;&nbsp;						
							<a href="detail.php?v=<?php echo $v;?>&amp;pid=<?php echo $pid;?>&amp;pdp=25" class="<?php if ($pdp==25) {?>dpactive<?php } else {?>menudp<?php }?>">25%</a>
							<?php if ($v==2&&$pid!=17) {?>
							<a href="detail.php?v=<?php echo $v;?>&amp;pid=<?php echo $pid;?>&amp;pdp=30" class="<?php if ($pdp==30) {?>dpactive<?php } else {?>menudp<?php }?>">30%</a>
							<?php } ?>
            </div>
        </div>
     </div>
   </div>
    <!--end dp-->

		<!--entry-->
		<?php
		$variant = '';
		$countp = 0;
		foreach($packages as $package) {
		$countp++;
		if($package['pkp_variant'] != $variant){
		?>
         <div class="entry-wrapper table-responsive">
              <table class="tabelmobil">
                   <tr>
										  <th scope="col" width="30%">TIPE MOBIL</th>
                      <th scope="col" width="20%">TENOR</th>
                      <th scope="col" width="20%">DP</th>
                      <th scope="col" width="20%">ANGSURAN</th>
											<th width="20%">APPLY</th>
                    </tr>

										<tr>
											<td rowspan="5" class="kolommobil">
												<h3 class="titlemobil"><?php echo $package["pkp_variant"] ;?></h3>
											  <div class="dprice">Rp <?php echo number_format($package["pkp_price"],0,',',',') ;?></div>
											 </td>

          <?php
              $variant = $package["pkp_variant"];
          }
					if ($countp==1) {?>
						<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><?php echo $package["pkp_term"] ;?> bulan</td>
						<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_dp"],0,',',',') ;?></td>
						<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_installment"],0,',',',') ;?></td>
						<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><a class="merah" data-fancybox-type="iframe" href="form.php?pid=<?php echo $package["pkp_id"] ;?>&amp;pkv=<?php echo $package["pkv_id"] ;?>">Apply</a></td>
				 </tr>

					<?php } else {?>
           <tr>
              <td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><?php echo $package["pkp_term"] ;?> bulan</td>
              <td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_dp"],0,',',',') ;?></td>
              <td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_installment"],0,',',',') ;?></td>
              <td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><a class="merah" data-fancybox-type="iframe" href="form.php?pid=<?php echo $package["pkp_id"] ;?>&amp;pkv=<?php echo $package["pkv_id"] ;?>">Apply</a></td>
           </tr>
					 <?php }?>
        <?php if ($countp==5) {?>
          </table>
     </div>
   	<?php
		   $countp=0;
			   }
    } ?>
		   <!--end entry-->
			<div style="font-size:12px;font-style:italic;font-weight:bold;color:red;margin:20px 0 40px 0;">Keterangan :
				<ul>
				<li>Bayar Pertama ADDB meliputi DP, administrasi, fidusia, dan polis</li>
				<li>Asuransi kendaraan dan ACP sudah termasuk di dalam angsuran per bulan</li>
				<li>Asuransi Kendaraan All Risk</li>
				<li>Paket dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu</li></ul>
			</div>

<?php include_once('footer.php');?>
