<?php
error_reporting(0);
require_once 'includes/config.php';
$pid = (empty($_GET['pid'])) ? 1 : sanitize($_GET['pid']);
$pdp = (empty($_GET['pdp'])) ? 25 : sanitize($_GET['pdp']);
$datas = $database->select("paketkredit_vehicle_types", [
"[>]paketkredit_categories" => ["pkc_id" => "pkc_id"],
	], [
	"paketkredit_categories.pkc_id",
	"paketkredit_categories.pkc_name",
	"paketkredit_vehicle_types.pkv_id",
	"paketkredit_vehicle_types.pkv_vehicletype",
	"paketkredit_vehicle_types.pkv_thumbnails"
], [
	"paketkredit_vehicle_types.pkv_id" => $pid,
	"LIMIT" => 1
]);
$vendor = strtolower($datas[0]["pkc_name"]);

$packages = $database->select("paketkredit_packages", [
	"pkp_id",
	"pkv_id",
	"pkp_pdp",
	"pkp_variant",
	"pkp_price",
	"pkp_term",
	"pkp_dp",
	"pkp_installment",
	"pkp_dp2",
	"pkp_installment2"
], [
"AND" => [
	"pkv_id" => $datas[0]["pkv_id"],
	"pkp_pdp" => $pdp
	],
	"ORDER" => ["pkp_variant ASC", "pkp_term ASC"]
]);
$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
	"ORDER" => "pkc_order"
]);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>Paket Kredit VIP ACCESS - ACC</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css?v=2.2" rel="stylesheet">
    <link href="css/jquery.fancybox.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script>
	       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	       })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	       ga('create', 'UA-48042405-1', 'acc.co.id');
	       ga('send', 'pageview');
	   </script>
		 <!-- Segment Pixel - Landing Page - DO NOT MODIFY -->
<script src="https://secure.adnxs.com/seg?add=4185821&t=1" type="text/javascript"></script>
<!-- End of Segment Pixel -->
  </head>
  <body>
    <div class="container">
    	<div class="row">
							<div class="col-md-2 hidden-xs"><div data-spy="affix" data-offset-top="0"><img src="images/banner-skyscrapper1.png" alt="Banner" /></div></div>
							<div class="col-sm-12 col-md-8">
								<div id="logo2" class="visible-xs"><a href="http://www.acc.co.id"><img src="images/logo.png" alt="ACC Logo" /></a></div>
							<h1>PAKET VIP ACCESS</h1>
							<div id="menuatas">
								<?php
		              foreach($categories as $kategori) {
		                  $menuclass = "menu".strtolower($kategori["pkc_name"]);
		              ?>
									<a href="index.php?v=<?php echo $kategori["pkc_id"]?>" class="<?php echo $menuclass;?>"><?php echo $kategori["pkc_name"]?></a>
								<?php }?>
							</div>
            <?php if (!$datas) {?>
            <div class="box-orange2 text-center">
			    MAAF, PAKET KREDIT YANG ANDA CARI TIDAK DITEMUKAN!
			    </div>
            <?php } else {?>
            <!--header -->
              <?php foreach($datas as $data) {?>
								<div class="entry-wrapper">
									<div class="row">
											<div class="col-sm-12 col-md-12 col-lg-12">
													<div class="box-orange box-<?php echo $vendor;?>" id="pkheader">
															<table>
																	<tr>
																			<td width="15%" class="abottom">PAKET</td>
																				<td width="1%" class="abottom">:</td>
																				<td width="84%">KREDIT&nbsp;&nbsp; <span class="mantap">VIP ACCESS <?php echo strtoupper($vendor);?></span></td>
																		</tr>
																	<tr>
																			<td width="20%">TENOR</td>
																				<td width="1%">:</td>
																				<td width="79%">1 - 5 TAHUN</td>
																		</tr>
																		<tr>
																				<td width="20%">BERLAKU</td>
																					<td width="1%">:</td>
																					<td width="79%">FEBRUARI - DESEMBER 2017</td>
																			</tr>
																</table>
																<div id="logo" class="hidden-xs"><a href="http://www.acc.co.id"><img src="images/logo.png" alt="ACC Logo" /></a></div>
														</div>
												</div>
										 </div>
									</div>

                 <!--dp-->
                   <div class="entry-wrapper">
                		<div class="row">
                    	<div class="col-sm-8 col-md-8 col-lg-8">
                        	<div class="pilihdp">
                            	Pilihan DP&nbsp;&nbsp;&nbsp;<a href="detail.php?pid=<?php echo $pid;?>&amp;pdp=25" class="<?php if ($pdp==25) {?>dpactive<?php } else {?>menudp<?php }?>">25%</a><a href="detail.php?pid=<?php echo $pid;?>&amp;pdp=30" class="<?php if ($pdp==30) {?>dpactive<?php } else {?>menudp<?php }?>">30%</a>
                            </div>
                        </div>
                     </div>
                   </div>
                    <!--end dp-->
                <!--end header -->
                <!--entry-->
                <?php
				$variant = '';
				$countp = 0;
				foreach($packages as $package) {
					$countp++;
					if($package['pkp_variant'] != $variant){
					?>

                       <div class="entry-wrapper table-responsive">
                            <table class="tabelmobil">
                                 <tr>
																	  <th scope="col" width="30%">TIPE MOBIL</th>
                                    <th scope="col" width="20%">TENOR</th>
                                    <th scope="col" width="20%">ANGSURAN</th>
                                    <th scope="col" width="20%">DP</th>
																		<th width="20%">APPLY</th>
                                  </tr>

																	<tr>
																		<td rowspan="5" class="kolommobil">
																			<h3 class="titlemobil"><?php echo $package["pkp_variant"] ;?></h3>
																		  <div class="dprice"><?php echo number_format($package["pkp_price"],0,',','.') ;?></div>
																		 </td>

                        <?php
                            $variant = $package["pkp_variant"];
                        }
												if ($countp==1) {?>
													<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><?php echo $package["pkp_term"] ;?> bulan</td>
													<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_dp"],0,',',',') ;?></td>
													<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_installment"],0,',',',') ;?></td>
													<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><a class="merah" data-fancybox-type="iframe" href="form.php?pid=<?php echo $package["pkp_id"] ;?>&amp;pkv=<?php echo $package["pkv_id"] ;?>">Apply</a></td>
											 </tr>

												<?php } else {?>
                         <tr>
                            <td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><?php echo $package["pkp_term"] ;?> bulan</td>
                            <td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_dp"],0,',',',') ;?></td>
                            <td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_installment"],0,',',',') ;?></td>
                            <td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><a class="merah" data-fancybox-type="iframe" href="form.php?pid=<?php echo $package["pkp_id"] ;?>&amp;pkv=<?php echo $package["pkv_id"] ;?>">Apply</a></td>
                         </tr>
												 <?php }?>
                      <?php if ($countp==5) {?>
		                    </table>
		               </div>
               <?php
			   $countp=0;
			   }

			   ?>
               <!--end entry-->
				 <?php } ?>
               <div style="font-size:12px;font-style:italic;font-weight:bold;color:red;margin:20px 0 40px 0;">Keterangan :
               <ul>
               <li>Bayar Pertama ADDB meliputi DP, administrasi, fidusia, dan polis</li>
<li>Asuransi kendaraan dan ACP sudah termasuk di dalam angsuran per bulan</li>
<li>Asuransi Kendaraan All Risk</li>
<li>Paket dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu</li></ul>

               </div>
            </div>
            <?php }?>
						<div class="col-md-2 hidden-xs"><div data-spy="affix" data-offset-top="0"><img src="images/banner-skyscrapper2.png" alt="Banner" /></div></div>
        </div>
				<!--menubawah-->
        <div class="row">
        	<div class="col-sm-12 col-md-8 col-md-offset-2">
				<div class="menu-wrapper">
	            <a href="index.php">Home</a> | <a href="http://www.acc.co.id/simulasi-kredit-mobil" target="_blank">Cek Simulasi Kredit</a> | <a href="http://www.acc.co.id" target="_blank">Web ACC</a>
              </div>
				<div class="row mart-20">
                	<div class="col-md-3 col-xs-6"><a href="http://www.acc.co.id" target="_blank"><img src="images/footer-web.jpg" alt="" class="img-responsive" /></a></div>
                    <div class="col-md-3 col-xs-6"><a href="https://www.facebook.com/ACCKreditMobil" target="_blank"><img src="images/footer-fb.jpg" alt="" class="img-responsive" /></a></div>
                    <div class="col-md-3 col-xs-6"><a href="https://www.twitter.com/ACCKreditMobil" target="_blank"><img src="images/footer-twitter.jpg" alt="" class="img-responsive" /></a></div>
                    <div class="col-md-3 col-xs-6"><img src="images/footer-phone.jpg" alt="" class="img-responsive" /></div>
                </div>
            </div>
        </div>
        <!--end menubawah-->
        <div class="row">
          <div id="menubawah">
						<?php
							foreach($categories as $kategori) {
									$menuclass = "menu".strtolower($kategori["pkc_name"]);
							?>
							<a href="index.php?v=<?php echo $kategori["pkc_id"]?>" class="<?php echo $menuclass;?>"><?php echo $kategori["pkc_name"]?></a>
						<?php }?>
          </div>
        </div>
	</div>
  <?php }?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
		$("#menuslide").click(function(){
				$("#menuslidecontent").animate({
					width: "toggle"
				});
			});
		});
    </script>
  </body>
</html>
