<?php
require_once 'config.php';
error_reporting(0);

if($_REQUEST['kota']!=""){
	$idkota = $_REQUEST['kota'];

	$branches = $database->select("branches", [
		"branch_id",
		"description"
	], [
		"area_id" => $idkota,
		"ORDER" => "description"
	]);
	?>
	<select class="form-control" name="cabang" id="cabang" required="required">
        <option value="">pilih kantor cabang ACC terdekat</option>
        <?php foreach($branches as $branch) {?>
        <option value="<?php echo $branch["description"] ;?>"><?php echo $branch["description"] ;?></option>
        <?php } ?>
    </select>
<?php } else {
	$branches = $database->select("branches", [
		"branch_id",
		"description"
	], [
		"area_id" => 1,
		"ORDER" => "description"
	]);
	?>
	<select class="form-control" name="cabang" id="cabang" required="required">
        <option>pilih kantor cabang ACC terdekat</option>
        <?php foreach($branches as $branch) {?>
        <option value="<?php echo $branch["description"] ;?>"><?php echo $branch["description"] ;?></option>
        <?php } ?>
    </select>

<?php }?>
