<?php
error_reporting(0);
session_start();
if ($_SESSION["referral"]=='') {$_SESSION["referral"] = (!empty($_GET['utm_source'])) ? $_GET['utm_source'] : 'WEBSITE';}
require_once 'includes/config.php';
$v = (empty($_GET['v'])) ? 1 : sanitize($_GET['v']);

$datas = $database->query("SELECT distinct `paketkredit_packages`.`pkv_id`, `paketkredit_vehicle_types`.`pkc_id`, `paketkredit_vehicle_types`.`pkv_vehicletype`, `paketkredit_vehicle_types`.`pkv_thumbnails` from `paketkredit_packages` left join `paketkredit_vehicle_types` on `paketkredit_packages`.`pkv_id`=`paketkredit_vehicle_types`.`pkv_id` where `paketkredit_vehicle_types`.`pkc_id`=$v AND `paketkredit_packages`.`pkp_packages` = 3 ORDER BY `paketkredit_vehicle_types`.`pkv_vehicletype`")->fetchAll();

if ($v==1) {
	$vendor = "toyota";
} else {
	$vendor = "daihatsu";
}

$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
	"ORDER" => "pkc_order"
]);
?>

  <?php include_once('header.php');?>
				<!--car info-->
         <?php if ($v<4) { ?>
            <div id="maincontent">
               <div class="row">
               <?php
      				   $i=0;
      				   foreach($datas as $data) {
      				   $i++;
      				   ?>
                  <div class="col-sm-6">
                      <div class="carmodel-wrapper">
                      <a href="detail.php?pid=<?php echo $data["pkv_id"] ;?>" class="carmodel-link">
                         		 <div class="text-center">
                           	   <img src="images/mobil/<?php echo $data["pkv_thumbnails"] ;?>" alt="paket kredit <?php echo $data["pkv_vehicletype"] ;?>" />
                        		  </div>
                          </a>
                      </div>
                  </div>
                    <?php if ($i==2) {?>
                    </div><div class="row">
                    <?php } ?>

                <!--end car-->
                <?php }?>
              </div>
            </div>
            <!--end entry -->
            <?php } else {?>
              <div id="maincontent">
                <div class="box-orange2 text-center">
			               MAAF, DATA YANG ANDA CARI TIDAK DITEMUKAN!
    			      </div>
              </div>
            <?php }?>
      <?php include_once('footer.php');?>
