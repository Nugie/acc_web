<?php
session_start();
require_once('class/database.class.php');
error_reporting(0);
ini_set('display_errors', false);

if($_REQUEST['area']!=""){
	$idArea = $_REQUEST['area'];

	$query_branch = "SELECT branch_id, description FROM cso_branches WHERE area_id = ".$idArea." ORDER BY description";
	$branch_list = database::getData($query_branch);
	?>
	<select name="branch" id="branch" required="required">
		<option value="">Select Branch</option>
		<?php foreach($branch_list as $branch){?>
		<option value="<?= $branch['branch_id']?>"><?= $branch['description']?></option>
		<?php } ?>
	</select>
<?php }

if($_REQUEST['type_id']!=""){
	$idType 	= $_REQUEST['type_id'];
	$query_model = "SELECT a_variantid, a_variant FROM cso_isuzu WHERE model_id ='". $idType ."'";
	$model_list = database::getData($query_model);
	?>
	<select name="vehicle_model" id="vehicle_model" class="styled-combo2">
		<option value="">Select Variant</option>
		<?php foreach($model_list as $mod){?>
		<option value="<?= $mod['a_variantid']?>"><?= $mod['a_variant']?></option>
		<?php } ?>
	</select>
    <?php

	$_SESSION['type'] = $idType;
	?>
<?php }


if ($_REQUEST['model_id']!=""){

		$idModel	= $_REQUEST['model_id'];
		$idType = ($_SESSION['type']);
		$query_otr = "SELECT * FROM cso_otrs WHERE branch_id = 1 AND brand_id = 1 AND type_id = ".$idType." AND model_id = ".$idModel." AND vehicle_status = 'New' ORDER BY otr_id DESC limit 1";
		$tmp_otr = database::getData($query_otr);
		$otr = $tmp_otr[0]['amount_otr'];
		?>
		<input name="otr" type="text" id="otr" size="23" class="text-field ui-widget-content ui-corner-all" value="dada"/>
<?php } ?>

	<script type="text/javascript">
	$(document).ready(function() {
		$('#vehicle_model').change(function(){
		$('#otr_loading').fadeOut();
		$('#loader').show();
		$.post("simulation.php", {
			model_id: $('#vehicle_model').val(),
			vehicle_type: $('#vehicle_type').val(),
		}, function(response){
			setTimeout("finishAjax('otr_loading', '"+escape(response)+"')", 400);
		});
		return false;
		});
	});

	$(function() {
			$("#branch").selectbox({
			onChange: function (val, inst) {

				$("#applybutton").html('');
			}
			});
		})
	function finishAjax(id, response){
	$('#loader').hide();
	$('#'+id).html(unescape(response));
	$('#'+id).fadeIn();
	}

	function alert_id()
	{
		if($('#vehicle_model').val() == '')
		alert('Please select a sub category.');
		else
		alert($('#vehicle_model').val());
		return false;
	}
	</script>
