<?php
ini_set('display_errors', 0);
$starttime = explode(' ', microtime());
$starttime = $starttime[1] + $starttime[0];

include_once('simplepie/autoloader.php');
include_once('simplepie/idn/idna_convert.class.php');

$feed = new SimplePie();
$feed->set_feed_url('http://www.acc.co.id/news/index.rss');
$feed->set_item_limit(5);
$success = $feed->init();
$feed->handle_content_type();
?><!DOCTYPE html>

<html lang="en-US">
<head>
<title>ACC - Widget</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48042405-1', 'acc.co.id');
  ga('send', 'pageview');

</script>
</head>

<body>
<div id="wrapper">
	<div id="logo"><img src="images/logo-acc.jpg" alt="Astra Credit Companies" width="180" /></div>
			<?php
			// Check to see if there are more than zero errors (i.e. if there are any errors at all)
			if ($feed->error())
			{
				// If so, start a <div> element with a classname so we can style it.
				echo '<div class="sp_errors">' . "\r\n";

					// ... and display it.
					echo '<p>' . htmlspecialchars($feed->error()) . "</p>\r\n";

				// Close the <div> element we opened.
				echo '</div>' . "\r\n";
			}
			?>

		<div id="sp_results">
			<?php if ($success): ?>
				<?php 
				$i=0;
				foreach($feed->get_items(0, 3) as $item): 
					$judul[$i] = $item->get_title();
					$desc[$i] = $item->get_content();	
					$link[$i] = $item->get_permalink();
				$i++;			
				endforeach; 
				
				$rnum = rand(0,2);
				?>
				<h4><?php echo $judul[$rnum];?></h4>
				<?php echo $desc[$rnum];?>
				<div id="more"><a href="<?php echo $link[$rnum];?>" target="_blank">Read More &raquo;</a></div>		
			<?php endif; ?>
		</div>
		
	</div>
	
	</body>
</html>
