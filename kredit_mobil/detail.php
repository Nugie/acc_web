<?php
ini_set('display_errors', 0);
?><!DOCTYPE html>

<html lang="en-US">
<head>
<title>ACC - Widget</title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48042405-1', 'acc.co.id');
  ga('send', 'pageview');

</script>
</head>

<body class="body2">
	<div id="wrapper">
	<?php 
	    $rnum = rand(1,4);
    ?>
    <a href="http://www.acc.co.id" title="Kredit Mobil ACC" target="_blank"><img src="images/img<?php echo $rnum;?>.png" alt="ACC" /></a>
	</div>
</body>
</html>
