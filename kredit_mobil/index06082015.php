<?php
ini_set('display_errors', 0);
$rnum = rand(1,4);
switch ($rnum) {
	case 1:
		$img = "img1.png";
		$url = "http://www.acc.co.id";
		$title = "Kredit Mobil - ACC";
		$description = "Astra Credit Companies adalah perusahaan pembiayaan mobil dan alat berat terbesar di Indonesia.";
		$keywords = "Kredit Mobil, Mobil Kredit, Simulasi Kredit Mobil, Kredit Mobil Murah, Kredit Mobil Baru, ACC Kredit Mobil, Kredit Mobil ACC, Kredit Mobil Terbaik, Beli Mobil Kredit";
		break;
	case 2:
		$img = "img2.png";
		$url = "http://www.acc.co.id/harga_mobil";
		$title = "Harga Mobil Baru - ACC";
		$description = "Harga Mobil Baru 2015";
		$keywords = "Harga Mobil Baru, daftar harga mobil";
		break;
	case 3:
		$img = "img3.png";
		$url = "http://www.acc.co.id/credit_simulations";
		$title = "Simulasi Kredit Mobil - ACC";
		$description = "Simulasi Kredit Mobil";
		$keywords = "Simulasi Kredit Mobil, Mobil Kredit, Kredit Mobil Murah, Kredit Mobil";		
		break;
	case 4:
		$img = "img4.png";
		$url = "http://www.acc.co.id/news/read/647/paket_kredit_mobil_toyota_juni_2015";
		$title = "Kredit Mobil - Paket Kredit Mobil Toyota Juni 2015 - ACC";
		$description = "Paket Kredit Mobil Toyota Juni 2015";
		$keywords = "Toyota Avanza, Toyota Rush, Toyota Kijang Innova, Toyota Fortuner, Toyota Yaris, Kredit Mobil";
		break;
	default:
		$img = "img1.png";
		$url = "http://www.acc.co.id";
		$title = "Kredit Mobil - ACC";
		$description = "Astra Credit Companies adalah perusahaan pembiayaan mobil dan alat berat terbesar di Indonesia.";
		$keywords = "Kredit Mobil, Mobil Kredit, Simulasi Kredit Mobil, Kredit Mobil Murah, Kredit Mobil Baru, ACC Kredit Mobil, Kredit Mobil ACC, Kredit Mobil Terbaik, Beli Mobil Kredit";		
}
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title><?php echo $title;?></title>
<meta name="description" content="<?php echo $description;?>" />
<meta name="keywords" content="<?php echo $keywords;?>" />
<link rel="stylesheet" type="text/css" href="style.css" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48042405-1', 'acc.co.id');
  ga('send', 'pageview');

</script>
</head>

<body class="body2">
	<div id="wrapper">
    <a href="<?php echo $url;?>" title="<?php echo $title;?>" target="_blank"><img src="images/<?php echo $img;?>" alt="<?php echo $title;?>" /></a>
	</div>
</body>
</html>
