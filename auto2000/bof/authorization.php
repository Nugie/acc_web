<?php

require_once 'components/page.php';
require_once 'components/security/datasource_security_info.php';
require_once 'components/security/security_info.php';
require_once 'components/security/hardcoded_auth.php';
require_once 'components/security/user_grants_manager.php';

include_once 'components/security/user_identity_storage/user_identity_session_storage.php';

$users = array('accadmin' => 'ACCbug1#');

$usersIds = array('accadmin' => -1);

$dataSourceRecordPermissions = array();

$grants = array('guest' => 
        array()
    ,
    'defaultUser' => 
        array('cso_applicants' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_areas' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_auto2000' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_auto2000_model' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_branches' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_insurances' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_new_rates' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_recipients' => new DataSourceSecurityInfo(false, false, false, false))
    ,
    'guest' => 
        array('cso_applicants' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_areas' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_auto2000' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_auto2000_model' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_branches' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_insurances' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_new_rates' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_recipients' => new DataSourceSecurityInfo(false, false, false, false))
    ,
    'accadmin' => 
        array('cso_applicants' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_areas' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_auto2000' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_auto2000_model' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_branches' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_insurances' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_new_rates' => new DataSourceSecurityInfo(false, false, false, false),
        'cso_recipients' => new DataSourceSecurityInfo(false, false, false, false))
    );

$appGrants = array('guest' => new DataSourceSecurityInfo(false, false, false, false),
    'defaultUser' => new DataSourceSecurityInfo(true, false, false, false),
    'guest' => new DataSourceSecurityInfo(false, false, false, false),
    'accadmin' => new AdminDataSourceSecurityInfo());

$tableCaptions = array('cso_applicants' => 'Applicants',
'cso_areas' => 'Areas',
'cso_auto2000' => 'Type',
'cso_auto2000_model' => 'Model',
'cso_branches' => 'Branch',
'cso_insurances' => 'Insurances',
'cso_new_rates' => 'New Rates',
'cso_recipients' => 'Recipients');

function SetUpUserAuthorization()
{
    global $usersIds;
    global $grants;
    global $appGrants;
    global $dataSourceRecordPermissions;
    $userAuthorizationStrategy = new HardCodedUserAuthorization(new UserIdentitySessionStorage(GetIdentityCheckStrategy()), new HardCodedUserGrantsManager($grants, $appGrants), $usersIds);
    GetApplication()->SetUserAuthorizationStrategy($userAuthorizationStrategy);

GetApplication()->SetDataSourceRecordPermissionRetrieveStrategy(
    new HardCodedDataSourceRecordPermissionRetrieveStrategy($dataSourceRecordPermissions));
}

function GetIdentityCheckStrategy()
{
    global $users;
    return new SimpleIdentityCheckStrategy($users, '');
}

?>