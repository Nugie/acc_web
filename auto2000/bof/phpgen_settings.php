<?php

//  define('SHOW_VARIABLES', 1);
//  define('DEBUG_LEVEL', 1);

//  error_reporting(E_ALL ^ E_NOTICE);
//  ini_set('display_errors', 'On');

set_include_path('.' . PATH_SEPARATOR . get_include_path());


include_once dirname(__FILE__) . '/' . 'components/utils/system_utils.php';

//  SystemUtils::DisableMagicQuotesRuntime();

SystemUtils::SetTimeZoneIfNeed('Asia/Krasnoyarsk');

function GetGlobalConnectionOptions()
{
    return array(
  'server' => 'localhost',
  'port' => '3306',
  'username' => 'acc_toyota',
  'password' => 'VrS6QOZc*WgP',
  'database' => 'acc_toyota'
);
}

function HasAdminPage()
{
    return false;
}

function GetPageGroups()
{
    $result = array('Default');
    return $result;
}

function GetPageInfos()
{
    $result = array();
    $result[] = array('caption' => 'Applicants', 'short_caption' => 'Applicants', 'filename' => 'index.php', 'name' => 'cso_applicants', 'group_name' => 'Default', 'add_separator' => false);
    $result[] = array('caption' => 'Areas', 'short_caption' => 'Areas', 'filename' => 'cso_areas.php', 'name' => 'cso_areas', 'group_name' => 'Default', 'add_separator' => false);
    $result[] = array('caption' => 'Type', 'short_caption' => 'Type', 'filename' => 'cso_auto2000.php', 'name' => 'cso_auto2000', 'group_name' => 'Default', 'add_separator' => false);
    $result[] = array('caption' => 'Model', 'short_caption' => 'Model', 'filename' => 'cso_auto2000_model.php', 'name' => 'cso_auto2000_model', 'group_name' => 'Default', 'add_separator' => false);
    $result[] = array('caption' => 'Branch', 'short_caption' => 'Branch', 'filename' => 'cso_branches.php', 'name' => 'cso_branches', 'group_name' => 'Default', 'add_separator' => false);
    $result[] = array('caption' => 'Insurances', 'short_caption' => 'Insurances', 'filename' => 'cso_insurances.php', 'name' => 'cso_insurances', 'group_name' => 'Default', 'add_separator' => false);
    $result[] = array('caption' => 'New Rates', 'short_caption' => 'New Rates', 'filename' => 'cso_new_rates.php', 'name' => 'cso_new_rates', 'group_name' => 'Default', 'add_separator' => false);
    $result[] = array('caption' => 'Recipients', 'short_caption' => 'Recipients', 'filename' => 'cso_recipients.php', 'name' => 'cso_recipients', 'group_name' => 'Default', 'add_separator' => false);
    return $result;
}

function GetPagesHeader()
{
    return
    'ACC Admin';
}

function GetPagesFooter()
{
    return
        ''; 
    }

function ApplyCommonPageSettings(Page $page, Grid $grid)
{
    $page->SetShowUserAuthBar(true);
    $page->OnCustomHTMLHeader->AddListener('Global_CustomHTMLHeaderHandler');
    $page->OnGetCustomTemplate->AddListener('Global_GetCustomTemplateHandler');
    $grid->BeforeUpdateRecord->AddListener('Global_BeforeUpdateHandler');
    $grid->BeforeDeleteRecord->AddListener('Global_BeforeDeleteHandler');
    $grid->BeforeInsertRecord->AddListener('Global_BeforeInsertHandler');
}

/*
  Default code page: 1252
*/
function GetAnsiEncoding() { return 'windows-1252'; }

function Global_CustomHTMLHeaderHandler($page, &$customHtmlHeaderText)
{

}

function Global_GetCustomTemplateHandler($part, $mode, &$result, &$params, Page $page = null)
{

}

function Global_BeforeUpdateHandler($page, &$rowData, &$cancel, &$message, $tableName)
{

}

function Global_BeforeDeleteHandler($page, &$rowData, &$cancel, &$message, $tableName)
{

}

function Global_BeforeInsertHandler($page, &$rowData, &$cancel, &$message, $tableName)
{

}

function GetDefaultDateFormat()
{
    return 'Y-m-d';
}

function GetFirstDayOfWeek()
{
    return 0;
}

function GetEnableLessFilesRunTimeCompilation()
{
    return false;
}



?>