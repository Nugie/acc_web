<?php
include('class/database.class.php');
session_start();
//error_reporting(0);

	
    $query_branch = "SELECT branch_id, description FROM cso_branches WHERE area_id = 1 ORDER BY description";
	$branch_list = database::getData($query_branch);

	$query_type = "SELECT type_id, description FROM cso_vehicle_types WHERE brand_id = 1 ORDER BY description";
	$type_list = database::getData($query_type);
	
	$tenor_list = array(
		"1" => "1 Tahun",
		"2" => "2 Tahun",
		"3" => "3 Tahun",
		"4" => "4 Tahun",
	);
	//print_r($_REQUEST);
	if ($_REQUEST['model_id']!=""){
		
		$idModel	= $_REQUEST['model_id'];
		$idType = ($_SESSION['type']);
		$query_otr = "SELECT * FROM cso_auto2000 WHERE a_variantid =  $idModel";
		$tmp_otr = database::getData($query_otr);
		$otr = $tmp_otr[0]['a_price'];
		$dpnya = (25/100)*$otr;
		?>
		<!--<input name="otrd" type="text" id="otrd" size="23" class="teksnya" value="<?php echo $otr?>" disabled="disabled"/>-->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>                             
            <td valign="top" style="padding-left:10px;">
                <input name="otr" type="text" id="otr" size="23" class="teksnya" value="<?php echo $otr?>" readonly style="margin-left:8px;" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="padding-top:6px;padding-left:10px;">
            <input name="dp" type="text" id="dp" size="23" class="teksnya" value="<?php echo $dpnya;?>" style="margin-left:8px;" />
            </td>
        </tr>
            </table>
<?php
	}
		
function hitung_simulasi($area, $branch, $type, $model, $acp_status, $otr, $dp,$tenor)
	{
		$percent_dp = round($dp / $otr * 100, 3);
		$downpayment = $dp;
		
		if ($acp_status == "Yes")
		{
			//$query_acp = "SELECT * FROM cso_acps WHERE tenor = ".$tenor." ORDER BY acp_id DESC limit 1";
			//$acp_rate = /*ambil nilai dari tenor yang dipilih*/
			//$tmp_rate = database::getData($query_acp);
			//$acp_rate = $tmp_rate[0]['acp_rate'];
			if ($tenor>1) {
				$acp_rate = 1.69;
			} else {
				$acp_rate = 0.69;
			}
		}
		else
			$acp_rate = 0;
		
		
		$query_kind = "SELECT a_type FROM cso_auto2000 WHERE a_variantid=".$model." limit 1";
		$tmp_kind = database::getData($query_kind);
		$kind = $tmp_kind[0]['a_type'];
		
		//$query_otr = "SELECT * FROM cso_otrs WHERE area_id = ".$area." AND branch_id = ".$branch." AND brand_id = 1 AND type_id = ".$type." AND model_id = ".$model." AND vehicle_status = 'New' ORDER BY otr_id DESC limit 1";
		//$tmp_otr = database::getData($query_otr);
		//$otr = $tmp_otr[0]['amount_otr'];

		$query_insc1 = "SELECT * FROM cso_insurances WHERE category_id = 1 AND tenor = ".$tenor." AND vehicle_status = 'New' AND otr_start <= ".$otr." AND otr_end >= ".$otr." limit 1";
		$query_insc2 = "SELECT * FROM cso_insurances WHERE category_id = 1 AND tenor = 1 AND vehicle_status = 'New' AND otr_start <= ".$otr." AND otr_end >= ".$otr." limit 1";
		$tmp_insc1 = database::getData($query_insc1);
		$tmp_insc2 = database::getData($query_insc2);
		$insurance1 = $tmp_insc1[0]['ar_gross_rate'];
		$insurance2 = $tmp_insc2[0]['ar_gross_rate'];
		
		$query_flatrate = "SELECT * FROM cso_new_rates WHERE kind_id = ".$kind." AND tenor = ".$tenor." AND from_dp <= ".$percent_dp." AND to_dp >= ".$percent_dp." ORDER BY nr_id DESC limit 1";
		$tmp_flatrate = database::getData($query_flatrate);
		$flat_rate = $tmp_flatrate[0]['flat_rate_percentage'];
		
		//add per area
		/*if ($branch==7||$branch==27||$branch==28||$branch==37||$branch==38||$branch==33||$branch==52||$branch==49) {
			$flat_rate = $flat_rate + 0.25;
		} elseif ($branch==44||$branch==46||$branch==32||$branch==35||$branch==34||$branch==31){
			$flat_rate = $flat_rate + 0.5;
		} elseif ($branch==51||$branch==40||$branch==36||$branch==39||$branch==41){
			$flat_rate = $flat_rate + 1;
		} else {
			$flat_rate = $flat_rate+0;
			}
		*/
		
		/*$query_adm = "SELECT * FROM cso_administrations WHERE tenor = ".$tenor." AND vehicle_status = 'New' ORDER BY adm_id DESC limit 1";
		$tmp_adm = database::getData($query_adm);
		$administration_polis = $tmp_adm[0]['polis'];
		$administration_adm = $tmp_adm[0]['administration'];
		$administration_fiducia = $tmp_adm[0]['fiducia'];
		*/
		// -------------------------------------------------------------------------------------------------------------------------------
		// Hitung Pokok Hutang
		// -------------------------------------------------------------------------------------------------------------------------------

		$D19 = $insurance1 - $insurance2;
		$F18 = ($otr * $insurance2) / 100;
		$G19 = ($otr * $D19) / 100;
		$G19 = 0;
		$G20 = ($otr - $downpayment) + $G19; /*PH Baru atau OTR setelah dikurangi DP*/
		$G22 = (($G20 * $flat_rate) / 100) * $tenor; /*bunga*/
		$G23 = $G20 + $G22; /*Total hutang*/
		$G14 = ($G23 * $acp_rate) / 100; /*ACP*/
		$H20 = ($otr - $downpayment) + $G14 + $G19;
		$H22 = (($H20 * $flat_rate) / 100) * $tenor;
		$H23 = $H20 + $H22;
		
		$H37 = ceil($H23 / ($tenor * 12) / 1000) * 1000;
		//$D35 = $downpayment + $H37 + $F18 + $administration_polis + $administration_adm + $administration_fiducia;
		$D35 = $downpayment + $H37 + $F18 + 1350000;
		$uang_muka = $D35;
		$angsuran =  $H37;
		//$result = "H20 : {$H20}\nH22 : {$H22}\nH23 : {$H23}\n";
		$result = array('uang_muka' => $uang_muka,'angsuran' => $angsuran);
		//$rate = $model;

		return $result;
		}

?>