<?php
error_reporting(0);
session_start();
require_once 'includes/config.php';
$pid = (empty($_GET['pid'])) ? 1 : sanitize($_GET['pid']);
$v = (empty($_GET['v'])) ? 1 : sanitize($_GET['v']);
$pdp = (empty($_GET['pdp'])) ? 20 : sanitize($_GET['pdp']);

$datas = $database->select("paketkredit_vehicle_types", [
	"[>]paketkredit_categories" => ["pkc_id" => "pkc_id"],
		], [
		"paketkredit_categories.pkc_id",
		"paketkredit_categories.pkc_name",
		"paketkredit_vehicle_types.pkv_id",
		"paketkredit_vehicle_types.pkv_vehicletype",
		"paketkredit_vehicle_types.pkv_thumbnails"
	], [
		"paketkredit_vehicle_types.pkv_id" => $pid,
		"LIMIT" => 1
	]);
$vendor = strtolower($datas[0]["pkc_name"]);

if ($v==1) {
	$pth = (empty($_GET['pth'])) ? 72 : sanitize($_GET['pth']);
	switch ($pth){
		case 72:
			$textterm1= "1-36";
			$textterm11= "2-36";
			$textterm2= "37";
			$textterm3= "38-72";
			$textterm4= "37-72";
		break;
		case 84:
			$textterm1= "1-48";
			$textterm11= "2-48";
			$textterm2= "49";
			$textterm3= "50-84";
			$textterm4= "49-84";
		break;
		default:
			$textterm1= "1-48";
			$textterm11= "2-48";
			$textterm2= "49";
			$textterm3= "50-96";
			$textterm4= "49-96";
		break;
	}
	//select toyota
	$packages = $database->select("paketkredit_packages", [
		"pkp_id",
		"pkv_id",
		"pkp_pdp",
		"pkp_variant",
		"pkp_price",
		"pkp_term",
		"pkp_dp",
		"pkp_installment",
		"pkp_dp2",
		"pkp_installment2",
		"pkp_installment_extra"
	], [
	"AND" => [
		"pkv_id" => $datas[0]["pkv_id"],
		"pkp_packages" => 2,
		"pkp_term" => $pth
		],
		"ORDER" => ["pkp_variant ASC", "pkp_term ASC"]
	]);
	//end select toyota
} else {
	$pth = (empty($_GET['pth'])) ? 83 : sanitize($_GET['pth']);
	//select daihatsu
	$packages = $database->select("paketkredit_packages", [
		"pkp_id",
		"pkv_id",
		"pkp_pdp",
		"pkp_variant",
		"pkp_price",
		"pkp_term",
		"pkp_dp",
		"pkp_installment",
		"pkp_dp2",
		"pkp_installment2",
		"pkp_installment_extra"
	], [
	"AND" => [
		"pkv_id" => $datas[0]["pkv_id"],
		"pkp_packages" => 2,
		"pkp_term" => $pth,
		"pkp_pdp" => $pdp,
		],
		"ORDER" => ["pkp_variant ASC", "pkp_term ASC"]
	]);
	//end select daihatsu
	switch ($pth){
		case 71:
			$textterm1= "1-36";
			$textterm2= "2-36";
			$textterm3= "36";
			$textterm4= "37-72";
			$textterm5= "37";
			$textterm6= "38-72";
		break;
		case 83:
			$textterm1= "1-48";
			$textterm11= "2-48";
			$textterm2= "49";
			$textterm3= "50-84";
			$textterm4= "49-84";
		break;
		default:
			$textterm1= "1-48";
			$textterm11= "2-48";
			$textterm2= "49";
			$textterm3= "50-96";
			$textterm4= "49-96";
		break;
	}
}



$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
	"ORDER" => "pkc_order"
]);
?>
<?php include_once('header.php');?>

 <!--dp-->
   <div class="entry-wrapper">
		<div class="row">
    	<div class="col-sm-10 col-md-10 col-lg-10">
        	<div class="pilihdp">
				Pilihan TENOR&nbsp;&nbsp;&nbsp;
				<?php if ($v==1) {?>
							<a href="detail.php?pid=<?php echo $pid;?>&amp;pth=72&amp;v=<?php echo $v;?>" class="<?php if ($pth==72) {?>dpactive<?php } else {?>menudp<?php }?>">6 Tahun</a><a href="detail.php?pid=<?php echo $pid;?>&amp;pth=84&amp;v=<?php echo $v;?>" class="<?php if ($pth==84) {?>dpactive<?php } else {?>menudp<?php }?>">7 Tahun</a>
							<a href="detail.php?pid=<?php echo $pid;?>&amp;pth=96&amp;v=<?php echo $v;?>" class="<?php if ($pth==96) {?>dpactive<?php } else {?>menudp<?php }?>">8 Tahun</a>
				<?php } else {?>
							<a href="detail.php?pid=<?php echo $pid;?>&amp;pdp=20&amp;pth=83&amp;v=<?php echo $v;?>" class="<?php if ($pth==83&&$pdp==20) {?>dpactive<?php } else {?>menudp<?php }?>">7 Tahun</a>
							<a href="detail.php?pid=<?php echo $pid;?>&amp;pdp=20&amp;pth=95&amp;v=<?php echo $v;?>" class="<?php if ($pth==95) {?>dpactive<?php } else {?>menudp<?php }?>">8 Tahun</a>
				<?php }?>
            </div>
        </div>
     </div>
   </div>
    <!--end dp-->
		<div class="entry-wrapper table-responsive">
		<?php if ($v==1) {?>
		 <!--toyota-->
		 <table class="tabelmobil">
		 <tr>
						 <th rowspan="2" scope="col">Tipe Mobil</th>
						 <th colspan="2" scope="col">Tenor <?php echo $textterm1?></th>
						 <th colspan="3" scope="col">Tenor <?php echo $textterm4?>*</th>
					 </tr>
					 <tr>
						 <td class="header">Angsuran ke<br /><?php echo $textterm11?></td>
						 <td class="header">Pembayaran<br />Pertama</td>
						 <td class="header">Angsuran ke<br /><?php echo $textterm3?></td>
						 <td class="header" colspan="2">Angsuran ke<br /><?php echo $textterm2?></td>
					 </tr>
		<!--entry-->
		<?php
		$countp = 0;
		foreach($packages as $package) {
		$countp++;
		?>
			<tr>
				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>
					<h3 class="titlemobil"><?php echo $package["pkp_variant"] ;?></h3>
				  <div class="dprice">Rp <?php echo number_format($package["pkp_price"],0,',',',') ;?></div>
				 </td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_dp"],0,',',',') ;?></td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_installment"],0,',',',') ;?></td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_dp2"],0,',',',') ;?></td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_installment2"],0,',',',') ;?></td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><a class="merah" data-fancybox-type="iframe" href="form.php?pid=<?php echo $package["pkp_id"] ;?>&amp;pkv=<?php echo $package["pkv_id"] ;?>">Apply</a></td>
				 </tr>
				 <?php }?>

          </table>
		  <!--/toyota-->
		<?php } else {?>
		 <!--daihatsu-->
 		<table class="tabelmobil">
		 <tr>
					<th rowspan="2" scope="col">Tipe Mobil</th>
					<th colspan="2" scope="col">Tenor <?php echo $textterm1?></th>
					<th colspan="3" scope="col">Tenor <?php echo $textterm4?>*</th>
				</tr>
				<tr>
						 <td class="header">Angsuran ke<br /><?php echo $textterm11?></td>
						 <td class="header">Pembayaran<br />Pertama</td>
						 <td class="header">Angsuran ke<br /><?php echo $textterm3?></td>
						 <td class="header" colspan="2">Angsuran ke<br /><?php echo $textterm2?></td>
					 </tr>
		<!--entry-->
		<?php
		$countp = 0;
		foreach($packages as $package) {
		$countp++;
		?>
			<tr>
				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>
					<h3 class="titlemobil"><?php echo $package["pkp_variant"] ;?></h3>
				  <div class="dprice">Rp <?php echo number_format($package["pkp_price"],0,',',',') ;?></div>
				 </td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_dp"],0,',',',') ;?></td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_installment"],0,',',',') ;?></td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_installment2"],0,',',',') ;?></td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>>Rp <?php echo number_format($package["pkp_dp2"],0,',',',') ;?></td>
	 				<td <?php if (($countp%2)!=0) {?> class="striped"<?php }?>><a class="merah" data-fancybox-type="iframe" href="form.php?pid=<?php echo $package["pkp_id"] ;?>&amp;pkv=<?php echo $package["pkv_id"] ;?>">Apply</a></td>
				 </tr>
			
				 <?php }?>

          </table>
		  <!--/daihatsu-->
		<?php }?>
     </div>
		   <!--end entry-->
			<div style="font-size:12px;font-style:italic;font-weight:bold;color:red;margin:20px 0 40px 0;">Keterangan :
				<ul>
				<li>Bayar Pertama ADDB meliputi DP, administrasi, fidusia, dan polis</li>
				<li>Asuransi kendaraan dan ACP sudah termasuk di dalam angsuran per bulan</li>
				<li>Asuransi Kendaraan All Risk</li>
				<li>* Tergantung dari Suku Bunga berjalan</li>
				<li>Paket dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu</li></ul>
			</div>

<?php include_once('footer.php');?>
