<?php
error_reporting(0);
session_start();
$_SESSION["referral"] = (!empty($_GET['utm_source'])) ? $_GET['utm_source'] : 'OTHER';
require_once 'includes/config.php';
$v = (empty($_GET['v'])) ? 1 : sanitize($_GET['v']);
$datas = $database->query("SELECT distinct a.pkv_vehicletype, a.pkv_id, a.pkv_thumbnails FROM `paketkredit_vehicle_types` a left join `paketkredit_packages` b on a.pkv_id=b.pkv_id where b.pkp_packages=2 order by a.pkv_vehicletype")->fetchAll();

$vendor = "toyota";

$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
	"ORDER" => "pkc_order"
]);
?>

  <?php include_once('header.php');?>
				<!--car info-->
         <?php if ($v<4) { ?>
            <div id="maincontent">
               <div class="row">
               <?php
      				   $i=0;
      				   foreach($datas as $data) {
      				   $i++;
      				   ?>
                  <div class="col-sm-6">
                      <div class="carmodel-wrapper">
                      <a href="detail.php?pid=<?php echo $data["pkv_id"] ;?>" class="carmodel-link">
                         		 <div class="text-center">
                           	   <img src="images/mobil/<?php echo $data["pkv_thumbnails"] ;?>" alt="paket kredit <?php echo $data["pkv_vehicletype"] ;?>" />
                        		  </div>
                          </a>
                      </div>
                  </div>
                    <?php if ($i==2) {?>
                    </div><div class="row">
                    <?php } ?>

                <!--end car-->
                <?php }?>
              </div>
            </div>
            <!--end entry -->
            <?php } else {?>
              <div id="maincontent">
                <div class="box-orange2 text-center">
			               MAAF, DATA YANG ANDA CARI TIDAK DITEMUKAN!
    			      </div>
              </div>
            <?php }?>
      <?php include_once('footer.php');?>
