$(document).foundation();
$('[data-toggle="datepicker"]').datepicker({
  autoHide: true,
  startDate: new Date(),
  format: 'dd/mm/yy'

});
$(document).ready(function(){
    $('.pilihkaryawan').click(function(){
        var inputValue = $(this).attr("value");
        if (inputValue=='Kontrak/Tetap') {
          $("#karyawan-wrapper").show('slow');
          $("#outsource-wrapper").hide();
        } else {
          $("#karyawan-wrapper").hide();
          $("#outsource-wrapper").show('slow');
        }
    });
});
