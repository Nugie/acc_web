<?php
error_reporting(0);
session_start();
$_SESSION["referral"] = (!empty($_GET['utm_source'])) ? $_GET['utm_source'] : 'OTHER';
 ?>
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kredit Multiguna ACC</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/foundation.css">
    <link rel="stylesheet" href="assets/css/datepicker.min.css">
    <link rel="stylesheet" href="assets/css/app.css?v=2.4">
    <script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
  </head>
  <body>
    <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

         ga('create', 'UA-48042405-1', 'acc.co.id');
         ga('send', 'pageview');
     </script>
    <div class="row expanded">
        <a href="/"><img src="assets/images/header2.jpg" alt="Kredit Multiguna ACC" /></a>
    </div>
    <div class="row expanded" id="content-area">
      <div class="row">
          <div class="large-10 medium-12 large-centered small-12 columns">
              <div class="row mb20">
                <div class="medium-12 medium-centered columns">
                <div class="bg1 roundedc shadow1"><i>Silahkan masukkan data yang dibutuhkan untuk pencairan dana cepat :</i></div>
                </div>
              </div>
             
              <div class="row">
                  <div class="medium-12 medium-centered columns">
                    <form method="post" action="apply.php" enctype="multipart/form-data">
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Cabang Booking</div></div>
                        <div class="medium-8 columns lastcol">
                        <select name="cabangterdekat" class="blueline" required="required">
                        <option value="" selected="selected"></option>
                        <option value='Aceh'>Aceh	</option>
                        <option value='Balikpapan'>Balikpapan	</option>
                        <option value='Bandar Jaya'>Bandar Jaya	</option>
                        <option value='Bandung Cibiru'>Bandung Cibiru	</option>
                        <option value='Bandung Naripan'>Bandung Naripan	</option>
                        <option value='Bandung Soekarno Hatta'>Bandung Soekarno Hatta	</option>
                        <option value='Banjarmasin'>Banjarmasin	</option>
                        <option value='Batam'>Batam	</option>
                        <option value='Bekasi'>Bekasi	</option>
                        <option value='Bengkulu'>Bengkulu	</option>
                        <option value='Bintaro'>Bintaro	</option>
                        <option value='Bogor'>Bogor	</option>
                        <option value='Bukittinggi'>Bukittinggi	</option>
                        <option value='C2C'>C2C	</option>
                        <option value='Cibinong'>Cibinong	</option>
                        <option value='Cibubur'>Cibubur	</option>
                        <option value='Cideng'>Cideng	</option>
                        <option value='Cikarang'>Cikarang	</option>
                        <option value='Cirebon'>Cirebon	</option>
                        <option value='Denpasar'>Denpasar	</option>
                        <option value='Denpasar Renon'>Denpasar Renon	</option>
                        <option value='Depok'>Depok	</option>
                        <option value='Duri'>Duri	</option>
                        <option value='Fatmawati'>Fatmawati	</option>
                        <option value='Gorontalo'>Gorontalo	</option>
                        <option value='Gresik'>Gresik	</option>
                        <option value='Harapan Indah'>Harapan Indah	</option>
                        <option value='Head Office'>Head Office	</option>
                        <option value='Jambi'>Jambi	</option>
                        <option value='Jember'>Jember	</option>
                        <option value='Kalimalang'>Kalimalang	</option>
                        <option value='Karawaci'>Karawaci	</option>
                        <option value='Karawang'>Karawang	</option>
                        <option value='Kebon Jeruk'>Kebon Jeruk	</option>
                        <option value='Kediri'>Kediri	</option>
                        <option value='Kelapa Gading'>Kelapa Gading	</option>
                        <option value='Kendari'>Kendari	</option>
                        <option value='Kudus'>Kudus	</option>
                        <option value='Kwitang'>Kwitang	</option>
                        <option value='Lampung'>Lampung	</option>
                        <option value='Magelang'>Magelang	</option>
                        <option value='Makassar'>Makassar	</option>
                        <option value='Malang'>Malang	</option>
                        <option value='Manado'>Manado	</option>
                        <option value='Mataram'>Mataram	</option>
                        <option value='Medan Adam Malik'>Medan Adam Malik	</option>
                        <option value='Medan Binjai'>Medan Binjai	</option>
                        <option value='Medan Juanda'>Medan Juanda	</option>
                        <option value='Muara Bungo'>Muara Bungo	</option>
                        <option value='Padang'>Padang	</option>
                        <option value='Palangkaraya'>Palangkaraya	</option>
                        <option value='Palembang'>Palembang	</option>
                        <option value='Palembang Plaju'>Palembang Plaju	</option>
                        <option value='Palu'>Palu	</option>
                        <option value='Pancoran'>Pancoran	</option>
                        <option value='Pangkal Pinang'>Pangkal Pinang	</option>
                        <option value='Pare Pare'>Pare Pare	</option>
                        <option value='Pekanbaru'>Pekanbaru	</option>
                        <option value='Pluit'>Pluit	</option>
                        <option value='Pondok Cabe'>Pondok Cabe	</option>
                        <option value='Pontianak'>Pontianak	</option>
                        <option value='Purwokerto'>Purwokerto	</option>
                        <option value='Rantau Prapat'>Rantau Prapat	</option>
                        <option value='Samarinda'>Samarinda	</option>
                        <option value='Semarang'>Semarang	</option>
                        <option value='Serang'>Serang	</option>
                        <option value='Sukabumi'>Sukabumi	</option>
                        <option value='Surabaya Merr'>Surabaya Merr	</option>
                        <option value='Surabaya Panglima Sudirman'>Surabaya Panglima Sudirman	</option>
                        <option value='Surabaya Waru'>Surabaya Waru	</option>
                        <option value='Surakarta'>Surakarta	</option>
                        <option value='Tangerang'>Tangerang	</option>
                        <option value='Tasikmalaya'>Tasikmalaya	</option>
                        <option value='Tegal'>Tegal	</option>
                        <option value='WTC Mangga Dua'>WTC Mangga Dua	</option>
                        <option value='Yogyakarta'>Yogyakarta	</option>

                        </select>
                        </div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Status Karyawan</div></div>
                        <div class="medium-8 columns lastcol">
                            <input type="radio" name="statuskaryawan" value="Kontrak/Tetap" class="option-input pilihkaryawan radio" required="required" /> <span class="roundedc2">Tetap / Kontrak ACC</span>&nbsp;&nbsp;
                            <input type="radio" name="statuskaryawan" value="Outsource" class="option-input pilihkaryawan radio" required="required" /> <span class="roundedc2">Outsource</span>
                        </div>
                    </div>
                    <div id="karyawan-wrapper">
                        <div class="row mb10">
                            <div class="medium-4 columns mb10"><div class="labelstyle bg2">NPK Karyawan</div></div>
                            <div class="medium-8 columns lastcol"><input type="text" name="npkkaryawan" maxlength="5" class="blueline" /></div>
                        </div>
                    </div>
                    <div id="outsource-wrapper">
                        <div class="row mb10">
                            <div class="medium-4 columns mb10"><div class="labelstyle bg2">Nama Karyawan</div></div>
                            <div class="medium-8 columns lastcol"><input type="text" name="namakaryawan" class="blueline" /></div>
                        </div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Customer Pernah Kredit di ACC?</div></div>
                        <div class="medium-8 columns lastcol">
                            <input type="radio" name="pernahkredit" value="Ya" class="option-input radio" /> <span class="roundedc" required="required">Ya</span>&nbsp;&nbsp;
                            <input type="radio" name="pernahkredit" value="Tidak" class="option-input radio" /> <span class="roundedc" required="required">Tidak</span>
                        </div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Nama Customer (sesuai KTP)</div></div>
                        <div class="medium-8 columns lastcol"><input type="text" name="namalengkap" class="blueline" required="required" /></div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">No. Telepon/HP</div></div>
                        <div class="medium-8 columns lastcol"><input type="text" name="notelp" class="blueline" required="required" /></div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Email</div></div>
                        <div class="medium-8 columns lastcol"><input type="email" name="alamatemail" class="blueline" required="required" /></div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Attachment STNK</div></div>
                        <div class="medium-8 columns lastcol"><input type="file" name="stnk" class="blueline" /></div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Pencairan</div></div>
                        <div class="medium-8 columns lastcol"><input type="text" name="pencairan" class="blueline"  /></div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Tipe Unit</div></div>
                        <div class="medium-8 columns lastcol"><input type="text" name="tipeunit" class="blueline"  /></div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-12 columns">
                        <div class="bg2 roundedc">Demi mempercepat proses, mohon masukkan tanggal Anda bisa datang ke ACC dalam waktu seminggu ke depan terhitung dari hari ini, dengan format &lt;dd/mm/yy&gt;. <br />Contoh : 25/09/18</div></div>
                      </div>
                        <div class="row mb20">
                            <div class="large-8 large-centered medium-12 columns lastcol"><input type="text" name="tanggalvisit" class="blueline" required="required" data-toggle="datepicker" class="tanggalvisit" /></div>
                        </div>
                   
                        <div class="row mb20">
                            <div class="medium-12 columns text-center"><b><i>Dengan ini saya menyatakan bahwa informasi yang saya berikan diatas adalah benar.</i></b></div>
                        </div>
                        <div class="row mb10">
                            <div class="medium-6 columns text-center mb10"><div class="g-recaptcha" data-sitekey="6LdpsxETAAAAAOlPjhE-uQbe-8Y3Bs60vgy9axSJ"></div></div>
                            <div class="medium-6 columns"><input type="submit" value="KIRIM" class="button-submit shadow2" /></div>
                        </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
      <div class="clearfix"></div>
    </div>

    <script src="assets/js/vendor/jquery.js"></script>
    <script src="assets/js/vendor/what-input.js"></script>
    <script src="assets/js/vendor/foundation.js"></script>
    <script src="assets/js/datepicker.min.js"></script>
    <script src="assets/js/app.js?v=1.0"></script>
  </body>
</html>
