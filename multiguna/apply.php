<?php
error_reporting(0);
session_start();
require_once 'includes/config.php';
require_once 'includes/class.upload.php';
require_once 'includes/PHPMailer/PHPMailerAutoload.php';
//require_once 'includes/autoload.php';

$namalengkap = sanitize($_POST['namalengkap']);
$notelp = sanitize($_POST['notelp']);
$alamatemail = sanitize($_POST['alamatemail']);
$cabangterdekat = sanitize($_POST['cabangterdekat']);
$tanggalvisit = sanitize($_POST['tanggalvisit']);
$statuskaryawan = sanitize($_POST['statuskaryawan']);
$npkkaryawan = sanitize($_POST['npkkaryawan']);
$namakaryawan = sanitize($_POST['namakaryawan']);
$pernahkredit = sanitize($_POST['pernahkredit']);
$pencairan = sanitize($_POST['pencairan']);
$tipeunit = sanitize($_POST['tipeunit']);

//upload
$handle = new Upload($_FILES['stnk']);
$tanggals = date("d").date("m");
if ($handle->uploaded) {
	$namafile = "STNK-".$tanggals."-".preg_replace("/\s+/","",$namalengkap);
	$handle->image_resize          = true;
	$handle->image_ratio_y	      = true;
	$handle->image_x               = 500;
	$handle->file_new_name_body = $namafile;
	$handle->image_convert = 'jpg';
	$handle->file_max_size = '3072000';
	$handle->mime_check = true;
	$handle->allowed = array('image/*');
	$handle->Process('uploads/');

	if ($handle->processed) {
		$fname = $handle->file_dst_name;
	} else {
		$fname = "";
	}

	$handle-> Clean();

} else {
	$fname = "";
}
//end upload


$siteKey = '6LdpsxETAAAAAOlPjhE-uQbe-8Y3Bs60vgy9axSJ';
$secret = '6LdpsxETAAAAAGB9VYRuOIZKOvVpAIEZnGs3vKsi';
?>
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kredit Multiguna ACC</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/foundation.css">
    <link rel="stylesheet" href="assets/css/datepicker.min.css">
    <link rel="stylesheet" href="assets/css/app.css?v=2.2">
    <script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
  </head>
  <body>
    <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

         ga('create', 'UA-48042405-1', 'acc.co.id');
         ga('send', 'pageview');
     </script>
    <div class="row expanded">
        <a href="/"><img src="assets/images/header2.jpg" alt="Kredit Multiguna ACC" /></a>
    </div>
    <div class="row expanded" id="content-area">
      <div class="row">
          <div class="large-10 medium-12 large-centered small-12 columns">
              <div class="row columns mb20">
                  <div class="bg1 roundedc"><i>
                    <?php
                    if (!isset($_POST['g-recaptcha-response'], $namalengkap, $notelp, $alamatemail, $tanggalvisit)) {
                        echo "Mohon lengkapi data Anda!";
                    } else {
          						$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
          						if ($response['success'] == '1') {
                        	 $pkentry = $database->insert("paketkredit_multiguna_applicants", [
									"pka_branch" => $cabangterdekat,
									"pka_statuskaryawan" => $statuskaryawan,
									"pka_npkkaryawan" => $npkkaryawan,
									"pka_namakaryawan" => $namakaryawan,
									"pka_pernahkredit" => $pernahkredit,
									"pka_name" => $namalengkap,
                    				"pka_phone" => $notelp,
                    				"pka_email" => $alamatemail,
									"pka_stnk" => $fname,
									"pka_visitdate" => $tanggalvisit,
									"pka_pencairan" => $pencairan,
									"pka_brand" => $tipeunit,
									"pka_referral" => $_SESSION["referral"],
                    				"pka_created" => date('Y-m-d H:i:s')
                    			]);
              				//send email

              				$recipients = $database->select("paketkredit_recipients", [
              				"name",
              				"email"
              				], [
              				"active" => 'Yes'
              				]);

							$pesanh = "Cabang Terdekat : $cabangterdekat <br />";
							$pesanh .= "Status Karyawan : $statuskaryawan <br />";
							$pesanh .= "NPK : $npkkaryawan <br />";
							$pesanh .= "Nama Karyawan : $namakaryawan <br />";
							$pesanh .= "Customer Pernah Kredit di ACC : $pernahkredit <br />";
							$pesanh .= "Nama Customer : $namalengkap <br />";
              				$pesanh .= "Email : $alamatemail <br />";
							$pesanh .= "No. Telepon : $notelp <br />";
							$pesanh .= "STNK : https://www.acc.co.id/multiguna/uploads/".$fname."<br />";
							$pesanh .= "Tanggal Kunjungan : $tanggalvisit <br />";
							$pesanh .= "Pencairan : $pencairan <br />";
							$pesanh .= "Tipe Unit : $tipeunit <br />";
							
							$pesan = "Cabang Terdekat : $cabangterdekat  \r\n";
							$pesan .= "Status Karyawan : $statuskaryawan  \r\n";
							$pesan .= "NPK : $npkkaryawan  \r\n";
							$pesan .= "Nama Karyawan : $namakaryawan  \r\n";
							$pesan .= "Customer Pernah Kredit di ACC : $pernahkredit  \r\n";
							$pesan .= "Nama Customer : $namalengkap  \r\n";
              				$pesan .= "Email : $alamatemail  \r\n";
							$pesan .= "No. Telepon : $notelp  \r\n";
							$pesan .= "STNK : https://www.acc.co.id/multiguna/uploads/".$fname. "\r\n";
              				$pesan .= "Tanggal Kunjungan : $tanggalvisit  \r\n";
							$pesan .= "Pencairan : $pencairan \r\n";
							$pesan .= "Tipe Unit : $tipeunit \r\n";

              				//send mail
              				$mail = new PHPMailer;
              				$mail->isSendmail();
              				$mail->setFrom("info@acc.co.id", "ACC Website");
              				$mail->addReplyTo($alamatemail, $namalengkap);
              				foreach($recipients as $rcpt_to){
              					$mail->addAddress($rcpt_to["email"],$rcpt_to["name"]);
              				}

              				$mail->isHTML(true);
              				$mail->Subject = '[Kredit Multiguna ACC] Customer Apply';
              				$mail->Body    = $pesanh;
              				$mail->AltBody = $pesan;

              				$pesannya = "Terima kasih, aplikasi kredit multiguna Anda akan segera kami proses. \r\n\r\n\r\n Astra Credit Companies";
              				$pesannyah = "Terima kasih, aplikasi kredit multiguna Anda akan segera kami proses. <br><br> Astra Credit Companies";

              				$mail2 = new PHPMailer;
              				$mail2->isSendmail();
              				$mail2->setFrom('info@acc.co.id','Astra Credit Companies');
              				$mail2->addReplyTo('info@acc.co.id','Astra Credit Companies');
              				$mail2->addAddress($alamatemail, $namalengkap);
              				$mail2->isHTML(true);
              				$mail2->Subject = '[Kredit Multiguna ACC] Thank You';
              				$mail2->Body    = $pesannyah;
              				$mail2->AltBody = $pesannya;

              				if($mail->send()||$mail2->send()) {
                          echo "Terima kasih, aplikasi Kredit Multiguna Anda telah terkirim. Kami akan memberikan respon kepada Anda secepatnya!";
                      } else {
              					  echo "Terima kasih, aplikasi Kredit Multiguna Anda telah terkirim. Kami akan memberikan respon kepada Anda secepatnya!";
	                    }
              	 } else { //wrong captha
    			    	 			echo "Silahkan cek kembali kode keamanan Anda!";
                  }
              }?>
                  </i></div>
                  <br /><br />
              </div>
          </div>
      </div>
      <div class="clearfix"></div>
    </div>

    <script src="assets/js/vendor/jquery.js"></script>
    <script src="assets/js/vendor/what-input.js"></script>
    <script src="assets/js/vendor/foundation.js"></script>
    <script src="assets/js/datepicker.min.js"></script>
    <script src="assets/js/app.js?v=1.0"></script>
  </body>
</html>
