<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>Paket Kredit MANTAP GIIAS 2018 - ACC</title>

	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css?v=3.8" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
		<script>
	       (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	       (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	       m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	       })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	       ga('create', 'UA-48042405-1', 'acc.co.id');
	       ga('send', 'pageview');
	   </script>
     <script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
  </head>
  <body>
    <div class="container-fluid">
    	<div class="row">
            <!--left skyscrapper-->
            <div class="col-sm-2 col-md-2 visible-md-block visible-lg-block"><!--div class="banner-sky"><a href="/paket-kredit-acc" target="_blank"><img src="images/banner-skyscrapper-vipaccess-2703.jpg" alt="Banner" /></a></div>--></div>
            <!--/left skyscrapper-->
            <!--middle-->
            <div class="col-sm-12 col-md-8">
              <div class="text-center m-10"><a href="index.php"><h1>PAKET KREDIT MANTAP - GIIAS 2018</h1></a></div>
              <div id="logo2" class="visible-xs"><a href="http://www.acc.co.id"><img src="../images/logo.png" alt="ACC Logo" /></a></div>
              <div id="menuatas">
                <a href="index.php?v=1" class="menubrand<?php if ($v==1) { echo ' mbactive';}?>"><img src="../images/button_toyota.jpg" alt="" class="logobrand" /></a>
                <a href="index.php?v=2" class="menubrand<?php if ($v==2) { echo ' mbactive';}?>"><img src="../images/button_daihatsu.jpg" alt="" class="logobrand" /></a>
              </div>
            	<!--top bar -->
                <div class="entry-wrapper">
                	<div class="row">
                    	<div class="col-sm-12 col-md-12 col-lg-12">
                        	<div class="box-orange box-<?php echo $vendor;?>" id="pkheader">
                            	<table>
                                	<tr>
                                    	<td width="15%" class="abottom">PAKET</td>
                                        <td width="1%" class="abottom">:</td>
                                        <td width="84%">KREDIT&nbsp;&nbsp; <span class="mantap">MANTAP <?php echo strtoupper($vendor);?> - GIIAS 2018</span></td>
                                    </tr>
                                	<tr>
                                    	<td width="15%">TENOR</td>
                                        <td width="1%">:</td>
                                        <td width="79%"><?php echo $tenor;?></td>
                                    </tr>
                                    <tr>
                                        <td width="15%">BERLAKU</td>
                                          <td width="1%">:</td>
                                          <td width="79%">GIIAS 2018</td>
                                      </tr>
                                </table>
                                <div id="logo" class="hidden-xs"><a href="https://www.acc.co.id"><img src="../images/logo.png" alt="ACC Logo" /></a></div>
                            </div>
                        </div>
                   	 </div>
                  </div>
                  <!--top bar -->
