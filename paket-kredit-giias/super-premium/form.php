<?php
error_reporting(0);
session_start();
require_once '../includes/config.php';
$v = (empty($_GET['v'])) ? 1 : sanitize($_GET['v']);
$pid = (empty($_GET['pid'])) ? 1 : sanitize($_GET['pid']);
$pkv = (empty($_GET['pkv'])) ? 1 : sanitize($_GET['pkv']);
$vendor = "toyota";
$areas = $database->select("areas", [
	"id",
	"description"
], [
	"ORDER" => "description"
]);
$branches = $database->select("branches", [
	"branch_id",
	"description"
], [
	"area_id" => 1,
	"ORDER" => "description"
]);
$packages = $database->select("paketkredit_packages", [
	"pkp_id",
	"pkv_id",
	"pkp_variant",
	"pkp_price",
	"pkp_term",
	"pkp_dp",
	"pkp_installment"
], [
	"pkp_id" => $pid
]);
$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
  "paketkredit_categories.pkc_id[<=]" => 2,
	"ORDER" => "pkc_order"
]);
switch ($v) {
	case 1:
    $vendor = "toyota";
    $tenor = "8 TAHUN";
	break;
	default:
    $vendor = "daihatsu";
    $tenor = "6 - 8 TAHUN";
	break;
}
?>
<?php include_once('header.php');?>
  <div id="maincontent">
		<div class="row">
			<div class="col-md-12">
      <?php if (!$packages) {?>
          <div class="box-orange2 text-center col-md-8">
			    MAAF, PAKET KREDIT YANG ANDA CARI TIDAK DITEMUKAN!
			  </div>
      <?php } else {?>
            <div class="box-orange2 text-center col-md-12">
            TERIMA KASIH ATAS KETERTARIKAN ANDA PADA PAKET KREDIT ACC. MOHON ISI DATA BERIKUT INI :
            </div>
            <div id="form-entry">
            <form id="accform" action="formsubmit.php" method="post" class="form-horizontal">
                <input type="hidden" name="paket" value="<?php echo $pid;?>" />
                <input type="hidden" name="vtype" value="<?php echo $pkv;?>" />
                <input type="hidden" name="v" value="<?php echo $v;?>" />
                  <div class="form-group">
                    <label for="nama" class="col-sm-3 col-md-3 control-label">NAMA</label>
										 <div class="col-sm-9 col-md-9">
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required="required">
										</div>
                  </div>
                  <div class="form-group">
                    <label for="notelp" class="col-sm-3 col-md-3 control-label">NOMOR TELEPON</label>
										 <div class="col-sm-9 col-md-9">
                    <input type="tel" class="form-control" id="notelp" name="notelp" placeholder="Nomor Telepon" required="required">
										</div>
                  </div>
                   <div class="form-group">
                    <label for="emailaddr" class="col-sm-3 col-md-3 control-label">EMAIL</label>
										 <div class="col-sm-9 col-md-9">
                    <input type="email" class="form-control h5-email" id="emailaddr" name="emailaddr" placeholder="Email" required="required">
										</div>
                  </div>

                  <div class="form-group">
                    <label for="kota" class="col-sm-3 col-md-3 control-label">KOTA</label>
										 <div class="col-sm-9 col-md-9">
                    <select class="form-control" id="kota" name="kota" required="required">
                        <option value="">pilih kota</option>
                        <?php foreach($areas as $area) {?>
                        <option value="<?php echo $area["id"] ;?>"><?php echo $area["description"] ;?></option>
                        <?php } ?>
                    </select>
										</div>
                  </div>

                   <div class="form-group">
                    <label for="cabang" class="col-sm-3 col-md-3 control-label">KANTOR CABANG ACC</label>
										 <div class="col-sm-9 col-md-9">
                    <div id="cabang-loading">
                    <select class="form-control" name="cabang" id="cabang" required="required">
                        <option value="">pilih kantor cabang ACC terdekat</option>
                        <?php foreach($branches as $branch) {?>
                        <option value="<?php echo $branch["description"] ;?>"><?php echo $branch["description"] ;?></option>
                        <?php } ?>
                    </select>
									</div>
                    </div>
                  </div>
                   <div class="form-group">
                    <label for="captcha" class="col-sm-3 col-md-3 control-label">&nbsp;</label>
										 <div class="col-sm-9 col-md-9">
                    <div class="g-recaptcha" data-sitekey="6LdpsxETAAAAAOlPjhE-uQbe-8Y3Bs60vgy9axSJ"></div>
									</div>
                  </div>
                  <div class="clearfix"></div>
                    <div class="col-md-12" style="margin:10px 0;font-size:12px;padding:0 20px;"><em><strong>Saya mengizinkan ACC dan mitranya untuk menghubungi Saya dalam membantu proses pembelian mobil. Dengan memberikan email dan nomor telepon, saya telah menyetujui untuk menerima semua pemberitahuan melalui ACC.</strong></em>

                  <div class="mart-20"><button type="submit" class="btn btn-primary btn-lg">Kirim</button></div></div>
                </form>

            </div>

          </div>
         <?php } ?>
			  </div>
			</div>
				<!--menubawah-->
<?php include_once('footer.php');?>
