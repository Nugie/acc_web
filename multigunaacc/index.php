<?php
error_reporting(0);
session_start();
$_SESSION["referral"] = (!empty($_GET['utm_source'])) ? $_GET['utm_source'] : 'OTHER';
 ?>
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kredit Multiguna ACC</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/foundation.css">
    <link rel="stylesheet" href="assets/css/datepicker.min.css">
    <link rel="stylesheet" href="assets/css/app.css?v=2.4">
    <script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
  </head>
  <body>
    <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

         ga('create', 'UA-48042405-1', 'acc.co.id');
         ga('send', 'pageview');
     </script>
    <div class="row expanded">
        <a href="/"><img src="assets/images/header2.jpg" alt="Kredit Multiguna ACC" /></a>
    </div>
    <div class="row expanded" id="content-area">
      <div class="row">
          <div class="large-10 medium-12 large-centered small-12 columns">
              <div class="row mb20">
                <div class="medium-12 medium-centered columns">
                <div class="bg1 roundedc shadow1"><i>Silahkan masukkan data yang dibutuhkan untuk pencairan dana cepat :</i></div>
                </div>
              </div>
              <div class="row">
                  <div class="medium-12 medium-centered columns">
                    <form method="post" action="apply.php">
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Nama (sesuai KTP)</div></div>
                        <div class="medium-8 columns lastcol"><input type="text" name="namalengkap" class="blueline" required="required" /></div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">No. Telepon/HP</div></div>
                        <div class="medium-8 columns lastcol"><input type="text" name="notelp" class="blueline" required="required" /></div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">Email</div></div>
                        <div class="medium-8 columns lastcol"><input type="email" name="alamatemail" class="blueline" required="required" /></div>
                    </div>
                    <div class="row mb10">
                        <div class="medium-4 columns mb10"><div class="labelstyle bg2">No. Plat Mobil</div></div>
                        <div class="medium-8 columns lastcol"><input type="text" name="noplatmobil" class="blueline" required="required" /></div>
                    </div>
                   
                      <div class="row mb10">
                        <div class="medium-12 columns">
                        <div class="bg2 roundedc">Demi mempercepat proses, mohon masukkan tanggal Anda bisa datang ke ACC dalam waktu seminggu ke depan terhitung dari hari ini, dengan format &lt;dd/mm/yy&gt;. <br />Contoh : 22/10/18</div></div>
                      </div>
                        <div class="row mb20">
                            <div class="large-8 large-centered medium-12 columns lastcol"><input type="text" name="tanggalvisit" class="blueline" required="required" data-toggle="datepicker" class="tanggalvisit" /></div>
                        </div>
                        <div class="row mb20">
                            <div class="medium-12 columns text-center"><b><i>Dengan ini Saya bersedia untuk dihubungi oleh ACC dalam proses pencairan dana cepat</i></b></div>
                        </div>
                        <div class="row mb10">
                            <div class="medium-6 columns text-center mb10"><div class="g-recaptcha" data-sitekey="6LdpsxETAAAAAOlPjhE-uQbe-8Y3Bs60vgy9axSJ"></div></div>
                            <div class="medium-6 columns"><input type="submit" value="KIRIM" class="button-submit shadow2" /></div>
                        </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
      <div class="clearfix"></div>
    </div>

    <script src="assets/js/vendor/jquery.js"></script>
    <script src="assets/js/vendor/what-input.js"></script>
    <script src="assets/js/vendor/foundation.js"></script>
    <script src="assets/js/datepicker.min.js"></script>
    <script src="assets/js/app.js?v=1.0"></script>
  </body>
</html>
