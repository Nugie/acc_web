$(document).foundation();
$('[data-toggle="datepicker"]').datepicker({
  autoHide: true,
  startDate: new Date(),
  format: 'dd/mm/yy'

});
$(document).ready(function(){
    $('.option-input').click(function(){
        var inputValue = $(this).attr("value");
        if (inputValue=='Y') {
          $("#alamatbaru-wrapper").hide('slow');
        } else {
          $("#alamatbaru-wrapper").show('slow');
        }
    });
});
