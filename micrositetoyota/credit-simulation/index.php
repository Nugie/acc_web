<?
session_cache_expire(10);
session_start();
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title>Kredit Mobil - Simulasi Kredit TOYOTA</title>
<meta name="description" content="Kredit Mobil Terbaik Dari ACC" />
<meta name="keywords" content="Kredit Mobil, Mobil Kredit, Simulasi Kredit Mobil, Kredit Mobil Murah, Kredit Mobil Baru, ACC Kredit Mobil, Kredit Mobil ACC, Kredit Mobil Terbaik, Beli Mobil Kredit" />
<?php
include 'simulation.php';
?>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
<link type="text/css" href="ui/demos.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="jquery.selectbox.css" />
<link rel="stylesheet" type="text/css" href="jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" href="jquery-ui-1.9.1.custom.min.css" />
<script type="text/javascript" src="js/jquery-1.8.2.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.9.1.custom.min.js"></script>
<script type="text/javascript" src="js/cufon-yui.js" ></script>
<script type="text/javascript" src="js/Myriad_Pro_400-Myriad_Pro_700.font.js"></script>
<script type="text/javascript" src="js/jquery.selectbox-0.2.min.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="js/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
Cufon.replace('.block_acc', { fontFamily: 'Myriad Pro' });
Cufon.replace('.company_title', { fontFamily: 'Myriad Pro' });

	$(function() {
		$(".duit").maskMoney({thousands:'.', decimal:'', allowZero:false, suffix: '', precision:0});
		$("#loader").hide();
		$("#hasil_hitung").hide();

		$("#area").selectbox({
			onChange: function (val, inst) {
				$("#branch_loading").html('<img src="images/ajax-loader.gif" />');
				$.ajax({
					type: "GET",
					data: {area: val},
					url: "load_query.php",
					success: function (data) {
						$("#branch_loading").html(data);
						$("#branch").selectbox({
							onChange: function (val, inst) {

								$("#applybutton").html('');
							}
							});
					}
				});
			},
			effect: "slide"
			});
		$("#vehicle_model").selectbox({
			width:300,
			onChange: function (val, inst) {
				$("#hasil_hitung").html('');
				$('#otr_loading').fadeOut("fast");
				$('#loader').show("fast");
				$.post("simulation.php", {
					model_id: $('#vehicle_model').val(),
				}, function(response){

					setTimeout("finishAjax('otr_loading', '"+escape(response)+"')", 200);
				});

				return false;

			}
			});

		$("#branch").selectbox({
			onChange: function (val, inst) {
				$("#applybutton").html('');
			}
			});

		$("#vehicle_type").selectbox({
			onChange: function (val, inst) {
				$("#otr").val('');
				$("#dp").val('');
				$("#hasil_hitung").html('');
				$("#model_loading").html('<img src="images/ajax-loader.gif" />');
				$.ajax({
					type: "GET",
					data: {type_id: val},
					url: "load_query.php",
					success: function (data) {
						$("#model_loading").html(data);
						$("#vehicle_model").selectbox();
					}
				});
			},
			effect: "slide"
			});

			$('#dp').keyup(function() {
	 			$("#hasil_hitung").html('');
			});

				$("#credit-simulations-form").submit(function(){

				var mindp0 = $("#otr").val();
				var mindp1 = parseInt(mindp0.replace(/\./g, ''), 10);
				var mindp = mindp1*25/100;
				var dpd0 = $("#dp").val();
				var dpd = parseInt(dpd0.replace(/\./g, ''), 10);
					if ( dpd < mindp ){
						alert("Mohon Maaf. Minimum DP yang dapat digunakan untuk perhitungan Kredit adalah 25% dari harga kendaraan");
						$("#dp").focus();
						return false;
					}
				});

	});


	function finishAjax(id, response){
	$('#loader').hide();
	$('#'+id).html(unescape(response));
	$('#'+id).fadeIn();
	}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48042405-1', 'acc.co.id');
  ga('send', 'pageview');

</script>

<?php

if($_POST){
	$area = $_POST['area'];
	$branch = $_POST['branch'];
	$v_type = $_POST['vehicle_type'];
	$v_model = $_POST['vehicle_model'];
	$acp = $_POST['acp_status'];
	$otr = $_POST['otr'];
	$dp = $_POST['dp'];

	if ($area==''||$branch==''||$v_type==''||$v_model==''){
		$errorm = 1;
		$angsuran = "";
	} else {
	 for ($tenor=1;$tenor<=4;$tenor++){
	 	$errorm = 0;
	 	$hit = hitung_simulasi($area,$branch,$v_type,$v_model,$acp,$otr,$dp,$tenor);
		$uang_muka[$tenor] = number_format($hit['uang_muka'],0,',','.');
		$angsuran[$tenor] = number_format($hit['angsuran'],0,',','.');
	 }
	}



 ?>
<script>

	//document.getElementById('vehicle_model').innerHTML = '<?php echo $_POST['vehicle_model']?>';
	//document.getElementById('acp_status').innerHTML = '<?php echo $_POST['acp_status']?>';
	//document.getElementById('otr').innerHTML = '<?php echo $_POST['otr']?>';
	//document.getElementById('dp').selectedIndex = '<?php echo $_POST['dp']?>';
	//document.getElementById('tenor').selectedIndex = '<?php echo $_POST['tenor']?>';
	//document.getElementById('uang-muka').innerHTML = '<?php echo $uang_muka?>';
	//document.getElementById('angsuran').innerHTML = '<?php echo $angsuran?>';


	$(document).ready(function() {
		$("#hasil_hitung").show();
		$('.fancybox').fancybox(
		{
			maxWidth	: 350,
			maxHeight	: 600,
			fitToView	: false,
			margin		: 0,
			padding		: 5,
			width		: '330px',
			height		: '540px',
			autoSize	: false,
			closeBtn	: false,
			closeClick	: false
		}
		);

	})
</script>
<? }?>

</head>
<body>
    <div class="container" style="margin-top:20px;">
    <div class="row">
          <!--main content-->
          <div class="col-md-11">
            <div class="headertitle">SIMULASI KREDIT ACC</div>
            <div class="logo"><img src="../img/logo-acc.jpg" alt="Logo ACC" /></div>
              <!--content-->
                <div>
	<div>Silahkan melakukan simulasi kredit menggunakan form di bawah ini dan klik apply jika Anda ingin dihubungi lebih lanjut oleh team sales kami.<br /><br /></div>
    <div>
		<form name="credit-simulations-form" id="credit-simulations-form" method="post" action="index.php" accept-charset="utf-8">
        	<div class="row marb">
            	<div class="col-md-3 fontbold">Model</div>
                <div class="col-md-9">
                <?php $idType = $_REQUEST['vehicle_type'];?>
					<select name="vehicle_type" id="vehicle_type" class="styled-combo">
                    <option value="">Select Model</option>
					<?php
						if(!empty($_REQUEST['variant_id'])){
							$id = $_REQUEST['variant_id'];
							$query_type = "SELECT a_variantid, model_id, a_variant, a_price FROM cso_auto2000 WHERE a_variantid = ".$id." limit 1";
							$type_list = database::getData($query_type);
							$price = $type_list[0]['a_price'];
							$modelnya = $type_list[0]['model_id'];
							$query_type1 = "SELECT * FROM cso_auto2000_model order by model_name";
							$type_list1 = database::getData($query_type1);
							foreach($type_list1 as $tipe1){
							?>
                            	<option value="<?php echo $tipe1['model_id']?>" <? if($tipe1['model_id'] == $modelnya) {?>selected="selected"<? }?>><?= $tipe1['model_name']?></option>
                             <? }?>
						<?
						} else {
							$query_type = "SELECT * FROM cso_auto2000_model ORDER BY model_name";
							$type_list = database::getData($query_type);?>
								<?php foreach($type_list as $tipe){?>
                                <?php if($tipe['model_id']==$idType){?>
										<option value="<?php echo $tipe['model_id']?>" selected="selected"><?php echo $tipe['model_name']?></option>
									<?php } else{ ?>
										<option value="<?php echo $tipe['model_id']?>"><?php echo $tipe['model_name']?></option>
								<?php } } }?>

					</select>
                </div>
            </div>
            <div class="row marb">
            	<div class="col-md-3 fontbold">Variant</div>
                <div class="col-md-9">
                <?php
					$idType = $_REQUEST['vehicle_type'];
					$idModel = $_REQUEST['vehicle_model'];
					?>
					<div id="model_loading">

					<select name="vehicle_model" id="vehicle_model" class="styled-combo">

					<?php
					if(!empty($_REQUEST['variant_id'])){
						$query_m = "SELECT a_variantid,a_variant FROM cso_auto2000 where model_id=$modelnya";
						$m_list = database::getData($query_m);
						foreach($m_list as $m){?>
							<option value="<?php echo $m['a_variantid']?>" <? if($m['a_variantid'] == $id) {?>selected="selected"<? }?>><? echo $m['a_variant']?></option>
							<?php }
					} else {
						$query_model = "SELECT a_variantid,a_variant FROM cso_auto2000 WHERE model_id ='". $idType ."' ";

						$model_list = database::getData($query_model);
						foreach($model_list as $model){
							if($model['a_variantid']==$idModel){?>
								<option value="<?php echo $model['a_variantid']?>" selected><?= $model['a_variant']?></option>
							<?php } else { ?>
								<option value="<?php echo $model['a_variantid']?>"><?= $model['a_variant']?></option>
					<?php } } }	?>
					</select>
                </div>
                </div>
            </div>
                <input type="hidden" name="tenor" id="tenor" value="">
               <div id="otr_loading" style="margin:0;">
                       <div class="row marb">
                        <div class="col-md-3 fontbold">Harga Kendaraan</div>
                        <div class="col-md-9"><?php $val_otr = $_REQUEST['otr'];?>

                                            <?php if(!empty($_REQUEST['variant_id'])){ ?>
                                                <!--<input name="otrd" type="text" id="otrd" size="23" class="teksnya" value="<?php echo $price?>" disabled="disabled"/>-->
                                                <input name="otr" type="text" id="otr" size="23" class="duit teksnya" value="<?php echo $price?>" />
                                            <?php } else { ?>
                                                <!--<input name="otrd" type="text" id="otrd" size="23" class="teksnya" value="<?php echo $val_otr?>" disabled="disabled"/>-->
                                                <input name="otr" type="text" id="otr" size="23" class="duit teksnya" value="<?php echo $val_otr?>" readonly />
                                            <?php } ?></div>
                       </div>
                       <div class="row marb">
                        <div class="col-md-3 fontbold">Down Payment (DP)</div>
                        <div class="col-md-9"> <input name="dp" type="text" id="dp" size="23" class="duit teksnya" value="<?php echo $_REQUEST['dp']?>" /></div>
                       </div>
               </div><div id="loader"></div>
               <div class="marb"><em>(DP dapat diubah secara manual, minimum 25% dari harga kendaraan)</em></div>
                <div class="row marb">
                	<div class="col-md-3 fontbold">Area / Daerah</div>
                    <div class="col-md-9"><input type="hidden" name="acp_status" value="Yes" /><?php $idArea = $_REQUEST['area'];?>
					<select name="area" id="area" class="styled-combo">
						<?
								$query_area = "SELECT area_id, description FROM cso_areas ORDER BY description";
								$area_list = database::getData($query_area);?>
							<option value="">Select Area</option>
						<?php foreach($area_list as $area){
								if($area['area_id'] == $idArea){
						?>
							<option value="<?= $area['area_id'] ?>" selected><?= $area['description'] ?></option>
							<?php } else { ?>
							<option value="<?= $area['area_id'] ?>" <? if($area['area_id'] == 1) {?>selected="selected"<? }?>><?= $area['description'] ?></option>
							<?php } } ?>
					</select></div>
                </div>
                <div class="row marb">
                	<div class="col-md-3 fontbold">Cabang ACC terdekat</div>
                    <div class="col-md-9"><?php $idBranch = $_REQUEST['branch'];
					?>
					<div id="branch_loading">
					<select name="branch" id="branch" class="styled-combo" required="required">
                    <option value="">Select Branch</option>
							<?
									if ($idArea=="") {
									$query_branch = "SELECT branch_id, description FROM cso_branches WHERE area_id = 1 ORDER BY description";
									}else {
									$query_branch = "SELECT branch_id, description FROM cso_branches WHERE area_id = ".$idArea." ORDER BY description";
									}
									$branch_list = database::getData($query_branch);?>

								<?php foreach($branch_list as $branch){
										if($branch['branch_id']==$idBranch){?>
											<option value="<?php echo $branch['branch_id']?>" selected><?= $branch['description']?></option>
										<?php } else {?>
											<option value="<?php echo $branch['branch_id']?>" <? if($branch['branch_id'] == 1&&$idBranch=="") {?>selected="selected"<? }?>><?= $branch['description']?></option>
										<?php } } ?>

					</select></div></div>
                </div>

			<?
				if(!empty($_REQUEST['variant_id'])){
					$idmodela = $_REQUEST['variant_id'];
				} else {
					$idmodela = $idModel;
				}
				$query_model = "SELECT a_variant, b.model_name FROM cso_auto2000 a left join cso_auto2000_model b on a.model_id=b.model_id WHERE a.a_variantid ='". $idmodela ."' ";

				$model_list = database::getData($query_model);
				foreach($model_list as $model){
					$pilihan = trim($model["model_name"])." - ".$model["a_variant"];
				}

	?>
    		<div class="row">
            	<div class="col-md-9 col-xs-offset-3" style="margin-top:10px;">
					<input id="form-submit" class="tombol ui-corner-all" type="submit" value="Calculate" />
					<? if (!empty($pilihan)&&!empty($idBranch)) {
                    	$_SESSION["pilihan"] = $pilihan;
						$_SESSION["idBranch"] = $idBranch;
					?>

					<? }?>
                    </div>
             </div>
</form></div>

            	<!--result-->
                <? if ($errorm==1) {?>
				 <div id="hasil_hitung">
               		 <span style="color:red;font-weight:bold;">Mohon isi Area, Cabang, Model dan Variant Mobil dengan lengkap!</span><br /><br />
                </div>
        	<? } else {?>
                <div id="hasil_hitung" style="margin-top:20px;">
                <table border='0' width="100%" class="tabelsimulasi">
                    <tr>
                        <td align='center'>&nbsp;</td>
                        <? for ($i=1;$i<=4;$i++){ ?>
                        <th><?=$i?> Tahun</th>
                        <? } ?>
                    </tr>
                    <tr>
                        <th>Uang Muka</th>
                        <? for ($i=1;$i<=4;$i++){
                            $bg = "#FFFFFF";
                            if ($i==2 || $i==4) $bg='#F0F0F0 ';
                        ?>
                        <td valign="top" align='right' bgcolor="<?=$bg?>"><div id="uang-muka"><?php if (isset($uang_muka) && !empty($uang_muka)) echo $uang_muka[$i]; ?></div></td>
                        <? } ?>

                    </tr>
                    <tr>
                        <th>Angsuran</th>
                        <? for ($i=1;$i<=4;$i++){
                            $bg = "#FFFFFF";
                            if ($i==2 || $i==4) $bg='#F0F0F0 ';
                        ?>
                        <td valign="top" align='right' bgcolor="<?=$bg?>"><div id="angsuran"><?php if (isset($angsuran) && !empty($angsuran)) echo $angsuran[$i] ?></div></td>
                        <? } ?>

                    </tr>
                    <tr>
                    	<td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                    <td></td>
					<? for ($i=1;$i<=4;$i++){ ?>
                        <td valign="top" align="center">
                        <a id="applys" class="tombol ui-corner-all fancybox fancybox.iframe" href="apply.php?vtenor=<? echo $i;?>&amp;votr=<? echo $val_otr;?>&amp;vangsuran=<? echo $angsuran[$i];?>&amp;vuangmuka=<? echo $uang_muka[$i];?>">Apply</a>
                        </td>
                        <? } ?>
                    </tr>
                    </table>

                    </div>
                    <? }?>
                <!--results-->
                    </div>
                    <div>
                        <ul>
                            <li>OTR DKI Jakarta</li>
                            <li>Rincian kredit diatas tidak mengikat dan dapat berubah sewaktu-waktu.</li>
                            <li>Harga diatas sudah termasuk asuransi all risk & ACP (ACC Credit Protection)</li>
                        </ul>
                      </div>
              <!--end content-->
          </div>
          <!--end main content-->

        </div>
        <!--footer-->
  		<div class="row">
        <div class="col-md-1"></div>
          <div class="col-md-11">
                <div id="footer"><img src="../img/footer.jpg" alt="Footer" border="0" usemap="#Map" class="img-responsive" />
                    <map name="Map">
                      <area shape="rect" coords="6,10,188,61" href="http://www.acc.co.id" alt="ACC Website" target="_blank">
                      <area shape="rect" coords="203,9,379,61" href="https://www.facebook.com/acckreditmobil" alt="Facebook ACC" target="_blank">
                      <area shape="rect" coords="403,10,579,61" href="https://www.twitter.com/ACCKreditMobil" alt="Twitter ACC" target="_blank">
                    </map>
            	</div>
             </div>
         </div>
         <!--end footer-->
	</div>


    <script src="../js/bootstrap.min.js"></script>


</body></html>
