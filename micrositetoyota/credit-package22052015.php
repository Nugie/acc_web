<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ACC - Credit Package</title>
<meta name="keywords" content="Kredit Mobil, Simulasi Kredit">
<meta name="description" content="Kredit Mobil Terbaik Dari ACC" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container" style="margin-top:20px;">
    <div class="row">
 		  <div class="col-md-1"></div>	
          <!--main content-->
          <div class="col-md-8">
            <div class="headertitle">PAKET KREDIT ACC</div>
            <div class="logo"><img src="img/logo-acc.jpg" alt="Logo ACC" /></div>
              <div class="row">
              	<div class="col-md-12 col-md-offset-1 fontbold">Berikut ini paket kredit Mobil TOYOTA yang dapat Anda pilih  : <br /><br />
                	<div class="row" style="margin-bottom:20px;">
    					<div class="col-md-6"><a href="files/Paket_Kredit_Toyota_Avanza-Mei_2015.pdf" target="_blank" title="Paket Kredit Avanza"><img src="img/paket_kredit_1.jpg" alt="" /></a></div>            
                        <div class="col-md-6"><a href="files/Paket_Kredit_Toyota_Vios_Altis-Mei_2015.pdf" target="_blank"><img src="img/paket_kredit_2.jpg" alt="Paket Kredit Altis &amp; Vios" title="Paket Kredit Altis &amp; Vios" /></a></div>
	               	</div>
                    <div class="row" style="margin-bottom:20px;">
    					<div class="col-md-6"><a href="files/Paket_Kredit_Toyota_Yaris_2015.pdf" target="_blank" title="Paket Kredit Yaris"><img src="img/paket_kredit_3.jpg" alt="Paket Kredit Yaris" /></a></div>            
                        <div class="col-md-6"><a href="files/Paket_Kredit_Toyota_Rush_Fortuner-Mei_2015.pdf" target="_blank"><img src="img/paket_kredit_4.jpg" alt="Paket Kredit Rush &amp; Fortuner" title="Paket Kredit Rush &amp; Fortuner" /></a></div>
	               	</div>
                    <div class="row">
    					<!--<div class="col-md-6"><img src="img/paket_kredit_5.jpg" alt="" /></div> -->
                        <div class="col-md-6"><a href="files/Paket_Kredit_Toyota_Innova-Mei_2015.pdf" target="_blank"><img src="img/paket_kredit_6.jpg" alt="Paket Kredit Innova" title="Paket Kredit Innova" /></a></div>
	               	</div>
                </div>
              </div>
          </div>
          <!--end main content-->
          <!--right content-->
          <div class="col-md-3">
          	
          </div>
          <!--end right-->
        </div>
        <!--footer-->
  		<div class="row">
        <div class="col-md-1"></div>	
          <div class="col-md-11">
                <div id="footer"><img src="img/footer.jpg" alt="Footer" border="0" usemap="#Map" class="img-responsive" />
                    <map name="Map">
                      <area shape="rect" coords="6,10,188,61" href="http://www.acc.co.id" alt="ACC Website" target="_blank">
                      <area shape="rect" coords="203,9,379,61" href="https://www.facebook.com/acckreditmobil" alt="Facebook ACC" target="_blank">
                      <area shape="rect" coords="403,10,579,61" href="https://www.twitter.com/ACCKreditMobil" alt="Twitter ACC" target="_blank">
                    </map>
            	</div>
             </div>
         </div>
         <!--end footer-->
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>