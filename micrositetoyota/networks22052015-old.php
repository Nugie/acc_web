<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ACC - Networks</title>
		<meta name="keywords" content="Kredit Mobil, Simulasi Kredit">
        <meta name="description" content="Kredit Mobil Terbaik Dari ACC" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="credit-simulation/jquery.selectbox.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container" style="margin-top:20px;">
    <div class="row">
 		  <div class="col-md-1"></div>	
          <!--main content-->
          <div class="col-md-8">
            <div class="headertitle">ACC NETWORKS</div>
            <div class="logo"><img src="img/logo-acc.jpg" alt="Logo ACC" /></div>
              <div class="fontbold">Berikut ini jaringan ACC diseluruh Indonesia :</div>
              <div><em>Pilih area untuk menampilkan alamat Kantor Pelayanan ACC</em></div>
              <div id="networkselect" class="visible-xs">
              	<select name="netsel" id="netsel">
                	<option value="">-- Pilih Area --</option>
                	<option value="aceh">Aceh</option>
                    <option value="bali-nusra">Bali Nusra</option>
                    <option value="batam">Batam</option>
                    <option value="bengkulu">Bengkulu</option>
                    <option value="dki-jakarta">DKI Jakarta</option>
                    <option value="gorontalo">Gorontalo</option>
                    <option value="jambi">Jambi</option>
                    <option value="jawa-barat">Jawa Barat</option>
                    <option value="jawa-tengah">Jawa Tengah</option>
                    <option value="jawa-timur">Jawa Timur</option>
                    <option value="kalimantan-barat">Kalimantan Barat</option>
                    <option value="kalimantan-selatan">Kalimantan Selatan</option>
                    <option value="kalimantan-tengah">Kalimantan Tengah</option>
                    <option value="kalimantan-timur">Kalimantan Timur</option>
                    <option value="lampung">Lampung</option>
                    <option value="pangkal-pinang">Pangkal Pinang</option>
                    <option value="riau">Riau</option>
                    <option value="sulawesi-selatan">Sulawesi Selatan</option>
                    <option value="sulawesi-tengah">Sulawesi Tengah</option>
                    <option value="sulawesi-tenggara">Sulawesi Tenggara</option>
                    <option value="sulawesi-utara">Sulawesi Utara</option>
                    <option value="sumatera-barat">Sumatera Barat</option>
                    <option value="sumatera-selatan">Sumatera Selatan</option>
                    <option value="sumatera-utara">Sumatera Utara</option>

                </select>
              </div>
              <div id="network-map" class="hidden-xs">
                    <img src="img/img-networks.jpg" alt="Peta Indonesia" border="0" width="700" height="332" class="image" />
                    <a href="#" title="Pangkal Pinang" class="maps" id="pangkal-pinang"><span class="pointer" style="left: 154px; top: 147px;"></span></a>
                    <a href="#" title="Batam" class="maps" id="batam"><span class="pointer" style="left: 129px; top: 106px;"></span></a>
                    <a href="#" title="Sulawesi Tenggara" class="maps" id="sulawesi-tenggara"><span class="pointer" style="left: 378px; top: 176px;"></span></a>
                    <a href="#" title="Sulawesi Selatan" class="maps" id="sulawesi-selatan"><span class="pointer" style="left: 346px; top: 189px;"></span></a>
                    <a href="#" title="Sulawesi Tengah" class="maps" id="sulawesi-tengah"><span class="pointer" style="left: 361px; top: 148px;"></span></a>
                    <a href="#" title="Gorontalo" class="maps" id="gorontalo"><span class="pointer" style="left: 384px; top: 106px;"></span></a>
                    <a href="#" title="Sulawesi Utara" class="maps" id="sulawesi-utara"><span class="pointer" style="left: 414px; top: 95px;"></span></a>
                    <a href="#" title="Kalimantan Timur" class="maps" id="kalimantan-timur"><span class="pointer" style="left: 305px; top: 100px;"></span></a>
                    <a href="#" title="Kalimantan Selatan" class="maps" id="kalimantan-selatan"><span class="pointer" style="left: 290px; top: 156px;"></span></a>
                    <a href="#" title="Kalimantan Tengah" class="maps" id="kalimantan-tengah"><span class="pointer" style="left: 250px; top: 144px;"></span></a>
                    <a href="#" title="Kalimantan Barat" class="maps" id="kalimantan-barat"><span class="pointer" style="left: 210px; top: 117px;"></span></a>
                    <a href="#" title="Bali Nusra" class="maps" id="bali-nusra"><span class="pointer" style="left: 287px; top: 233px;"></span></a>
                    <a href="#" title="Lampung" class="maps" id="lampung"><span class="pointer" style="left: 136px; top: 184px;"></span></a>
                    <a href="#" title="Sumatera Selatan" class="maps" id="sumatera-selatan"><span class="pointer" style="left: 126px; top: 159px;"></span></a>
                    <a href="#" title="Bengkulu" class="maps" id="bengkulu"><span class="pointer" style="left: 101px; top: 166px;"></span></a>
                    <a href="#" title="Jambi" class="maps" id="jambi"><span class="pointer" style="left: 91px; top: 123px;"></span></a>
                    <a href="#" title="Riau" class="maps" id="riau"><span class="pointer" style="left: 92px; top: 99px;"></span></a>
                    <a href="#" title="Sumatera Barat" class="maps" id="sumatera-barat"><span class="pointer" style="left: 63px; top: 104px;"></span></a>
                    <a href="#" title="Sumatera Utara" class="maps" id="sumatera-utara"><span class="pointer" style="left: 42px; top: 64px;"></span></a>
                    <a href="#" title="Aceh" class="maps" id="aceh"><span class="pointer" style="left: 12px; top: 49px;"></span></a>
                    <a href="#" title="Jawa Timur" class="maps" id="jawa-timur"><span class="pointer" style="left: 230px; top: 209px;"></span></a>
                    <a href="#" title="Jawa Tengah" class="maps" id="jawa-tengah"><span class="pointer" style="left: 195px; top: 210px;"></span></a>
                    <a href="#" title="Jawa Barat" class="maps" id="jawa-barat"><span class="pointer" style="left: 170px; top: 207px;"></span></a>
                    <a href="#" title="DKI Jakarta" class="maps" id="dki-jakarta"><span class="pointer" style="left: 154px; top: 196px;"></span></a>
                    </div>
<!--acc branch-->
                  <div id="acc-branch">
                      <div id="maps-aceh" class="maps-content">
                     	<div class="line-header-blue2">Aceh</div>
                            <div>Ph. (0651) 34900<br />
                               Fax (0651) 33499<br />
                               Jl. T. Imum Leung Bata No.3/14<br />
                               Simpang Surabaya, Desa Lampseupeung</div>
                            <div>Banda Aceh - 23247</div>
                      </div>	
                      <div id="maps-sumatera-utara" class="maps-content">
                      		<div class="line-header-blue2">Medan 1</div>
                            <div>Ph. (061) 4159900 (H) ; 4575688 / 1847 / 48 (D)<br />
                               Fax (061) 4151160 ; 4154412 <br /> 
                               Jl. H. Adam Malik No.28<br />Glugur By Pass</div>
                            <div>Medan - 20114</div>
                            <br />
                            <div class="line-header-blue2">Medan 2</div>
                            <div>Ph. (061) 4552900 (H)<br />
                               Fax (061 ) 4513010</div>
                            <div>Jl. Ir H. Djuanda No.3 J</div>
                            <div>Medan - 20152</div>
                            <br />
                            <div class="line-header-blue2">Rantau Prapat</div>
                            <div>Ph. (0624) 22234<br />
                               Fax (0624) 25092<br />
                                Jl. M.H. Thamrin No.2 <br />Rantau Prapat</div>
                            <div>Sumatera Utara - 21412</div>
                            <br />
                            <div class="line-header-blue2">Medan Binjai</div>
                            <div>Jl. Gatot Subroto, Makro Business Center Blok A  No. 7-8</div>
                            <div>Medan - 20127</div>
                      </div>
                      <div id="maps-sumatera-barat" class="maps-content">
	                      <div class="line-header-blue2">Bukit Tinggi</div>
                        <div>Ph. (0752) 70001300 ; 35600 ; 48600<br />
                           Fax (0752) 34813<br />
                           Pertokoan Jambu Air<br />
                           Jl. Raya Jambu Air No. 112</div>
                        <div>Bukittinggi - 26181</div><br>

                        <div class="line-header-blue2">Padang</div>
                        <div>Ph. (0751) 41300 (H) - 51805 (D)<br />
                           Fax (0751) 41303 ; 41310<br />
                           Jl. Khatib Sulaiman No. 101</div>
                        <div>Padang  - 25173</div>

                      </div>
                      <div id="maps-riau" class="maps-content">
                      <div class="line-header-blue2">Pekanbaru</div>
                        <div>Ph. (0761) 32177 / 32877(H) - 23415  (D) <br />
                           Fax (0761) 32362<br />
                            Jl. Ahmad Yani No.152</div>
                        <div>Pekanbaru  - 28126</div>
                        <br />
                        <div class="line-header-blue2">Duri</div>
                        <div>Ph. (0765) 92810 ; 92813 ; 92842<br />
                           Fax (0765) 93120<br />
                          Jl. Hang Tuah No. 98B</div>
                        <div>Duri Barat</div>
                        <div>Riau - 28884</div>
                      </div>

					<div id="maps-jambi" class="maps-content">
                        <div class="line-header-blue2">Jambi</div>
                        <div>Ph. (0741) 667456 ; 64644 - 60444 (H)<br /> Fax (0741) 62177 <br /> Jl. Sumantri Brojonegoro No. 96</div>
                        <div>Jambi  - 36135</div>
                        <br>
                        <div class="line-header-blue2">Muara Bungo</div>
                        <div>Ph. (0747) 7324007 (H) ; 700699<br /> Fax. (0747) 7324006<br />Jl. M. Yamin<br />Komplek Ruko Wiltop Blok C No. 18
                        </div>
                        <div>Muara Bungo - 37212</div> 
                    </div>	
					<div id="maps-batam" class="maps-content">
                    	<div class="line-header-blue2">Batam</div>
                        <div>Ph. (0778) 7486625<br /> 
                          Fax (0778) 7486620<br />
                           Komplek Seruni Indah Blok I-2 No. 8, Batam Center</div>
                        <div>Batam - 29432</div>
                    </div>
					<div id="maps-bengkulu" class="maps-content">
	                    <div class="line-header-blue2">Bengkulu</div>
                        <div>Ph. (0736) 22083 ; 28808<br />
                           Fax (0736) 25152<br />
                           Jl. Soewondo Parman No. 54B</div>
                        <div>Bengkulu &ndash; 38224</div>
                    </div>
					<div id="maps-sumatera-selatan" class="maps-content">
                    <div class="line-header-blue2">Palembang</div>
                        <div>Ph. (0711) 368368 (H) - 313664(D)<br />
                           Fax (0711) 364018<br /> 
                           Jl. Veteran No.195-197B</div>
                        <div>Palembang  - 30126</div>
                        <br />
                        <div class="line-header-blue2">Palembang Plaju</div>
                        <div>Ph. (0711) 517070<br />
                           Fax (0711) 517068<br /> 
                           Jl. Ahmad Yani No. 10 & 11, RT 02, RW 01 Kel. 9/10 Ulu, Kec. Seberang Ulu 1</div>
                        <div>Palembang - 30251</div>
                        <br />
                        <div class="line-header-blue2">Bandar Jaya</div>
                        <div>Ph. (0725) 25110<br />
                           Fax (0725) 25118<br /> 
                           Jl. Proklamator Raya Desa Seputih Jaya Blok B5-B6, Bandar Jaya</div>
                        <div>Lampung - 34163</div>
                    </div>
					<div id="maps-pangkal-pinang" class="maps-content">
                    	<div class="line-header-blue2">Pangkal Pinang</div>
                        <div>Ph. (0717) 433195<br />Fax (0717) 435060<br />
                                 Jl. Mentok/Depati Hamza Komp. Ruko Yy Tower No. 7<br />
                                   Pintu Air</div>
                                <div>Pangkal Pinang - 33133</div>
                    </div>
                    <div id="maps-lampung" class="maps-content">
                        <div class="line-header-blue2">Lampung</div>
                        <div>Ph.(0721) 262 569 / 259999 (H) / 463 (D) / 252305<br /> Fax (0721) 262 495<br />Jl. Jenderal Sudirman No. 6</div>
                        <div>Bandar lampung &ndash; 35118</div>
                    </div>
                    <div id="maps-dki-jakarta" class="maps-content">
                    <div class="line-header-blue2">Bekasi</div>
                            <div>Ph. (021) 888 63600<br /> Fax. (021) 888 63680 / 888 63681<br />Ruko Suncity Square Blok A 5-7<br />
                            Jl.Mayor Hasibuan - Margajaya</div>
                            <div>Bekasi Barat - 17141</div>
                            <br>
                            
                            <div class="line-header-blue2">Bintaro</div>
                            <div>Ph. (021) 745 8999<br /> Fax. (021) 7453077<br />
                            Komplek Bintaro Trade Center (BTC)<br />Jl Jend Sudirman Blok A1 No.3 &amp; 3A</div>
                            <div>Bintaro, Tangerang - 15224</div>
                            <br>
                            
                            <div class="line-header-blue2">Cibubur</div>
                            <div>Ph. (021) 84310120<br /> Fax. (021) 84310115<br />
                              Jl. Alternatif Cibubur No. 39F Rt.001/010<br>
                              Jati Sampurna</div>
                            <div>Bekasi - 17435</div>
                            <br>
                            
                            <div class="line-header-blue2">Cideng</div>
                            <div>Ph. (021) 3523969<br /> Fax. (021) 3516937<br />JL. Cideng Timur No. 67 C<br>Petojo Selatan</div>
                            <div>Jakarta - 10160</div>
                            <br>
                            
                            <div class="line-header-blue2">Cikarang</div>
                            <div>Ph. (021) 899903009<br /> Fax. (021) 899903031</div>
                            <div>Ruko Robson Blok B No. 6<br>Cikarang Selatan</div>
                            <div>Bekasi - 17550</div>
                            <br>
                            
                            <div class="line-header-blue2">Depok</div>
                            <div>Ph. (021) 77205888<br /> 
                              Fax (021) 77215630</div>
                            <div>Komplek Ruko Mall Depok, Blok B1 No. 52-53<br>Kemiri Muka, Beiji</div>
                            <div>Depok -  16423</div>
                            <br>
                            
                            <div class="line-header-blue2">Fatmawati</div>
                            <div>Ph. (021) 7507766 (H) ; 7509000 (H)<br />
                               Fax (021) 7506246;  7658770-2<br />Jl. R.S.Fatmawati No.9</div>
                            <div>Jakarta - 12420</div>
                            <br>
                            
                            <div class="line-header-blue2">Kalimalang</div>
                            <div>Ph. (021) 86906990<br />
                               Fax (021) 86906991<br />Jl. K.H. Noer Ali Kalimalang No. 1E</div>
                            <div>Kalimalang, Jakarta Timur - 13450</div>
                            <br>
                            
                            
                            <div class="line-header-blue2">Karawaci</div>
                            <div>Ph. (021) 29025997<br /> 
                              Fax (021) 29025996</div>
                            <div>Komplek Ruko Victoria Park Blok A2 No. 68<br />Jl. Imam Bonjol, Karawaci</div>
                            <div>Tangerang - 15115</div>
                            <br>
                            
                            <div class="line-header-blue2">Kebon Jeruk</div>
                            <div>Ph. (021)  5325919<br /> Fax (021) 5325920<br />Rukan Graha Kencana Blok A<br />Jl.Raya Perjuangan No.88 Kebon Jeruk</div>
                            <div>Jakarta - 11530</div>
                            <br>
                            
                            <div class="line-header-blue2">Kemayoran</div>
                            <div>Ph. (021) 6543014<br /> Fax (021)  6542672<br />
                              Jl. Bursa Mobil Kemayoran Blok P No. 18-19<br>
                              Kemayoran</div>
                            <div>Jakarta - 10620</div>
                            <br>
                            
                            <div class="line-header-blue2">Kelapa Gading</div>
                            <div>Ph. (021) 450 9000 (H)<br /> Fax (021) 450 7466, 4534172, 4510707<br />
                            Jl. Raya Barat Boulevard Blok XB/7<br />Kelapa Gading</div>
                            <div>Jakarta Utara -14240</div>
                            <br>
                            
                            <div class="line-header-blue2">Kwitang</div>
                            <div>Ph. (021) 391 9000 (H)<br /> Fax (021) 3100400, 3906028<br />Jl. Kwitang Raya No.10</div>
                            <div>Jakarta - 10420</div>
                            <br>
                            
                            <div class="line-header-blue2">Mangga Dua</div>
                            <div>Ph. (021) 30012320<br /> Fax (021) 30012310<br />
                              Gedung WTC Mangga Dua<br>
                                Jl. Mangga Dua Raya Kav. 8, Lt 3A</div>
                            <div>Jakarta Utara - 14420</div>
                            <br>
                            <!--
                            <div class="line-header-blue2">Palem Semi</div>
                            <div>Ph. (021) 28912121/ 2323<br /> Fax (021) 5585413<br />SERBA OTO BLOK C5 NO. 6<br>JL. IMAM BONJOL KARAWACI</div>
                            <div>TANGERANG 15166</div>
                            <br>-->
                            
                            <div class="line-header-blue2">Pancoran</div>
                            <div>Ph. (021) 7998041<br /> Fax (021) 7998042<br />
                              Komp. Ruko Graha Permata Pancoran Blok A5 &amp; A6<br>
                              Jl. Raya Pasar Minggu Kav. 32<br>
                              Triloka Pancoran</div>
                            <br>
                            
                            <div class="line-header-blue2">Pluit</div>
                            <div>Ph. (021) 66602852<br>
                              Fax (021) 6601140</div>
                            <div>Jl. Pluit Karang Utara No. 129 A-B Blok A3 Utara Kav No.7</div>
                            <div>Jakarta Utara - 14450</div>
                            <br>
                            
                            <div class="line-header-blue2">Serang</div>
                            <div>Ph. ( 0254 ) 201887<br /> Fax ( 0254 )  201864<br />Komplek Titan Arum Blok K<br />
                            Jl. Raya Serang Cilegon</div>
                            <div>Serang - Banten 42162</div>
                            <br>
                            
                            <div class="line-header-blue2">Tangerang</div>
                            <div>Ph. (021) 5398900<br />
                               Fax (021)  53125175<br />
                              Jl. Raya Serpong No.90</div>
                            <div>Tangerang Selatan - 15311</div>
                            <br>
                            
                            
                            <div class="line-header-blue2">TB SIMATUPANG</div>
                            <div class="line-header-gray2">[Head Office &amp; C2C]</div>
                            <div>Ph. (021) 78859000<br /> 
                            Fax. (021) 78851182<br /> Jl. TB Simatupang No. 90</div>
                            <div>Jakarta Selatan - 12530</div>
                    </div>
					<div id="maps-jawa-barat" class="maps-content">
                   		 <div class="line-header-blue2">Bandung I</div>
                        <div>Ph. (022) 4241552 (H) ; 4209320-1 (D)<br /> 
                          Fax (022) 4209318 / 4208956<br />
                          Jl. Naripan No. 24-26</div>
                        <div>Bandung - 40112</div>
                        <br>
                        
                        <div class="line-header-blue2">Bandung II</div>
                        <div>Ph. (022) 88889900 (H)<br /> Fax (022) 88889800<br />Gedung Rotunda A3-A4<br>Jl. Soekarno Hatta No. 438D</div>
                        <div>Bandung - 40254</div>
                        <br>
                        
                        <div class="line-header-blue2">Bandung III - Cibiru</div>
                        <div>Ph. (022) 87836033<br /> Fax (022) 88889800<br />Jl. Soekarno Hatta Km. 15 No. 885 Kav. A5-07</div>
                        <div>Bandung - 40254</div>
                        <br>
                        
                        <div class="line-header-blue2">Cibinong</div>
                        <div>Ph. (021) 29230999<br /> Fax (021) 29230656<br />Komplek Ruko Cibinong City Center Blok B-18</div>
                        <div>Jl. Raya Pemda, Cibinong</div>
                        <div>Bogor - 16915</div>
                        <br>
                        <div class="line-header-blue2">Bogor</div>
                        <div>Ph. (0251)  8379000  ( H )<br /> Fax (0251)  8313837<br />Jl. Raya Pajajaran No. 24</div>
                        <div>Bogor  - 16151</div>
                        <br>
                        
                        <div class="line-header-blue2">Karawang</div>
                        <div>Ph. (0267) 8452929<br />Fax (0267) 8453177<br />Jl. Kertabumi No. 89</div>
                        <div>Karawang - 41313</div>
                        <br>
                        
                        <div class="line-header-blue2">Cirebon</div>
                        <div>Ph. (0231) 4899000 (H)<br /> Fax (0231) 489908 ; 487054<br />Jl. Brigjen Dharsono 84</div>
                        <div>Cirebon - 45153</div>
                        <br>
                        
                        <div class="line-header-blue2">Sukabumi</div>
                        <div>Ph. (0266) 232999<br /> Fax (0266) 222006<br />Jl. Jend. Sudirman No. 77A</div>
                        <div>Sukabumi - 43123</div>
                        <br>
                        
                        <div class="line-header-blue2">Tasikmalaya</div>
                        <div>Ph. (0265) 313000 : 312666 : 325254<br /> Fax (0265) 328500<br />Ruko Permata Regency No. 1<br />Jl. KH. Z. Mustofa No. 283</div>
                        <div>Tasikmalaya - 46181</div>
                    </div>
					<div id="maps-jawa-tengah" class="maps-content">
                    		<div class="line-header-blue2">Kudus</div>
                        <div>Ph. (0291) 4248900<br /> Fax (0291) 4248899<br />Ruko Jati Kulon<br />Jl. Agil Kusumadya No.32 Blok 7</div>
                        <div>Kudus - 59347</div>
                        <br>
                        <div class="line-header-blue2">Magelang</div>
                        <div>Ph. (0293) 326999 <br /> Fax (0293) 326895<br />Ruko Metro Square Blok F10<br />JL. Bambang Soegeng - Mertoyudan</div>
                        <div>Magelang - 56172</div>
                        <br>
                        
                        <div class="line-header-blue2">Purwokerto</div>
                        <div>Ph. (0281) 6353 89 ; 6393 78 ; 6375 23  <br /> 
                          Fax (0281) 6345 98<br />Jl. Jend. Sudirman No.69</div>
                        <div>Purwokerto - 53133</div>
                        <br>
                        
                        <div class="line-header-blue2">Semarang</div>
                        <div>Ph. (024) 8319000 (H)<br /> Fax ( 024 )  8411910, 8318815<br />Jl. M.H Thamrin No.150</div>
                        <div>Semarang - 50134</div>
                        <br>
                        
                        <div class="line-header-blue2">Surakarta</div>
                        <div>Ph. (0271) 715938 ;  724222<br /> 
                          Fax (0271) 713 190<br />Jl. Bhayangkara No.47</div>
                        <div>Surakarta - 57149</div>
                        <br>
                        
                        <div class="line-header-blue2">Tegal</div>
                        <div>Ph. (0283) 342800 <br /> Fax (0283) 351298<br />Jl. Kolonel Sugiono No. 124</div>
                        <div>Tegal - 52114</div>
                        <br>
                        
                        <div class="line-header-blue2">Yogyakarta</div>
                        <div>Ph. (0274) 620069 (H) ; 625100 ;  625118<br /> 
                          Fax (0274) 625123<br />Jl. H.O.S Cokroaminoto No. 161 A<br />
                        </div>
                        <div>Yogyakarta  - 55244</div>
                    </div>
					<div id="maps-jawa-timur" class="maps-content">
                   		 <div class="line-header-blue2">Gresik</div>
                        <div>Ph. (031) 3970900<br />
                           Fax (031) 3970400<br />
                           Jl. R.A. Kartini 220</div>
                        <div>Gresik &ndash; 61111</div>
                        <br />
                        <div class="line-header-blue2">Jember</div>
                        <div>Ph. (0331) 339000 ; 423642<br /> 
                          Fax (0331) 485 841<br />Komplek Ruko Gajah Mada Square Blok A 20<br />Jl. Gajah Mada No. 187</div>
                        <div>Jember - 68133</div>
                        <br>
                        <div class="line-header-blue2">Kediri</div>
                        <div>Ph. (0354) 689000 (H) ; 684946 ; 686389<br /> 
                          Fax (0354) 680 578<br />Jl. Let. Jend. Suparman  No. 73</div>
                        <div>Kediri - 64132</div>
                        <br>
                        <div class="line-header-blue2">Malang</div>
                        <div>Ph. (0341) 364 996 (H)<br />
                           Fax (0341) 360981 ; 343882<br />
                           Jl. Jaksa Agung Suprapto No.28</div>
                        <div>Malang - 65111</div>
                        <br>
                        
                        <div class="line-header-blue2">Surabaya I</div>
                        <div>Ph. (031)  535 3400 / 5329000(H) <br /> Fax (031) 5464527 ; 5461462 ; 5329865<br />Jl. Panglima Sudirman 24-30</div>
                        <div>Surabaya - 60271</div>
                        <br>
                        <div class="line-header-blue2">Surabaya II</div>
                        <div>Ph. (031) 99005200<br />
                           Fax (031) 99005222<br /></div>
                        <div>Komplek Ruko Icon 21 Blok S No. 5 & 6<br>
                        Jl. Dr. Ir. H. Soekarno</div>
                        <div>Surabaya - 61254</div>
                        <br>
                        <div class="line-header-blue2">Surabaya III</div>
                        <div>Ph.(031) 8533900<br /> Fax(031) 8534900<br /></div>
                        <div>Jl. Raya Waru, Ruko Gateway A 18-19</div>
                        <div>Surabaya - 61254</div>

                    </div>
                    <div id="maps-bali-nusra" class="maps-content">
                    	<div class="line-header-blue2">Denpasar I</div>
                        <div>Ph. (0361) 419000 (H)<br /> Fax (0361) 418800, 437000<br />Jl. Gatot Subroto Barat No. 99</div>
                        <div>Denpasar - 80117</div>
                        <br>
                        <div class="line-header-blue2">Denpasar Renon</div>
                        <div>Ph. (0361) 8497600<br /> 
                          Fax (0361) 8497700</div>
                        <div>Komplek Ruko Griya Alamanda Kav. 15<br />Jl. Cok Agung Tresna, Renon</div>
                        <div>Denpasar - 80235</div>
                        <br>
                        <div class="line-header-blue2">Mataram</div>
                        <div>Ph. (0370) 648000<br /> Fax (0370) 621728<br />Jl. AA Gde Ngurah No.87B-C</div>
                        <div>Cakra Negara - Mataram - 83231</div>
                    </div>
                    <div id="maps-kalimantan-barat" class="maps-content">
                  	  <div class="line-header-blue2">Pontianak</div>
                        <div>Ph. (0561) 769000<br />
                           Fax (0561) 575375<br />
                          Komplek Ruko Perdana Square Blok C12<br />
                          Jl. Perdana Pontianak - 78121 (Kantor Sementara)</div>
                        <div>Pontianak - 78121</div>
                    </div>
					<div id="maps-kalimantan-tengah" class="maps-content">
                        <div class="line-header-blue2">Palangkaraya</div>
                        <div>Ph. (0536) 3239955<br /> Fax (0536) 3238982<br />
                        Jl. Tjilik Riwut Km 6<br /> 
                        Palangka
                        </div>
                        <div>Palangkaraya - 73112</div>                    
                    </div>
                    <div id="maps-kalimantan-selatan" class="maps-content">
                        <div class="line-header-blue2">Banjarmasin</div>
                        <div>Ph. (0511) 3259000 (H) ; 3253633 RO ; (0511) 3260026 (H) Fleet<br /> Fax. (0511) 3250214 (RO); (0511) 3253399 (Fleet)
                        <br /> Jl. Jend. A. Yani No.438 B - C, Km 5</div>
                        <div>Banjarmasin - 70349</div>
                    
                    </div>
                     <div id="maps-kalimantan-timur" class="maps-content">
                             <div class="line-header-blue2">Samarinda</div>
                            <div>Ph. (0541) 765735-36 ; 765732-33 ; 765900 (H)<br /> Fax (0541 ) 765734 ; 765733<br />Jl. Ir. H. Juanda No. 39</div>
                            <div>Samarinda - 75123</div>
                            <br />
                            <div class="line-header-blue2">Balikpapan</div>
                            <div>Ph. (0542) 7206890<br /> Fax (0542) 7206891<br />Jl. MT. Haryono No. 99</div>
                            <div>Balikpapan - 76114</div>
                    
                     </div>
                     <div id="maps-sulawesi-selatan" class="maps-content">
                        <div class="line-header-blue2">Makasar</div>
                        <div>Ph. (0411) 8309000 <br /> Fax (0411) 833050<br />Jl. Jend. Sudirman No. 68</div>
                        <div>Makasar - 90113</div>
                        <br>
                        <div class="line-header-blue2">Pare Pare</div>
                        <div>Ph. (0421) 25800<br /> Fax (0421) 28765<br />Jl. Bau Massepe No. 39 Pare Pare</div>
                        <div>Sulawesi Selatan - 91121</div>                    
                     </div>
                     <div id="maps-sulawesi-tenggara" class="maps-content">
                        <div class="line-header-blue2">Kendari</div>
                        <div>Ph. (0401) 3190299 (H)<br /> Fax (0401) 3196296<br /> Jl. Jend. Achmad Yani No. 184, Kendari</div>
                        <div>Sulawesi Tenggara - 93117</div>
                     </div>
                     <div id="maps-sulawesi-tengah" class="maps-content">
                        <div class="line-header-blue2">Palu</div>
                        <div>Ph. (0451) 458900<br /> Fax (0451)  485909<br /> Jl. Basuki Rahmat No. 62-63 Palu</div>
                        <div>Sulawesi Tengah - 94113</div>
                     </div>
                     <div id="maps-gorontalo" class="maps-content">
                        <div class="line-header-blue2">Gorontalo</div>
                        <div>Ph. (0435) 830905<br /> Fax (0435) 824455<br />Jl. Haji Agus Salim No. 436 B</div>
                        <div>Kota Tengah - Gorontalo - 96126</div>                    
                     </div>
                     <div id="maps-sulawesi-utara" class="maps-content">
                     	<div class="line-header-blue2">Manado</div>
                        <div>Ph. (0431) 876000 (H) ; 8880234<br /> Fax (0431) 874000<br /> Jl. Bethesda No. 34</div>
                        <div>Manado - 95116</div>
                     </div>
		       </div>
              <!--end acc branch-->
          </div>
          <!--end main content-->
          <!--right content-->
          <div class="col-md-3">
          	
          </div>
          <!--end right-->
        </div>
        <!--footer-->
  		<div class="row">
        <div class="col-md-1"></div>	
          <div class="col-md-11">
                <div id="footer"><img src="img/footer.jpg" alt="Footer" border="0" usemap="#Map" class="img-responsive" />
                    <map name="Map">
                      <area shape="rect" coords="6,10,188,61" href="http://www.acc.co.id" alt="ACC Website" target="_blank">
                      <area shape="rect" coords="203,9,379,61" href="https://www.facebook.com/acckreditmobil" alt="Facebook ACC" target="_blank">
                      <area shape="rect" coords="403,10,579,61" href="https://www.twitter.com/ACCKreditMobil" alt="Twitter ACC" target="_blank">
                    </map>
            	</div>
             </div>
         </div>
         <!--end footer-->
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="credit-simulation/js/jquery.selectbox-0.2.min.js"></script>
     <script type="text/javascript">
		$(document).ready(function() {
			$('.maps').click(function() {
			var themaps = $( this ).attr( "id" );
			$('.maps-content').hide();
			$('#maps-'+themaps).fadeIn("slow");
			return false;
			});
			
			$("#netsel").selectbox({
			width:300,
			onChange: function (val, inst) {
				var themaps = $('#netsel').val();
				$('.maps-content').hide();
				$('#maps-'+themaps).fadeIn("slow");
				return false;
				
			}	
			});
		});
    </script>

  </body>
</html>