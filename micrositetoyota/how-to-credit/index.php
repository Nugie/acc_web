<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ACC - How To Credit</title>
	<meta name="keywords" content="Kredit Mobil, Simulasi Kredit">
    <meta name="description" content="Kredit Mobil Terbaik Dari ACC" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container" style="margin-top:20px;">
    <div class="row">
 		  <div class="col-md-1"></div>	
          <!--main content-->
          <div class="col-md-8">
            <div class="headertitle">HOW TO CREDIT</div>
            <div class="logo"><img src="img/logo-acc.jpg" alt="Logo ACC" /></div>
              <div>
              Bila Anda tertarik untuk mengajukan kredit mobil di ACC, silahkan mengikuti langkah-langkah di bawah ini :
				<ol>
                	<li>Pilih jenis mobil yang anda inginkan</li>
					<li>Lakukan perhitungan kredit menggunakan simulasi kredit ACC untuk mengetahui perkiraan angsuran</li>
					<li>Hubungi dealer TOYOTA rekanan ACC terdekat di kota Anda</li>
					<li>Penuhi persyaratan pengajuan kredit yang dibutuhkan dan miliki kendaraan idaman Anda sekarang juga!
                    <div class="fontbold fontblue" style="margin-top:10px;">Beberapa Persyaratan Umum untuk Mendapatkan Kredit Mobil antara lain :</div>
                    <img src="img/How_to_Credit_ACC1.jpg" alt="How To Credit" class="img-responsive" /><br />
                    </li>
                    <li>Bayarlah angsuran anda secara teratur setiap bulannya lewat channel-channel pembayaran kami.<br>
					  <img src="img/pdf.png" alt="" style="vertical-align:bottom" /> <a href="http://www.acc.co.id/files/Petunjuk_Pembayaran_ACC_2012.pdf">Download Petunjuk Pembayaran ACC</a>
					</li>
                </ol>
              </div>
          </div>
          <!--end main content-->
          <!--right content-->
          <div class="col-md-3">
          	
          </div>
          <!--end right-->
        </div>
        <!--footer-->
  		<div class="row">
        <div class="col-md-1"></div>	
          <div class="col-md-11">
                <div id="footer"><img src="img/footer.jpg" alt="Footer" border="0" usemap="#Map" class="img-responsive" />
                    <map name="Map">
                      <area shape="rect" coords="6,10,188,61" href="http://www.acc.co.id" alt="ACC Website" target="_blank">
                      <area shape="rect" coords="203,9,379,61" href="https://www.facebook.com/acckreditmobil" alt="Facebook ACC" target="_blank">
                      <area shape="rect" coords="403,10,579,61" href="https://www.twitter.com/ACCKreditMobil" alt="Twitter ACC" target="_blank">
                    </map>
            	</div>
             </div>
         </div>
         <!--end footer-->
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>