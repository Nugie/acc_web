<?php 
include 'simulation.php';
?>
<link rel="stylesheet" type="text/css" href="ui/ui.all.css" />
<link type="text/css" href="ui/demos.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="jquery.selectbox.css" />
<link rel="stylesheet" type="text/css" href="jquery-ui-1.9.1.custom.min.css" />
<script type="text/javascript" src="js/jquery-1.8.2.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.9.1.custom.min.js"></script>
<script type="text/javascript" src="js/cufon-yui.js" ></script>
<script type="text/javascript" src="js/Myriad_Pro_400-Myriad_Pro_700.font.js"></script>
<script type="text/javascript" src="js/jquery.selectbox-0.2.min.js"></script>


<script type="text/javascript">
Cufon.replace('.block_acc', { fontFamily: 'Myriad Pro' });
Cufon.replace('.company_title', { fontFamily: 'Myriad Pro' });


	$(function() {
		
		$("#loader").hide();
		$("#branch").selectbox();
		$("#dp_id").selectbox();
		$("#tenor_id").selectbox();
		$("#area").selectbox({
			onChange: function (val, inst) {
				$("#branch_loading").html('<img src="images/ajax-loader.gif" />');
				$.ajax({
					type: "GET",
					data: {area: val},
					url: "load_query.php",
					success: function (data) {
						$("#branch_loading").html(data);
						$("#branch").selectbox();
					}
				});
			},
			effect: "slide"
			});	
		$("#vehicle_model").selectbox({width:300});
		$("#vehicle_type").selectbox({
			onChange: function (val, inst) {
				$("#model_loading").html('<img src="images/ajax-loader.gif" />');
				$.ajax({
					type: "GET",
					data: {type_id: val},
					url: "load_query.php",
					success: function (data) {
						$("#model_loading").html(data);
						$("#vehicle_model").selectbox();
					}
				});
			},
			effect: "slide"
			});		
			
			
		var name = $("#name"),
			email = $("#email"),
			placebirth = $("#placebirth"),
			datebirth = $("#datebirth"),
			phone = $("#phone"),
			address = $("#address"),
			model = $("#model"),
			allFields = $([]).add(name).add(email).add(placebirth).add(datebirth).add(phone).add(address).add(model),
			tips = $("#validateTips");

		function updateTips(t) {
			tips.text(t).effect("highlight",{},1500);
		}

		function checkLength(o,n,min,max) {

			if ( o.val().length > max || o.val().length < min ) {
				o.addClass('ui-state-error');
				updateTips("Jumlah karakter kolom " + n + " harus diantara "+min+" dan "+max+" karakter.");
				return false;
			} else {
				return true;
			}

		}

		function checkRegexp(o,regexp,n) {

			if ( !( regexp.test( o.val() ) ) ) {
				o.addClass('ui-state-error');
				updateTips(n);
				return false;
			} else {
				return true;
			}

		}
				var currentyear=(new Date).getFullYear();
				maxyear = currentyear-10;
		$("#dialog").dialog({
			bgiframe: true,
			open: function(event, ui) {
				
				$("#datebirth" ).datepicker({ maxDate: maxyear, dateFormat:"d MM yy", changeMonth: true, changeYear: true,showButtonPanel: true,yearRange: "-100:-10"  });
				},
			autoOpen: false,
			width:300,
			height: 550,
			modal: true,
			buttons: {
				'Apply': function() {
					var bValid = true;
					allFields.removeClass('ui-state-error');

					bValid = bValid && checkLength(name,"Nama",3,30);
					bValid = bValid && checkRegexp(name,/^[a-zA-Z0-9\s]+$/,"Nama harus terdiri dari huruf dan angka");
					bValid = bValid && checkLength(email,"Email",6,80);
					bValid = bValid && checkRegexp(email,/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,"Masukkan alamat email yang benar, misalnya saya@gmail.com");
					bValid = bValid && checkLength(placebirth,"Tempat Lahir",4,50);
					bValid = bValid && checkRegexp(placebirth,/[a-zA-Z0-9\s]+$/i,"Tempat lahir hanya bisa diisi dengan huruf");
					bValid = bValid && checkLength(phone,"No. Telepon",5,15);
					bValid = bValid && checkRegexp(phone,/[0-9]+$/i,"No. Telepon hanya bisa diisi dengan angka");
					bValid = bValid && checkLength(address,"Alamat",5,70);
					bValid = bValid && checkRegexp(address,/[a-zA-Z0-9\s]+$/i,"Alamat hanya boleh diisi dengan huruf dan angka");
					
					// From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
					
					if (bValid) {
						$.post("sendapps.php", 
						{name: $('#name').val(),email: $('#email').val(),placebirth: $('#placebirth').val(),datebirth: $('#datebirth').val(),phone: $('#phone').val(),address: $('#address').val(),model: $('#model').val()},function(response){});
						//document.write("Terima kasih, aplikasi Anda akan segera kami proses..");
						$(this).dialog('close');
					}
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			},
			close: function() {
				allFields.val('').removeClass('ui-state-error');
			}
		});
		
		
		
		$('#apply').click(function() {
			$('#dialog').dialog('open');
		})
		.hover(
			function(){ 
				$(this).addClass("ui-state-hover"); 
			},
			function(){ 
				$(this).removeClass("ui-state-hover"); 
			}
		).mousedown(function(){
			$(this).addClass("ui-state-active"); 
		})
		.mouseup(function(){
				$(this).removeClass("ui-state-active");
		});

	});
	
</script>
<div id="wrapper">
	<div id="header-center">
    	<div class="blueblock gradien1">
					<div class="top_logo">
						<a href="http://www.astracreditcompanies.com/" target="_blank">
							<img src="images/logo.jpg" border="0" />
						</a>
					</div>
					<div class="company_title"><a href="http://www.astracreditcompanies.com/" class="putih" target="_blank">Astra Credit Companies</a>
					</div>
					<a href="http://www.astracreditcompanies.com/" target="_blank">
						<div class="block_acc">
							<div class="ic_acc">
							<b>ACC</b><br/>
							Credit Simulation</div>
						</div>
					</a>
              </div>
	</div>
	<div id="content">
	
		<form name="credit-simulations-form" id="credit-simulations-form" method="post" action="index.php" accept-charset="utf-8">
			<table width="100%" border="0" cellspacing="0" cellpadding="6">
				<tbody>
				<tr>
					<td valign="top" width="140">Area / Daerah</td>
					<td valign="top">:</td>
					<td valign="top"><?php $idArea = $_REQUEST['area'];?>
					<select name="area" id="area" class="styled-combo">
						<?
								$query_area = "SELECT area_id, description FROM cso_areas ORDER BY description";
								$area_list = database::getData($query_area);?>
							<option value="">Select Area</option>
						<?php foreach($area_list as $area){
								if($area['area_id'] == $idArea){
						?>
							<option value="<?= $area['area_id'] ?>" selected><?= $area['description'] ?></option>
							<?php } else { ?>
							<option value="<?= $area['area_id'] ?>" <? if($area['area_id'] == 1) {?>selected="selected"<? }?>><?= $area['description'] ?></option>
							<?php } } ?>
					</select>
					<td>
				</tr>
				<tr>
					<td valign="top" width="140">Cabang ACC terdekat</td>
					<td valign="top">:</td>
					<td valign="top"><?php $idBranch = $_REQUEST['branch'];
					?>
					<div id="branch_loading">
					<select name="branch" id="branch" class="styled-combo">
                    <option value="">Select Branch</option>
							<?
									if ($idArea=="") {
									$query_branch = "SELECT branch_id, description FROM cso_branches WHERE area_id = 1 ORDER BY description";
									}else {
									$query_branch = "SELECT branch_id, description FROM cso_branches WHERE area_id = ".$idArea." ORDER BY description";
									}
									$branch_list = database::getData($query_branch);?>
									
								<?php foreach($branch_list as $branch){
										if($branch['branch_id']==$idBranch){?>
											<option value="<?php echo $branch['branch_id']?>" selected><?= $branch['description']?></option>
										<?php } else {?>
											<option value="<?php echo $branch['branch_id']?>" <? if($branch['branch_id'] == 11&&$idBranch=="") {?>selected="selected"<? }?>><?= $branch['description']?></option>
										<?php } } ?>

					</select></div></td>
				</tr>
				<tr>
					<td valign="top" width="140">Model</td>
					<td valign="top">:</td>
					<td valign="top"><?php $idType = $_REQUEST['vehicle_type'];?>
					<select name="vehicle_type" id="vehicle_type" class="styled-combo">
                    <option value="">Select Model</option>
					<?php
						if(!empty($_REQUEST['variant_id'])){
							$id = $_REQUEST['variant_id'];
							$query_type = "SELECT a_variantid, model_id, a_variant, a_price FROM cso_auto2000 WHERE a_variantid = ".$id." limit 1";
							$type_list = database::getData($query_type);
							$price = $type_list[0]['a_price'];
							$modelnya = $type_list[0]['model_id'];
							$query_type1 = "SELECT * FROM cso_auto2000_model order by model_name";
							$type_list1 = database::getData($query_type1);
							foreach($type_list1 as $tipe1){
							?>
                            	<option value="<?php echo $tipe1['model_id']?>" <? if($tipe1['model_id'] == $modelnya) {?>selected="selected"<? }?>><?= $tipe1['model_name']?></option>
                             <? }?>
						<?	
						} else {
							$query_type = "SELECT * FROM cso_auto2000_model ORDER BY model_name";
							$type_list = database::getData($query_type);?>
								<?php foreach($type_list as $tipe){?>
                                <?php if($tipe['model_id']==$idType){?>
										<option value="<?php echo $tipe['model_id']?>" selected="selected"><?php echo $tipe['model_name']?></option>
									<?php } else{ ?>
										<option value="<?php echo $tipe['model_id']?>"><?php echo $tipe['model_name']?></option>
								<?php } } }?>
						
					</select></td>
				</tr>
				<tr>
					<td valign="top" width="140">Variant</td>
					<td valign="top">:</td>
					<td valign="top">
					<?php
					$idType = $_REQUEST['vehicle_type'];
					$idModel = $_REQUEST['vehicle_model'];
					?>
					<div id="model_loading">
                    
					<select name="vehicle_model" id="vehicle_model" class="styled-combo2">
					<?php
					if(!empty($_REQUEST['variant_id'])){
						$query_m = "SELECT a_variantid,a_variant FROM cso_auto2000 where model_id=$modelnya";
						$m_list = database::getData($query_m);
						foreach($m_list as $m){?>
							<option value="<?php echo $m['a_variantid']?>" <? if($m['a_variantid'] == $id) {?>selected="selected"<? }?>><? echo $m['a_variant']?></option>
							<?php }
					} else {
						$query_model = "SELECT a_variantid,a_variant FROM cso_auto2000 WHERE model_id ='". $idType ."' ";
						
						$model_list = database::getData($query_model);
						foreach($model_list as $model){
							if($model['a_variantid']==$idModel){?>
								<option value="<?php echo $model['a_variantid']?>" selected><?= $model['a_variant']?></option>
							<?php } else { ?>
								<option value="<?php echo $model['a_variantid']?>"><?= $model['a_variant']?></option>
					<?php } } }	?>
					</select></div>
					</td>
				</tr>
				<tr>
					<td valign="top" width="140">Menggunakan ACP</td>
					<td valign="top">:</td>
					<td valign="top"><?php $idStatus = $_REQUEST['acp_status'];?>
					<?php if(!empty($_REQUEST['variant_id'])){ ?>
					<ul id="acp_status">
						<li><input type="radio" name="acp_status" id="AcpStatusYes" value="Yes"  checked/> &nbsp;Ya &nbsp;&nbsp;&nbsp;</li>
						<li><input type="radio" name="acp_status" id="AcpStatusNo" value="No"  /> &nbsp;Tidak</li>
					</ul>
					<?php } else { 
					if($idStatus == "Yes"){?>
					<ul id="acp_status">
						<li><input type="radio" name="acp_status" id="AcpStatusYes" value="Yes"  checked/> &nbsp;Ya &nbsp;&nbsp;&nbsp;</li>
						<li><input type="radio" name="acp_status" id="AcpStatusNo" value="No"  /> &nbsp;Tidak</li>
					</ul>
					<?php } else { ?>
					<ul id="acp_status">					
						<li><input type="radio" name="acp_status" id="AcpStatusYes" value="Yes"/> &nbsp;Ya &nbsp;&nbsp;&nbsp;</li>
						<li><input type="radio" name="acp_status" id="AcpStatusNo" value="No" checked/> &nbsp;Tidak</li>
					</ul>
					<?php } }?>
					</td>
				</tr>
				<tr>
					<td valign="top" width="140">Harga Kendaraan</td>
					<td valign="top">:</td>
					<td valign="top"><?php $val_otr = $_REQUEST['otr'];?>
					<div id="otr_loading">
					<?php if(!empty($_REQUEST['variant_id'])){ ?>
						<input name="otrd" type="text" id="otrd" size="23" class="teksnya" value="<?php echo $price?>" disabled="disabled"/>
                        <input name="otr" type="hidden" id="otr" size="23" class="teksnya" value="<?php echo $price?>" />
					<?php } else { ?>
						<input name="otrd" type="text" id="otrd" size="23" class="teksnya" value="<?php echo $val_otr?>" disabled="disabled"/>
                        <input name="otr" type="hidden" id="otr" size="23" class="teksnya" value="<?php echo $val_otr?>" />
					<?php } ?></div><div id="loader"></div>
					</td>
				</tr>
				<tr>
					<td valign="top" width="140">Down Payment (DP)</td>
					<td valign="top">:</td>
					<td valign="top"><?php $dp = $_REQUEST['dp'];?>
					<select name="dp" id="dp_id" class="styled-combo">
						<?php if(!empty($_REQUEST['variant_id'])){ ?>
	                        <option value="0.25">25%</option>
							<option value="0.3">30%</option>
							<option value="0.4">40%</option>
							<option value="0.5">50%</option>
                            <option value="0.6">60%</option>
						<?php } else {
								if($dp == 0.25){?>
                                <option value="0.25" selected="selected">25%</option>
									<option value="0.3">30%</option>
									<option value="0.4">40%</option>
									<option value="0.5">50%</option>
                                     <option value="0.6">60%</option>
						<?php } elseif($dp == 0.3){ ?>
                        <option value="0.25">25%</option>
									<option value="0.3" selected>30%</option>
									<option value="0.4">40%</option>
									<option value="0.5">50%</option>
                                    <option value="0.6">60%</option>
                                    <?php } elseif($dp == 0.4){ ?>
                        			<option value="0.25">25%</option>
									<option value="0.3">30%</option>
									<option value="0.4" selected>40%</option>
									<option value="0.5">50%</option>
                                    <option value="0.6">60%</option>
                                     <?php } elseif($dp == 0.5){ ?>
                        			<option value="0.25">25%</option>
									<option value="0.3">30%</option>
									<option value="0.4">40%</option>
									<option value="0.5" selected="selected">50%</option>
                                    <option value="0.6">60%</option>
						<?php } else { ?>
                        <option value="0.25">25%</option>
									<option value="0.3">30%</option>
									<option value="0.4">40%</option>
									<option value="0.5">50%</option>
                                    <option value="0.6" selected="selected">60%</option>
						<?php } } ?> 
					</select>
					</td>
				</tr>
				<tr>
					<td valign="top" width="140">Term / Tenor</td>
					<td valign="top">:</td>
					<td valign="top"><?php $tenor = $_REQUEST['tenor'];?>
					<select name="tenor" id="tenor_id" class="styled-combo">
					<?php if(!empty($_REQUEST['variant_id'])){
						for($i=1;$i<=4;$i++){?>
							<option value="<?=$i?>"><?= $tenor_list[$i]?></option>
						<?php } 
						} else {
						for($i=1;$i<=4;$i++){
							if($i == $tenor){ ?>
								<option value="<?=$i?>" selected><?= $tenor_list[$i]?></option>
						<?php } else { ?>
								<option value="<?=$i?>"><?= $tenor_list[$i]?></option>
						<?php } } } ?>
					</select>
					</td>
				</tr>
		
			<tr>
				<td valign="top" width="140">Uang Muka</td>
				<td valign="top">:</td>
				<td valign="top"><strong><div id="uang-muka"></div></strong></td>
			</tr>
			<tr>
				<td valign="top" width="140">Angsuran</td>
				<td valign="top">:</td>
				<td valign="top"><strong><div id="angsuran"></div></strong></td>
			</tr>
			<tr>
				<td valign="top" width="140">&nbsp;</td>
				<td valign="top">&nbsp;</td>
				<td valign="top" style="padding-top:50px;">
				<ul id="button_calc">
					<li><a id="form-submit" class="tombol ui-corner-all" href="#" onclick= document.forms["credit-simulations-form"].submit();>Calculate</a></li>
					<li>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</li>
					<li><a id="apply" class="tombol ui-corner-all" href="#">Apply</a></li>
				</ul>
				</td>
			</tr>
	</tbody>
</table>
</form>
	  
    </div>
    <div id="term">
		<ul id="toc">
			<li>&nbsp;* Harga sudah termasuk asuransi All-Risk</li>
			<li>**&nbsp;Rincian kredit diatas tidak mengikat dan dapat berubah sewaktu-waktu. Untuk lebih lanjut, Anda dapat menghubungi cabang ACC terdekat.</li>
		</ul>
	  </div>
</div>
<?php 

if($_POST){
	$area = $_POST['area'];
	$branch = $_POST['branch'];
	$v_type = $_POST['vehicle_type'];
	$v_model = $_POST['vehicle_model'];
	$acp = $_POST['acp_status'];
	$otr = $_POST['otr'];
	$dp = $_POST['dp'];
	$tenor = $_POST['tenor'];
	
	$hit = hitung_simulasi($area,$branch,$v_type,$v_model,$acp,$otr,$dp,$tenor);
	if ($area==''){
	$uang_muka = "<span style=\"color:red;\">Mohon isi Area dan Cabang terlebih dahulu</span>";
	$angsuran = "";
	} else {
	$uang_muka = number_format($hit['uang_muka'],0,',','.');
	$angsuran = number_format($hit['angsuran'],0,',','.');
	}
	
}
 ?>
<script>
	//document.getElementById('vehicle_type').selectedIndex = '<?php echo $_POST['vehicle_type']?>';
	//document.getElementById('vehicle_model').innerHTML = '<?php echo $_POST['vehicle_model']?>';
	//document.getElementById('acp_status').innerHTML = '<?php echo $_POST['acp_status']?>';
	//document.getElementById('otr').innerHTML = '<?php echo $_POST['otr']?>';
	//document.getElementById('dp').selectedIndex = '<?php echo $_POST['dp']?>';
	//document.getElementById('tenor').selectedIndex = '<?php echo $_POST['tenor']?>';
	document.getElementById('uang-muka').innerHTML = '<?php echo $uang_muka?>';
	document.getElementById('angsuran').innerHTML = '<?php echo $angsuran?>';
</script>
<?
if(!empty($_REQUEST['variant_id'])){ 
	$idmodela = $_REQUEST['variant_id'];
} else {
	$idmodela = $idModel;
}
$query_model = "SELECT a_variant, b.model_name FROM cso_auto2000 a left join cso_auto2000_model b on a.model_id=b.model_id WHERE a.a_variantid ='". $idmodela ."' ";
	
	$model_list = database::getData($query_model);
	foreach($model_list as $model){
		$pilihan = $model["model_name"]." - ".$model["a_variant"];
	} 	
	?>

<div class="demo">

<div id="dialog" title="Apply">
	<p id="validateTips">All form fields are required.</p>

	<form action="sendapps.php" method="post">
	<fieldset>
		<label for="name">Name</label>
		<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" required="required" />
		<label for="email">Email</label>
		<input type="email" name="email" id="email" value="" class="text ui-widget-content ui-corner-all" required="required" />
		<label for="placebirth">Tempat, Tanggal Lahir</label>
		<input type="text" name="placebirth" id="placebirth" value="" class="text ui-widget-content ui-corner-all" required="required" placeholder="Tempat Lahir" /><input type="text" name="datebirth" id="datebirth" value="" placeholder="Tanggal Lahir" class="text ui-widget-content ui-corner-all" required="required" size="10" />
		<label for="phone">No. Telp</label>
		<input type="text" name="phone" id="phone" value="" class="text ui-widget-content ui-corner-all" required="required" />
		<label for="address">Alamat</label>
		<input type="text" name="address" id="address" value="" class="text ui-widget-content ui-corner-all" required="required" />
        <input type="hidden" name="model" id="model" value="<? echo $pilihan;?>" class="text ui-widget-content ui-corner-all" />
	</fieldset>
	</form>
</div>

</div>
</body></html>