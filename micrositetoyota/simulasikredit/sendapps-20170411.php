<? ini_set('display_errors', 0);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apply Credit</title>
<link type="text/css" href="ui/demos.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="jquery-ui-1.9.1.custom.min.css" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48042405-1', 'acc.co.id');
  ga('send', 'pageview');

</script>
</head>

<body class="bodydialog">
<div class="demo" style="margin:0;padding:0;">
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-dialog-buttons" tabindex="-1" role="dialog" aria-labelledby="ui-id-1" style="outline: 0px; z-index: 1002; height: auto; width: 325px; display: block;margin:0;padding:0;">
<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span id="ui-id-1" class="ui-dialog-title">Apply Credit</span><a href="javascript:void(0)" onclick="javascript:parent.jQuery.fancybox.close();" class="ui-dialog-titlebar-close ui-corner-all" role="button"><span class="ui-icon ui-icon-closethick">close</span></a></div>

<?php
include ('class/database.class.php');
include ('class/class.upload.php');
function safe($value){
   return mysql_real_escape_string($value);
}

if (isset($_POST)) {

   $name = safe($_POST['name']);
   $email = safe($_POST['email']);
   $placebirth = safe($_POST['placebirth']);
   $datebirth = safe($_POST['datebirth']);
   $phone = safe($_POST['phone']);
   $address = safe($_POST['address']);
   $branch = safe($_POST['dbranch']);
   $model = safe($_POST['model']);
   $votr = safe($_POST['votr']);
   $vtenor = safe($_POST['vtenor']);
   $vuangmuka = safe($_POST['vuangmuka']);
   $vangsuran = safe($_POST['vangsuran']);

//upload
	$handle = new Upload($_FILES['scanid']);
	$tanggals = date("d").date("m");
	if ($handle->uploaded) {
	   	$namafolder = date("M-Y");
		$namafile = "KTP-".$tanggals."-".$name;
		$handle->image_resize          = true;
		$handle->image_ratio_y	      = true;
		$handle->image_x               = 500;
		$handle->file_new_name_body = $namafile;
		$handle->image_convert = 'jpg';
		$handle->file_max_size = '1024000';
		$handle->mime_check = true;
		$handle->allowed = array('image/*');
		$handle->Process('uploads/'.$namafolder);

		if ($handle->processed) {
			$fname = $handle->file_dst_name;
		} else {
			echo '  Error: ' . $handle->error . '';
			echo "<div style=\"text-align:right;\"><button type=\"button\" onclick=\"javascript:window.location='apply.php';\" id=\"backbutton\">Back</button></div>";
			$fname = "error";
		}

		$handle-> Clean();

	} else {
		$fname = "";
	}
//end upload

	if (!empty($name)&&!empty($email)&&!empty($placebirth)&&!empty($datebirth)&&!empty($phone)&&!empty($address)&&!empty($model)&&$fname!='error') {

   $query_insert = "INSERT INTO `cso_applicants` (`id`,`name`,`email`,`place_birth`,`date_birth`, `phone`,`address`,`branch`,`vmodel`, `votr`, `vtenor`, `vuangmuka`, `vangsuran`, `files`, `regdate`) VALUES ('','$name','$email','$placebirth','$datebirth','$phone','$address','$branch', '$model', '$votr', '$vtenor', '$vuangmuka', '$vangsuran', '$fname', current_timestamp)";

   @mysql_query($query_insert);

   //send email

    $to = "";
    $cc = "";
    $query_recipients = "SELECT r_email FROM cso_recipients";
    $r_list = database::getData($query_recipients);
    foreach($r_list as $rcpt){
        $to .= $rcpt["r_email"].",";
     }
	 
	 	$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	 	$headers = 'From: ACC Website <info@acc.co.id>'."\r\n" .
 		 'Cc: '.$cc."\r\n" .
		'Reply-To: '.$email. "\r\n" .
		'X-Mailer: PHP/' . phpversion();
		$pesan = "Nama : $name \r\n";
		$pesan .= "Email : $email \r\n";
		$pesan .= "Tempat, Tanggal Lahir : $placebirth, $datebirth \r\n";
		$pesan .= "No. Telepon : $phone \r\n";
		$pesan .= "Alamat : $address \r\n";
		$pesan .= "Cabang Terdekat : $branch \r\n";
		$pesan .= "Jenis Mobil : $model \r\n";
		$pesan .= "Harga OTR : $votr \r\n";
		$pesan .= "Tenor : $vtenor Tahun\r\n";
		$pesan .= "Uang Muka : $vuangmuka \r\n";
		$pesan .= "Angsuran : $vangsuran \r\n";
		$pesan .= 'File KTP : http://www.acc.co.id/micrositetoyota/simulasikredit/uploads/'.$namafolder.'/'.$fname;
		mail($to,"[Toyota Credit Simulation] Customer Apply",$pesan,$headers);

		$pesannya = "Terima kasih, aplikasi kredit Anda akan segera kami proses. \r\n\r\n\r\n Astra Credit Companies";
		$headers = 'From: Astra Credit Companies <no-reply@acc.co.id>'."\r\n" .
		'Reply-To: no-reply@acc.co.id' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
		mail($email,"[Toyota Credit Simulation] Thank You",$pesannya,$headers);

		?>
        <div id="box-yellow">
			<b>Information</b> : <br /><br />
Terima kasih, aplikasi simulasi kredit anda telah terkirim. <br /><br />
Kami akan memberikan respon kepada anda secepatnya.
		</div>
        <?
		}
}else{
   die('error');
}

?>
   </div>
 </div>
 </body></html>
