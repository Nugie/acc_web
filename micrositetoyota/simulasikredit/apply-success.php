<?
error_reporting(0);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Simulasi Kredit ACC</title>
<link type="text/css" href="ui/demos.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="jquery.selectbox.css" />
<link rel="stylesheet" type="text/css" href="jquery-ui-1.9.1.custom.min.css" />
<script type="text/javascript" src="js/jquery-1.8.2.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.9.1.custom.min.js"></script>
<script type="text/javascript" src="js/cufon-yui.js" ></script>
<script type="text/javascript" src="js/Myriad_Pro_400-Myriad_Pro_700.font.js"></script>
<script type="text/javascript" src="js/jquery.selectbox-0.2.min.js"></script>


<script type="text/javascript">
Cufon.replace('.block_acc', { fontFamily: 'Myriad Pro' });
Cufon.replace('.company_title', { fontFamily: 'Myriad Pro' });

</script>
</head>
<body>
<div id="wrapper">
	<div id="header-center">
    	<div class="blueblock gradien1">
					<div class="top_logo">
						<a href="http://www.astracreditcompanies.com/" target="_blank">
							<img src="images/logo.jpg" border="0" />
						</a>
					</div>
					<div class="company_title"><a href="http://www.astracreditcompanies.com/" class="putih" target="_blank">Astra Credit Companies</a>
					</div>
					<a href="http://www.astracreditcompanies.com/" target="_blank">
						<div class="block_acc">
							<div class="ic_acc">
							<b>ACC</b><br/>
							Credit Simulation</div>
						</div>
					</a>
              </div>
	</div>
	<div id="content">
		<div id="box-yellow">
			<b>Information</b> : <br /><br />
Terima kasih, aplikasi simulasi kredit anda telah terkirim. <br />
Kami akan memberikan respon kepada anda secepatnya.
		</div>
    </div>
  
</div>

</body></html>