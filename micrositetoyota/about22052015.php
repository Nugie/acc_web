<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ACC - About</title>
	<meta name="keywords" content="Kredit Mobil, Simulasi Kredit">
    <meta name="description" content="Kredit Mobil Terbaik Dari ACC" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="css/style.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container" style="margin-top:20px;">
    <div class="row">
 		  <div class="col-md-1"></div>	
          <!--main content-->
          <div class="col-md-8">
            <div class="headertitle">ABOUT ACC</div>
            <div class="logo"><img src="img/logo-acc.jpg" alt="Logo ACC" /></div>
              <div>
              <!--<p>Astra Credit Companies atau biasa disingkat dengan ACC adalah perusahaan pembiayaan Kredit Mobil dan alat berat terbesar di Indonesia. ACC didirikan guna mendukung bisnis otomotif kelompok Astra. PT Astra Sedaya Finance, yang merupakan cikal bakal ACC berdiri pada 15 Juli 1982, dengan nama PT Rahardja Sedaya.</p>

<p>Di tahun 1990, PT Rahardja Sedaya berganti nama menjadi PT Astra Sedaya Finance. Sejak tahun 1994, ACC mengembangkan merek Astra Credit Companies untuk mendukung usahanya. Dalam perkembangannya, PT Astra Sedaya Finance memiliki penyertaan saham pada perusahaan asosiasi, yaitu PT Pratama Sedaya Finance, PT Swadharma Bhakti Sedaya Finance, PT Staco Estika Sedaya Finance dan PT Astra Auto Finance.</p>

<p>ACC berkomitmen penuh untuk meningkatkan layanan pada masyarakat. ACC menyediakan fasilitas pembiayaan Kredit Mobil untuk pembelian mobil atau alat berat dalam kondisi baru ataupun bekas. ACC juga mendukung penjualan mobil melalui jaringan dealer, showroom maupun perseorangan di seluruh wilayah Indonesia. Jaringan ACC tersebar dihampir seluruh kota besar di Indonesia. Saat ini ACC memiliki 70 kantor layanan di 52 kota, dan akan terus bertambah. </p>

<p>Demi memenuhi kebutuhan customer akan informasi, ACC memfasilitasi informasi produk-produknya melalui website <a href="http://www.acc.co.id" target="_blank">ACC.CO.ID</a>. Website ini akan memberi kemudahan bagi customer ACC, baik untuk melihat pembayaran angsuran, melakukan  melakukan perhitungan Kredit Mobil melalui fitur <a href="http://www.acc.co.id/credit_simulations" target="_blank" title="Simulasi Kredit Mobil">Simulasi Kredit</a>, dan juga melihat update <a href="http://www.acc.co.id/harga_mobil" target="_blank" title="harga mobil terbaru">Harga Mobil terbaru</a>! </p>-->
<p>Astra Credit Companies (ACC) adalah grup perusahaan pembiayaan kendaraan bermotor yang terdiri dari PT Astra Sedaya Finance, PT Swadharma Bhakti Sedaya Finance, PT Astra Auto Finance, PT Staco Estika Sedaya Finance dan PT Pratama Sedaya Finance. Sebagai perusahaan pembiayaan yang merupakan anak perusahaan PT Astra International Tbk, ACC menawarkan produk-produk pembiayaan yang lengkap dan bervariasi mulai dari pembiayaan mobil baru dan  mobil bekas, pembiayaan alat berat,  Fastrack, pelayanan pengurusan STNK dan ACC Credit Protection. </p>
<p>ACC merupakan perusahaan pembiayaan kendaraan bermotor yang sudah berpengalaman selama lebih dari 32 tahun selalu memberikan kemudahan untuk pelanggannya. ACC memiliki 73 cabang dan jaringan operasional yang tersebar di seluruh Indonesia, bekerja sama dengan lebih dari 9.000 dealer rekanan. Untuk kemudahan pembayaran angsuran, pelanggan dapat membayar angsuran melalui lebih dari 76.000 payment point seperti ATM (Bersama, Mandiri, Permata, BNI, BCA), SMS/mobile banking, internet banking, Kantor Pos, Cheque/giro ataupun tunai. </p>
<p>Demi memenuhi kebutuhan pelanggan akan informasi, ACC memfasilitasi informasi produk-produknya melalui website <a href="http://www.acc.co.id" target="_blank">www.acc.co.id</a>. Website ini akan memberikan kemudahan bagi pelanggan ACC baik untuk melihat pembayaran angsuran, melakukan perhitungan kredit mobil melalui fitur <a href="http://www.acc.co.id/credit_simulations" target="_blank" title="Simulasi Kredit Mobil">Simulasi Kredit</a> dan juga melihat update <a href="http://www.acc.co.id/harga_mobil" target="_blank" title="harga mobil terbaru">harga mobil terbaru</a>. Selain itu untuk memperoleh informasi lebih lanjut mengenai ACC, pelanggan dapat menghubungi Kontak ACC di nomer <b>1500599</b>.</p>


<p class="text-center"><a class="btn btn-primary btn-default" href="http://www.acc.co.id/about_acc" role="button" target="_blank">More Info</a></p>
              </div>
          </div>
          <!--end main content-->
          <!--right content-->
          <div class="col-md-3">
          	
          </div>
          <!--end right-->
        </div>
        <!--footer-->
  		<div class="row">
        <div class="col-md-1"></div>	
          <div class="col-md-11">
                <div id="footer"><img src="img/footer.jpg" alt="Footer" border="0" usemap="#Map" class="img-responsive" />
                    <map name="Map">
                      <area shape="rect" coords="6,10,188,61" href="http://www.acc.co.id" alt="ACC Website" target="_blank">
                      <area shape="rect" coords="203,9,379,61" href="https://www.facebook.com/acckreditmobil" alt="Facebook ACC" target="_blank">
                      <area shape="rect" coords="403,10,579,61" href="https://www.twitter.com/ACCKreditMobil" alt="Twitter ACC" target="_blank">
                    </map>
            	</div>
             </div>
         </div>
         <!--end footer-->
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>