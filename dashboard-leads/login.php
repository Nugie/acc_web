<?php
error_reporting(0);
session_cache_expire(10);
session_start();
include 'includes/config.php';

$siteKey = '6LdpsxETAAAAAOlPjhE-uQbe-8Y3Bs60vgy9axSJ';
$secret = '6LdpsxETAAAAAGB9VYRuOIZKOvVpAIEZnGs3vKsi';

if(isset($_GET['logout']))
{
	unset($_SESSION['login']);
	session_destroy();
}

if (isset($_SESSION['login']) && $_SESSION['login'] == $hash)
{
	header("location:index.php");
}
else if (isset($_POST['submit']))
{

	$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
	if ($response['success'] == '1') {
		//capcha sukses
			if ($_POST['username'] == $ausername && $_POST['password'] == $apassword)
			{
				$_SESSION["login"] = $hash;
				header("Location: index.php");
			}
			else
			{
				display_login_form('1');
			}
		//end capcha
	} else {
		display_login_form('1');
	}
}
else
{
	display_login_form('0');
}

function display_login_form($err)
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- META TAGS -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="ACC dashboard">
<meta name="author" content="cakradigital">

<!-- TITLE -->
<title>ACC LEADS - Dashboard</title>

<!-- FAVICON -->
<link rel="shortcut icon" href="assets/images/favicon.ico">

<!-- STYLESHEETS -->
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/core.css" type="text/css" />
<link rel="stylesheet" href="assets/css/components.css" type="text/css" />
<link rel="stylesheet" href="assets/css/icons.css" type="text/css" />
<link rel="stylesheet" href="assets/css/pages.css" type="text/css" />
<link rel="stylesheet" href="assets/css/responsive.css" type="text/css" />

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<!-- MODERNIZER -->
<script src="assets/js/modernizr.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
</head>

<body class="loreg-page close-it">
<!-- Begin page -->
<div id="logreg-wrapper" class="text-center">
	<div class="container">
		<p class="lead">LEADS DASHBOARD</p>
		<?php if ($err==1) {
			 echo "<p>Invalid Username/Password/CAPTCHA</p>";
		 }
		?>
		<form action="login.php" method="post">
			<div class="form-group">
				<input type="text" name="username" placeholder="Username" class="form-control" id="email">
			</div>
			<div class="form-group">
				<input type="password" name="password" placeholder="Password" class="form-control" id="pwd">
			</div>
			<div class="form-group">
  			<div class="g-recaptcha" data-sitekey="6LdpsxETAAAAAOlPjhE-uQbe-8Y3Bs60vgy9axSJ"></div>
			</div>
			<input type="submit" class="loginbutton btn btn-success btn-lg" name="submit" value="Submit">
		</form>


	</div>
</div>
<!-- END wrapper -->

<!-- SmartBox Js files -->
<script>
       var resizefunc = [];
</script>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/pace.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/plugins/moment/moment.js"></script>
<script src="assets/js/jquery.app.js"></script>

</body>
</html>
<?php }?>
