<?php
session_start();
error_reporting(0);

if (isset($_SESSION['login'])) {
include "includes/config.php";

//db connection
$database = new medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname,
    'server' => $servername,
    'username' => $username,
    'password' => $password,
    'charset' => 'utf8'
]);
//db connection
?>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- META TAGS -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="ACC Dashboard">
<meta name="author" content="cakradigital">

<!-- TITLE -->
<title>LEADS - PAKET KREDIT ACC</title>

<!-- FAVICON -->
<link rel="shortcut icon" href="assets/images/favicon.ico">

<!-- STYLESHEETS -->
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/core.css" type="text/css" />
<link rel="stylesheet" href="assets/css/components.css" type="text/css" />
<link rel="stylesheet" href="assets/css/icons.css" type="text/css" />
<link rel="stylesheet" href="assets/css/pages.css" type="text/css" />
<link rel="stylesheet" href="assets/css/responsive.css" type="text/css" />
<link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css" type="text/css" />
<link rel="stylesheet" href="assets/plugins/datatables/buttons.dataTables.min.css" type="text/css" />
<link rel="stylesheet" href="assets/plugins/datatables/responsive.dataTables.min.css" type="text/css" />

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<!-- MODERNIZER -->
<script src="assets/js/modernizr.min.js"></script>

</head>

<body class="fixed-left close-it">

<!-- Begin page -->
<div id="wrapper">
	<!-- Top Bar Start -->
	<div class="topbar">
		<!-- LOGO -->
		<div class="topbar-left">
			<a href="javascript:void(0)" class="logo d-logo hidden-xs hidden-sm">
			</a>
			<a href="javascript:void(0)" class="logo d-logo hidden visible-xs visible-sm">
				<img src="assets/images/m-logo.png" alt="SmartBox" class="img-responsive hidden-xs" height="0">
			</a>
			<!-- Image Logo here -->
		</div>

		<!-- Button mobile view to collapse sidebar menu -->
		<div class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="top-fix-navbar">
					<div class="pull-left">
						<button class="button-menu-mobile open-left waves-effect waves-light"> <i class="fa fa-outdent"></i> </button>
						<span class="clearfix"></span>
					</div>

				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- Top Bar End -->

<?php include "includes/leftmenu.php";?>


	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<!-- Page-Title -->
			<div class="page-title-group">
				<h4 class="page-title">PAKET KREDIT ACC</h4>
				<h5 class="text-muted page-title-alt"></h5>
			</div>
			<div class="cb-page-content">
				<div class="container">
					<!--content-->
					<?php
          $data = $database->query("SELECT * FROM `paketkredit_applicants` where `pka_created` >= curdate() - interval 1 year order by `pka_created` desc")->fetchAll();
					$j=0;
					?>

						<table id="example" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="text-center"><b>NO</b></th>
									<th class="text-center"><b>PAKET</b></th>
									<th class="text-center"><b>REFERRAL</b></th>
									<th class="text-center"><b>TANGGAL</b></th>
									<th class="text-center"><b>NAMA</b></th>
									<th class="text-center"><b>NO HP</b></th>
									<th class="text-center"><b>EMAIL</b></th>
									<th class="text-center"><b>NPK</b></th>
									<th class="text-center"><b>KOTA</b></th>
									<th class="text-center"><b>CABANG</b></th>
									<th class="text-center"><b>BRAND</b></th>
									<th class="text-center"><b>TIPE</b></th>
									<th class="text-center"><b>MODEL</b></th>
									<th class="text-center"><b>HARGA</b></th>
									<th class="text-center"><b>TERM</b></th>
									<th class="text-center"><b>DP</b></th>
									<th class="text-center"><b>INSTALLMENT</b></th>
									<th class="text-center"><b>PERSENTASE</b></th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($data as $row) {
								$j++;
									?>
								<tr>
									<td><?php echo $j;?></td>
									<td class="text-center"><?php echo $row["pka_packages"]?></td>
									<td class="text-center"><?php echo $row["pka_referral"]?></td>
									<td class="text-center"><?php echo date("j-m-Y",strtotime($row["pka_created"]))?></td>
									<td class="text-center"><?php echo ucwords($row["pka_name"])?></td>
									<td class="text-center"><?php echo $row["pka_phone"]?></td>
									<td class="text-center"><?php echo strtolower($row["pka_email"])?></td>
									<td class="text-center"><?php echo $row["pka_extrainfo"]?></td>
									<td class="text-center"><?php echo $row["pka_city"]?></td>
									<td class="text-center"><?php echo $row["pka_branch"]?></td>
									<td class="text-center"><?php echo $row["pka_category"]?></td>
									<td class="text-center"><?php echo $row["pka_type"]?></td>
									<td class="text-center"><?php echo $row["pka_variant"]?></td>
									<td class="text-center"><?php echo number_format($row["pka_price"],0,'',',')?></td>
									<td class="text-center"><?php echo $row["pka_term"]?></td>
									<td class="text-center"><?php echo number_format($row["pka_installment"],0,'',',')?></td>
									<td class="text-center"><?php echo number_format($row["pka_dp"],0,'',',')?></td>
									<td class="text-center"><?php echo $row["pka_pdp"]?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
										<!--/content-->

		</div></div>

		</div>
		<!-- container -->
	</div>
	<!-- content -->


</div>
<!-- END wrapper -->

<script>
       var resizefunc = [];
</script>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/plugins/datatables/dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/js/jquery.app.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
      "scrollX": true,
        dom: 'Bfrtip',
        buttons: [
					{
            extend: 'excelHtml5',
            text: 'Export to Excel'
        }

        ]
    } );
} );
</script>
</body>
</html>

<?php } else {
header("location:login.php");
exit;
}?>
