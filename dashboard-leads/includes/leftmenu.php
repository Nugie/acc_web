<div class="left side-menu">
  <div class="sidebar-inner slimscrollleft">
    <!--- Divider -->
    <div id="sidebar-menu">
      <ul>
        <li class="menu-title">Navigation</li>
        <li>
          <a href="index.php" class="waves-effect"> <i class="fa fa-check-square"></i> <span> Paket Kredit </span></a>
        </li>
        <li>
          <a href="simulasi.php" class="waves-effect"> <i class="fa fa-check-square"></i> <span> Simulasi Kredit </span></a>
        </li>
        <li>
          <a href="multiguna.php" class="waves-effect"> <i class="fa fa-check-square"></i> <span> Multiguna </span></a>
        </li>
        <li>
          <a href="toyota.php" class="waves-effect"> <i class="fa fa-check-square"></i> <span> Toyota </span></a>
        </li>
        <li>
          <a href="daihatsu.php" class="waves-effect"> <i class="fa fa-check-square"></i> <span> Daihatsu </span></a>
        </li>

          <li> <a href="login.php?logout=true" class="waves-effect"> <i class="fa fa-sign-out"></i> <span> Log Out</span> </a> </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>

  </div>
</div>
