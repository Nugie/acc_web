<?php
error_reporting(0);
session_start();

if (isset($_SESSION['login'])) {
include "includes/config.php";

//db connection
$database = new medoo([
    'database_type' => 'mysql',
    'database_name' => $dbname2,
    'server' => $servername,
    'username' => $username2,
    'password' => $password2,
    'charset' => 'utf8'
]);
//db connection
?>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- META TAGS -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="ACC Dashboard">
<meta name="author" content="cakradigital">

<!-- TITLE -->
<title>LEADS - TOYOTA, AUTO2000, TUNAS</title>

<!-- FAVICON -->
<link rel="shortcut icon" href="assets/images/favicon.ico">

<!-- STYLESHEETS -->
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/core.css" type="text/css" />
<link rel="stylesheet" href="assets/css/components.css" type="text/css" />
<link rel="stylesheet" href="assets/css/icons.css" type="text/css" />
<link rel="stylesheet" href="assets/css/pages.css" type="text/css" />
<link rel="stylesheet" href="assets/css/responsive.css" type="text/css" />
<link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css" type="text/css" />
<link rel="stylesheet" href="assets/plugins/datatables/buttons.dataTables.min.css" type="text/css" />
<link rel="stylesheet" href="assets/plugins/datatables/responsive.dataTables.min.css" type="text/css" />

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<!-- MODERNIZER -->
<script src="assets/js/modernizr.min.js"></script>

</head>

<body class="fixed-left close-it">

<!-- Begin page -->
<div id="wrapper">
	<!-- Top Bar Start -->
	<div class="topbar">
		<!-- LOGO -->
		<div class="topbar-left">
			<a href="javascript:void(0)" class="logo d-logo hidden-xs hidden-sm">
			</a>
			<a href="javascript:void(0)" class="logo d-logo hidden visible-xs visible-sm">
				<img src="assets/images/m-logo.png" alt="SmartBox" class="img-responsive hidden-xs" height="0">
			</a>
			<!-- Image Logo here -->
		</div>

		<!-- Button mobile view to collapse sidebar menu -->
		<div class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="top-fix-navbar">
					<div class="pull-left">
						<button class="button-menu-mobile open-left waves-effect waves-light"> <i class="fa fa-outdent"></i> </button>
						<span class="clearfix"></span>
					</div>

				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- Top Bar End -->

<?php include "includes/leftmenu.php";?>


	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<!-- Page-Title -->
			<div class="page-title-group">
				<h4 class="page-title">TOYOTA / AUTO200 / TUNAS</h4>
				<h5 class="text-muted page-title-alt"></h5>
			</div>
			<div class="cb-page-content">
				<div class="container">
					<!--content-->
					<?php
					$sql = "SELECT * FROM `cso_applicants` where `regdate` >= curdate() - interval 1 year order by regdate desc";
					$data = $database->query($sql)->fetchAll();
					$j=0;
					?>

						<table id="example" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="text-center"><b>NO</b></th>
									<th class="text-center"><b>REFERRAL</b></th>
									<th class="text-center"><b>TANGGAL</b></th>
									<th class="text-center"><b>NAMA</b></th>
									<th class="text-center"><b>NO HP</b></th>
									<th class="text-center"><b>EMAIL</b></th>
									<th class="text-center"><b>TEMPAT LAHIR</b></th>
									<th class="text-center"><b>TANGGAL LAHIR</b></th>
									<th class="text-center"><b>ALAMAT</b></th>
									<th class="text-center"><b>CABANG TERDEKAT</b></th>
									<th class="text-center"><b>MODEL MOBIL</b></th>
									<th class="text-center"><b>HARGA</b></th>
									<th class="text-center"><b>TENOR</b></th>
									<th class="text-center"><b>DP</b></th>
									<th class="text-center"><b>ANGSURAN</b></th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($data as $row) {
								$j++;
									?>
								<tr>
									<td><?php echo $j;?></td>
									<td class="text-center"><?php echo $row["referral"]?></td>
									<td class="text-center"><?php echo date("j-m-Y",strtotime($row["regdate"]))?></td>
									<td class="text-center"><?php echo ucwords($row["name"])?></td>
									<td class="text-center"><?php echo $row["phone"]?></td>
									<td class="text-center"><?php echo strtolower($row["email"])?></td>
									<td class="text-center"><?php echo strtolower($row["place_birth"])?></td>
									<td class="text-center"><?php echo $row["date_birth"]?></td>
									<td class="text-center"><?php echo $row["address"]?></td>
									<td class="text-center"><?php echo $row["branch"]?></td>
									<td class="text-center"><?php echo $row["vmodel"]?></td>
									<td class="text-center"><?php echo $row["votr"]?></td>
									<td class="text-center"><?php echo $row["vtenor"]?></td>
									<td class="text-center"><?php echo $row["vuangmuka"]?></td>
									<td class="text-center"><?php echo $row["vangsuran"]?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
										<!--/content-->

		</div></div>

		</div>
		<!-- container -->
	</div>
	<!-- content -->


</div>
<!-- END wrapper -->

<script>
       var resizefunc = [];
</script>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/plugins/datatables/dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/js/jquery.app.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
      "scrollX": true,
        dom: 'Bfrtip',
        buttons: [
					{
            extend: 'excelHtml5',
            text: 'Export to Excel'
        }

        ]
    } );
} );
</script>
</body>
</html>

<?php } else {
header("location:login.php");
exit;
}?>
