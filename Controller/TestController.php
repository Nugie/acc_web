<?php

App::uses('CroogoAppController', 'Croogo.Controller');

/**
 * Base Application Controller
 *
 * @package  Croogo
 * @link     http://www.croogo.org
 */
class TestController extends CroogoAppController {
	 public function index() {
        echo "Test";
    }
}
