$(document).foundation();
$(document).ready(function(){
  $('.slider1').bxSlider({
    slideWidth: 425,
    maxSlides: 3,
    slideMargin: 20,
    moveSlides: 1,
    hideControlOnEnd: true,
    infiniteLoop:false,
    pager: false
  });
  $('.slider2').bxSlider({
    slideWidth: 400,
    maxSlides: 3,
    slideMargin: 30,
    moveSlides: 1,
    hideControlOnEnd: true,
    infiniteLoop:false,
    pager: false
  });
  $('.sliderpromo').bxSlider({
    slideWidth: 330,
    maxSlides: 4,
    moveSlides: 1,
    infiniteLoop:true,
    auto:true,
    pager: false
  });
  $('.bxslider').bxSlider({
    speed:1000,
    pause:6000,
    controls:false,
    auto: true,
    autoControls: false
  });
//accordion opener
  var hash = window.location.hash.replace('#', '');
  if (hash != '') {
    if (!$("#"+hash).parent(".is-active").length) {
      $("#jenisproduk").foundation('down',$("#"+hash));
    }
    // to toggle item 1
    $("#jenisproduk").foundation('toggle',$("#"+hash));
    // if accordion no id
    $("#"+hash).parent().parent().foundation('toggle',$("#"+hash));
  }
//end accordion
  $('.linkshoppingtools').click(function(){
    $('.shoppingtools-short').toggleClass('shoppingtools-full');
  });

  $('.popup-link').magnificPopup({
    type: 'iframe'
    });
  $('.popup-link-inline').magnificPopup({
      type: 'inline'
  });
  $('.image-link').magnificPopup({type:'image'});
});

$(function(){

	$(document).on( 'scroll', function(){
		if ($(window).scrollTop() > 100) {
			$('.scroll-top-wrapper').addClass('show');
		} else {
			$('.scroll-top-wrapper').removeClass('show');
		}
    if ($(window).scrollTop() == $(document).height() - $(window).height())
      {
          $('.scroll-top-wrapper').css('bottom','160px');
      } else
       {
          $('.scroll-top-wrapper').css('bottom','20px');
       }
	});
	$('.scroll-top-wrapper').on('click', scrollToTop);
});

function scrollToTop() {
	verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	element = $('body');
	offset = element.offset();
	offsetTop = offset.top;
	$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}

var addthis_share = {
     // ... other options
     url_transforms : {
          shorten: {
               twitter: 'bitly',
				facebook: 'bitly',
				linkedin: 'bitly',
				google: 'bitly',
				google_plusone: 'bitly',
				whatsapp: 'bitly',
				email: 'bitly'
          }
     },
     shorteners : {
          bitly : {}
     }
}
