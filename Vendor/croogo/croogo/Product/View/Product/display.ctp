<!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="#">DEPAN</a></li>
              <li><a href="#">PRODUK &amp; LAYANAN</a></li>
              <li class="disabled">Ringkasan Informasi Produk</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
              <li class="active"><a href="produk-ringkasan.html">Ringkasan Informasi Produk</a></li>
              <li><a href="produk-jenis.html">Jenis dan Produk </a></li>
              <li><a href="#">Paket Kredit</a></li>
              <li><a href="#">Simulasi Kredit</a></li>
              <li><a href="hargamobil.html">Harga Mobil</a></li>
              <li><a href="#">Promo</a></li>
              <li><a href="produk-carapengajuan.html">Cara Pengajuan Kredit</a></li>
              <li><a href="produk-carapembayaran.html">Cara Pembayaran</a></li>
              <li><a href="produk-cekkontrak.html">Cek Kontrak</a></li>
              <li><a href="produk-layanan.html">Layanan Asuransi, Astra Credit Protection (ACP), STNK dan BPKB</a></li>
              <li><a href="produk-testimoni.html">Testimoni</a></li>
            </ul>

          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="large-8 columns" id="main-content">
            <h1>Ringkasan Informasi Produk</h1>
            <!--content detail-->
            Content Here
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->