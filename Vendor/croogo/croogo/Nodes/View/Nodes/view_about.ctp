 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58508076c43393da"></script>
 <?php $this->Nodes->set($node); ?>
 <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->webroot; ?>">Home</a></li>
              <li class="disabled">TENTANG KAMI</li>
              <li class="disabled"><?php echo $this->Nodes->field('title'); ?></li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->

          <div class="medium-4 large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
			  <?php foreach ($menus as $menu) {
					//$pos = strrpos($menu['Link']['link']['slug'], $this->request->params['named']['slug']);
					//print_r($menu['Link']['link']);
			   ?>
					<li <?php if ($menu['Link']['link']['slug']==$this->request->params['named']['slug']) { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url($menu['Link']['link']) ?>"><?php echo $menu['Link']['title'] ?></a></li>
			   <?php } ?>

            </ul>

          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1><?php echo $this->Nodes->field('title'); ?></h1>
			<div class="mb-20">
				  <!-- AddThis Button BEGIN -->
                  <div id="share-article" class="float-left">Bagikan :</div><div class="float-left"><div class="addthis_inline_share_toolbox"></div></div><div class="clearfix"></div>
              </div>
            <!--content detail-->
				<?php

					 //echo $this->Nodes->field('body');
					 echo $this->Nodes->info();
					 echo $this->Nodes->body();
					 echo $this->Nodes->moreInfo();
				?>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
