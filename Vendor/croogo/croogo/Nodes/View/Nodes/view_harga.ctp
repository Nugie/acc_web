 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58508076c43393da"></script>
 <?php $this->Nodes->set($node);
	//print_r($node);
 ?>
  <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo  $this->Html->url('/', true) ?>">DEPAN</a></li>
              <li class="disabled"><?php echo $this->Nodes->field('title') ?></li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="medium-4 large-4 columns">
            <ul id="sidebar-menu">
			<?php
				//print_r($this->request);
			?>
              <li <?php if($this->request->params['action']=="view_harga") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/harga-mobil', true) ?>">Harga Mobil</a></li>
              <li <?php if($this->request->params['controller']=="credit") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/simulasi-kredit-mobil', true) ?>">Simulasi Kredit</a></li>
              <li <?php if($this->request->params['action']=="view_product") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/product/package', true) ?>">Paket Kredit</a></li>
            </ul>
            <div class="sidebar-umum mt-20 mb-20">
                <h3>PAKET KREDIT</h3>
                <?php foreach($pakets as $row) { ?>
                 <div><a href="<?php echo $row['Node']['link'] ?>"><img src="<?php echo $this->Html->url('/', true);?><?php echo $row['Node']['path'] ?>" alt="<?php echo $row['Node']['title'] ?>" class="banner" /></a></div>
               <?php } ?>
            </div>
            <div class="sidebar-umum mt-20 mb-20">
                <h3>APLIKASI ACC YES</h3>
                <div><img src="<?php echo $this->webroot; ?>images/banner-aplikasi.jpg" alt="" class="banner" usemap="#accyes2" />
                  <map name="accyes2" id="accyes2">
                    <area shape="rect" coords="8,38,140,91" href="https://itunes.apple.com/id/app/accyes!/id1129623253?mt=8" target="_blank" alt="ACCYes! on AppStore" />
                    <area shape="rect" coords="145,39,285,86" href="https://play.google.com/store/apps/details?id=com.ACCYes" target="_blank" alt="ACCYes! on google play store" />
                  </map>
                </div>
            </div>
          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1><?php echo $this->Nodes->field('title') ?></h1>
            <!--content detail-->
            <div class="row mb-20">
                <div class="large-7 columns"><span class="periode-hargamobil">Periode : <?php echo $node['CustomFields']['periode']?><span></div>
                <div class="large-5 columns"><div id="share-article" class="float-left">Bagikan :</div><div class="float-left"><div class="addthis_inline_share_toolbox"></div></div><div class="clearfix"></div></div>
            </div>
            <ul class="car-menu">
                <li class="active"><a href="<?php echo  $this->Html->url('/harga-mobil/toyota', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-toyota.jpg" alt="Harga Mobil Toyota" width="148" /></a></li>
                <li><a href="<?php echo  $this->Html->url('/harga-mobil/daihatsu', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-daihatsu.jpg" alt="Harga Mobil Daihatsu" width="148" /></a></li>
                <li><a href="<?php echo  $this->Html->url('/harga-mobil/isuzu', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-isuzu.jpg" alt="Harga Mobil Isuzu" width="148" /></a></li>
                <li><a href="<?php echo  $this->Html->url('/harga-mobil/honda', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-honda.jpg" alt="Harga Mobil Honda" width="148" /></a></li>
                <li><a href="<?php echo  $this->Html->url('/harga-mobil/mazda', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-mazda.jpg" alt="Harga Mobil Mazda" width="148" /></a></li>
                <li><a href="<?php echo  $this->Html->url('/harga-mobil/suzuki', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-suzuki.jpg" alt="Harga Mobil Suzuki" width="148" /></a></li>
                <li><a href="<?php echo  $this->Html->url('/harga-mobil/mitsubishi', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-mitsubishi.jpg" alt="Harga Mobil Mitsubishi" width="148" /></a></li>
                <li><a href="<?php echo  $this->Html->url('/harga-mobil/nissan', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-nissan.jpg" alt="Harga Mobil Nissan" width="148" /></a></li>
                <li><a href="<?php echo  $this->Html->url('/harga-mobil/ford', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-ford.jpg" alt="Harga Mobil Ford" width="148" /></a></li>
                <li><a href="<?php echo  $this->Html->url('/harga-mobil/datsun', true) ?>"><img src="<?php echo $this->webroot; ?>images/brandlogo-datsun.jpg" alt="Harga Mobil Datsun" width="148" /></a></li>
            </ul>
            <div class="clearfix"></div>
			<!--HARGA-->
				  <?php echo $this->Nodes->body();?>
			<!--/HARGA-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
