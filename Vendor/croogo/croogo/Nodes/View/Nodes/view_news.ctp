 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58508076c43393da"></script>
  <?php $this->Nodes->set($node); ?>
 <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">HOME</a></li>
              <li class="disabled"><?php echo $type['Type']['title'] ?></li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="medium-4 large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
                <li <?php if($this->request->params['named']['type']=="news") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/news', true) ?>">Berita Terbaru</a></li>
                <li <?php if($this->request->params['named']['type']=="corporatenews") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/corporatenews', true) ?>">Siaran Pers</a></li>
                <li <?php if($this->request->params['named']['type']=="events") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/events', true) ?>">Agenda Kegiatan Perusahaan</a></li>
            </ul>
            <div id="sidebar-populer" class="mt-20 mb-20">
                <h3>TERPOPULER</h3>
                <ul id="sidebar-populer-list">
							<?php foreach ($populars as $popular) {?>
								<li><a href="<?php echo  $this->Html->url('/'. $this->request->params['named']['type'] .'/read/', true) .$popular['Node']['slug'] ?>" title="<?php echo $popular['Node']['title'] ?>"><?php echo $popular['Node']['title'] ?></a></li>
							<?php } ?>

          				</ul>

            </div>
          </div>
          <!--/sidebar-->
           <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1>BERITA</h1>
            <!--news detail-->
             <div class="article-tags mb-10"><b>
			 <?php
						$date=date_create($this->Nodes->field('created'));

					?>
			 <?php if (isset($node['Taxonomy'][0]['Term']['title'])) echo $node['Taxonomy'][0]['Term']['title'] ." - "?> <?php echo date_format($date,"d M Y"); ?></b></div>
              <h2 class="content-title"><?php echo $this->Nodes->field('title'); ?></h2>
              <div class="mb-20">
				  <!-- AddThis Button BEGIN -->
                  <div id="share-article" class="float-left">Bagikan Artikel Ini :</div><div class="float-left"><div class="addthis_inline_share_toolbox"></div></div><div class="clearfix"></div>
              </div>
              <?php echo $this->Nodes->body();?>
			   <div id="content-disclaimer" class="mb-20">
                "Astra Credit Companies (ACC), perusahaan pembiayaan kredit mobil terbaik yang memberikan solusi kepemilikan mobil baru dan mobil bekas dengan syarat ringan, proses cepat dan aman. ACC juga menawarkan kredit mobil untuk jenis kendaraan pickup, truk dan alat berat. Gunakan <a href="http://www.acc.co.id/simulasi-kredit-mobil">simulasi kredit mobil</a> untuk menghitung jumlah angsuran. Segera hubungi ACC di 1500599 dan miliki kendaraan idaman Anda sekarang juga!"
              </div>
              <div id="content-lainnya">
                 <div><b>BACA BERITA LAINNYA</b></div>

                 <ul>
				 <?php foreach ($relateds as $related) {?>
								<li><a href="<?php echo  $this->Html->url('/'. $this->request->params['named']['type'] .'/read/', true) .$related['Node']['slug'] ?>" title="<?php echo $related['Node']['title'] ?>"><?php echo $related['Node']['title'] ?></a></li>
							<?php } ?>


                				</ul>
              </div>
            <!--/news detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
