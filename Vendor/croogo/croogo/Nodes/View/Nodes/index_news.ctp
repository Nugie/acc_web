 <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">HOME</a></li>
              <li class="disabled"><?php echo $type['Type']['title'] ?></li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="medium-4 large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
                <li <?php if($this->request->params['named']['type']=="news") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/news', true) ?>">Berita Terbaru</a></li>
                <li <?php if($this->request->params['named']['type']=="corporatenews") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/corporatenews', true) ?>">Siaran Pers</a></li>
                <li <?php if($this->request->params['named']['type']=="events") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/events', true) ?>">Agenda Kegiatan Perusahaan</a></li>
            </ul>
            <div id="sidebar-populer" class="mt-20 mb-20">
                <h3>TERPOPULER</h3>
                <ul id="sidebar-populer-list">
							<?php foreach ($populars as $popular) {?>
								<li><a href="<?php echo  $this->Html->url('/'. $this->request->params['named']['type'] .'/read/', true) .$popular['Node']['slug'] ?>" title="<?php echo $popular['Node']['title'] ?>"><?php echo $popular['Node']['title'] ?></a></li>
							<?php } ?>

          				</ul>

            </div>
          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <?php
				//print_r($type);
			?>
			<h1><?php echo $type['Type']['title'] ?></h1>
            <!--news menu-->
			<?php if (count($terms) > 0) { ?>
            <ul id="news-menu">
				 <?php if (isset($this->request->params['locale'])) { $lang=$this->request->params['locale']; } else {$lang='ind';}
					   if (isset($this->request->params['slug'])) { $slug=$this->request->params['slug']; } else {$slug='';}
				 ?>
                <li <?php if ($slug=='') { ?>class="active"<?php }?>><a href="<?php echo  $this->Html->url('/'. $this->request->params['named']['type'], true) ?>"><?php if ($lang=='ind') { ?>SEMUA<?php } else {?>ALL<?php }?></a></li>
				<?php
				//print_r($terms);
				foreach($terms as $term) {

				?>
                <li <?php if ($slug==$term['Term']['slug'] ) { ?>class="active"<?php }?>><a href="<?php echo  $this->Html->url('/'. $this->request->params['named']['type'] .'/', true) .$term['Term']['slug'] ?>"><?php echo strtoupper($term['Term']['title']) ?></a></li>
				<?php } ?>

            </ul>
			<?php } ?>
            <!--/news menu-->
            <!--news list-->
			<?php
				if (count($nodes) == 0) {
					echo __d('croogo', 'No items found.');
				}
			?>
			<?php
				foreach ($nodes as $node):
					$this->Nodes->set($node);
			?>
            <div class="row mb-10">
                <div class="medium-5 large-5 columns mb-5">
						<?php if (isset($node['Multiattach'][0])){ ?>
							<?php
//echo $node['Multiattach'][0]['Multiattach']['filename'];
							if ($node['Multiattach'][0]['Multiattach']['mime']=="application/json"){
                if (strpos($node['Multiattach'][0]['Multiattach']['filename'],"http://172.16.4.35/")==false) {
              ?>
                <a href="<?php echo $this->Html->url('/'. $this->request->params['named']['type'] .'/read/', true) .$this->Nodes->field('slug') ?>"><img src="<?php echo $node['Multiattach'][0]['Multiattach']['filename'] ?>" alt="<?php if (isset($node['Multiattach'][0]['Multiattach']['metaDisplay'])) echo $node['Multiattach'][0]['Multiattach']['metaDisplay']?>" width="400" /></a>
              <?php } else {?>
                <a href="<?php echo $this->Html->url('/'. $this->request->params['named']['type'] .'/read/', true) .$this->Nodes->field('slug') ?>"><img src="<?php echo $node['Multiattach'][0]['Multiattach']['filename'] ?>" alt="<?php if (isset($node['Multiattach'][0]['Multiattach']['metaDisplay'])) echo $node['Multiattach'][0]['Multiattach']['metaDisplay']?>" width="400" /></a>
              <?php }?>
							<?php } else { ?>
									<?php $str = $this->Html->url('/', true) . str_replace("webroot","",$node['Multiattach'][0]['Multiattach']['real_filename']);
										//echo $str;
									//if (@getimagesize($str)){ ?>
										<a href="<?php echo  $this->Html->url('/'. $this->request->params['named']['type'] .'/read/', true) .$this->Nodes->field('slug') ?>"><img src="<?php echo $str ?>" alt="<?php if (isset($node['Multiattach'][0]['Multiattach']['metaDisplay'])) echo $node['Multiattach'][0]['Multiattach']['metaDisplay']?>" width="400" /></a>
									<?php //} ?>
							<?php }  ?>
						<?php } ?>
				</div>
                <div class="medium-7 large-7 columns">
					<?php
						$date=date_create($this->Nodes->field('created'));

					?>
                    <div class="article-tags"><?php if (isset($node['Taxonomy'][0]['Term']['title'])) echo $node['Taxonomy'][0]['Term']['title'] ." - "?> <?php echo date_format($date,"d M Y"); ?></div>
                    <h2><a href="<?php echo  $this->Html->url('/'. $this->request->params['named']['type'] .'/read/', true) .$this->Nodes->field('slug') ?>"><?php echo $this->Nodes->field('title') ?></a></h2>
                    <p class="mb-5"><?php echo $this->Nodes->field('excerpt') ?> </p>
                    <p class="link-more"><a href="<?php echo  $this->Html->url('/'. $this->request->params['named']['type'] .'/read/', true) .$this->Nodes->field('slug') ?>">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
                </div>
            </div>
			<?php
				endforeach;
			?>


            <!--/news list-->
            <ul class="pagination text-center mt-20" role="navigation" aria-label="Pagination">
              <?php echo $this->Paginator->numbers(array('tag'=>'li')); ?>
			  <!--
			  <li class="pagination-previous disabled">Previous</li>
              <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
              <li><a href="#" aria-label="Page 2">2</a></li>
              <li><a href="#" aria-label="Page 3">3</a></li>
              <li><a href="#" aria-label="Page 4">4</a></li>
              <li class="ellipsis"></li>
              <li><a href="#" aria-label="Page 12">12</a></li>
              <li><a href="#" aria-label="Page 13">13</a></li>
              <li class="pagination-next"><a href="#" aria-label="Next page">Next</a></li>
			  -->
            </ul>
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
