 <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="#">DEPAN</a></li>
              <li><a href="#">TENTANG KAMI</a></li>
              <li class="disabled">Riwayat Singkat Perusahaan</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
              <li class="active"><a href="about-sejarah.html">Riwayat Singkat Perusahaan</a></li>
              <li><a href="about-visi.html">Visi, Misi dan Nilai</a></li>
              <li><a href="about-strukturgrup.html">Struktur Grup</a></li>
              <li><a href="about-strukturkepemilikan.html">Struktur Kepemilikan</a></li>
              <li><a href="about-strukturorganisasi.html">Struktur Organisasi</a></li>
              <li><a href="about-boc.html">Dewan Komisaris</a></li>
              <li><a href="about-syariah.html">Dewan Pengawas Syariah</a></li>
              <li><a href="about-direksi.html">Direksi</a></li>
              <li><a href="about-secretary.html">Sekretaris Perusahaan</a></li>
              <li><a href="about-komite.html">Komite</a></li>
              <li><a href="about-ekuitas.html">Perusahaan-Perusahaan Dimana Dimiliki Ekuitas</a></li>
              <li><a href="about-anggaran.html">Anggaran Dasar Perusahaan</a></li>
              <li><a href="about-penghargaan.html">Penghargaan dan Pencapaian</a></li>
            </ul>

          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="large-8 columns" id="main-content">
            <h1>Riwayat Singkat Perusahaan</h1>
            <!--content detail-->
            <p><strong>Astra Credit Companies</strong> atau biasa di singkat dengan <strong>ACC </strong>adalah perusahaan pembiayaan mobil dan alat berat. Sesuai dengan Peraturan Otoritas Jasa Keuangan No. 29/POJK.05/2015 ACC melakukan perluasan usaha di bidang Pembiayaan Investasi, Pembiayaan Modal Kerja, Pembiayaan Multiguna dan Sewa Operasi (Operating Lease), baik dengan skema konvensional maupun syariah. PT Astra Sedaya Finance yang merupakan cikal bakal ACC berdiri pada 15 Juli 1982 dengan nama PT Rahardja Sedaya, didirikan  guna mendukung bisnis otomotif kelompok Astra.</p>
            <p>Di tahun 1990, PT Rahardja Sedaya berganti nama menjadi PT Astra Sedaya Finance. Dalam perkembangannya, PT Astra Sedaya Finance memiliki penyertaan saham pada perusahaan asosiasi, yaitu PT Swadharma Bhakti Sedaya Finance, PT Pratama Sedaya Finance, PT Staco Estika Sedaya Finance dan PT Astra Auto Finance yang semuanya telah terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK) serta mendapatkan izin dari Departemen Keuangan Republik Indonesia Direktorat Jenderal Lembaga Keuangan dengan nomor izin usaha sebagai berikut : </p>
            <ul>
            <li>PT Astra Sedaya Finance dengan nomor izin usaha 1093/KMK.013/1989</li>
            <li>PT Swadharma Bhakti Sedaya Finance dengan nomor izin usaha 1095/KMK.013/1989</li>
            <li>PT Astra Auto Finance dengan nomor izin usaha 437/KMK.017/1995</li>
            <li>PT Staco Estika Sedaya Finance dengan nomor izin usaha 590/KMK.013/1990</li>
            <li>PT Pratama Sedaya Finance dengan nomor izin usaha 1258/KMK.013/1989</li>
            </ul>
            <p>Sejak tahun 1994, PT Astra Sedaya Finance dan perusahaan asosiasinya mengembangkan merek Astra Credit Companies untuk mendukung usahanya.ACC berkomitmen penuh untuk meningkatkan layanan pada masyarakat.ACC menyediakan fasilitas pembiayaan untuk pembelian mobil dan alat berat dalam kondisi baru ataupun bekas serta fasilitas Pembiayaan Investasi, Pembiayaan Modal Kerja, Pembiayaan Multiguna dan Sewa Operasi (Operating Lease).ACC juga mendukung penjualan mobil melalui jaringan dealer, showroom maupun perseorangan di seluruh wilayah Indonesia. Jaringan ACC tersebar di hampir seluruh kota besar di Indonesia. Saat ini ACC memiliki 73 kantor cabang yang tersebar di 59 kota di Indonesia, dan akan terus bertambah. </p>
            <p>ACC selalu mempertahankan reputasinya sebagai perusahaan pembiayaan terkemuka di Indonesia. Saat krisis ekonomi melanda Indonesia di tahun 1998, ACC dapat melewati krisis ini dengan baik. Termasuk juga dapat melunasi pinjaman sindikasi pada tahun 1999, tanpa restrukturisasi.</p>
            <p>Pada tahun 2000, ACC mulai melakukan penerbitan obligasi dengan rating A- dari PT Pemeringkat Efek Indonesia.Saat ini ACC telah meraih peringkat AAA (idn) Stable Outlook dari PT Fitch Ratings Indonesia dan peringkat idAAA dari PT Pemeringkat Efek Indonesia.ACC sebagai perusahaan pembiayaan terbesar dan terkemuka di Indonesia, selalu membayarkan nilai pokok hutang dan bunga tepat pada waktunya.</p>

            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
     
	  
	  
   