<?php
App::uses('TestimonialAppController', 'Testimonial.Controller');

use Cake\View\Helper;
/**
 * Acps Controller
 *
 * @property Acp $Acp
 * @property PaginatorComponent $Paginator
 */
class TestimonialController extends TestimonialAppController {

	public $components = array('Paginator');

	public $paginate = array(
        'limit' => 15,
		'conditions' => array("Testimonial.published" => "Yes"),
        'order' => array(
            'Testimonial.created' => 'DESC'
        )
    );
/**
 * index method
 *
 * @return void
 */
	public function display() {
		$this->set('title_for_layout', __d('croogo', 'Testimonial'));
		$this->_converter = new StringConverter();

		/*
		$data = $this->Testimonial->find(
			"all",
			array(
				"conditions" => array("Testimonial.published" => "Yes"),
				"order" => "Testimonial.id DESC"
			)
		);*/
		//'conditions' => array("Testimonial.published" => "Yes")
		$this->Paginator->settings = $this->paginate;

		// similar to findAll(), but fetches paged results
		$data = $this->Paginator->paginate('Testimonial');
		$this->set('data', $data);

		$this->_converter = new StringConverter();

		$this->loadModel('Link');
		$menus = $this->Link->find('all',array(
			'conditions'=> array('`Link`.`parent_id`'=>21),
		));
		$i=0;
		foreach($menus as $menu){
			$menus[$i]['Link']['link'] = $this->_converter->linkStringToArray($menu['Link']['link']);
			$i=$i+1;
		}
		if (!isset($this->request->params['named']['slug'])){
			$this->request->params['named']['slug'] = "";
		}
		$this->set('menus', $menus);


	}

	public function add() {
		$this->set('title_for_layout', __d('croogo', 'Add Testimony'));
		if ($this->request->is('post')) {
			if (!empty($this->request->data)){
				$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
				if ($response['success'] == true) {
					$this->request->data['Testimonial']['published'] = 'No';
					$this->request->data['Testimonial']['created'] = date("Y-m-d H:i:s");
					$this->request->data['Testimonial']['modified'] = date("Y-m-d H:i:s");
					if ($this->Testimonial->save($this->request->data)) {
						$this->Session->setFlash("Terima kasih, testimoni Anda sudah tersimpan dan menunggu proses moderasi sebelum ditayangkan.", 'flash', array('class' => 'warning callout'), 'auth');
						return $this->redirect("/testimonials");
					}

				}else{
					$this->Session->setFlash("Silahkan cek kembali kode keamanan Anda!", 'flash', array('class' => 'warning callout'), 'auth');
				}
			}
		}
		$this->_converter = new StringConverter();

		$this->loadModel('Link');
		$menus = $this->Link->find('all',array(
			'conditions'=> array('`Link`.`parent_id`'=>21),
		));
		$i=0;
		foreach($menus as $menu){
			$menus[$i]['Link']['link'] = $this->_converter->linkStringToArray($menu['Link']['link']);
			$i=$i+1;
		}
		if (!isset($this->request->params['named']['slug'])){
			$this->request->params['named']['slug'] = "";
		}
		$this->set('menus', $menus);

	}
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Security->unlockedActions = array('add');
		if (isset($this->request->params['page'])) {
			$this->request->params['named']['page'] = $this->request->params['page'];
		}
	}
}
