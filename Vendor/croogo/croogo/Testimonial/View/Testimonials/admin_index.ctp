<?php
$this->viewVars['title_for_layout'] = __d('testimonial', 'Testimonials');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('testimonial', 'Testimonials'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('id'),
		$this->Paginator->sort('email_address'),
		$this->Paginator->sort('name'),
		$this->Paginator->sort('content'),
		$this->Paginator->sort('published'),
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($testimonials as $testimonial):
		$row = array();
		$row[] = h($testimonial['Testimonial']['id']);
		$row[] = h($testimonial['Testimonial']['email_address']);
		$row[] = h($testimonial['Testimonial']['name']);
		$row[] = h($testimonial['Testimonial']['content']);
		$row[] = h($testimonial['Testimonial']['published']);
		$row[] = $this->Time->format($testimonial['Testimonial']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = $this->Time->format($testimonial['Testimonial']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($testimonial['Testimonial']['id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $testimonial['Testimonial']['id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$testimonial['Testimonial']['id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$testimonial['Testimonial']['id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $testimonial['Testimonial']['id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
