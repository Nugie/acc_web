<script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
<!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">DEPAN</a></li>
              <li class="disabled">Testimoni</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="medium-4 large-4 columns">
                <ul id="sidebar-menu" class="mb-20">

                 <?php
			  //print_r($this->request->params['action']);
			  // print_r($this->request->params['named']['type']);
			  if (!isset($this->request->params['named']['type'])){
				$this->request->params['named']['type'] = "";
			  }
			  foreach ($menus as $menu) {
					//print_r($menu['Link']['link']);
					if (!isset($menu['Link']['link']['type'])){
						$menu['Link']['link']['type'] =  "";
					}
					if (isset($menu['Link']['link']['slug'])){
			   ?>
					<li <?php if ($menu['Link']['link']['slug']==$this->request->params['named']['slug']) { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url($menu['Link']['link']) ?>"><?php echo $menu['Link']['title'] ?></a></li>
			   <?php } else {?>
						<li <?php if ($menu['Link']['link']['controller']==$this->request->params['controller'] && $menu['Link']['link']['type']==$this->request->params['named']['type']) { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url($menu['Link']['link']) ?>"><?php echo $menu['Link']['title'] ?></a></li>
			   <?php
					}
			   } ?>
                </ul>
          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content" >
            <h1>Testimoni</h1>
            <!--content detail-->
               <?php
				echo $this->Form->create(
					"Testimonial",
					array(
						"name" => "member-register-form",
						"id" => "member-register-form",
						"method" => "post"
					)
				);

			?>
			 <div class="row">
                 <div class="large-3 small-3 columns">
                  <label for="username" class="text-right middle">Nama</label>
                </div>
                <div class="large-5 small-9 columns end">

				  <?php
					echo $this->Form->input(
						"name",
						array(
							"type" => "text",
							"id" => "name",
							"size" => "50",
							"div" => false,
							"label" => false,
							"maxlength" => 50,
							"required"=>true
						)
					);
				?>
                </div>
              </div>
			  <div class="row">
                <div class="large-3 small-3 columns">
                  <label for="username" class="text-right middle">Email</label>
                </div>
                <div class="large-5 small-9 columns end">

				  <?php
					echo $this->Form->input(
						"email_address",
						array(
							"type" => "email",
							"id" => "email_address",
							"size" => "50",
							"div" => false,
							"label" => false,
							"maxlength" => 50,
							"required"=>true
						)
					);
				?>
                </div>
              </div>
			   <div class="row">
                <div class="large-3 small-3 columns">
                  <label for="username" class="text-right middle">Testimoni</label>
                </div>
               <div class="large-5 small-9 columns end">

				  <?php
					echo $this->Form->input(
						"content",
						array(
							"type" => "text",
							"id" => "content",
							"rows" => "6",
							"cols" => "100",
							"div" => false,
							"label" => false,
							"required"=>true
						)
					);
				?>
                </div>
              </div>
			   <div class="row">
                <div class="large-3 small-3 columns text-right">
                    <label for="captcha">&nbsp;</label>
                </div>
               <div class="large-5 small-9 columns end">
					<div class="g-recaptcha" data-sitekey="<?php echo Configure::read('Service.recaptcha_public_key') ?>"></div>
                </div>
              </div>
			           <div class="row">
                <div class="small-3 large-3 columns">
                  <label for="password" class="text-right middle">&nbsp;</label>
                </div>
                <div class="small-9 large-9 columns end">
								  <?php
						echo $this->Form->submit(
							"Submit",
							array(
								"id" => "form-submit",
								"class" => "button",
								"div" => false
							)
						);
					?>
                </div>
              </div>

             <?php echo $this->Form->end(); ?>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
