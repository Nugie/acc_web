<?php

CroogoNav::add('sidebar', 'contacts', array(
	'icon' => 'comments',
	'title' => __d('croogo', 'Contacts'),
	'url' => array(
		'admin' => true,
		'plugin' => 'contacts',
		'controller' => 'contacts',
		'action' => 'index',
	),
	'weight' => 50,
	'children' => array(
		'testimoni' => array(
			'title' => __d('croogo', 'Testimoni'),
			'url' => array(
				'admin' => true,
				'plugin' => 'testimonial',
				'controller' => 'Testimonials',
				'action' => 'index',
			),
		),
		'testdrive' => array(
			'title' => __d('croogo', 'Test Drive'),
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'TrnTestDrives',
				'action' => 'index',
			),
		),
		
		/*
		'contacts' => array(
			'title' => __d('croogo', 'Contacts'),
			'url' => array(
				'admin' => true,
				'plugin' => 'contacts',
				'controller' => 'contacts',
				'action' => 'index',
			),
		),
		'messages' => array(
			'title' => __d('croogo', 'Messages'),
			'url' => array(
				'admin' => true,
				'plugin' => 'contacts',
				'controller' => 'messages',
				'action' => 'index',
			),
		),*/
	),
));
