<?php
App::uses('ContactsAppModel', 'Contacts.Model');

class ContactRecipient extends ContactsAppModel
{
	var $name = "ContactRecipient";
	var $primaryKey = "recipient_id";
	var $useTable = "contact_recipients";
	
	var $validate = array(
		"name" => array(
			"minLength" => array(
				"rule" => array("minLength", 3),
				"message" => "Recipient name must contain at least 3 characters."
			),
			"isUnique" => array(
				"rule" => "isUnique",
				"message" => "This recipient name has already been taken."
			)
		),
		"email" => array(
			"minLength" => array(
				"rule" => array("minLength", 3),
				"message" => "Email address must contain at least 3 characters."
			),
			/*
			"email" => array(
				"rule" => array("email", true),
				"message" => "Please provide a valid email address."
			)
			*/
		)
	);
}

?>