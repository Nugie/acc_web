<?php

App::uses('CakeEmail', 'Network/Email');
App::uses('ContactsAppController', 'Contacts.Controller');

/**
 * Contacts Controller
 *
 * @category Controller
 * @package  Croogo.Contacts.Controller
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class SearchController extends ContactsAppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */

var $name = 'Search';
/**
 * Components
 *
 * @var array
 * @access public
 */
	public $components = array(
		'Croogo.Akismet',
		'Croogo.Recaptcha',
	);

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Contacts.Contact', 'Contacts.Message');

/**
 * Admin index
 *
 * @return void
 * @access public
 */
	public function admin_index() {
		$this->set('title_for_layout', __d('croogo', 'Hubungi Kami'));

		$this->Contact->recursive = 0;
		$this->paginate['Contact']['order'] = 'Contact.title ASC';
		$this->set('contacts', $this->paginate());
		$this->set('displayFields', $this->Contact->displayFields());
	}

/**
 * Admin add
 *
 * @return void
 * @access public
 */
	public function admin_add() {
		$this->set('title_for_layout', __d('croogo', 'Add Contact'));

		if (!empty($this->request->data)) {
			$this->Contact->create();
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', 'The Contact has been saved'), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $this->Contact->id));
			} else {
				$this->Session->setFlash(__d('croogo', 'The Contact could not be saved. Please, try again.'), 'flash', array('class' => 'error'));
			}
		}
	}

/**
 * Admin edit
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_edit($id = null) {
		$this->set('title_for_layout', __d('croogo', 'Edit Contact'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid Contact'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', 'The Contact has been saved'), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $this->Contact->id));
			} else {
				$this->Session->setFlash(__d('croogo', 'The Contact could not be saved. Please, try again.'), 'flash', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Contact->read(null, $id);
		}
	}

/**
 * Admin delete
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__d('croogo', 'Invalid id for Contact'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->Contact->delete($id)) {
			$this->Session->setFlash(__d('croogo', 'Contact deleted'), 'flash', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
	}

/**
 * View
 *
 * @param string $alias
 * @return void
 * @access public
 * @throws NotFoundException
 */

	public function search_keluhan(){
		if($this->request->data)
		{			
			$searchNumber = $this->request->data['Search']['keywords'];
			$this->set(
				'searchData',
				$this->_retrieveFromCCRP('/?r=service/searchKeluhan&nomor='.$searchNumber)
			);
			// $url = "172.16.4.111/ccrp_dev/?r=service/searchKeluhan&nomor=$searchNumber";
			// $curl = curl_init($url);
			
			// $searchData = curl_exec($curl);
			// curl_close($curl);
			// $this->set(
			// 	'searchReturn',
			// 	$searchData
			// );
		}
		else
		{
			$this->set(
				'searchData',
				$this->_retrieveFromCCRP('/?r=service/searchKeluhan&nomor=')
			);
		}
	}

	public function _retrieveFromCCRP($url)
	{
		//$url = Configure::read("CCRP.url").$url;
		//172.16.5.36
		//$url = "172.16.5.36".$url;
		// $url = "serviceccrp.acc.co.id".$url;
		$url = "172.16.4.111/ccrp_dev/".$url;

		$curl = curl_init($url);
    	curl_setopt($curl, CURLOPT_PORT, 80);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, true);
    	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,10);
    	curl_setopt($curl, CURLOPT_TIMEOUT, 10);

		$json_response = curl_exec($curl);
		curl_close($curl);

		//mencegah error apabila satu hal dan lain hal curl tidak muncul data
		if(empty($json_response) || $json_response==false){
			$json_response = '[]';
		}

		return json_decode($json_response, true);
	}

	public function beforeFilter() {

		//$this->Security->csrfCheck = false;
		//$this->Security->validatePost = false;
		$this->Security->unlockedActions = array('search_keluhan');
		$this->Auth->allow('search_keluhan');
		parent::beforeFilter();
	}

	

}
