<!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="#">DEPAN</a></li>
              <li class="disabled">HUBUNGI KAMI</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="medium-4 large-4 columns">
                <ul id="sidebar-menu" class="mb-20">
                    <li <?php if ($this->params['controller']=='network') {?>class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>network">Jaringan ACC</a></li>
                    <li <?php if ($this->params['action']=='view') {?>class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>contact">Hubungi Kami</a></li>
				    				<li <?php if ($this->params['action']=='search_keluhan') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>search_keluhan">Cari Keluhan</a></li>
                    <li <?php if ($this->params['slug']=='faq') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>faq">FAQ</a></li>
                    <li <?php if ($this->params['slug']=='kamus-pembiayaan') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>kamus-pembiayaan">Kamus Pembiayaan</a></li>
                    <li <?php if ($this->params['slug']=='kebijakan-website') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>kebijakan-website">Kebijakan Website</a></li>
                    <li <?php if ($this->params['slug']=='jalur-pelayanan-penyelesaian-pengaduan') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>jalur-pelayanan-penyelesaian-pengaduan">JALUR PELAYANAN & PENYELESAIAN PENGADUAN</a></li>
                </ul>
          </div>
          <!--/sidebar-->
       <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1>CARI KELUHAN</h1>
            <!--content detail-->
            <p>Berikut ini fungsi untuk mencari keluhan yang pernah anda buat.
    				<b>Silakan masukkan ID keluhan yang pernah anda buat</b></p>
							<?php echo $this->Form->create('Search',array('url'=>'search_keluhan'));?>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="unstriped tabel-formkontak" style="margin-bottom:0px">
									<td valign="top" colspan="12" style="padding-bottom : 0px; padding-left : 0px">
										<?php
											echo $this->Form->input('Search.keywords', array("class" => "text-field ui-widget-content ui-corner-all", "placeholder" => "Silakan cari", "label" => ""));
										?>
									</td>
									<td style="padding-bottom : 0px">
										<?php 
											echo $this->Form->submit('Search', array("class" => "button"));
										?>
									</td>
							<?php echo $this->Form->end();?>
						
							<table class ="table table-bordered table-stripped" style="font-size:12px">
								<thead>
									<tr>
										<th>Nomor Tiket</th>
										<th>Subject Keluhan</th>
										<th>Pesan</th>
										<th>Jawaban</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
										<?php if($searchData) { ?>
											<tr>
													<td><?php echo $searchData['CD_TICKET'] ?></td>	
													<td><?php echo $searchData['SUBJECT']?></td>
													<td><?php echo $searchData['MESSAGE']?></td>
													<td><?php echo $searchData['ANSWER']?></td>
													<td><?php echo $searchData['DESC_GCM']?></td>
											</tr>
											<?php } else {?>
											<tr>
												<td colspan="5" align="center">
														<?php echo "Silakan masukan sesuai ID Tiket anda"; ?>
												</td>
											</tr>
											<?php } ?>
								</tbody>
							</table>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
	  <script type="text/javascript">
							$(document).ready(function () {

								//$("#form-submit").button();
								$("#identity").change(function () {
									var identity = $(this).val();

									if (identity == "Pelanggan") {
										$(".pelanggan").show();
									} else {
										$(".pelanggan").hide();
									}
								});

								$("#recipient_id").change(function () {
									var value = $(this).val();
									if (value == 'saran' || value == 'pertanyaan') {
										$("#cd_sp_handler").hide()
										//$("#identity option[value='Non Pelanggan']").removeAttr('disabled');
										$("#cd_sp_handler").show();
										$(".inquiry").show();
										$(".complaint").hide();
									} else {
										$("#cd_sp_handler").show();
										$("#identity").val("Pelanggan").change();
										//$("#identity option[value='Non Pelanggan']").attr('disabled','disabled');
										$(".inquiry").hide();
										$(".complaint").show();
									}
								});


								function getCustomerName() {
									if ($("#no_aggr").val().length != 17)
										return false;
									var type = $("#jenis_pelanggan").val();
									$("#name").val("Dicari...");
									$.ajax({
										"url": "<?php echo $this->webroot?>contacts/contacts/customer/&id=" + $("#no_aggr").val() + "&t=" + type,
										"success": function (data) {
											$("#name").val(data);
										},
										"error": function () {
											$("#name").val("");
										}
									});
								}


								$("#jenis_pelanggan").change(function () {
									getCustomerName();
								});

								$("#no_aggr").change(function () {
									getCustomerName();
								});

								$("#recipient_id").change();
								$("#jenis_pelanggan").change();
								$("#identity").change();

								$('form').submit(function(){
									$('input[type=submit]', this).attr('disabled', 'disabled');
								});
							});
						</script>
