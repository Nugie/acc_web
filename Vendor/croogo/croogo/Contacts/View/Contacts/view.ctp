<script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
<!--content area-->
<!--breadcrumb-->
<div class="row columns mt-20">
	<nav aria-label="You are here:" role="navigation">
		<ul class="breadcrumbs">
			<li><a href="#">DEPAN</a></li>
			<li class="disabled">HUBUNGI KAMI</li>
		</ul>
	</nav>
</div>
<!--/breadcrumb-->
<div class="row mb-20">
	<!--sidebar-->
	<div class="medium-4 large-4 columns">
		<ul id="sidebar-menu" class="mb-20">
			<li <?php if ($this->params['controller']=='network') {?>class="active" <?php } ?>><a
					href="<?php echo $this->webroot; ?>network">Jaringan ACC</a></li>
			<li <?php if ($this->params['action']=='view') {?>class="active" <?php } ?>><a
					href="<?php echo $this->webroot; ?>contact">Hubungi Kami</a></li>
			<li <?php if ($this->params['action']=='search_keluhan') { ?> class="active" <?php } ?>><a
					href="<?php echo $this->webroot; ?>search_keluhan">Cari Keluhan</a></li>
			<li <?php if ($this->params['slug']=='faq') { ?> class="active" <?php } ?>><a
					href="<?php echo $this->webroot; ?>faq">FAQ</a></li>
			<li <?php if ($this->params['slug']=='kamus-pembiayaan') { ?> class="active" <?php } ?>><a
					href="<?php echo $this->webroot; ?>kamus-pembiayaan">Kamus Pembiayaan</a></li>
			<li <?php if ($this->params['slug']=='kebijakan-website') { ?> class="active" <?php } ?>><a
					href="<?php echo $this->webroot; ?>kebijakan-website">Kebijakan Website</a></li>
			<li <?php if ($this->params['slug']=='jalur-pelayanan-penyelesaian-pengaduan') { ?> class="active" <?php } ?>><a
					href="<?php echo $this->webroot; ?>jalur-pelayanan-penyelesaian-pengaduan">JALUR PELAYANAN & PENYELESAIAN
					PENGADUAN</a></li>
		</ul>
	</div>
	<!--/sidebar-->
	<!--main content-->
	<div class="medium-8 large-8 columns" id="main-content">
		<h1>HUBUNGI KAMI</h1>
		<!--content detail-->
		<p>Jika anda ingin menyampaikan keluhan, saran serta informasi mengenai kredit mobil, silahkan mengisi kolom di
			bawah ini. <br> <br>
			<b>Untuk setiap keluhan mengenai kontrak, dimohon memasukkan nomer kontrak untuk mempercepat proses follow
				up..</b></p>
		<?php
   echo $this->Form->create(
		"Contact",
		array(
			"name" => "contact-us-form",
			"id" => "contact-us-form",
			"method" => "post",
                        "type"=>"file",
		)
	);
	if (count($recipient_list) == 1)
	{
		$recipient_list = array_keys($recipient_list);
		echo $this->Form->input(
			"recipient_id",
			array(
				"type" => "hidden",
				"id" => "recipient_id",
				"label" => false,
				"value" => $recipient_list[0]
			)
		);
	}
	elseif (count($recipient_list) == 0)
	{
		echo $this->Form->input(
			"recipient_id",
			array(
				"type" => "hidden",
				"id" => "recipient_id",
				"label" => false,
				"value" => "NULL"
			)
		);
	}

	?>
		<table width="100%" border="0" cellspacing="0" cellpadding="4" class="unstriped tabel-formkontak">
			<tbody>
				<?php if (isset($form_error) && !empty($form_error)) : ?>
				<tr>
					<td valign="top" colspan="3">
						<div class="ui-state-error ui-corner-all" style="margin-bottom: 16px; padding: 8px;">
							<b>Error :</b>
							<br /><br />
							<?php echo $form_error; ?>
						</div>
					</td>
				</tr>
				<?php endif; ?>
				<?php if (isset($form_info) && !empty($form_info)) : ?>
				<tr>
					<td valign="top" colspan="3">
						<div class="ui-state-highlight ui-corner-all" style="margin-bottom: 16px; padding: 8px;">
							<b>Information :</b>
							<br /><br />
							<?php echo $form_info; ?>
						</div>
					</td>
				</tr>
				<?php endif; ?>
				<tr>
					<td valign="top" width="200">Jenis Pesan</td>
					<td valign="top">:</td>
					<td valign="top"><?php
                    		echo $this->Form->input(
                    			"RECIPIENT_ID",
                    			array(
                    				"id" => "recipient_id",
                    				"div" => false,
                    				"label" => false,
                    				"options" => $recipient_list,
              				"required"=>false,
                    			)
                    		);
                    	?></td>
				</tr>
				<tr>
					<td valign="top" width="200">Identitas</td>
					<td valign="top">:</td>
					<td valign="top"><?php
                    		echo $this->Form->input(
                    			"IDENTITY",
                    			array(
                    				"id" => "identity",
                    				"div" => false,
                    				"label" => false,
                    				"options" => $identity_list,
              				"required"=>false,
                    			)
                    		);
                    	?></td>
				</tr>
				<tr class="pelanggan">
					<td valign="top" width="200">Jenis Pelanggan</td>
					<td valign="top">:</td>
					<td valign="top"><?php
                    		echo $this->Form->input(
                    			"JENIS_PELANGGAN",
                    			array(
                    				"id" => "jenis_pelanggan",
                    				"div" => false,
                    				"label" => false,
                    				"options" => $jenis_pelanggan,							
              				"required"=>false,
                    			)
                    		);
                    	?></td>
				</tr>
				<tr class="pelanggan">
					<td valign="top" width="200">No. Kontrak</td>
					<td valign="top">:</td>
					<td valign="top">
						<?php
                		echo $this->Form->input(
                			"NO_AGGR",
                			array(
                				"type" => "text",
                				"id" => "no_aggr",
                				"size" => "40",
                				"class" => "text-field ui-widget-content ui-corner-all",
                				"div" => false,
                				"label" => false,
                				"required"=>false,
                			)
                		);
                	?>
					</td>
				</tr>
				<tr class="pelanggan">
					<td valign="top" width="200">Nama Pelanggan</td>
					<td valign="top">:</td>
					<td valign="top">
						<?php
              		echo $this->Form->input(
              			"NAME",
              			array(
              				"type" => "text",
              				"id" => "name",
              				"size" => "40",
              				"class" => "text-field ui-widget-content ui-corner-all",
              				"div" => false,
              				"label" => false,
              				"readonly"=>"readonly",
              				"required"=>false,
              			)
              		);
              	?>
					</td>
				</tr>
				<tr>
					<td valign="top" width="200">Nama Pelapor</td>
					<td valign="top">:</td>
					<td valign="top">
						<?php
		echo $this->Form->input(
			"CALLER_NAME_NONCUST",
			array(
				"type" => "text",
				"id" => "caller_name_noncust",
				"size" => "40",
				"class" => "text-field ui-widget-content ui-corner-all",
				"div" => false,
				"label" => false,
				"required"=>false,
			)
		);
	?>
					</td>
				</tr>
				<tr>
					<td valign="top" width="200">Hubungan Pelapor dengan Pelanggan</td>
					<td valign="top">:</td>
					<td valign="top"><?php
		echo $this->Form->input(
			"RELATION",
			array(
				"id" => "relation",
				"div" => false,
				"label" => false,
				"options" => $relation,
   				"required"=>false,
			)
		);
	?></td>
				</tr>
				<tr>
					<td valign="top" width="200">Alamat</td>
					<td valign="top">:</td>
					<td valign="top">
						<?php
		echo $this->Form->input(
			"ALAMAT",
			array(
				"type" => "text",
				"id" => "alamat",
				"size" => "40",
				"class" => "text-field ui-widget-content ui-corner-all",
				"div" => false,
				"label" => false,
				   				"required"=>false,
			)
		);
	?>
					</td>
				</tr>
				<tr>
					<td valign="top" width="200">Telepon</td>
					<td valign="top">:</td>
					<td valign="top">
						<?php
		echo $this->Form->input(
			"TELEPON",
			array(
				"type" => "text",
				"id" => "telepon",
				"size" => "40",
				"class" => "text-field ui-widget-content ui-corner-all",
				"div" => false,
				"label" => false,
				   				"required"=>false,
			)
		);
	?>
					</td>
				</tr>
				<tr>
					<td valign="top" width="200">Alamat Email</td>
					<td valign="top">:</td>
					<td valign="top">
						<?php
		echo $this->Form->input(
			"EMAIL",
			array(
				"type" => "text",
				"id" => "email",
				"size" => "40",
				"class" => "text-field ui-widget-content ui-corner-all",
				"div" => false,
				"label" => false,
				   				"required"=>false,
			)
		);
	?>
					</td>
				</tr>
				<tr class="inquiry" style="display:none">
					<td valign="top" width="200">Kategori Pesan</td>
					<td valign="top">:</td>
					<td valign="top"><?php
		echo $this->Form->input(
			"SUBJECT",
			array(
				"id" => "subject",
				"div" => false,
				"label" => false,
				"options" => $subject,
				"empty"=>'--Harap pilih--',
				   				"required"=>false,
			)
		);
	?></td>
				</tr>

				<tr>
					<td valign="top" width="200">Kategori Pesan</td>
					<td valign="top">:</td>
					<td valign="top"><?php
		echo $this->Form->input(
			"COMPLAINT_CATEGORY_ID",
			array(
				"id" => "complaint_category_id",
				"div" => false,
				"label" => false,
				"options" => $complaint_category_id,
				"empty"=>'--Harap pilih--',
				   				"required"=>false,
			)
		);
	?></td>
				</tr>
				<tr class="complaint">
					<td valign="top" width="200">Pesan ditujukan kepada kantor ACC</td>
					<td valign="top">:</td>
					<td valign="top"><?php
		echo $this->Form->input(
			"CD_SP_HANDLER",
			array(
				"id" => "cd_sp_handler",
				"div" => false,
				"label" => false,
				"options" => $cd_sp_handler,
				   				"required"=>false,
			)
		);
	?></td>
				<tr class="complaint">
					<td valign="top" width="200">Cabang Booking</td>
					<td valign="top">:</td>
					<td valign="top"><?php
		echo $this->Form->input(
			"DESC_CBG_BOOKING",
			array(
				"id" => "desc_cbg_booking",
				"div" => false,
				"label" => false,
				"options" => $desc_cbg_booking,
				   				"required"=>false,
			)
		);
	?></td>
				</tr>
				<tr>
					<td valign="top" width="200">Pesan / Komentar</td>
					<td valign="top">:</td>
					<td valign="top">
						<?php
		echo $this->Form->input(
			"MESSAGE_CONTENT",
			array(
				"type" => "textarea",
				"id" => "message_content",
				"cols" => "20",
				"rows" => "6",
				"class" => "text-field ui-widget-content ui-corner-all",
				"div" => false,
				"label" => false,
				   				"required"=>false,
				"style" => "width: 100%;"
			)
		);
	?>
					</td>
				</tr>
				<tr>
					<td valign="top" width="200">Dokumen Pendukung / Bukti</td>
					<td valign="top">:</td>
					<td valign="top">
						<?php
												echo $this->Form->input(
													"ATTACHMENT",
													array(
														"type" => "file",
														"id" => "attachment",
														"class" => "text-field ui-widget-content ui-corner-all",
														"div" => false,
														"label" => false,
														   				"required"=>false,
													)
												);
											?>

						<br />
						JPG maksimal berukuran 500 Kb</td>
				</tr>
				<tr>
					<td valign="top" width="120">&nbsp;</td>
					<td valign="top">&nbsp;</td>
					<td valign="top">
						<div class="g-recaptcha" data-sitekey="<?php echo Configure::read('Service.recaptcha_public_key') ?>"></div>
					</td>
				</tr>
				<tr>
					<td valign="top" width="200">&nbsp;</td>
					<td valign="top">&nbsp;</td>
					<td valign="top">
						<?php echo $this->Form->submit(
													"Submit",
													array(
														"id" => "form-submit",
														"class" => "button",
														"div" => false
													)
												);
											?>
					</td>
				</tr>
			</tbody>
		</table>
		<?php echo $this->Form->end(); ?>
		<!--/content detail-->
	</div>
	<!--/main content-->
</div>
<!--/content area-->
<script type="text/javascript">
	$(document).ready(function () {

		//$("#form-submit").button();
		$("#identity").change(function () {
			var identity = $(this).val();

			if (identity == "Pelanggan") {
				$(".pelanggan").show();
			} else {
				$(".pelanggan").hide();
			}
		});

		$("#recipient_id").change(function () {
			var value = $(this).val();
			if (value == 'saran' || value == 'pertanyaan') {
				$("#cd_sp_handler").hide()
				//$("#identity option[value='Non Pelanggan']").removeAttr('disabled');
				$("#cd_sp_handler").val('000000');
				$(".inquiry").show();
				$(".complaint").hide();
			} else {
				$("#cd_sp_handler").show();
				$("#identity").val("Pelanggan").change();
				//$("#identity option[value='Non Pelanggan']").attr('disabled','disabled');
				$(".inquiry").hide();
				$(".complaint").show();
			}
		});


		function getCustomerName() {
			if ($("#no_aggr").val().length != 17)
				return false;
			var type = $("#jenis_pelanggan").val();
			$("#name").val("Dicari...");
			$.ajax({
				"url": "/contacts/contacts/customer/&id=" + $("#no_aggr").val() + "&t=" + type,
				"success": function (data) {
					$("#name").val(data);
				},
				"error": function () {
					$("#name").val("");
				}
			});
		}


		$("#jenis_pelanggan").change(function () {
			getCustomerName();
		});

		$("#no_aggr").keyup(function () {
			getCustomerName();
		});

		$("#recipient_id").change();
		$("#jenis_pelanggan").change();
		$("#identity").change();

		$('form').submit(function () {
			$('input[type=submit]', this).attr('disabled', 'disabled');
		});
	});

</script>
<!--  -->
