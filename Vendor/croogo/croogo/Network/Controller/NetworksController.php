<?php
App::uses('NetworkAppController', 'Network.Controller');
/**
 * Networks Controller
 *
 * @property Network $Network
 * @property PaginatorComponent $Paginator
 */
class NetworksController extends NetworkAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Network->recursive = 0;
		$this->set('networks', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Network->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('network', 'network')));
		}
		$options = array('conditions' => array('Network.' . $this->Network->primaryKey => $id));
		$this->set('network', $this->Network->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Network->create();
			if ($this->Network->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('network', 'network')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->Network->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('network', 'network')), 'default', array('class' => 'error'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Network->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('network', 'network')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Network->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('network', 'network')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('network', 'network')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('Network.' . $this->Network->primaryKey => $id));
			$this->request->data = $this->Network->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Network->id = $id;
		if (!$this->Network->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('network', 'network')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Network->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('network', 'Network')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('network', 'Network')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}
}
