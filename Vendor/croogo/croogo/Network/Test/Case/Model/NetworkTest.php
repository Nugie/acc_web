<?php
App::uses('Network', 'Network.Model');

/**
 * Network Test Case
 */
class NetworkTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.network.network'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Network = ClassRegistry::init('Network.Network');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Network);

		parent::tearDown();
	}

}
