<?php
$this->viewVars['title_for_layout'] = __d('network', 'Networks');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('network', 'Networks'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('network_id'),
		$this->Paginator->sort('location'),
		$this->Paginator->sort('x_axis'),
		$this->Paginator->sort('y_axis'),
		$this->Paginator->sort('title'),
		$this->Paginator->sort('subtitle'),
		$this->Paginator->sort('description'),
		$this->Paginator->sort('tags'),
		$this->Paginator->sort('read_count'),
		$this->Paginator->sort('review_count'),
		$this->Paginator->sort('review_rates'),
		$this->Paginator->sort('published'),
		$this->Paginator->sort('active'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($networks as $network):
		$row = array();
		$row[] = h($network['Network']['network_id']);
		$row[] = h($network['Network']['location']);
		$row[] = h($network['Network']['x_axis']);
		$row[] = h($network['Network']['y_axis']);
		$row[] = h($network['Network']['title']);
		$row[] = h($network['Network']['subtitle']);
		$row[] = h($network['Network']['description']);
		$row[] = h($network['Network']['tags']);
		$row[] = h($network['Network']['read_count']);
		$row[] = h($network['Network']['review_count']);
		$row[] = h($network['Network']['review_rates']);
		$row[] = h($network['Network']['published']);
		$row[] = h($network['Network']['active']);
		$actions = array($this->Croogo->adminRowActions($network['Network']['network_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $network['Network']['network_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$network['Network']['network_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$network['Network']['network_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $network['Network']['network_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
