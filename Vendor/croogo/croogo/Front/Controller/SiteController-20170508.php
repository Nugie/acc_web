<?php
App::uses('FrontAppController', 'Front.Controller','Simulation.Model');



/**
 * Acps Controller
 *
 * @property Acp $Acp
 * @property PaginatorComponent $Paginator
 */
class SiteController extends FrontAppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->set('title_for_layout', __d('croogo', 'Kredit Mobil'));
		
		
		
		$this->loadModel('Network');
		$pointer_data = $this->Network->find(
			"all",
			array(
				"conditions" => array("Network.active" => "Yes", "Network.published" => "1"),
				"order" => "Network.network_id DESC",
				"recursive" => 2
			)
		);
		
		$this->loadModel('Node');
		$news = $this->Node->find(
			"all",
			array(
				"conditions" => array("Node.type" => array("news","corporatenews","events"), "Node.status" => "1", "Node.promote" => "1"),
				"order" => "Node.created DESC",
				"contain" => array(
								'Meta',
								'Taxonomy' => array(
									'Term',
									'Vocabulary',
								),
							),
				"limit"=>6,
			)
		);
		//print_r($news);
		$slides = $this->Node->find(
			"all",
			array(
				"conditions" => array("Node.type" => array("attachment"), "Node.status" => "1", "Node.terms" => "slide"),
				"order" => "Node.id DESC",
				
				"limit"=>6,
			)
		);
		
		$pakets = $this->Node->find(
			"all",
			array(
				"conditions" => array("Node.type" => array("attachment"), "Node.status" => "1", "Node.terms" => "paket"),
				"order" => "Node.id DESC",
				"limit"=>6,
			)
		);
		
		$products = $this->Node->find(
			"all",
			array(
				"conditions" => array("Node.type" => array("attachment"), "Node.status" => "1", "Node.terms" => "product"),
				"order" => "Node.id DESC",
				"limit"=>6,
			)
		);
		
		$promos = $this->Node->find(
			"all",
			array(
				"conditions" => array("Node.type" => array("attachment"), "Node.status" => "1", "Node.terms" => "promo"),
				"order" => "Node.id DESC",
				"limit"=>6,
			)
		);
		
		$this->loadModel('Brand');
		$this->set(
			"brand_list",
			$this->Brand->find(
				"list",
				array(
					"fields" => array("Brand.brand_id", "Brand.description"),
					"conditions"=>" Brand.Brand_id IN (SELECT Brand_id From vehicle_types)",
				"order" => "Brand.description ASC"
				)
			)
		);
		$this->set("group_list",array());
		
		/*
		$this->loadModel('VehicleType');
		$this->set(
					"group_list",
					$this->VehicleType->find(
						"list",
						array(
							"conditions" => array("VehicleType.brand_id" => ""),
							"fields" => array("VehicleType.code_group", "VehicleType.desc_group"),
							"order" => "VehicleType.desc_group ASC",
							"group" => "VehicleType.code_group"
						)
					)
				);
		*/
		//print_r($news);
		
		$this->set(compact('pointer_data', 'news','slides','products','promos','pakets'));
		
		
		
	}
}
