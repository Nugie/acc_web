<!--main slider-->
  <section id="mainslider">
    <ul class="bxslider">
  <?php foreach($slides as $slide) { ?>
      <li><img src="<?php echo $this->Html->url('/',true);?><?php echo $slide['Node']['path'] ?>" /></li>
  <?php } ?>

    </ul>
  </section>
  <!--/main slider-->

 <!--simulasi & paket-->
  <section id="tab-simulasipaket">
    <div class="section-title">
      <ul class="tabs" data-tabs id="simulasi-paket">
        <li class="tabs-title is-active"><a href="#simulasikredit" aria-selected="true">SIMULASI KREDIT</a></li>
        <li class="tabs-title"><a href="#paketkredit">PAKET KREDIT</a></li>
      </ul>
    </div>
    <!--content tab-->
    <div class="tabs-content" data-tabs-content="simulasi-paket">
        <div class="tabs-panel" id="paketkredit">
          <div class="expanded row small-collapse">
            <?php foreach($pakets as $row) { ?>
      <div class="small-4 columns"><a href="<?php echo $row['Node']['link'] ?>" target="_blank"><img src="<?php echo $this->Html->url('/', true);?><?php echo $row['Node']['path'] ?>" alt="<?php echo $row['Node']['title'] ?>" class="imgbanner" /></a></div>
    <?php } ?>

          </div>
        </div>
        <div class="tabs-panel is-active" id="simulasikredit">
          <!--tabsimulasi-->
          <div class="row columns simulasi-mobil" id="simulasi-mobil-baru" >
            <?php
      echo $this->Form->create(
        "CreditSimulation",
        array(
          "url" => "/simulasi-kredit-mobil",
          "name" => "credit-simulations-form",
          "id" => "credit-simulations-form",
          "method" => "post"
        )
      );

      echo $this->Form->input(
              "type_id",
              array(
                "type" => "hidden",
                "id" => "type_id",
                "label" => false
              )
            );
            echo $this->Form->input(
              "year_of_manufactured",
              array(
                "type" => "hidden",
                "id" => "year_of_manufactured",
                "label" => false,
                "value"=> date("Y")
              )
            );
            echo $this->Form->input(
              "vehicle_status",
              array(
                "type" => "hidden",
                "id" => "vehicle_status",
                "label" => false,
                "value"=> "New"
              )
            );
            echo $this->Form->input(
              "acp_status",
              array(
                "type" => "hidden",
                "id" => "acp_status",
                "label" => false,
                "value"=> "Yes"
              )
            );
    ?>

            <div class="formholder mb-10">
                <label>Pilihan Mobil</label>
                <div class="select-style">
                  <select name="kondisi" id="kondisi" class="kondisi">
                    <option value="baru">BARU</option>
                    <option value="bekas">BEKAS</option>
                </select>
                </div>
            </div>
            <div class="formholder mb-10">
                <label>Merk</label>
                <div class="select-style">
                  <?php
                    echo $this->Form->input(
                        "brand_id",
                        array(
                            "id" => "brand_id",
            "class" => "input-baru",
                            "div" => false,
                            "label" => false,
                            "options" => $brand_list
                        )
                    );
                ?>
              </div>
            </div>
            <div class="formholder mb-10">
                <label>Type</label>
                <div class="select-style">
                <?php
                    echo $this->Form->input(
                        "code_group",
                        array(
                            "id" => "code_group",
            "class" => "input-baru",
                            "div" => false,
                            "label" => false,
                            "options" => $group_list
                        )
                    );
                ?> <img id="vehicle-group-loading" src="/img/ajax-loader.gif" alt="loading" style="display: none;">
              </div>
            </div>
            <div class="formholder mb-10">
                <label>Harga Mobil</label>
                <?php
                    echo $this->Form->input(
                        "otr",
                        array(
                            "type" => "text",
                            "id" => "otr_display",
            "placeholder"=>"Harga",
                            "class" => "simulasi",
                            "div" => false,
            'required' => true,
                            "label" => false
                        )
                    );
					 echo $this->Form->input(
                        "otr",
                        array(
                            "type" => "hidden",
                            "id" => "otr",
                            "label" => false
                        )
                    );
                ?>
            </div>
            <div class="formholder mb-10">
                <label>DP</label>
                <?php
                    echo $this->Form->input(
                        "down_payment",
                        array(
                            "type" => "text",
                            "id" => "down_payment_display",
                            "class" => "simulasi",
                            "div" => false,
            "placeholder"=>"Minimal 25%",
            'required' => true,
                            "label" => false
                        )
                    );
					 echo $this->Form->input(
                        "down_payment",
                        array(
                            "type" => "hidden",
                            "id" => "down_payment",
                            "label" => false
                        )
                    );
                ?>
            </div>
            <div class="buttonholder">
<label class="show-for-small-only">&nbsp;</label>
                <?php
                    echo $this->Form->submit(
                        "Simulasikan",
                        array(
                            "id" => "form-submit",
                            "class" => "button",
                            "div" => false
                        )
                    );
                ?>
            </div>
           <?php echo $this->Form->end(); ?>
          </div>
          <div class="clearfix"></div>
          <!--/tabsimulasi-->

     <!--tabsimulasi-->
          <div class="row columns simulasi-mobil" id="simulasi-mobil-bekas" style="display:none;" >
            <?php
      echo $this->Form->create(
        "CreditSimulation",
        array(
          "url" => "/simulasi-kredit-mobil2",
          "name" => "credit-simulations-form",
          "id" => "credit-simulations-form2",
          "method" => "post"
        )
      );

      echo $this->Form->input(
              "type_id",
              array(
                "type" => "hidden",
                "id" => "type_id",
                "label" => false
              )
            );

            echo $this->Form->input(
              "vehicle_status",
              array(
                "type" => "hidden",
                "id" => "vehicle_status",
                "label" => false,
                "value"=> "Used"
              )
            );
            echo $this->Form->input(
              "acp_status",
              array(
                "type" => "hidden",
                "id" => "acp_status",
                "label" => false,
                "value"=> "Yes"
              )
            );
    ?>

            <div class="formholder mb-10">
                <label>Pilihan Mobil</label>
                <div class="select-style-smaller">
                  <select name="kondisi" id="kondisi" class="kondisi">
                    <option value="baru">BARU</option>
                    <option value="bekas">BEKAS</option>
                </select>
                </div>
            </div>
            <div class="formholder mb-10">
                <label>Merk</label>
                <div class="select-style">
                  <?php
                    echo $this->Form->input(
                        "brand_id",
                        array(
                            "id" => "brand_id-b",
            "class" => "input-bekas",
                            "div" => false,
                            "label" => false,
                            "options" => $brand_list
                        )
                    );
                ?>
              </div>
            </div>
            <div class="formholder mb-10">
                <label>Type</label>
                <div class="select-style">
                <?php
                    echo $this->Form->input(
                        "code_group",
                        array(
                            "id" => "code_group-b",
            "class" => "input-baru",
                            "div" => false,
                            "label" => false,
                            "options" => $group_list
                        )
                    );
                ?> <img id="vehicle-group-loading-b" src="/img/ajax-loader.gif" alt="loading" style="display: none;">
              </div>
            </div>
     <div class="formholder mb-10">
                <label>Tahun</label>
                <?php
                    echo $this->Form->input(
                        "year_of_manufactured",
                        array(
                            "type" => "number",
                            "id" => "year_of_manufactured-b",
            "placeholder"=>"Tahun",
                            "class" => "simulasi-smaller",
                            "div" => false,
            "size" => 5,
            "maxlength" => 4,
            'required' => true,
                            "label" => false
                        )
                    );
                ?>
            </div>
            <div class="formholder mb-10">
                <label>Harga Mobil</label>
                <?php
                    echo $this->Form->input(
                        "otr",
                        array(
                            "type" => "text",
                            "id" => "otr-b_display",
							"placeholder"=>"Harga",
                            "class" => "simulasi",
                            "div" => false,
            'required' => true,
						
                            "label" => false
                        )
                    );
					echo $this->Form->input(
                        "otr",
                        array(
                            "type" => "hidden",
                            "id" => "otr-b",
                            "label" => false
                        )
                    );
			
                ?>
            </div>
            <div class="formholder mb-10">
                <label>DP</label>
                <?php
                    echo $this->Form->input(
                        "down_payment",
                        array(
                            "type" => "text",
                            "id" => "down_payment-b_display",
                            "class" => "simulasi",
                            "div" => false,
            "placeholder"=>"Minimal 25%",
            'required' => true,
                            "label" => false
                        )
                    );
					echo $this->Form->input(
                        "down_payment",
                        array(
                            "type" => "hidden",
                            "id" => "down_payment-b",
                            "label" => false
                        )
                    );
                ?>
            </div>
            <div class="buttonholder">
<label class="show-for-small-only">&nbsp;</label>
                <?php
                    echo $this->Form->submit(
                        "Simulasikan",
                        array(
                            "id" => "form-submit-bekas",
                            "class" => "button",
                            "div" => false
                        )
                    );
                ?>
            </div>
           <?php echo $this->Form->end(); ?>
          </div>
          <div class="clearfix"></div>
          <!--/tabsimulasi-->
        </div>
      </div>
  </section>
  <!--/content tab-->
  <!--/simulasi & paket-->

  <!--produk & layanan-->
  <section id="produklayanan">
    <div class="section-title mb-20">
      Produk &amp; Layanan
    </div>
    <!--content tab-->
    <div class="expanded row" id="produklayanan-content">
      <div class="slider1">
  <?php foreach($products as $product) { ?>
        <div>
            <a href="<?php echo $product['Node']['link'] ?>"><img src="<?php echo $this->Html->url('/', true);?><?php echo $product['Node']['path'] ?>" alt="<?php echo $product['Node']['title'] ?>" width="425" /></a>
            <h2 class="product-caption"><a href="<?php echo $product['Node']['link'] ?>"><?php echo $product['Node']['title'] ?></a></h2>
            <p><?php echo $product['Node']['excerpt'] ?></p>
            <p class="link-more"><a href="<?php echo $product['Node']['link'] ?>">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
        </div>
  <?php } ?>

      </div>
    </div>
  </section>
  <!--/produk & layanan-->
  <!--info promo-->
  <section id="infopromo">
    <div class="section-title mb-20">
      INFO &amp; PROMO ACC
    </div>
    <div class="expanded row">
      <div class="sliderpromo">
  <?php foreach ($promos as $promo) { ?>
    <div><a href="<?php echo $promo['Node']['link'] ?>"><img src="<?php echo $this->Html->url('/', true);?><?php echo $promo['Node']['path'] ?>" alt="<?php echo $promo['Node']['title'] ?>" /></a></div>
  <?php } ?>

      </div>
    </div>
  </section>
  <!--/info promo-->
  <!--info promo-->
  <section id="jaringanacc" class="mb-20">
    <div class="section-title mb-20">
      JARINGAN ACC
    </div>
    <div class="expanded row">
      <div class="small-12 medium-4 large-4 columns mb-20">
          <h2>LEBIH MUDAH DENGAN ACC YES!</h2>
          <p class="text-14">Unduh ACC YES! sekarang juga!</p>
          <div>
            <img src="/images/img-accyes.jpg" alt="ACC YES" usemap="#accyes" class="hide-for-small-only" />
            <img src="/images/img-accyes.jpg" alt="ACC YES" usemap="#accyes2" class="show-for-small-only" />
            <map name="accyes" id="accyes">
              <area shape="rect" coords="124,354,260,402" href="https://itunes.apple.com/id/app/accyes!/id1129623253?mt=8" target="_blank" alt="ACCYES! on AppStore" />
              <area shape="rect" coords="264,353,403,400" href="https://play.google.com/store/apps/details?id=com.ACCYes" target="_blank" alt="ACCYes! on google play" />
            </map>
            <map name="accyes2" id="accyes2">
              <area shape="rect" coords="116,308,202,344" href="https://itunes.apple.com/id/app/accyes!/id1129623253?mt=8" target="_blank" alt="ACCYES! on AppStore" />
              <area shape="rect" coords="233,305,322,348" href="https://play.google.com/store/apps/details?id=com.ACCYes" target="_blank" alt="ACCYes! on google play" />
            </map>
          </div>
      </div>
      <div class="small-12 medium-8 large-8 columns">
          <h2>JARINGAN ACC DI SELURUH INDONESIA</h2>
          <p>ACC mendukung penjualan mobil melalui jaringan dealer, showroom maupun perseorangan di seluruh wilayah Indonesia. Jaringan ACC tersebar di hampir seluruh kota besar di Indonesia. Saat ini ACC memiliki 73 kantor cabang yang tersebar di 59 kota di Indonesia, dan akan terus bertambah.  </p>
          <div id="network-map" class="hide-for-small-only">
             <img src="/files/template_images/map_indonesia.jpg" alt="Peta Indonesia" border="0" width="700" height="332" class="image" />
    <?php foreach ($pointer_data as $tmp) : ?>
    <a href="/network/view/<?php echo $tmp["Network"]["network_id"]; ?>/<?php echo Inflector::slug(strtolower($tmp["Network"]["location"])); ?>" title="<?php echo $tmp["Network"]["location"]; ?>"><span class="pointer" style="left: <?php echo $tmp["Network"]["x_axis"]; ?>px; top: <?php echo $tmp["Network"]["y_axis"]; ?>px;"></span></a>
    <?php endforeach; ?>
          </div>
            <div class="show-for-small-only">
               <a href="/network" title="Jaringan ACC"><img src="/images/img-network.jpg" alt="Peta Indonesia" border="0" id="imgnetwork-d" /></a>
            </div>

      </div>
    </div>
  </section>
  <!--/info promo-->
  <!--berita-->
  <section id="berita">
    <div class="section-title mb-20">
        BERITA
    </div>
    <div class="expanded row" id="berita-content">
      <div class="slider2">
  <?php
  //print_r($news);
  foreach($news as $new) {?>
        <div>
    <?php if (isset($new['Multiattach'][0])){ ?>
          <?php
//echo $node['Multiattach'][0]['Multiattach']['filename'];
          if ($new['Multiattach'][0]['Multiattach']['mime']=="application/json"){

          ?>
            <a href="<?php echo $this->Html->url('/'. $new['Node']['type'].'/read/', true) .$new['Node']['slug'] ?>"><img src="<?php echo $new['Multiattach'][0]['Multiattach']['filename'] ?>" alt="<?php if (isset($new['Multiattach'][0]['Multiattach']['metaDisplay'])) echo $new['Multiattach'][0]['Multiattach']['metaDisplay']?>" width="400" /></a>
          <?php } else { ?>
              <?php $str = $this->Html->url('/', true) . str_replace("webroot","",$new['Multiattach'][0]['Multiattach']['real_filename']);
                //echo $str;
              if (@getimagesize($str)){ ?>
                <a href="<?php echo  $this->Html->url('/'. $new['Node']['type'] .'/read/', true) . $new['Node']['slug'] ?>"><img src="<?php echo $str ?>" alt="<?php if (isset($new['Multiattach'][0]['Multiattach']['metaDisplay'])) echo $new['Multiattach'][0]['Multiattach']['metaDisplay']?>" width="400" /></a>
              <?php } ?>
          <?php }  ?>
        <?php } ?>
      <?php
		//print_r($new);
        $date=date_create($new['Node']['created']);
        $obj = json_decode($new['Node']['terms']);
        $term = "";
        foreach ($obj as $key => $value){
          $term = $value ." - ";
        }

      ?>
     <div class="article-tags"><?php if (isset($new['Taxonomy'][0]['Term']['title'])) echo $new['Taxonomy'][0]['Term']['title'] ." - "?> <?php echo date_format($date,"d M Y"); ?></div>
            <h2><a href="<?php echo  $this->Html->url('/'. $new['Node']['type'] .'/read/', true) .$new['Node']['slug'] ?>"><?php echo $new['Node']['title'] ?></a></h2>
            <p class="mb-5"><?php echo $new['Node']['excerpt'] ?></p>
            <p class="link-more"><a href="<?php echo  $this->Html->url('/'. $new['Node']['type'] .'/read/', true) .$new['Node']['slug'] ?>">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
        </div>
  <?php } ?>

      </div>
    </div>
  </section>
  <!--/berita-->

  <script>
$(document).ready(function(){
  var textbox = '#otr_display';
  var hidden = '#otr';
   $(textbox).keypress(function (e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
    }
  });
  $(textbox).keyup(function (e) {
	var num = $(textbox).val();
	var comma = /,/g;
    num = num.replace(comma,'');
	num = num.replace('Rp','');
	num = num.replace(' ','');
    $(hidden).val(num);
    var numCommas = addCommas(num);
    $(textbox).val(numCommas);
	var dp = 0;
	if ($("#otr").val()!=''){
	  dp = $("#otr").val()*25/100;
    }
	$("#down_payment").val(dp);
	$("#down_payment_display").val(addCommas(dp));
  });
  
  var textbox2 = '#otr-b_display';
  var hidden2 = '#otr-b';
  $(textbox2).keypress(function (e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
    }
  });
  $(textbox2).keyup(function (e) {
    var num = $(textbox2).val();
    var comma = /,/g;
    num = num.replace(comma,'');
	num = num.replace('Rp','');
	num = num.replace(' ','');
    $(hidden2).val(num);
    var numCommas = addCommas(num);
    $(textbox2).val(numCommas);
	var dp = 0;
	if ($("#otr-b").val()!=''){
	  dp = $("#otr-b").val()*25/100;
    }
	$("#down_payment-b").val(dp);
	$("#down_payment-b_display").val(addCommas(dp));
  });
  
  var textboxdp = '#down_payment_display';
  var hiddendp = '#down_payment';
   $(textboxdp).keypress(function (e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
    }
  });
  $(textboxdp).keyup(function (e) {
	var num = $(textboxdp).val();
	var comma = /,/g;
    num = num.replace(comma,'');
	num = num.replace('Rp','');
	num = num.replace(' ','');
    $(hiddendp).val(num);
    var numCommas = addCommas(num);
    $(textboxdp).val(numCommas);
  });
  var textboxdp2 = '#down_payment-b_display';
  var hiddendp2 = '#down_payment-b';
   $(textboxdp2).keypress(function (e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         return false;
    }
  });
  $(textboxdp2).keyup(function (e) {
	var num = $(textboxdp2).val();
	var comma = /,/g;
    num = num.replace(comma,'');
	num = num.replace('Rp','');
	num = num.replace(' ','');
    $(hiddendp2).val(num);
    var numCommas = addCommas(num);
    $(textboxdp2).val(numCommas);
  });
  
  
  
	/*
  $("#otr").focusout(function() {
	
    if ($("#otr").val()!=''){
      var dp = $("#otr").val()*25/100;
      $("#down_payment").val(dp);
    }
  });
  $("#otr-b").focusout(function() {
    if ($("#otr-b").val()!=''){
      var dp = $("#otr-b").val()*25/100;
      $("#down_payment-b").val(dp);
    }
  });
  */
  function load_vehicle_group()
  {
    $("#vehicle-group-loading-b").show();

    var brand_id = $("#brand_id-b").val();
    $.get("/simulation/credit/query_vehicle_group/" + brand_id, function(data) {
      data = $.trim(data);
      $("#code_group-b").empty().html(data);
      $("#vehicle-group-loading-b").hide();

    });
  }
  function load_vehicle_group_new()
  {
    $("#vehicle-group-loading").show();

    var brand_id = $("#brand_id").val();
    $.get("/simulation/credit/query_vehicle_group_new/" + brand_id, function(data) {
      data = $.trim(data);
      $("#code_group").empty().html(data);
      $("#vehicle-group-loading").hide();

    });
  }
  $("#brand_id-b").change(function(){
    load_vehicle_group();
  });

  $("#credit-simulations-form").submit(function(){
    var dp = $('#down_payment').val();
    var min_dp = $("#otr").val()*25/100;
	 if ($("#otr").val()=='' || $("#otr").val()==0){
		alert('Harga mobil harus diisi');
		$("#otr_display").focus();
		return false;
	 }
    if (dp < min_dp){
      alert('Minimal DP adalah 25% dari harga mobil');
      $('#down_payment_display').focus();
      return false;
    }
    return true;
  });
  $("#credit-simulations-form2").submit(function(){
    var dp = $('#down_payment-b').val();
    var min_dp = $("#otr-b").val()*25/100;
	if ($("#otr-b").val()=='' ||  $("#otr-b").val()==0 ){
		alert('Harga mobil harus diisi');
		$("#otr-b_display").focus();
		return false;
	 }
    if (dp < min_dp){
      alert('Minimal DP adalah 25% dari harga mobil');
      $('#down_payment-b_display').focus();
      return false;
    }
    return true;
  });
  $("#brand_id").change(function(){
    load_vehicle_group_new();
  });
  load_vehicle_group();
  load_vehicle_group_new();
  $('.kondisi').change(function () {
    $('.kondisi').val($(this).val());
    $('#otr').val('');
    $('#otr-b').val('');
    $('#down_payment').val('');
    $('#down_payment-b').val('');
    $('.simulasi-mobil').toggle('slow');
  });
});
function addCommas(nStr) {
  nStr += '';
  var comma = /,/g;
  nStr = nStr.replace(comma,'');
  nStr = nStr.replace('Rp','');
  nStr = nStr.replace(' ','');
  
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return 'Rp '+ x1 + x2;
}
</script>
