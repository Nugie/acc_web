<?php
App::uses('AccLib', 'Members.Lib');

class WebServiceComponent extends Component
{
	var $controller;
	var $components = array();
	var $acc_lib;

	// called before Controller::beforeFilter()
	function initialize(Controller $controller, $settings=array())
	{
		//App::import('Vendor', 'acc_lib');

		//App::uses('AccLib');

		$this->controller = $controller;
		$this->acc_lib = new AccLib();
	}

	// called after Controller::beforeFilter(), but before the controller executes the current action handler.
	function startup(Controller $controller)
	{
	}

	// called after the controller executes the requested action's logic but before the controller's renders views and layout.
	function beforeRender(Controller $controller)
	{
	}

	// called after Controller::render() and before the output is printed to the browser
	function shutdown(Controller $controller)
	{
	}

	function _get_web_service_data($operation, $params)
	{
		//$url = "http://" . WEBSERVICE_HOST . "/web_services/" . $operation . "/" . $this->_generate_url_params($params);
		//$url = "http://" . Configure::read('SVC.url') . "/web_services/" . $operation . "/" . $this->_generate_url_params($params);
		$url = "http://172.16.34.28/web_services/" . $operation . "/" . $this->_generate_url_params($params);
		$content = "";
		$handle = @fopen($url, "r");

		 //print_r($url.'<br />');
		 //print_r('Content Handle: '.$handle);

		if ($handle)
		{
			while (!feof($handle)) {
				$content .= fread($handle, 8192);
				 //print_r('<br />test '.$content);
			}
			fclose($handle);
		}
		//echo phpinfo();
		//print_r('YUK: '.$content);
		//print_r($this->acc_lib);
		if (strlen(trim($content)) > 0)
			$content = $this->acc_lib->decrypt($content);

		//print_r('<br />eah: '.$content);
		return $this->_parse_xml($content);
	}

	function _parse_xml($data)
	{
		$result = array(
			"modelName" => "",
			"recordCount" => 0,
			"error" => "",
			"rowsData" => array()
		);
		$data = trim($data);

		 //var_dump($data);

		if (strlen($data) > 0)
		{
			$data = explode("\n", $data);

			$xml_started = false;
			$data_started = false;
			$row_started = false;
			$row_index = 0;
			$row_data = array();

			foreach ($data as $tmp)
			{
				$tmp = trim($tmp);

				if (strlen($tmp) == 0)
					continue;
				elseif ($tmp == "<?xml version=\"1.0\" encoding=\"utf-8\"?>")
					$xml_started = true;
				elseif ($tmp == "<dataset>")
					$data_started = true;

				elseif ($data_started && (strpos($tmp, "<modelName>") === 0) && (strpos($tmp, "</modelName>") !== 0))
				{
					$tmp = str_replace("<modelName>", "", $tmp);
					$tmp = str_replace("</modelName>", "", $tmp);
					$result["modelName"] = $tmp;
				}
				elseif ($data_started && (strpos($tmp, "<error>") === 0) && (strpos($tmp, "</error>") !== 0))
				{
					$tmp = str_replace("<error>", "", $tmp);
					$tmp = str_replace("</error>", "", $tmp);
					$result["error"] = $tmp;
				}
				elseif ($data_started && (strpos($tmp, "<errorMessage>") === 0) && (strpos($tmp, "</errorMessage>") !== 0))
				{
					$tmp = str_replace("<errorMessage>", "", $tmp);
					$tmp = str_replace("</errorMessage>", "", $tmp);
					$result["error"] = $tmp;
				}
				elseif ($data_started && (strpos($tmp, "<recordCount>") === 0) && (strpos($tmp, "</recordCount>") !== 0))
				{
					$tmp = str_replace("<recordCount>", "", $tmp);
					$tmp = str_replace("</recordCount>", "", $tmp);
					$result["recordCount"] = $tmp;
				}
				elseif ($tmp == "<row>")
					$row_started = true;
				elseif ($tmp == "</row>")
				{
					$result["rowsData"][$row_index] = $row_data;

					$row_started = false;
					$row_index++;
					$row_data = array();
				}
				elseif ($row_started)
				{
					$field_name = substr($tmp, 1, strpos($tmp, ">", 0)-1);
					$vstart = strpos($tmp, "<![CDATA[")+9;
					$field_value = substr(
						$tmp,
						$vstart,
						strpos($tmp, "]]></")-$vstart
					);

					$row_data[$field_name] = $field_value;

					//echo "field_name : {$field_name}\n<br />\n";
				}


				elseif ($tmp == "</dataset>")
					$data_started = false;


			}
			//print_r($tmp);
		}
		else
			// print_r('Data: '. $data);
			$result["error"] = "Gagal melakukan koneksi dengan server.";
		//print_r($result);
		//exit;
		return $result;
	}

	function _generate_url_params($params)
	{
		$result = "?session_id=" . $this->acc_lib->getSession();


		foreach ($params as $key => $value)
			$result .= "&" . $key . "=" . $value;

		return $result;
	}
}

?>
