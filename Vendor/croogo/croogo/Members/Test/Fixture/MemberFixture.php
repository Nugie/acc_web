<?php
/**
 * Member Fixture
 */
class MemberFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'member_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 15, 'unsigned' => true, 'key' => 'primary'),
		'username' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 16, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 32, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cd_customer' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 16, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'no_ktp' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'nama' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'alamat' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'rt' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 3, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'rw' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 3, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'kelurahan' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'kecamatan' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'city' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'postcode' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 6, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'telephone' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'fax' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'npwp' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'mtr_maiden' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'placeofbirth' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'dateofbirth' => array('type' => 'date', 'null' => false, 'default' => null),
		'religion' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'nationality' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'jml_tanggungan' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 3, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'education' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'job' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'residence' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'verification_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'no_aggr' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 17, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'nopol' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'member_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'member_id' => '',
			'username' => 'Lorem ipsum do',
			'password' => 'Lorem ipsum dolor sit amet',
			'cd_customer' => 'Lorem ipsum do',
			'no_ktp' => 'Lorem ipsum dolor ',
			'nama' => 'Lorem ipsum dolor sit amet',
			'alamat' => 'Lorem ipsum dolor sit amet',
			'rt' => 'L',
			'rw' => 'L',
			'kelurahan' => 'Lorem ipsum dolor ',
			'kecamatan' => 'Lorem ipsum dolor ',
			'city' => 'Lorem ipsum dolor ',
			'postcode' => 'Lore',
			'telephone' => 'Lorem ipsum dolor ',
			'fax' => 'Lorem ipsum dolor ',
			'npwp' => 'Lorem ipsum dolor sit amet',
			'mtr_maiden' => 'Lorem ipsum dolor sit amet',
			'placeofbirth' => 'Lorem ipsum dolor ',
			'dateofbirth' => '2016-12-25',
			'religion' => 'Lorem ip',
			'nationality' => 'Lorem ipsum dolor sit amet',
			'jml_tanggungan' => 'L',
			'education' => 'Lorem ip',
			'job' => 'Lorem ipsum dolor sit amet',
			'residence' => 'Lorem ipsum dolor ',
			'email' => 'Lorem ipsum dolor sit amet',
			'verification_code' => 'Lorem ipsum dolor sit amet',
			'no_aggr' => 'Lorem ipsum dol',
			'nopol' => 'Lorem ip',
			'created' => '2016-12-25 04:24:50',
			'modified' => '2016-12-25 04:24:50'
		),
	);

}
