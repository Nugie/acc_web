<?php
$this->viewVars['title_for_layout'] = __d('members', 'Members');
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('members', 'Members'), array('action' => 'index'));

if ($this->action == 'admin_edit') {
	$this->Html->addCrumb($this->request->data['Member']['member_id'], '/' . $this->request->url);
	$this->viewVars['title_for_layout'] = __d('members', 'Members') . ': ' . $this->request->data['Member']['member_id'];
} else {
	$this->Html->addCrumb(__d('croogo', 'Add'), '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('Member'));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('members', 'Member'), '#member');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('member');

		echo $this->Form->input('member_id');

		echo $this->Form->input('username', array(
			'label' =>  __d('members', 'Username'),
		));
		echo $this->Form->input('password', array(
			'label' =>  __d('members', 'Password'),
		));
		echo $this->Form->input('member_status', array(
			'label' =>  __d('members', 'Member Status'),
		));
		echo $this->Form->input('cd_customer', array(
			'label' =>  __d('members', 'Cd Customer'),
		));
		echo $this->Form->input('customer_type', array(
			'label' =>  __d('members', 'Customer Type'),
		));
		echo $this->Form->input('no_ktp', array(
			'label' =>  __d('members', 'No Ktp'),
		));
		echo $this->Form->input('nama', array(
			'label' =>  __d('members', 'Nama'),
		));
		echo $this->Form->input('alamat', array(
			'label' =>  __d('members', 'Alamat'),
		));
		echo $this->Form->input('rt', array(
			'label' =>  __d('members', 'Rt'),
		));
		echo $this->Form->input('rw', array(
			'label' =>  __d('members', 'Rw'),
		));
		echo $this->Form->input('kelurahan', array(
			'label' =>  __d('members', 'Kelurahan'),
		));
		echo $this->Form->input('kecamatan', array(
			'label' =>  __d('members', 'Kecamatan'),
		));
		echo $this->Form->input('city', array(
			'label' =>  __d('members', 'City'),
		));
		echo $this->Form->input('postcode', array(
			'label' =>  __d('members', 'Postcode'),
		));
		echo $this->Form->input('telephone', array(
			'label' =>  __d('members', 'Telephone'),
		));
		echo $this->Form->input('fax', array(
			'label' =>  __d('members', 'Fax'),
		));
		echo $this->Form->input('npwp', array(
			'label' =>  __d('members', 'Npwp'),
		));
		echo $this->Form->input('mtr_maiden', array(
			'label' =>  __d('members', 'Mtr Maiden'),
		));
		echo $this->Form->input('placeofbirth', array(
			'label' =>  __d('members', 'Placeofbirth'),
		));
		echo $this->Form->input('dateofbirth', array(
			'label' =>  __d('members', 'Dateofbirth'),
		));
		echo $this->Form->input('religion', array(
			'label' =>  __d('members', 'Religion'),
		));
		echo $this->Form->input('nationality', array(
			'label' =>  __d('members', 'Nationality'),
		));
		echo $this->Form->input('marital', array(
			'label' =>  __d('members', 'Marital'),
		));
		echo $this->Form->input('jml_tanggungan', array(
			'label' =>  __d('members', 'Jml Tanggungan'),
		));
		echo $this->Form->input('education', array(
			'label' =>  __d('members', 'Education'),
		));
		echo $this->Form->input('job', array(
			'label' =>  __d('members', 'Job'),
		));
		echo $this->Form->input('residence', array(
			'label' =>  __d('members', 'Residence'),
		));
		echo $this->Form->input('email', array(
			'label' =>  __d('members', 'Email'),
		));
		echo $this->Form->input('verified', array(
			'label' =>  __d('members', 'Verified'),
		));
		echo $this->Form->input('verification_code', array(
			'label' =>  __d('members', 'Verification Code'),
		));
		echo $this->Form->input('no_aggr', array(
			'label' =>  __d('members', 'No Aggr'),
		));
		echo $this->Form->input('nopol', array(
			'label' =>  __d('members', 'Nopol'),
		));

	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'primary')) .
		$this->Form->button(__d('croogo', 'Save & New'), array('button' => 'success', 'name' => 'save_and_new')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('button' => 'danger'));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
