<?php
$this->viewVars['title_for_layout'] = __d('members', 'Members');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('members', 'Members'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('member_id'),
		$this->Paginator->sort('username'),
		$this->Paginator->sort('password'),
		$this->Paginator->sort('member_status'),
		$this->Paginator->sort('cd_customer'),
		$this->Paginator->sort('customer_type'),
		$this->Paginator->sort('no_ktp'),
		$this->Paginator->sort('nama'),
		$this->Paginator->sort('alamat'),
		$this->Paginator->sort('rt'),
		$this->Paginator->sort('rw'),
		$this->Paginator->sort('kelurahan'),
		$this->Paginator->sort('kecamatan'),
		$this->Paginator->sort('city'),
		$this->Paginator->sort('postcode'),
		$this->Paginator->sort('telephone'),
		$this->Paginator->sort('fax'),
		$this->Paginator->sort('npwp'),
		$this->Paginator->sort('mtr_maiden'),
		$this->Paginator->sort('placeofbirth'),
		$this->Paginator->sort('dateofbirth'),
		$this->Paginator->sort('religion'),
		$this->Paginator->sort('nationality'),
		$this->Paginator->sort('marital'),
		$this->Paginator->sort('jml_tanggungan'),
		$this->Paginator->sort('education'),
		$this->Paginator->sort('job'),
		$this->Paginator->sort('residence'),
		$this->Paginator->sort('email'),
		$this->Paginator->sort('verified'),
		$this->Paginator->sort('verification_code'),
		$this->Paginator->sort('no_aggr'),
		$this->Paginator->sort('nopol'),
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($members as $member):
		$row = array();
		$row[] = h($member['Member']['member_id']);
		$row[] = h($member['Member']['username']);
		$row[] = h($member['Member']['password']);
		$row[] = h($member['Member']['member_status']);
		$row[] = h($member['Member']['cd_customer']);
		$row[] = h($member['Member']['customer_type']);
		$row[] = h($member['Member']['no_ktp']);
		$row[] = h($member['Member']['nama']);
		$row[] = h($member['Member']['alamat']);
		$row[] = h($member['Member']['rt']);
		$row[] = h($member['Member']['rw']);
		$row[] = h($member['Member']['kelurahan']);
		$row[] = h($member['Member']['kecamatan']);
		$row[] = h($member['Member']['city']);
		$row[] = h($member['Member']['postcode']);
		$row[] = h($member['Member']['telephone']);
		$row[] = h($member['Member']['fax']);
		$row[] = h($member['Member']['npwp']);
		$row[] = h($member['Member']['mtr_maiden']);
		$row[] = h($member['Member']['placeofbirth']);
		$row[] = h($member['Member']['dateofbirth']);
		$row[] = h($member['Member']['religion']);
		$row[] = h($member['Member']['nationality']);
		$row[] = h($member['Member']['marital']);
		$row[] = h($member['Member']['jml_tanggungan']);
		$row[] = h($member['Member']['education']);
		$row[] = h($member['Member']['job']);
		$row[] = h($member['Member']['residence']);
		$row[] = h($member['Member']['email']);
		$row[] = h($member['Member']['verified']);
		$row[] = h($member['Member']['verification_code']);
		$row[] = h($member['Member']['no_aggr']);
		$row[] = h($member['Member']['nopol']);
		$row[] = $this->Time->format($member['Member']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = $this->Time->format($member['Member']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($member['Member']['member_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $member['Member']['member_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$member['Member']['member_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$member['Member']['member_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $member['Member']['member_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
