<?php
App::uses('SimulationAppController', 'Simulation.Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Acps Controller
 *
 * @property Acp $Acp
 * @property PaginatorComponent $Paginator
 */
class CreditController extends SimulationAppController {


	public $components = array('Security');

	var $uses = array(
			"Nodes.Node","Simulation.VehicleType", "Simulation.Acp","Simulation.Brand","Simulation.Insurance",
			"Simulation.NewRate","Simulation.UsedRate","Simulation.Administration",
	);



	function apply($tenor=1)
	{
		  /*
		  $this->member_data = $this->Member->find(
				"first",
				array(
					"conditions" => array("Member.member_id" => $this->Session->read("Member.member_id")),
					"order" => "Member.member_id ASC",
					"limit" => 1
				)
			);
			*/
			$status = 0;
			$this->_build_sim_data($this->Session->read("Member.personal_id"),$tenor);
			$this->loadModel('TrnSimKredit');
			//print_r($this->sim_data);
			 // $this->CreditSimulationCrs->create($this->sim_data);
			if ($this->TrnSimKredit->save($this->sim_data)){
				 $no_trans = $this->TrnSimKredit->id;
				 $personal_id = $this->Session->read("Member.personal_id");
				 // ---------------------------------------------------------------------------------------------------------
				 //  Submit web pull data
				 // ---------------------------------------------------------------------------------------------------------
				 $this->TrnSimKredit->query(
					 "INSERT INTO `tbl_web_pull_data` SET `no_trans`='{$no_trans}', `personal_id`='{$personal_id}', `dt_process`='0000-00-00 00:00:00', `dt_entered`=NOW(), `flag_distribute`=''"
				 );
				 // ---------------------------------------------------------------------------------------------------------
				 //  Submit bucket to call
				 // ---------------------------------------------------------------------------------------------------------
				 $this->TrnSimKredit->query(
					 "INSERT INTO `tbl_bucket_tocall_web` SET `no_trans`='{$no_trans}', `date_to_call`='0000-00-00', `personal_id`='{$personal_id}'"
				 );
				 $this->_send_email();
				 $this->_send_email_customer();
				 $this->set("msg", "Terima kasih, aplikasi simulasi kredit anda telah terkirim.<br />Kami akan memberikan respon kepada anda secepatnya.");
				$status = 1;
			}else{
				$status = 0;
				$this->set("msg", "Error : Maaf aplikasi simulasi kredit anda tidak terkirim");
			}





	}

		function _build_sim_data($cust_id,$tenor)
	{

		$amt_down_payment = $this->Session->read("CSO.amt_down_payment");
		$amt_installment = $this->Session->read("CSO.amt_installment");
		$installment = $amt_installment[$tenor];
		$down_payment = $amt_down_payment[$tenor];
		$this->sim_data = array(
			"TrnSimKredit" => array(
				"personal_id" => $cust_id,
				"cd_vehicle_brand" => $this->Session->read("CSO.cd_vehicle_brand"),
				"cd_vehicle_type" => $this->Session->read("CSO.cd_vehicle_type"),
				"des_vehicle_type" => $this->Session->read("CSO.des_vehicle_type"),

				"cd_vehicle_model" => $this->Session->read("CSO.cd_vehicle_model"),
				"cd_vehicle_kind" => $this->Session->read("CSO.cd_vehicle_kind"),
				"vehicle_status" => $this->Session->read("CSO.vehicle_status"),
				"year_of_mfg" => $this->Session->read("CSO.year_of_mfg"),
				"otr" => $this->Session->read("CSO.otr"),
				"perc_dp" => $this->Session->read("CSO.perc_dp"),
				"amt_dp" => $this->Session->read("CSO.amt_dp"),
				"tenor" => $tenor, //$this->Session->read("CSO.tenor"),
				"flag_acp" => $this->Session->read("CSO.flag_acp"),
				"perc_acp_rate" => $this->Session->read("CSO.perc_acp_rate"),
				"perc_insu_total" => $this->Session->read("CSO.perc_insu_total"),
				"perc_insu_cash" => $this->Session->read("CSO.perc_insu_cash"),
				"amt_insu_cash" => $this->Session->read("CSO.amt_insu_cash"),
				"perc_insu_credit" => $this->Session->read("CSO.perc_insu_credit"),
				"amt_insu_credit" => $this->Session->read("CSO.amt_insu_credit"),
				"amt_total_debt" => $this->Session->read("CSO.amt_total_debt"),
				"perc_rate" => $this->Session->read("CSO.perc_rate"),
				"amt_rate" => $this->Session->read("CSO.amt_rate"),
				"amt_insu_policy" => $this->Session->read("CSO.amt_insu_policy"),
				"amt_administration" => $this->Session->read("CSO.amt_administration"),
				"amt_fiducia" => $this->Session->read("CSO.amt_fiducia"),
				"amt_down_payment" => $amt_down_payment[$tenor],//$this->Session->read("CSO.amt_down_payment"),
				"amt_installment" => $amt_installment[$tenor] //$this->Session->read("CSO.amt_installment")
			)
		);
	}

	function _send_email_customer()
	{
		$site_name = Configure::read("Site.title");
		$domain = preg_replace("/(http:\/\/|https:\/\/)*(www\.)*/", "", Configure::read("Site.home_url"));
		$domain = preg_replace("/(\:[0-9]+)*(\/)*/", "", $domain);
		$email = new CakeEmail();
		//$email->from(Configure::read("Site.title") . " <no_reply@{$domain}>");
		//$email->replyTo(Configure::read("Site.title") . " <no_reply@{$domain}>");
		$email->from("no_reply@{$domain}",Configure::read('Site.title'));
				$email->replyTo("no_reply@{$domain}",Configure::read('Site.title'));
		$email->to(array($this->member_data["personal_email"]));
		//$this->Email->bcc = "yfuadi@gmail.com";
		$email->bcc(explode(",", Configure::read("Site.admin_emails")));
		$email->subject('Credit simulation data submitted');
		$email->emailFormat('html');
		$message = <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en">
<head>
<title>Apply Credit Simulation</title>
</head>
<body>
<style type="text/css">
.text
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	text-decoration: none;
	color: #000000;
}
</style>
<div class="text">
Dear <b>{$this->member_data["personal_name"]}</b>,
<br /><br />
Terima kasih, telah mengajukan kredit mobil anda ke Astra Credit Companies.<br />
Team sales kami akan memproses aplikasi anda segera.
<br /><br /><br />
Hormat kami,
<br /><br /><br /><br />
<b><u>{$site_name}</u></b>
</div>
</body>
</html>
END;
				$email->send($message);
	}
	function _send_email()
	{
		if (isset($this->sim_data) && !empty($this->sim_data) && isset($this->member_data) && !empty($this->member_data))
		{
		   $this->loadModel('Brand');
			$brand = $this->Brand->find(
				"first",
				array(
					"conditions" => array("Brand.code_brand" => $this->sim_data["TrnSimKredit"]["cd_vehicle_brand"]),
					"order" => "Brand.brand_id ASC",
					"limit" => 1
				)
			);
			/*
			$this->loadModel('VehicleType');
			$type = $this->VehicleType->find(
				"first",
				array(
					"conditions" => array("VehicleType.brand_id" => $brand["Brand"]["brand_id"], "VehicleType.code_type" => $this->sim_data["TrnSimKredit"]["cd_vehicle_type"]),
					"order" => "VehicleType.type_id ASC",
					"limit" => 1
				)
			);*/
			//print_r($this->sim_data);
			$otr = number_format($this->sim_data["TrnSimKredit"]["otr"]);
			$amt_dp = number_format($this->sim_data["TrnSimKredit"]["amt_dp"]);
			$amt_down_payment = number_format($this->sim_data["TrnSimKredit"]["amt_down_payment"]);
			$amt_installment = number_format($this->sim_data["TrnSimKredit"]["amt_installment"]);
			$credit_info = <<<END
<b>Credit Information</b>
<br />
Vehicle brand : {$brand["Brand"]["description"]}<br />
Vehicle type : {$this->sim_data["TrnSimKredit"]["des_vehicle_type"]}<br />
Vehicle status : {$this->sim_data["TrnSimKredit"]["vehicle_status"]}<br />
Year of manufactured : {$this->sim_data["TrnSimKredit"]["year_of_mfg"]}<br />
Vehicle price : {$otr}<br />
Down payment (DP) percentage : {$this->sim_data["TrnSimKredit"]["perc_dp"]} %<br />
Down payment (DP) amount : {$amt_dp}<br />
Tenor : {$this->sim_data["TrnSimKredit"]["tenor"]} year(s).<br />
Using ACP : {$this->sim_data["TrnSimKredit"]["flag_acp"]}<br />
First installment : {$amt_down_payment}<br />
Regular installment : {$amt_installment}<br />
END;

				$customer_info = <<<END
<b>Nama :</b>
<br />
{$this->member_data["personal_name"]}
<br /><br />
<b>Telp :</b>
<br />
{$this->member_data["personal_phone"]}
<br /><br />
<b>Email :</b>
<br />
{$this->member_data["personal_email"]}
<br /><br />
<b>Area :</b>
<br />
{$this->member_data["area_name"]}
<br /><br />
<b>Branch :</b>
<br />
{$this->member_data["branch_name"]}
<br /><br />
<b>Identitas Diri :</b>
<br />
<a href='www.acc.co.id/uploads/{$this->Session->read("Member.personal_ktp")}'>Klik</a>
<br /><br />
END;
			$this->loadModel('CreditPackageRecipient');
			$temp = $this->CreditPackageRecipient->find(
				"all",
				array(
					"conditions" => array("CreditPackageRecipient.active" => "Yes"),
					"order" => "CreditPackageRecipient.recipient_id ASC"
				)
			);

			$data = array();
			foreach ($temp as $tmp)
			{
				$data[] = $tmp["CreditPackageRecipient"]["email"];
			}
			if ($data)
			{
				$site_name = Configure::read('Site.title');
				$domain = preg_replace("/(http:\/\/|https:\/\/)*(www\.)*/", "", Configure::read('Site.home_url'));
				$domain = preg_replace("/(\:[0-9]+)*(\/)*/", "", $domain);
				$email = new CakeEmail();
				$email->from("no_reply@{$domain}",Configure::read('Site.title'));
				$email->replyTo("no_reply@{$domain}",Configure::read('Site.title'));
				//$email->replyTo(Configure::read("Site.title") . " <no_reply@{$domain}>");
				$email->to($data);
				//$this->Email->bcc = "yfuadi@gmail.com";
				$email->bcc(explode(",", Configure::read("Site.admin_emails")));
				$email->subject('Credit simulation data submitted');
				$email->emailFormat('html');

			$message = <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en">
<head>
<title>Apply Credit Simulation</title>
</head>
<body>
<style type="text/css">
.text
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	text-decoration: none;
	color: #000000;
}
</style>
<div class="text">
Dear <b>Administrator</b>,
<br /><br />
This is an automated email generated by credit simulation section of your website.
This email will be sent to you when a user posting some information into your website's credit simulation form.
And the full informations is listed below :
<br /><br /><br />
{$credit_info}
<br /><br />
{$customer_info}
<br /><br /><br />
Best regards,
<br /><br /><br /><br />
<b><u>Your automated system</u></b>
<br />
{$site_name}
</div>
</body>
</html>
END;
				$email->send($message);
			}
		}
	}
	/**
 * index method
 *
 * @return void
 */
	public function apply_form() {
		//print_r($this->request->params['id']);
		$this->layout = "blank";
		$this->set('title_for_layout', __d('croogo', 'Simulasi Kredit Mobil'));
		$this->loadModel('Area');
		$msg = "Terima kasih atas ketertarikan Anda untuk melakukan KREDIT. <br />Mohon isi data berikut ini untuk memudahkan team sales kami untuk menghubungi Anda.";
		$status = 0;
		if ($this->data){
			//print_r($this->data);
			//Check if image has been uploaded
			$data = $this->data['Personal'];

			$this->member_data = $data;
			$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

			if ($response['success'] == true) {
				if(!empty($this->data['Personal']['upload']['name']))
				{
					$file = $this->data['Personal']['upload']; //put the data into a var for easy use

					$ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
					$arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions

					$maxsize = 307200;
					$acceptable = array(
						'image/png',
						'image/jpeg',
						'image/jpeg',
						'image/jpeg',
						'image/gif'
					);

					$imageInfo = getimagesize($file['tmp_name']);

					//only process if the extension is valid
					if(in_array($ext, $arr_ext) && $imageInfo !="")
					{
						if(($file['size'] <= $maxsize) && ($file["size"] > 0))
						{
							//echo WWW_ROOT . 'webroot/img/' . time() .'_'. $file['name'];
							//do the actual uploading of the file. First arg is the tmp name, second arg is
							//where we are putting it
							$file_name = time() .'_'. $file['name'];
							move_uploaded_file($file['tmp_name'], WWW_ROOT . 'uploads/' . $file_name);

							//prepare the filename for database entry
							$data['personal_ktp'] = $file_name;

							//now do the save
							$this->loadModel('TrnSimPersonal');
							if ($this->TrnSimPersonal->saveAssociated($data)) {
								$areas = $this->Area->find(
									"first",
									array(
										"conditions" => array("Area.id" => $this->member_data['personal_area'])
									)
								);
								$this->member_data['area_name'] = $areas["Area"]["description"];

								$this->loadModel('Branch');
								$branch = $this->Branch->find(
									"first",
									array(
										"conditions" => array("Branch.branch_id" => $this->member_data['personal_branch'])
									)
								);
								$this->member_data['branch_name'] = $branch["Branch"]["description"];

								$status = 1;
								$msg = "Terima kasih, aplikasi Kredit ACC Anda telah terkirim. Kami akan memberikan respon kepada Anda secepatnya!";
								$personal_id = $this->TrnSimPersonal->id;
								$this->Session->write("Member.personal_id", $personal_id);
								$this->Session->write("Member.personal_ktp", $data['personal_ktp']);

								$this->apply($this->request->params['id']);
								//$this->products->save($this->data) ;
							}else{
								$status = 0;
								$msg = "Silahkan cek kembali data Anda!";
							}
						}else{
							$status = 0;
							$msg = "File terlalu besar, maksimal 300KB!";
						}
					}else{
						$status = 0;
						$msg = "Format Foto KTP/SIM harus image!";
					}
				}

			}else{
				$status = 0;
				$msg = "Silahkan cek kembali kode keamanan Anda!";
			}

			//echo $msg;

		}

		$this->set('msg',$msg);
		$this->set('status',$status);


		$this->set(
			"area_list",
			$this->Area->find(
				"list",
				array(
					"fields" => array("Area.id", "Area.description"),
					"order" => "Area.description ASC"
				)
			)
		);

		$this->set("cabang_list",array());


	}
/**
 * index method
 *
 * @return void
 */
	public function index($brand="",$type="",$harga="") {
		$this->_clear_session();
		$this->set('title_for_layout', __d('croogo', 'Simulasi Kredit Mobil'));
		//print_r($this->data);
		if ($this->data){
			//print_r();
			$brand = $this->data['CreditSimulation']['brand_id'];
			$type = $this->data['CreditSimulation']['code_group'];
			$harga = $this->data['CreditSimulation']['otr'];

			$result = $this->_hitung_simulasi();
		}

		$pakets = $this->Node->find(
			"all",
			array(
				"conditions" => array("Node.type" => array("attachment"), "Node.status" => "1", "Node.terms" => "paket"),
				"order" => "Node.id DESC"
			)
		);
		$this->set("brand",$brand);
		$this->set("type",$type);
		$this->set("harga",$harga);
		$this->set("pakets",$pakets);

		$this->set(
			"brand_list",
			$this->Brand->find(
				"list",
				array(
					"fields" => array("Brand.brand_id", "Brand.description"),
					"conditions"=>" Brand.Brand_id IN (SELECT Brand_id From vehicle_types)",
				"order" => "Brand.description ASC"
				)
			)
		);
		$this->set("group_list",array());
	}
	/**
 * index method
 *
 * @return void
 */
	public function index2() {
		$this->_clear_session();
		$this->set('title_for_layout', __d('croogo', 'Simulasi Kredit Mobil Bekas'));
		//print_r($this->data);
		$type = "";
if ($this->data){
                       $type = $this->data['CreditSimulation']['code_group'];
			$result = $this->_hitung_simulasi();
		}

		$pakets = $this->Node->find(
			"all",
			array(
				"conditions" => array("Node.type" => array("attachment"), "Node.status" => "1", "Node.terms" => "paket"),
				"order" => "Node.id ASC"
			)
		);
		$this->set("pakets",$pakets);
                $this->set("type",$type);
		$this->set(
			"brand_list",
			$this->Brand->find(
				"list",
				array(
					"fields" => array("Brand.brand_id", "Brand.description"),
					"conditions"=>" Brand.Brand_id IN (SELECT Brand_id From vehicle_types)",
				"order" => "Brand.description ASC"
				)
			)
		);
		$this->set("group_list",array());
	}
	 function query_branch($area)
	{

		$this->layout = "blank";

		$this->loadModel('Branch');
		$data =$this->Branch->find(
				"all",
				array(
					'conditions' => array('Branch.area_id' => $area),
					"order" => "Branch.branch_id ASC",
					'group' => 'Branch.description',
				)
			);

		//$data = $this->VehicleType->query("SELECT * FROM `vehicle_types` WHERE `brand_id`='{$brand_id}' GROUP BY `code_group` ORDER BY `desc_group` ASC");
		$result = "";

		$result .= "<option value=\"\">Pilih Cabang</option>";
		//print_r($data);
		foreach ($data as $tmp)
		{

			$result .= "<option value=\"{$tmp["Branch"]["branch_id"]}\">{$tmp["Branch"]["description"]}</option>";

		}

		$this->set("result", $result);

	}
	 function query_vehicle_group($brand_id)
	{

		$this->layout = "blank";

		$this->loadModel('VehicleType');
		$data =$this->VehicleType->find(
				"all",
				array(
					'conditions' => array('VehicleType.brand_id' => $brand_id),
					"order" => "VehicleType.desc_group ASC",
					'group' => 'VehicleType.code_group',
				)
			);

		//$data = $this->VehicleType->query("SELECT * FROM `vehicle_types` WHERE `brand_id`='{$brand_id}' GROUP BY `code_group` ORDER BY `desc_group` ASC");
		$result = "";

		//print_r($data);
		foreach ($data as $tmp)
		{

			$result .= "<option value=\"{$tmp["VehicleType"]["code_group"]}\">{$tmp["VehicleType"]["desc_group"]}</option>";

		}

		$this->set("result", $result);

	}
	function query_vehicle_group_new($brand_id)
	{

		$this->layout = "blank";

		$this->loadModel('VehicleTypeNew');
		$data =$this->VehicleTypeNew->find(
				"all",
				array(
					'conditions' => array('VehicleTypeNew.brand_id' => $brand_id),
					"order" => "VehicleTypeNew.desc_group ASC",
					'group' => 'VehicleTypeNew.code_group',
				)
			);

		//$data = $this->VehicleType->query("SELECT * FROM `vehicle_types` WHERE `brand_id`='{$brand_id}' GROUP BY `code_group` ORDER BY `desc_group` ASC");
		$result = "";

		//print_r($data);
		foreach ($data as $tmp)
		{

			$result .= "<option value=\"{$tmp["VehicleTypeNew"]["code_group"]}\">{$tmp["VehicleTypeNew"]["desc_group"]}</option>";

		}

		$this->set("result", $result);

	}
	function _hitung_simulasi()

	{

	  $data = $this->data;
	 //cek tipe mobil berdasarkan kategorisasi terbaru
	 if ($data['CreditSimulation']['vehicle_status']=="New"){
			$this->loadModel('VehicleTypeNew');
			$tipebaru = $this->VehicleTypeNew->find(
				"first",
				array(
					"conditions" => array(
						"VehicleTypeNew.code_group" =>$data["CreditSimulation"]["code_group"]
					),
					"limit" => 1

				)
			);
			$tipemobilbaru = $tipebaru["VehicleTypeNew"]["code_type"];
			$tipebaru_desc = $tipebaru["VehicleTypeNew"]["description"];
			$tipemobilbaru_code = $tipebaru["VehicleTypeNew"]["code_group"];
	 }else{
			$this->loadModel('VehicleType');
			$tipebaru = $this->VehicleType->find(
				"first",
				array(
					"conditions" => array(
						"VehicleType.code_group" =>$data["CreditSimulation"]["code_group"]
					),
					"limit" => 1

				)
			);
			$tipemobilbaru = $tipebaru["VehicleType"]["code_type"];
			$tipebaru_desc = $tipebaru["VehicleType"]["description"];
			$tipemobilbaru_code = $tipebaru["VehicleTypeNew"]["code_group"];
	 }
		//end cek

		for ($i=1;$i<=4;$i++){
			$data["CreditSimulation"]["tenor"] = $i;
			$percent_dp = round($data["CreditSimulation"]["down_payment"] /$data["CreditSimulation"]["otr"] * 100, 3);
			$data["CreditSimulation"]["acp_status"] = "Yes";
			if ($data["CreditSimulation"]["acp_status"] == "Yes")
			{

				$acp = $this->Acp->find(
					"first",
					array(
						"conditions" => array("Acp.tenor" =>$data["CreditSimulation"]["tenor"]),
						"order" => "Acp.acp_id DESC",
						"limit" => 1
					)
				);
			}
			else
				$acp = array("Acp" => array("acp_rate" => 0));
			if (!$acp)
				return $this->_simulasi_error("Sistem tidak dapat menemukan data ACP.");

			$brand = $this->Brand->find(

				"first",
				array(
					"conditions" => array("Brand.brand_id" =>$data["CreditSimulation"]["brand_id"]),
					"order" => "Brand.brand_id DESC",
					"limit" => 1
				)
			);
			if (!$brand)
				return $this->_simulasi_error("Sistem tidak dapat menemukan data Vehicle Brand.");


			//-------------------------------------------------------------------------------------------------------------------------------
			// Validasi OTR
			// -------------------------------------------------------------------------------------------------------------------------------

			// $otr = $this->Otr->find(
				// "first",
				// array(
					// "conditions" => array(
						// //"Otr.branch_id" =>$data["CreditSimulation"]["branch_id"],
						// //"Otr.model_id" =>$data["CreditSimulation"]["model_id"],
						// "Otr.vehicle_status" =>$data["CreditSimulation"]["vehicle_status"],
						// "Otr.year_of_manufactured" =>$data["CreditSimulation"]["year_of_manufactured"]
					// ),
					// "order" => "Otr.otr_id DESC",
					// "limit" => 1
				// )
			// );
			// if ($otr)
			// {
				// $amt_otr = $otr["Otr"]["amount_otr"];

				// $tolerance = $otr["Otr"]["tolerance_percentage"];

				// $variant = floor(($amt_otr * $tolerance) / 100);
				// $min_otr = $amt_otr - $variant;
				// $max_otr = $amt_otr + $variant;

				// ----------------------------------------------------------------------------------------------------------------------
					if (!ctype_digit($data["CreditSimulation"]["otr"]))
						return $this->_simulasi_error("Nilai OTR yang anda berikan tidak valid.");
					$amt_otr =$data["CreditSimulation"]["otr"];
				// ----------------------------------------------------------------------------------------------------------------------
				/*
				if (($amt_otr < $min_otr) || ($amt_otr > $max_otr))

				return $this->_simulasi_error("Nilai OTR kurang dari / lebih dari toleransi.");
				*/
			// }

			// -------------------------------------------------------------------------------------------------------------------------------
			// Validasi Tahun Kendaraan
			// -------------------------------------------------------------------------------------------------------------------------------
			if (!ctype_digit($data["CreditSimulation"]["year_of_manufactured"]))
				return $this->_simulasi_error("Nilai Tahun Kendaraan yang anda berikan tidak valid.");
			// -------------------------------------------------------------------------------------------------------------------------------
			// Validasi Down Payment
			// -------------------------------------------------------------------------------------------------------------------------------
			if (!ctype_digit($data["CreditSimulation"]["down_payment"]))
				return $this->_simulasi_error("Nilai Down Payment (DP) yang anda berikan tidak valid.");

				$this->loadModel('Insurance');

				$insurance1 = $this->Insurance->find(
				"first",
				array(
					"conditions" => array(
						//"Insurance.category_id" => $model["VehicleModel"]["category_id"],
						"Insurance.tenor" =>$data["CreditSimulation"]["tenor"],
						"Insurance.vehicle_status" =>$data["CreditSimulation"]["vehicle_status"],
						"Insurance.otr_start <=" =>$data["CreditSimulation"]["otr"],
						"Insurance.otr_end >=" =>$data["CreditSimulation"]["otr"]
					),
					"order" => "",
					"limit" => 1
				)
			);
			$insurance2 = $this->Insurance->find(
				"first",
				array(
					"conditions" => array(
						//"Insurance.category_id" => $model["VehicleModel"]["category_id"],
						"Insurance.tenor" => 1,
						"Insurance.vehicle_status" =>$data["CreditSimulation"]["vehicle_status"],
						"Insurance.otr_start <=" =>$data["CreditSimulation"]["otr"],
						"Insurance.otr_end >=" =>$data["CreditSimulation"]["otr"]

					),

					"order" => "",
					"limit" => 1

				)

			);

			if (!$insurance1 || !$insurance2)

				return $this->_simulasi_error("Sistem tidak dapat menemukan data Asuransi.");

			if ($data["CreditSimulation"]["vehicle_status"] == "New")
			{
				$flat_rate = $this->NewRate->find(

					"first",
					array(
						"conditions" => array(
						//sebelumnya query type_id=9
							//"NewRate.branch_id" =>$data["CreditSimulation"]["branch_id"],
							"NewRate.type_id" => 700,//$data["CreditSimulation"]["type_id"],
							"NewRate.kind_id" => $tipemobilbaru,
							//"NewRate.kind_id" => $model["VehicleModel"]["kind_id"],
							"NewRate.tenor" =>$data["CreditSimulation"]["tenor"],
							//"NewRate.from_dp <=" =>$data["CreditSimulation"]["down_payment"],
							//"NewRate.to_dp >=" =>$data["CreditSimulation"]["down_payment"]
							"NewRate.from_dp <=" => $percent_dp,
							"NewRate.to_dp >=" => $percent_dp
						),
						"order" => "NewRate.nr_id DESC",
						"limit" => 1
					)
				);

				if (!$flat_rate)
					return $this->_simulasi_error("Sistem tidak dapat menemukan data Flat Rate Percentage.");

				$flat_rate["FlatRate"] = $flat_rate["NewRate"];

			}

			elseif ($data["CreditSimulation"]["vehicle_status"] == "Used")
			{
				$flat_rate = $this->UsedRate->find(
					"first",
					array(
						"conditions" => array(
						//sebelumnya query tenor saja
							//"UsedRate.branch_id" =>$data["CreditSimulation"]["branch_id"],
							"UsedRate.type_id" => 700, //$data["CreditSimulation"]["type_id"],
							//"UsedRate.kind_id" => $kind["VehicleKind"]["kind_id"],
							//"UsedRate.kind_id" => $model["VehicleModel"]["kind_id"],
							//"UsedRate.year_of_manufacture" =>$data["CreditSimulation"]["year_of_manufactured"],
							"UsedRate.year_from <=" =>$data["CreditSimulation"]["year_of_manufactured"],
							"UsedRate.year_to >=" =>$data["CreditSimulation"]["year_of_manufactured"],
							"UsedRate.tenor" =>$data["CreditSimulation"]["tenor"],
							//"UsedRate.otr_start <=" =>$data["CreditSimulation"]["otr"],
							//"UsedRate.otr_end >=" =>$data["CreditSimulation"]["otr"],
							//"UsedRate.from_dp <=" =>$data["CreditSimulation"]["down_payment"],
							//"UsedRate.to_dp >=" =>$data["CreditSimulation"]["down_payment"]
							"UsedRate.from_dp <=" => $percent_dp,
							"UsedRate.to_dp >=" => $percent_dp
						),
						"order" => "UsedRate.ur_id DESC",
						"limit" => 1
					)
				);
				if (!$flat_rate)

				return $this->_simulasi_error("Sistem tidak dapat menemukan data Flat Rate Percentage.");

				$flat_rate["FlatRate"] = $flat_rate["UsedRate"];
			}

			$administration = $this->Administration->find(
				"first",
				array(
					"conditions" => array(
						"Administration.tenor" =>$data["CreditSimulation"]["tenor"],
						"Administration.vehicle_status" =>$data["CreditSimulation"]["vehicle_status"]
					),
					"order" => "Administration.adm_id DESC",
					"limit" => 1

				)
			);



			if (!$administration)
				return $this->_simulasi_error("Sistem tidak dapat menemukan data Administrasi.");

			// -------------------------------------------------------------------------------------------------------------------------------
			// Hitung Pokok Hutang
			// -------------------------------------------------------------------------------------------------------------------------------
			//$D19 = $insurance1["Insurance"]["ar_gross_rate"] - $insurance2["Insurance"]["ar_gross_rate"];
			$F18 =$data["CreditSimulation"]["otr"] * $insurance1["Insurance"]["ar_gross_rate"] / 100;//asuransi kredit
			//$G19 =$data["CreditSimulation"]["otr"] * $D19 / 100;
			$G20 = ($data["CreditSimulation"]["otr"] -$data["CreditSimulation"]["down_payment"]);//hargaotr
			$G22 = $G20 * $flat_rate["FlatRate"]["flat_rate_percentage"] / 100 *$data["CreditSimulation"]["tenor"];//bunga
			$G23 = $F18+$G20 + $G22;//TotalHutang0
			$G140 = $G23 * $acp["Acp"]["acp_rate"] / 100;
			$G14 = ceil($G140 / 1000) * 1000;//ACP
			$H20 = $G20 + $G22 + $G14 + $F18;//totalhutang

			$H22 = ($G20 + $G14 + $F18) * $flat_rate["FlatRate"]["flat_rate_percentage"] / 100 *$data["CreditSimulation"]["tenor"];// bungabaru
			$H23 = $G20 + $G14 + $F18;//PHBaru
			$ARBaru = $H22 + $H23;
			$H37[$i] = ceil($ARBaru / ($data["CreditSimulation"]["tenor"] * 12) / 1000) * 1000;
			$D35[$i] =$data["CreditSimulation"]["down_payment"] + $H37[$i] + 1500000;
		}

		$this->set("uang_muka", $D35);
		$this->set("angsuran", $H37);


		$result = "H20 : {$H20}\nH22 : {$H22}\nH23 : {$H23}\n";


		$this->set("result", $result);

		//$this->set("flat_rate", $model);

		if (isset($H37) && !empty($H37))

		{
			$this->Session->write("CSO.cd_vehicle_brand", $brand["Brand"]["code_brand"]);
			$this->Session->write("CSO.cd_vehicle_type", $tipemobilbaru_code);
			$this->Session->write("CSO.des_vehicle_type", $tipebaru_desc);

			//$this->Session->write("CSO.cd_vehicle_model", $model["VehicleModel"]["code_model"]);
			//$this->Session->write("CSO.cd_vehicle_kind", $model["VehicleKind"]["code_kind"]);
			$this->Session->write("CSO.vehicle_status",$data["CreditSimulation"]["vehicle_status"]);
			$this->Session->write("CSO.year_of_mfg",$data["CreditSimulation"]["year_of_manufactured"]);
			$this->Session->write("CSO.otr",$data["CreditSimulation"]["otr"]);
			$this->Session->write("CSO.perc_dp", round($data["CreditSimulation"]["down_payment"] /$data["CreditSimulation"]["otr"] * 100, 4));
			$this->Session->write("CSO.amt_dp",$data["CreditSimulation"]["down_payment"]);
			$this->Session->write("CSO.tenor", $data["CreditSimulation"]["tenor"]);
			$this->Session->write("CSO.flag_acp", $data["CreditSimulation"]["acp_status"]);
			$this->Session->write("CSO.perc_acp_rate", $acp["Acp"]["acp_rate"]);
			$this->Session->write("CSO.perc_insu_total", $insurance1["Insurance"]["ar_gross_rate"]);
			$this->Session->write("CSO.perc_insu_cash", $insurance2["Insurance"]["ar_gross_rate"]);
			$this->Session->write("CSO.amt_insu_cash", $F18);
			$this->Session->write("CSO.perc_insu_credit", 0);
			$this->Session->write("CSO.amt_insu_credit", $F18);
			$this->Session->write("CSO.amt_total_debt", $H20);
			$this->Session->write("CSO.perc_rate", $flat_rate["FlatRate"]["flat_rate_percentage"]);
			$this->Session->write("CSO.amt_rate", $H22);
			$this->Session->write("CSO.amt_insu_policy", $administration["Administration"]["polis"]);
			$this->Session->write("CSO.amt_administration", $administration["Administration"]["administration"]);
			$this->Session->write("CSO.amt_fiducia", $administration["Administration"]["fiducia"]);
			$this->Session->write("CSO.amt_down_payment", $D35);
			$this->Session->write("CSO.amt_installment", $H37);
		}
		else
			$this->_clear_session();

	}
	function _simulasi_error($error_message)
	{
		$this->set("form_error", $error_message);
		return false;
	}

	function _clear_session(){
		$this->Session->write("CSO.cd_vehicle_brand", "");
		$this->Session->write("CSO.cd_vehicle_type", "");
		$this->Session->write("CSO.cd_vehicle_model", "");
		$this->Session->write("CSO.cd_vehicle_kind", "");
		$this->Session->write("CSO.vehicle_status", "");
		$this->Session->write("CSO.year_of_mfg", "");
		$this->Session->write("CSO.otr", "");
		$this->Session->write("CSO.perc_dp", "");
		$this->Session->write("CSO.amt_dp", "");
		$this->Session->write("CSO.tenor", "");
		$this->Session->write("CSO.flag_acp", "");
		$this->Session->write("CSO.perc_acp_rate", "");
		$this->Session->write("CSO.perc_insu_total", "");
		$this->Session->write("CSO.perc_insu_cash", "");
		$this->Session->write("CSO.amt_insu_cash", "");
		$this->Session->write("CSO.perc_insu_credit", "");
		$this->Session->write("CSO.amt_insu_credit", "");
		$this->Session->write("CSO.amt_total_debt", "");
		$this->Session->write("CSO.perc_rate", "");
		$this->Session->write("CSO.amt_rate", "");
		$this->Session->write("CSO.amt_insu_policy", "");
		$this->Session->write("CSO.amt_administration", "");
		$this->Session->write("CSO.amt_fiducia", "");
		$this->Session->write("CSO.amt_down_payment", "");
		$this->Session->write("CSO.amt_installment", "");
	}
	protected function _getSenderEmail() {
		return 'noreply@' . preg_replace('#^www\.#', '', strtolower($_SERVER['SERVER_NAME']));
	}
	public function beforeFilter() {
        $this->Security->unlockedActions = array('index','index2','_hitung_simulasi','apply_form');
        parent::beforeFilter();
	}

}
