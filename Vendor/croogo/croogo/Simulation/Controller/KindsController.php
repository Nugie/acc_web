<?php
App::uses('SimulationAppController', 'Simulation.Controller');
/**
 * Kinds Controller
 *
 * @property Kind $Kind
 * @property PaginatorComponent $Paginator
 */
class KindsController extends SimulationAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Kind->recursive = 0;
		$this->set('kinds', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Kind->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'kind')));
		}
		$options = array('conditions' => array('Kind.' . $this->Kind->primaryKey => $id));
		$this->set('kind', $this->Kind->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Kind->create();
			if ($this->Kind->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'kind')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->Kind->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'kind')), 'default', array('class' => 'error'));
			}
		}
		$categories = $this->Kind->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->loadModel('Category');
		$ops=array(
			'fields' => array('category_id', 'description')
		);
		$categories = $this->Category->find('list',$ops); 
		if (!$this->Kind->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'kind')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Kind->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'kind')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'kind')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('Kind.' . $this->Kind->primaryKey => $id));
			$this->request->data = $this->Kind->find('first', $options);
		}
		//print_r($this->Kind->Category->find('all'));
		//$categories = $this->Kind->Category->find('all');
		$this->set(compact('categories'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Kind->id = $id;
		if (!$this->Kind->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'kind')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Kind->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Kind')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Kind')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}
}
