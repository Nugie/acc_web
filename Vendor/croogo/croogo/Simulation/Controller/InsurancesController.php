<?php
App::uses('SimulationAppController', 'Simulation.Controller');
/**
 * Insurances Controller
 *
 * @property Insurance $Insurance
 * @property PaginatorComponent $Paginator
 */
class InsurancesController extends SimulationAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Insurance->recursive = 0;
		$this->set('insurances', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Insurance->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'insurance')));
		}
		$options = array('conditions' => array('Insurance.' . $this->Insurance->primaryKey => $id));
		$this->set('insurance', $this->Insurance->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Insurance->create();
			if ($this->Insurance->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'insurance')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->Insurance->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'insurance')), 'default', array('class' => 'error'));
			}
		}
		$categories = $this->Insurance->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Insurance->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'insurance')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Insurance->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'insurance')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'insurance')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('Insurance.' . $this->Insurance->primaryKey => $id));
			$this->request->data = $this->Insurance->find('first', $options);
		}
		$categories = $this->Insurance->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Insurance->id = $id;
		if (!$this->Insurance->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'insurance')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Insurance->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Insurance')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Insurance')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}
}
