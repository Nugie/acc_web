<?php
App::uses('SimulationAppController', 'Simulation.Controller');
/**
 * Acps Controller
 *
 * @property Acp $Acp
 * @property PaginatorComponent $Paginator
 */
class AcpsController extends SimulationAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Acp->recursive = 0;
		$this->set('acps', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Acp->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'acp')));
		}
		$options = array('conditions' => array('Acp.' . $this->Acp->primaryKey => $id));
		$this->set('acp', $this->Acp->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Acp->create();
			if ($this->Acp->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'acp')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->Acp->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'acp')), 'default', array('class' => 'error'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Acp->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'acp')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Acp->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'acp')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'acp')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('Acp.' . $this->Acp->primaryKey => $id));
			$this->request->data = $this->Acp->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Acp->id = $id;
		if (!$this->Acp->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'acp')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Acp->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Acp')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Acp')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Acp->recursive = 0;
		$this->set('acps', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Acp->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'acp')));
		}
		$options = array('conditions' => array('Acp.' . $this->Acp->primaryKey => $id));
		$this->set('acp', $this->Acp->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Acp->create();
			if ($this->Acp->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'acp')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->Acp->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'acp')), 'default', array('class' => 'error'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Acp->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'acp')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Acp->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'acp')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'acp')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('Acp.' . $this->Acp->primaryKey => $id));
			$this->request->data = $this->Acp->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Acp->id = $id;
		if (!$this->Acp->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'acp')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Acp->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Acp')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Acp')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}
}
