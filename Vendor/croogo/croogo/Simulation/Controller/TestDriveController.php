<?php
App::uses('SimulationAppController', 'Simulation.Controller');


/**
 * Acps Controller
 *
 * @property Acp $Acp
 * @property PaginatorComponent $Paginator
 */
class TestDriveController extends SimulationAppController {


	public $components = array('Security');
	
	var $uses = array(
			"Simulation.TrnTestDrive",
	);
	
	/**
 * index method
 *
 * @return void
 */
	public function apply_form($brand_id="",$code_group="") {
		//print_r($this->request->params['id']);
		
		$this->layout = "blank";
		$this->set('title_for_layout', __d('croogo', 'Test Drive'));
		$this->loadModel('Area');
		$msg = "Terima kasih atas ketertarikan Anda untuk melakukan Test Drive. <br />Mohon isi data berikut ini untuk memudahkan team sales kami untuk menghubungi Anda.";
		$status = 0;
		
		//exit;
		if ($this->data){
			//print_r($this->data);
			//Check if image has been uploaded
			$data = $this->data['Personal'];
			
			$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			
			if ($response['success'] == true) {	
				//now do the save
				$this->loadModel('TrnTestDrive');
				
				$this->TrnTestDrive->create();
				//print_r($this->TrnTestDrive->validationErrors);
				 if ($this->TrnTestDrive->saveAssociated($data)) {
					$status = 1;
					$msg = "Terima kasih, aplikasi Test Drive Anda telah terkirim. Kami akan memberikan respon kepada Anda secepatnya!";
				
					//$this->products->save($this->data) ;
				}else{
					$status = 0;
					$msg = "Silahkan cek kembali data Anda!";
				} 
			}else{
				$status = 0;
				$msg = "Silahkan cek kembali kode keamanan Anda!";
			}
			
			//echo $msg;
			
		}
		$this->set('brand_id',$brand_id);
		$this->set('code_group',$code_group);
		$this->set('msg',$msg);
		$this->set('status',$status);
		
		
		$this->set(
			"area_list",
			$this->Area->find(
				"list",
				array(
					"fields" => array("Area.id", "Area.description"),
					"order" => "Area.description ASC"
				)
			)
		);
		
		$this->set("cabang_list",array());
		
		
	}
	
	 function query_branch($area)
	{

		$this->layout = "blank";
		
		$this->loadModel('Branch');
		$data =$this->Branch->find(
				"all",
				array(
					'conditions' => array('Branch.area_id' => $area),
					"order" => "Branch.branch_id ASC",
					'group' => 'Branch.description',
				)
			);
			
		//$data = $this->VehicleType->query("SELECT * FROM `vehicle_types` WHERE `brand_id`='{$brand_id}' GROUP BY `code_group` ORDER BY `desc_group` ASC");
		$result = "";

		$result .= "<option value=\"\">Pilih Cabang</option>";
		//print_r($data);
		foreach ($data as $tmp)
		{

			$result .= "<option value=\"{$tmp["Branch"]["branch_id"]}\">{$tmp["Branch"]["description"]}</option>";

		}

		$this->set("result", $result);

	}	
	
	public function beforeFilter() {
		
		$this->Security->csrfCheck = false;
		$this->Security->validatePost = false;
        $this->Security->unlockedActions = array('apply_form','test_drive');
		
		parent::beforeFilter();
	}	
	
}
