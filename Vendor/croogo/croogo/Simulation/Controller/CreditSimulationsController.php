<?php
App::uses('SimulationAppController', 'Simulation.Controller');
/**
 * Acps Controller
 *
 * @property Acp $Acp
 * @property PaginatorComponent $Paginator
 */
class CreditsimulationsController extends SimulationAppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->set('title_for_layout', __d('croogo', 'Network Acc'));
		
		
		$pointer_data = $this->Network->find(
			"all",
			array(
				"conditions" => array("Network.active" => "Yes", "Network.published" => "1"),
				"order" => "Network.network_id DESC",
				"recursive" => 2
			)
		);
		
		$this->set("pointer_data", $pointer_data);
		
		//print_r($pointer_data);
	}
	
}
