   <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo  $this->Html->url('/', true) ?>">DEPAN</a></li>
              <li class="disabled">SIMULASI KREDIT</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="medium-4 large-4 columns">
            <ul id="sidebar-menu">
			<?php
				//print_r($this->request);
			?>
              <li <?php if($this->request->params['action']=="view_harga") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/harga_mobil', true) ?>">Harga Mobil</a></li>
              <li <?php if($this->request->params['controller']=="credit") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/simulasi-kredit-mobil', true) ?>">Simulasi Kredit</a></li>
              <li <?php if($this->request->params['action']=="view_product") { ?>class="active" <?php } ?>><a href="<?php echo  $this->Html->url('/produk-layanan/paket-kredit', true) ?>">Paket Kredit</a></li>
            </ul>
            <div class="sidebar-umum mt-20 mb-20 show-for-large">
                <h3>PAKET KREDIT</h3>
				 <?php foreach($pakets as $row) { ?>
					<div><a href="<?php echo $row['Node']['link'] ?>"><img src="<?php echo $this->Html->url('/', true);?><?php echo $row['Node']['path'] ?>" alt="<?php echo $row['Node']['title'] ?>" class="banner" /></a></div>
				<?php } ?>

            </div>
            <div class="sidebar-umum mt-20 mb-20">
                <h3>APLIKASI ACC YES</h3>
                <div><img src="/images/banner-aplikasi.jpg" alt="" class="banner" usemap="#accyes2" />
                  <map name="accyes2" id="accyes2">
                    <area shape="rect" coords="8,48,175,107" href="https://itunes.apple.com/id/app/accyes!/id1129623253?mt=8" target="_blank" alt="ACCYes! on AppStore" />
                    <area shape="rect" coords="184,47,356,109" href="https://play.google.com/store/apps/details?id=com.ACCYes" target="_blank" alt="ACCYes! on google play store" />
                  </map>
                </div>
            </div>

          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1>SIMULASI KREDIT</h1>
            <!--content detail-->
          <!--simulasi-->
            <p>Anda ingin mencari mobil impian sesuai dengan budget? Silahkan lakukan SIMULASI KREDIT dengan menggunakan form di bawah ini dan klik <strong>APPLY</strong> jika Anda ingin dihubungi lebih lanjut oleh tim sales kami dengan penawaran HARGA TERBAIK. ACC selalu MEMBERI KEMUDAHAN.</p>
            <div id="simulasi-wrapper">
                <div id="simulasi-header">
                    <div class="row">
                      <div class="medium-8 large-8 columns"><h2>HITUNG ANGSURAN MOBIL<h2></div>
                      <div class="medium-4 large-4 columns" id="menusimulasi">
                          <a href="<?php echo  $this->Html->url('/simulasi-kredit-mobil', true) ?>" class="active"><img src="/images/icon-simulasi-selected.jpg" alt="" /> Mobil Baru</a>
                          <a href="<?php echo  $this->Html->url('/simulasi-kredit-mobil-bekas', true) ?>" class="last"><img src="/images/icon-simulasi-notselected.jpg" alt="" /> Mobil Bekas</a>
                        </div>
                    </div>
                </div>
                <div id="simulasi-body">
				    <?php if (isset($form_error) && !empty($form_error)) : ?>
					 <div class="row">
                        <div class="large-7 large-offset-1 columns">
							<div class="ui-state-error ui-corner-all" style="margin-bottom: 16px; padding: 8px;color:red!important">
								<b>Error :</b>
								<br /><br />
								<?php echo $form_error; ?>
							</div>
						</div>
					</div>
					<?php endif; ?>
                    <div class="row">
                        <div class="large-7 large-offset-1 columns">
                          <!--form simulasi-->
                           <?php
								echo $this->Form->create(
									"CreditSimulation",
									array(
										"url" => "/simulasi-kredit-mobil",
										"name" => "credit-simulations-form",
										"id" => "credit-simulations-form",
										"method" => "post"
									)
								);

								echo $this->Form->input(
									"type_id",
									array(
										"type" => "hidden",
										"id" => "type_id",
										"label" => false
									)
								);
								echo $this->Form->input(
									"year_of_manufactured",
									array(
										"type" => "hidden",
										"id" => "year_of_manufactured",
										"label" => false,
										"value"=> date("Y")
									)
								);
								echo $this->Form->input(
									"vehicle_status",
									array(
										"type" => "hidden",
										"id" => "vehicle_status",
										"label" => false,
										"value"=> "New"
									)
								);
								echo $this->Form->input(
									"acp_status",
									array(
										"type" => "hidden",
										"id" => "acp_status",
										"label" => false,
										"value"=> "Yes"
									)
								);
							?>
                            <div class="row">
                              <div class="small-4 columns">
                                <label for="merk" class="middle">Merk Kendaraan</label>
                              </div>
                              <div class="small-8 columns">
                                <div class="select-style2">
                                   <?php
									echo $this->Form->input(
										"brand_id",
										array(
											"id" => "brand_id",
											"class" => "input-baru",
											"div" => false,
											"label" => false,
											"options" => $brand_list,
											"value" => $brand
										)
									);
								?>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="small-4 columns">
                                <label for="tipe" class="middle">Tipe Kendaraan</label>
                              </div>
                              <div class="small-8 columns">
                                <div class="select-style2">
                                 <?php
									echo $this->Form->input(
									"code_group",
									array(
										"id" => "code_group",
										"class" => "input-baru",
										"div" => false,
										"label" => false,
										"options" => $group_list,
										"value" => $type
									)
								);
								?> <img id="vehicle-group-loading" src="/img/ajax-loader.gif" alt="loading" style="display: none;">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="small-4 columns">
                                <label for="harga" class="middle">Harga Kendaraan</label>
                              </div>
                              <div class="small-8 columns">
                                <?php
                  									echo $this->Form->input(
                  										"otr",
                  										array(
                  											"type" => "number",
                  											"id" => "otr",
                  											"placeholder"=>"Harga",
                  											"div" => false,
                  											'required' => true,
                  											"label" => false,
                  											"value" => $harga
                  										)
                  									);
                  								?>
                              </div>
                            </div>
                            <div class="row">
                              <div class="small-4 columns">
                                <label for="dp" class="middle">Down Payment</label>
                              </div>
                              <div class="small-8 columns">
                                   <?php
                      									echo $this->Form->input(
                      										"down_payment",
                      										array(
                      											"type" => "number",
                      											"id" => "down_payment",
                      											"div" => false,
                      											"placeholder"=>"Minimal 25%",
                      											'required' => true,
                      											"label" => false
                      										)
                      									);
                      								?>
                                <p class="help-text text-left" id="passwordHelpText">Minimal 25%</p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="large-4 small-4 columns">
                                <label for="password" class="text-right middle">&nbsp;</label>
                              </div>
                              <div class="large-8 small-8 columns">
								 <?php
									echo $this->Form->submit(
										"HITUNG",
										array(
											"id" => "form-submit",
											"class" => "button button-orange float-left",
											"div" => false
										)
									);
								?>

                              </div>
                            </div>
                           <?php echo $this->Form->end(); ?>
                          <!--form simulasi-->
                        </div>
                        <div class="medium-4 large-4 columns show-for-large"><img src="/images/simulasi_logo.jpg" alt="" /></div>
                    </div>
                    <!--hasil hitung-->
					<?php if ($this->Session->read("CSO.amt_installment")) {?>
                      <div id="hasilhitung-wrapper" class="mt-20">
                          <div id="hasilhitung-header">
                              HASIL PERHITUNGAN ANGSURAN
                          </div>
                          <div id="hasilhitung-body">
                            <table border="0" width="100%" class="tabelsimulasi">
                                <tr>
                                    <td align='center'>&nbsp;</td>
										<?php for ($i=1;$i<=4;$i++){ ?>
                                        <th><?=$i?> Tahun</th>
										<?php } ?>
                                    </tr>
                                <tr>
                                    <th>Uang Muka</th>
									<?php for ($i=1;$i<=4;$i++){
										$bg = "#FFFFFF";
										if ($i==2 || $i==4) $bg='#F0F0F0 ';
									?>
                                    <td valign="top" class="text-right" bgcolor="<?=$bg?>"><div id="uang-muka"><?php if (isset($uang_muka) && !empty($uang_muka)) echo number_format($uang_muka[$i]); ?></div></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <th>Angsuran</th>
									<?php for ($i=1;$i<=4;$i++){ ?>
                                    <td valign="top" class="text-right"><div id="angsuran"><?php if (isset($angsuran) && !empty($angsuran)) echo number_format($angsuran[$i]) ?></div></td>
                                   <?php } ?>
                                </tr>
                                <tr>
                                  <td></td>
          					      <?php for ($i=1;$i<=4;$i++){ ?>
								  <td valign="top" align="center">
									    <a href="<?php echo  $this->Html->url('/apply_form/'. $i, true) ?>" class="popup-link button-apply">Apply</a>
                                  </td>
								  <?php } ?>
                                  </tr>
                                </table>

                                </div>

                                <div class="mt-20">
                                    <ul>
                                        <li>Rincian kredit diatas tidak mengikat dan dapat berubah sewaktu-waktu.</li>
                                        <li>Harga diatas sudah termasuk asuransi all risk &amp; ACP (ACC Credit Protection)</li>
                                    </ul>
                                  </div>

                      </div>
					  <?php } ?>
                    <!--/hasil hitung-->
                </div>
                <div id="simulasi-topleft-img"><img src="/images/simulasikredit-top.png" alt="" /></div>
            </div>
            <!--/simulasi-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
         <script>
		$(document).ready(function(){
			$("#otr").focusout(function() {
				if ($("#otr").val()!=''){
					var dp = $("#otr").val()*25/100;
					$("#down_payment").val(dp);
				}
			});
			$("#credit-simulations-form").click(function(){
				var dp = $('#down_payment').val();
				var min_dp = $("#otr").val()*25/100;
				if (dp < min_dp){
					alert('Minimal DP adalah 25% dari harga mobil');
					$('#down_payment').focus();
					return false;
				}
				return true;
			});
			function load_vehicle_group()
			{
				$("#vehicle-group-loading").show();

				var brand_id = $("#brand_id").val();

				$.get("/simulation/credit/query_vehicle_group_new/" + brand_id, function(data) {
					data = $.trim(data);
					$("#code_group").empty().html(data);
					$("#vehicle-group-loading").hide();
					var type = '<?php echo $type ?>';
					$("#code_group").val(type);
					//alert($("#code_group").val());
					//load_vehicle_model2();
				});




			}
			$("#brand_id").change(function(){
				load_vehicle_group();
			});
			if ($("#otr").val() !=''){
				var dp = $("#otr").val()*25/100;
				$("#down_payment").val(dp);

			}

			load_vehicle_group();
		});
	  </script>
