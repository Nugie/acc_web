<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Branchs'), h($branch['Branch']['branch_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Branchs'), array('action' => 'index'));

if (isset($branch['Branch']['branch_id'])):
	$this->Html->addCrumb($branch['Branch']['branch_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Branch'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Branch'), array(
		'action' => 'edit',
		$branch['Branch']['branch_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Branch'), array(
		'action' => 'delete', $branch['Branch']['branch_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $branch['Branch']['branch_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Branchs'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Branch'), array('action' => 'add'), array('button' => 'success'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Areas'), array('controller' => 'areas', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Area'), array('controller' => 'areas', 'action' => 'add'));
$this->end();

$this->append('main');
?>
<div class="branchs view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Branch Id'); ?></dt>
		<dd>
			<?php echo h($branch['Branch']['branch_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Area'); ?></dt>
		<dd>
			<?php echo $this->Html->link($branch['Area']['id'], array('controller' => 'areas', 'action' => 'view', $branch['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Code Branch'); ?></dt>
		<dd>
			<?php echo h($branch['Branch']['code_branch']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Description'); ?></dt>
		<dd>
			<?php echo h($branch['Branch']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($branch['Branch']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($branch['Branch']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>