<div class="vehicleTypes index">
	<h2><?php echo __('Vehicle Types'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('type_id'); ?></th>
			<th><?php echo $this->Paginator->sort('brand_id'); ?></th>
			<th><?php echo $this->Paginator->sort('code_group'); ?></th>
			<th><?php echo $this->Paginator->sort('desc_group'); ?></th>
			<th><?php echo $this->Paginator->sort('code_type'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($vehicleTypes as $vehicleType): ?>
	<tr>
		<td><?php echo h($vehicleType['VehicleType']['type_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($vehicleType['Brand'][''], array('controller' => 'brands', 'action' => 'view', $vehicleType['Brand']['id'])); ?>
		</td>
		<td><?php echo h($vehicleType['VehicleType']['code_group']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['desc_group']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['code_type']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['description']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['created']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $vehicleType['VehicleType']['type_id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $vehicleType['VehicleType']['type_id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $vehicleType['VehicleType']['type_id']), array('confirm' => __('Are you sure you want to delete # %s?', $vehicleType['VehicleType']['type_id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Vehicle Type'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Brands'), array('controller' => 'brands', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Brand'), array('controller' => 'brands', 'action' => 'add')); ?> </li>
	</ul>
</div>
