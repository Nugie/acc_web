<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Trn Test Drives'), h($trnTestDrive['TrnTestDrive']['trn_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Trn Test Drives'), array('action' => 'index'));

if (isset($trnTestDrive['TrnTestDrive']['trn_id'])):
	$this->Html->addCrumb($trnTestDrive['TrnTestDrive']['trn_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Trn Test Drive'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Trn Test Drive'), array(
		'action' => 'edit',
		$trnTestDrive['TrnTestDrive']['trn_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Trn Test Drive'), array(
		'action' => 'delete', $trnTestDrive['TrnTestDrive']['trn_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $trnTestDrive['TrnTestDrive']['trn_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Trn Test Drives'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Trn Test Drive'), array('action' => 'add'), array('button' => 'success'));
$this->end();

$this->append('main');
?>
<div class="trnTestDrives view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Trn Id'); ?></dt>
		<dd>
			<?php echo h($trnTestDrive['TrnTestDrive']['trn_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Trn Date'); ?></dt>
		<dd>
			<?php echo $this->Time->format($trnTestDrive['TrnTestDrive']['trn_date'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Personal Name'); ?></dt>
		<dd>
			<?php echo h($trnTestDrive['TrnTestDrive']['personal_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Personal Phone'); ?></dt>
		<dd>
			<?php echo h($trnTestDrive['TrnTestDrive']['personal_phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Personal Email'); ?></dt>
		<dd>
			<?php echo h($trnTestDrive['TrnTestDrive']['personal_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Personal Area'); ?></dt>
		<dd>
			<?php echo h($trnTestDrive['TrnTestDrive']['personal_area']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Personal Branch'); ?></dt>
		<dd>
			<?php echo h($trnTestDrive['TrnTestDrive']['personal_branch']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>