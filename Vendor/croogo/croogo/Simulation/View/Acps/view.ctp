<div class="acps view">
<h2><?php echo __('Acp'); ?></h2>
	<dl>
		<dt><?php echo __('Acp Id'); ?></dt>
		<dd>
			<?php echo h($acp['Acp']['acp_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tenor'); ?></dt>
		<dd>
			<?php echo h($acp['Acp']['tenor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Acp Rate'); ?></dt>
		<dd>
			<?php echo h($acp['Acp']['acp_rate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($acp['Acp']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($acp['Acp']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Acp'), array('action' => 'edit', $acp['Acp']['acp_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Acp'), array('action' => 'delete', $acp['Acp']['acp_id']), array('confirm' => __('Are you sure you want to delete # %s?', $acp['Acp']['acp_id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Acps'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Acp'), array('action' => 'add')); ?> </li>
	</ul>
</div>
