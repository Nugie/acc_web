<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Vehicle Types');
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Vehicle Types'), array('action' => 'index'));

if ($this->action == 'admin_edit') {
	$this->Html->addCrumb($this->request->data['VehicleType']['type_id'], '/' . $this->request->url);
	$this->viewVars['title_for_layout'] = __d('simulation', 'Vehicle Types') . ': ' . $this->request->data['VehicleType']['type_id'];
} else {
	$this->Html->addCrumb(__d('croogo', 'Add'), '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('VehicleType'));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('simulation', 'Vehicle Type'), '#vehicle-type');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('vehicle-type');

		echo $this->Form->input('type_id');

		echo $this->Form->input('brand_id', array(
			'label' =>  __d('simulation', 'Brand'),
		));
		echo $this->Form->input('code_group', array(
			'label' =>  __d('simulation', 'Code Group'),
		));
		echo $this->Form->input('desc_group', array(
			'label' =>  __d('simulation', 'Desc Group'),
		));
		echo $this->Form->input('code_type', array(
			'label' =>  __d('simulation', 'Code Type'),
		));
		echo $this->Form->input('description', array(
			'label' =>  __d('simulation', 'Description'),
		));

	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'primary')) .
		$this->Form->button(__d('croogo', 'Save & New'), array('button' => 'success', 'name' => 'save_and_new')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('button' => 'danger'));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
