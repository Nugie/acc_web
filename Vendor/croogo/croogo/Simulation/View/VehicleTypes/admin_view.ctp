<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Vehicle Types'), h($vehicleType['VehicleType']['type_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Vehicle Types'), array('action' => 'index'));

if (isset($vehicleType['VehicleType']['type_id'])):
	$this->Html->addCrumb($vehicleType['VehicleType']['type_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Vehicle Type'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Vehicle Type'), array(
		'action' => 'edit',
		$vehicleType['VehicleType']['type_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Vehicle Type'), array(
		'action' => 'delete', $vehicleType['VehicleType']['type_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $vehicleType['VehicleType']['type_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Vehicle Types'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Vehicle Type'), array('action' => 'add'), array('button' => 'success'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Brands'), array('controller' => 'brands', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Brand'), array('controller' => 'brands', 'action' => 'add'));
$this->end();

$this->append('main');
?>
<div class="vehicleTypes view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Type Id'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['type_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Brand'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vehicleType['Brand'][''], array('controller' => 'brands', 'action' => 'view', $vehicleType['Brand']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Code Group'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['code_group']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Desc Group'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['desc_group']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Code Type'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['code_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Description'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($vehicleType['VehicleType']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($vehicleType['VehicleType']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>