<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Administrations'), h($administration['Administration']['adm_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Administrations'), array('action' => 'index'));

if (isset($administration['Administration']['adm_id'])):
	$this->Html->addCrumb($administration['Administration']['adm_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Administration'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Administration'), array(
		'action' => 'edit',
		$administration['Administration']['adm_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Administration'), array(
		'action' => 'delete', $administration['Administration']['adm_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $administration['Administration']['adm_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Administrations'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Administration'), array('action' => 'add'), array('button' => 'success'));
$this->end();

$this->append('main');
?>
<div class="administrations view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Adm Id'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['adm_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Tenor'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['tenor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Vehicle Status'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['vehicle_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Polis'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['polis']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Fiducia'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['fiducia']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Administration'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['administration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($administration['Administration']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($administration['Administration']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>