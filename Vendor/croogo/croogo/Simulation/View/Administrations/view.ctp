<div class="administrations view">
<h2><?php echo __('Administration'); ?></h2>
	<dl>
		<dt><?php echo __('Adm Id'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['adm_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tenor'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['tenor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vehicle Status'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['vehicle_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Polis'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['polis']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fiducia'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['fiducia']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Administration'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['administration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($administration['Administration']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Administration'), array('action' => 'edit', $administration['Administration']['adm_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Administration'), array('action' => 'delete', $administration['Administration']['adm_id']), array('confirm' => __('Are you sure you want to delete # %s?', $administration['Administration']['adm_id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Administrations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Administration'), array('action' => 'add')); ?> </li>
	</ul>
</div>
