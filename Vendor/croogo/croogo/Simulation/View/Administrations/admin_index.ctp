<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Administrations');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Administrations'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('adm_id'),
		$this->Paginator->sort('tenor'),
		$this->Paginator->sort('vehicle_status'),
		$this->Paginator->sort('polis'),
		$this->Paginator->sort('fiducia'),
		$this->Paginator->sort('administration'),
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($administrations as $administration):
		$row = array();
		$row[] = h($administration['Administration']['adm_id']);
		$row[] = h($administration['Administration']['tenor']);
		$row[] = h($administration['Administration']['vehicle_status']);
		$row[] = h($administration['Administration']['polis']);
		$row[] = h($administration['Administration']['fiducia']);
		$row[] = h($administration['Administration']['administration']);
		$row[] = $this->Time->format($administration['Administration']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = $this->Time->format($administration['Administration']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($administration['Administration']['adm_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $administration['Administration']['adm_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$administration['Administration']['adm_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$administration['Administration']['adm_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $administration['Administration']['adm_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
