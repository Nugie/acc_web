<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'New Rates');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'New Rates'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('nr_id'),
		$this->Paginator->sort('area_id'),
		$this->Paginator->sort('branch_id'),
		$this->Paginator->sort('brand_id'),
		$this->Paginator->sort('type_id'),
		$this->Paginator->sort('category_id'),
		$this->Paginator->sort('kind_id'),
		$this->Paginator->sort('tenor'),
		$this->Paginator->sort('from_dp'),
		$this->Paginator->sort('to_dp'),
		$this->Paginator->sort('flat_rate_percentage'),
		//$this->Paginator->sort('created'),
		//$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($newRates as $newRate):
		$row = array();
		$row[] = h($newRate['NewRate']['nr_id']);
		$row[] = $this->Html->link($newRate['Area']['description'], array(
			'controller' => 'areas',
		'action' => 'view',
			$newRate['Area']['id'],
	));
		$row[] = $this->Html->link($newRate['Branch']['description'], array(
			'controller' => 'branches',
		'action' => 'view',
			$newRate['Branch']['branch_id'],
	));
		$row[] = $this->Html->link($newRate['Brand']['description'], array(
			'controller' => 'brands',
		'action' => 'view',
			$newRate['Brand']['brand_id'],
	));
		$row[] = $this->Html->link($newRate['Type']['description'], array(
			'controller' => 'types',
		'action' => 'view',
			$newRate['Type']['type_id'],
	));
		$row[] = $this->Html->link($newRate['Category']['description'], array(
			'controller' => 'categories',
		'action' => 'view',
			$newRate['Category']['category_id'],
	));
		$row[] = $this->Html->link($newRate['Kind']['description'], array(
			'controller' => 'kinds',
		'action' => 'view',
			$newRate['Kind']['kind_id'],
	));
		$row[] = h($newRate['NewRate']['tenor']);
		$row[] = h($newRate['NewRate']['from_dp']);
		$row[] = h($newRate['NewRate']['to_dp']);
		$row[] = h($newRate['NewRate']['flat_rate_percentage']);
		//$row[] = $this->Time->format($newRate['NewRate']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		//$row[] = $this->Time->format($newRate['NewRate']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($newRate['NewRate']['nr_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $newRate['NewRate']['nr_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$newRate['NewRate']['nr_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$newRate['NewRate']['nr_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $newRate['NewRate']['nr_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
