<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'New Rates');
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'New Rates'), array('action' => 'index'));

if ($this->action == 'admin_edit') {
	$this->Html->addCrumb($this->request->data['NewRate']['nr_id'], '/' . $this->request->url);
	$this->viewVars['title_for_layout'] = __d('simulation', 'New Rates') . ': ' . $this->request->data['NewRate']['nr_id'];
} else {
	$this->Html->addCrumb(__d('croogo', 'Add'), '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('NewRate'));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('simulation', 'New Rate'), '#new-rate');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('new-rate');

		echo $this->Form->input('nr_id');

		echo $this->Form->input('area_id', array(
			'label' =>  __d('simulation', 'Area'),
		));
		echo $this->Form->input('branch_id', array(
			'label' =>  __d('simulation', 'Branch'),
		));
		echo $this->Form->input('brand_id', array(
			'label' =>  __d('simulation', 'Brand'),
		));
		echo $this->Form->input('type_id', array(
			'label' =>  __d('simulation', 'Type'),
		));
		echo $this->Form->input('category_id', array(
			'label' =>  __d('simulation', 'Category'),
		));
		echo $this->Form->input('kind_id', array(
			'label' =>  __d('simulation', 'Kind'),
		));
		echo $this->Form->input('tenor', array(
			'label' =>  __d('simulation', 'Tenor'),
		));
		echo $this->Form->input('from_dp', array(
			'label' =>  __d('simulation', 'From Dp'),
		));
		echo $this->Form->input('to_dp', array(
			'label' =>  __d('simulation', 'To Dp'),
		));
		echo $this->Form->input('flat_rate_percentage', array(
			'label' =>  __d('simulation', 'Flat Rate Percentage'),
		));

	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'primary')) .
		$this->Form->button(__d('croogo', 'Save & New'), array('button' => 'success', 'name' => 'save_and_new')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('button' => 'danger'));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
