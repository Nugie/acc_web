<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'New Rates'), h($newRate['NewRate']['nr_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'New Rates'), array('action' => 'index'));

if (isset($newRate['NewRate']['nr_id'])):
	$this->Html->addCrumb($newRate['NewRate']['nr_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'New Rate'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit New Rate'), array(
		'action' => 'edit',
		$newRate['NewRate']['nr_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete New Rate'), array(
		'action' => 'delete', $newRate['NewRate']['nr_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $newRate['NewRate']['nr_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List New Rates'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New New Rate'), array('action' => 'add'), array('button' => 'success'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Areas'), array('controller' => 'areas', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Area'), array('controller' => 'areas', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Branches'), array('controller' => 'branches', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Branch'), array('controller' => 'branches', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Brands'), array('controller' => 'brands', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Brand'), array('controller' => 'brands', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Types'), array('controller' => 'types', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Type'), array('controller' => 'types', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Categories'), array('controller' => 'categories', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Category'), array('controller' => 'categories', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Kinds'), array('controller' => 'kinds', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Kind'), array('controller' => 'kinds', 'action' => 'add'));
$this->end();

$this->append('main');
?>
<div class="newRates view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Nr Id'); ?></dt>
		<dd>
			<?php echo h($newRate['NewRate']['nr_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Area'); ?></dt>
		<dd>
			<?php echo $this->Html->link($newRate['Area']['id'], array('controller' => 'areas', 'action' => 'view', $newRate['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Branch'); ?></dt>
		<dd>
			<?php echo $this->Html->link($newRate['Branch'][''], array('controller' => 'branches', 'action' => 'view', $newRate['Branch']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Brand'); ?></dt>
		<dd>
			<?php echo $this->Html->link($newRate['Brand'][''], array('controller' => 'brands', 'action' => 'view', $newRate['Brand']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($newRate['Type']['title'], array('controller' => 'types', 'action' => 'view', $newRate['Type']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($newRate['Category'][''], array('controller' => 'categories', 'action' => 'view', $newRate['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Kind'); ?></dt>
		<dd>
			<?php echo $this->Html->link($newRate['Kind'][''], array('controller' => 'kinds', 'action' => 'view', $newRate['Kind']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Tenor'); ?></dt>
		<dd>
			<?php echo h($newRate['NewRate']['tenor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'From Dp'); ?></dt>
		<dd>
			<?php echo h($newRate['NewRate']['from_dp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'To Dp'); ?></dt>
		<dd>
			<?php echo h($newRate['NewRate']['to_dp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Flat Rate Percentage'); ?></dt>
		<dd>
			<?php echo h($newRate['NewRate']['flat_rate_percentage']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($newRate['NewRate']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($newRate['NewRate']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>