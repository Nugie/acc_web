<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Kinds');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Kinds'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('kind_id'),
		$this->Paginator->sort('category_id'),
		$this->Paginator->sort('code_kind'),
		$this->Paginator->sort('description'),
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	//print_r($kinds);
	foreach ($kinds as $kind):
		$row = array();
		$row[] = h($kind['Kind']['kind_id']);
		$row[] = $this->Html->link($kind['Category']['description'], array(
			'controller' => 'categories',
		'action' => 'view',
			$kind['Category']['category_id'],
	));
		$row[] = h($kind['Kind']['code_kind']);
		$row[] = h($kind['Kind']['description']);
		$row[] = $this->Time->format($kind['Kind']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = $this->Time->format($kind['Kind']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($kind['Kind']['kind_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $kind['Kind']['kind_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$kind['Kind']['kind_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$kind['Kind']['kind_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $kind['Kind']['kind_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
