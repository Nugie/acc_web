<div class="vehicleModels view">
<h2><?php echo __('Vehicle Model'); ?></h2>
	<dl>
		<dt><?php echo __('Model Id'); ?></dt>
		<dd>
			<?php echo h($vehicleModel['VehicleModel']['model_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Brand'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vehicleModel['Brand'][''], array('controller' => 'brands', 'action' => 'view', $vehicleModel['Brand']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vehicleModel['Type']['title'], array('controller' => 'types', 'action' => 'view', $vehicleModel['Type']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vehicleModel['Category'][''], array('controller' => 'categories', 'action' => 'view', $vehicleModel['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Kind'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vehicleModel['Kind'][''], array('controller' => 'kinds', 'action' => 'view', $vehicleModel['Kind']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Group'); ?></dt>
		<dd>
			<?php echo h($vehicleModel['VehicleModel']['code_group']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Model'); ?></dt>
		<dd>
			<?php echo h($vehicleModel['VehicleModel']['code_model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($vehicleModel['VehicleModel']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($vehicleModel['VehicleModel']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($vehicleModel['VehicleModel']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vehicle Model'), array('action' => 'edit', $vehicleModel['VehicleModel']['model_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vehicle Model'), array('action' => 'delete', $vehicleModel['VehicleModel']['model_id']), array('confirm' => __('Are you sure you want to delete # %s?', $vehicleModel['VehicleModel']['model_id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicle Models'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle Model'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Brands'), array('controller' => 'brands', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Brand'), array('controller' => 'brands', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Kinds'), array('controller' => 'kinds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Kind'), array('controller' => 'kinds', 'action' => 'add')); ?> </li>
	</ul>
</div>
