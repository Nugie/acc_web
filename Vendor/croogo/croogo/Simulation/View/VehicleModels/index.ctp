<div class="vehicleModels index">
	<h2><?php echo __('Vehicle Models'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('model_id'); ?></th>
			<th><?php echo $this->Paginator->sort('brand_id'); ?></th>
			<th><?php echo $this->Paginator->sort('type_id'); ?></th>
			<th><?php echo $this->Paginator->sort('category_id'); ?></th>
			<th><?php echo $this->Paginator->sort('kind_id'); ?></th>
			<th><?php echo $this->Paginator->sort('code_group'); ?></th>
			<th><?php echo $this->Paginator->sort('code_model'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($vehicleModels as $vehicleModel): ?>
	<tr>
		<td><?php echo h($vehicleModel['VehicleModel']['model_id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($vehicleModel['Brand'][''], array('controller' => 'brands', 'action' => 'view', $vehicleModel['Brand']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($vehicleModel['Type']['title'], array('controller' => 'types', 'action' => 'view', $vehicleModel['Type']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($vehicleModel['Category'][''], array('controller' => 'categories', 'action' => 'view', $vehicleModel['Category']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($vehicleModel['Kind'][''], array('controller' => 'kinds', 'action' => 'view', $vehicleModel['Kind']['id'])); ?>
		</td>
		<td><?php echo h($vehicleModel['VehicleModel']['code_group']); ?>&nbsp;</td>
		<td><?php echo h($vehicleModel['VehicleModel']['code_model']); ?>&nbsp;</td>
		<td><?php echo h($vehicleModel['VehicleModel']['description']); ?>&nbsp;</td>
		<td><?php echo h($vehicleModel['VehicleModel']['created']); ?>&nbsp;</td>
		<td><?php echo h($vehicleModel['VehicleModel']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $vehicleModel['VehicleModel']['model_id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $vehicleModel['VehicleModel']['model_id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $vehicleModel['VehicleModel']['model_id']), array('confirm' => __('Are you sure you want to delete # %s?', $vehicleModel['VehicleModel']['model_id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Vehicle Model'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Brands'), array('controller' => 'brands', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Brand'), array('controller' => 'brands', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Kinds'), array('controller' => 'kinds', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Kind'), array('controller' => 'kinds', 'action' => 'add')); ?> </li>
	</ul>
</div>
