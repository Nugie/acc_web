<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Brands'), h($brand['Brand']['brand_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Brands'), array('action' => 'index'));

if (isset($brand['Brand']['brand_id'])):
	$this->Html->addCrumb($brand['Brand']['brand_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Brand'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Brand'), array(
		'action' => 'edit',
		$brand['Brand']['brand_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Brand'), array(
		'action' => 'delete', $brand['Brand']['brand_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $brand['Brand']['brand_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Brands'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Brand'), array('action' => 'add'), array('button' => 'success'));
$this->end();

$this->append('main');
?>
<div class="brands view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Brand Id'); ?></dt>
		<dd>
			<?php echo h($brand['Brand']['brand_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Code Brand'); ?></dt>
		<dd>
			<?php echo h($brand['Brand']['code_brand']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Description'); ?></dt>
		<dd>
			<?php echo h($brand['Brand']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($brand['Brand']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($brand['Brand']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>