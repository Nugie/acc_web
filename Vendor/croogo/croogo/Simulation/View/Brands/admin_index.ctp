<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Brands');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Brands'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('brand_id'),
		$this->Paginator->sort('code_brand'),
		$this->Paginator->sort('description'),
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($brands as $brand):
		$row = array();
		$row[] = h($brand['Brand']['brand_id']);
		$row[] = h($brand['Brand']['code_brand']);
		$row[] = h($brand['Brand']['description']);
		$row[] = $this->Time->format($brand['Brand']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = $this->Time->format($brand['Brand']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($brand['Brand']['brand_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $brand['Brand']['brand_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$brand['Brand']['brand_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$brand['Brand']['brand_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $brand['Brand']['brand_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
