<?php
App::uses('SimulationAppModel', 'Simulation.Model');
/**
 * Administration Model
 *
 */
class Administration extends SimulationAppModel {

public $useTable = 'cso_administrations'; 

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'adm_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tenor' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
