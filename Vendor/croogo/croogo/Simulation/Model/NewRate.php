<?php
App::uses('SimulationAppModel', 'Simulation.Model');
/**
 * NewRate Model
 *
 * @property Area $Area
 * @property Branch $Branch
 * @property Brand $Brand
 * @property Type $Type
 * @property Category $Category
 * @property Kind $Kind
 */
class NewRate extends SimulationAppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'nr_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tenor' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'flat_rate_percentage' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Area' => array(
			'className' => 'Area',
			'foreignKey' => 'area_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Branch' => array(
			'className' => 'Branch',
			'foreignKey' => false,
			'conditions' => array(
				'`Branch`.`branch_id`=`NewRate`.`branch_id`',
			),
			'fields' => '',
			'order' => ''
		),
		'Brand' => array(
			'className' => 'Brand',
			'foreignKey' => false,
			'conditions' => array(
				'`Brand`.`brand_id`=`NewRate`.`brand_id`',
			),
			'fields' => '',
			'order' => ''
		),
		 'Type' => array(
			'className' => 'VehicleType',
			'foreignKey' => false,
			'conditions' => array(
				'`Type`.`type_id`=`NewRate`.`type_id`',
				'`Type`.`brand_id`=`NewRate`.`brand_id`'
			),
			'fields' => '',
			'order' => ''
		),
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => false,
			'conditions' => array(
				'`Category`.`category_id`=`NewRate`.`category_id`',
			),
			'fields' => '',
			'order' => ''
		),
		'Kind' => array(
			'className' => 'Kind',
			'foreignKey' => false,
			'conditions' => array(
				'`Kind`.`kind_id`=`NewRate`.`kind_id`',
			),
			'fields' => '',
			'order' => ''
		)
	);
}
