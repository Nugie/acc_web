<?php
App::uses('SimulationAppModel', 'Simulation.Model');
/**
 * Kind Model
 *
 * @property Category $Category
 */
class Kind extends SimulationAppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'kind_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'code_kind' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => false,
			'conditions' => array(
				'`Kind`.`category_id`=`Category`.`category_id`',
			),
			'fields' => '',
			'order' => ''
		)
	);
}
