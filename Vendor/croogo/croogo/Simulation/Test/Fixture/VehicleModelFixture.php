<?php
/**
 * VehicleModel Fixture
 */
class VehicleModelFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'model_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'brand_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'type_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'category_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'kind_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'code_group' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'code_model' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 32, 'key' => 'index', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'description' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 64, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'model_id', 'unique' => 1),
			'brand_id' => array('column' => 'brand_id', 'unique' => 0),
			'type_id' => array('column' => 'type_id', 'unique' => 0),
			'category_id' => array('column' => 'category_id', 'unique' => 0),
			'kind_id' => array('column' => 'kind_id', 'unique' => 0),
			'code_model' => array('column' => 'code_model', 'unique' => 0),
			'brand_id_2' => array('column' => array('brand_id', 'type_id', 'category_id', 'kind_id', 'code_model'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'model_id' => '',
			'brand_id' => '',
			'type_id' => '',
			'category_id' => '',
			'kind_id' => '',
			'code_group' => 'Lorem ipsum dolor ',
			'code_model' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet',
			'created' => '2016-11-06 04:28:50',
			'modified' => '2016-11-06 04:28:50'
		),
	);

}
