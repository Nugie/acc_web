<?php
/**
 * Administration Fixture
 */
class AdministrationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'adm_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'tenor' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true),
		'polis' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true),
		'fiducia' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true),
		'administration' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'adm_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'adm_id' => '',
			'tenor' => 1,
			'polis' => '',
			'fiducia' => '',
			'administration' => '',
			'created' => '2016-11-04 20:58:59',
			'modified' => '2016-11-04 20:58:59'
		),
	);

}
