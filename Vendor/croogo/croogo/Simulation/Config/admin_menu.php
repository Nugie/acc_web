<?php

CroogoNav::add('sidebar', 'Simulation', array(
	'title' => 'Credit Simulation',
	'icon' => 'credit-card',
	'url' => '#',
	'weight' => 50,
	'children' => array(
		'New Rates' => array(
			'title' => 'New Rates',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'NewRates',
				'action' => 'index',
			),
		),
		'Used Rates' => array(
			'title' => 'Used Rates',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'UsedRates',
				'action' => 'index',
			),
		),
		'ACPs' => array(
			'title' => 'ACPs',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'acps',
				'action' => 'index',
			),
		),
		'Administration Fee' => array(
			'title' => 'Administration Fee',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'administrations',
				'action' => 'index', 
			),
		),
		'Areas' => array(
			'title' => 'Areas',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'Areas',
				'action' => 'index',
			),
		),
		'Branches' => array(
			'title' => 'Branches',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'branchs',
				'action' => 'index',
			),
		),
		'Brands' => array(
			'title' => 'Brands',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'brands',
				'action' => 'index',
			),
		),
		'Categories' => array(
			'title' => 'Categories',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'categories',
				'action' => 'index',
			),
		),
		'Kinds' => array(
			'title' => 'Kinds',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'kinds',
				'action' => 'index',
			),
		),
		'Models' => array(
			'title' => 'Models',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'vehicle_models',
				'action' => 'index',
			),
		),
		'Types' => array(
			'title' => 'Types',
			'url' => array(
				'admin' => true,
				'plugin' => 'simulation',
				'controller' => 'vehicle_types',
				'action' => 'index',
			),
		),
	),
));