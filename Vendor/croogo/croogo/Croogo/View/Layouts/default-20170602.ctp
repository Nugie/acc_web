<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title><?php echo $title_for_layout; ?> - <?php echo Configure::read('Site.title'); ?></title>
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@acckreditmobil">
		<meta name="twitter:title" content="<?php echo $title_for_layout; ?>">
		<meta property="og:title" content="<?php echo $title_for_layout; ?>" />
		<?php if (isset($image_for_layout)) { ?>
		<meta property="og:image" content="<?php echo $image_for_layout; ?>" />
		<meta name="twitter:image:src" content="<?php echo $image_for_layout; ?>" />
		<?php } else { ?>
		<meta property="og:image" content="<?php echo $this->webroot; ?>images/logo-acc-1216.png" />
		<meta name="twitter:image:src" content="<?php echo $this->webroot; ?>images/logo-acc-1216.png" />
		<?php }  ?>
<meta property="og:image:width" content="400" />
    <meta property="og:image:height" content="267" />
		<?php
		echo $this->Meta->meta();
		echo $this->Layout->feed();
		echo $this->Html->css(array(
			'/css/foundation.min.css',
			'/css/jquery.bxslider.css',
			'/css/jquery.magnificpopup.css',
			'/css/app.css?ver=4.2',
		));
//echo $this->Layout->js();
		echo $this->Html->script(array(
			'/js/vendor/jquery',
		));
		/*
		echo $this->Html->script(array(
			'/js/jquery/jquery.min',
			'/js/jquery/jquery.hoverIntent.minified',
			'/js/jquery/superfish',
			'/js/jquery/supersubs',
			'/js/theme',
		));*/
		//echo $this->Blocks->get('css');
		//echo $this->Blocks->get('script');
	?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="/images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
  </head>
  <body>
 <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-48042405-1', 'acc.co.id');
      ga('send', 'pageview');
  </script>
  <div data-sticky-container>
    <div data-sticky data-sticky-on="small" data-options="marginTop:0;" class="acc-sticky-header">
    <header>
	  <!--top header-->
      <div id="topheader" class="row">
          <div class="small-12 medium-3 large-4 columns text-center"><a href="<?php echo $this->Html->url('/', true);?>"><img src="<?php echo $this->webroot; ?>images/logo-acc-1216.png" alt="Astra Credit Companies" /></a></div>
          <div class="small-12 medium-5 large-4 columns topheader-menu">
              <div class="text-center"><a href="<?php echo $this->Html->url('/paket-kredit-acc', true);?>" target="_blank">PAKET KREDIT</a> | <a href="<?php echo $this->Html->url('/harga-mobil', true);?>">HARGA MOBIL</a></div>
					<?php
						//$loguser = $this->request->session()->read('Auth.User');
						//print_r($_SESSION);
						//$loguser = ;
					?>
					<?php if (isset($_SESSION['Auth']['User'])) { ?>
 <div class="text-center"><a href="<?php echo $this->Html->url('/member', true);?>">MEMBER</a> | <a href="<?php echo $this->Html->url('/logout', true);?>">LOGOUT</a></div>
					<?php } else { ?>
						 <div class="text-center"><a href="<?php echo $this->Html->url('/register', true);?>">DAFTAR</a> | <a href="<?php echo $this->Html->url('/login', true);?>">LOGIN</a></div>
					<?php } ?>

          </div>
            <div class="small-12 medium-3 large-2 columns text-center">
              <form method="get" action="<?php echo $this->webroot;?>search">
                <input name="q" type="text" placeholder="cari" class="topsearch" /><input type="image" src="<?php echo $this->webroot; ?>images/button-search.png" class="middle" />
              </form>
          </div>
          <div class="small-12 medium-3 large-2 topheader-menu columns end" id="topheader-lang">
			   <?php if (isset($this->request->params['locale'])) { $lang=$this->request->params['locale']; } else {$lang='ind';} ?>
              <a href="<?php echo $this->Html->url('/', true);?>" <?php if ($lang=='ind') { ?>class="active" <?php } ?>>INDONESIA</a> | <a href="<?php echo $this->Html->url('/eng', true);?>" <?php if ($lang=='eng') { ?>class="active" <?php } ?>>ENGLISH</a>
          </div>
      </div>
      <!--/top header-->


      <!--menu-->
      <div id="menutop">
        <div class="title-bar" data-responsive-toggle="top-bar" data-hide-for="medium">
          <button class="menu-icon" type="button" data-toggle></button>
          <div class="title-bar-title">Main Menu</div>
        </div>

        <div class="row">
		  <?php echo $this->Menus->menu('main',array('tagAttributes'=> array('class'=>'vertical medium-horizontal menu','data-responsive-menu'=>'drilldown medium-dropdown'))); ?>
        </div>
      </div>
      <!--/menu-->
	 </header>
 </div></div>
			<?php
				echo $this->Layout->sessionFlash();
				echo $content_for_layout;
			?>


	   <!--shopping tools-->
      <div id="shoppingtools">
        <div class="shoppingtools shoppingtools-short">
            <div class="float-left">
              <a href="javascript:void(0)" class="linkshoppingtools"><img src="<?php echo $this->webroot; ?>images/img-shoppingtool.png" width="81" height="150" /></a>
            </div>
            <div>
                <ul id="shoppingtools-menu">
                    <li><a href="<?php echo $this->Html->url('/simulasi-kredit-mobil', true);?>">Simulasi Kredit</a></li>
                    <li><a href="<?php echo $this->Html->url('/harga-mobil', true);?>">Harga Mobil</a></li>
                    <li><a href="<?php echo $this->Html->url('/paket-kredit-acc', true);?>" target="_blank">Paket Kredit</a></li>
                    <li><a href="<?php echo $this->Html->url('/produk-layanan/cara-pengajuan-kredit', true);?>">Cara Ajukan Kredit</a></li>
                </ul>
            </div>
        </div>
      </div>
      <!--/shopping tools-->
      <!--footer-->
      <footer>
      <!--peta situs-->
      <div id="petasitus">
        <div id="menubottom">PETA SITUS</div>
        <div class="row petasitus-content columns">
              <div class="large-9 large-centered columns">
                <div class="row">
					             <?php print_r($this->Menus->footer('main')); ?>
                </div>
             </div>
        </div>
      </div>
      <!--end peta situs-->

        <div class="row columns" id="footer-bottom">
              <div class="large-7 large-centered columns">
                  <div class="row small-collapse">
                      <div class="small-6 medium-3 large-3 columns mb-10">
                          <p class="text-center mb-10"><b>Ikuti kami di media sosial</b></p>
                          <p class="text-center">
                              <a href="https://www.facebook.com/ACCKreditMobil" target="_blank"><img src="<?php echo $this->webroot; ?>images/icon-fb.jpg" alt="" /></a>
                              <a href="https://www.twitter.com/ACCKreditMobil" target="_blank"><img src="<?php echo $this->webroot; ?>images/icon-twitter.jpg" alt="" /></a>
                              <a href="https://www.instagram.com/acckreditmobil/" target="_blank"><img src="<?php echo $this->webroot; ?>images/icon-instagram.jpg" alt="" /></a>
                              <a href="https://www.youtube.com/channel/UCGD0ql_0Zqc4NhrQutIKcFw" target="_blank"><img src="<?php echo $this->webroot; ?>images/icon-youtube.jpg" alt="" /></a>
                          </p>
                      </div>
                          <div class="small-6 medium-4 large-4 columns mb-10">
                          <p class="text-center mb-5"><b>ACC terdaftar &amp; diawasi oleh</b></p>
                          <p class="text-center">
                              <img src="<?php echo $this->webroot; ?>images/img-ojk.jpg" alt="" />
                          </p>
                      </div>
                        <div class="small-8 medium-3 large-3 columns mb-10">
                          <p class="text-center mb-5"><b>Hubungi kami</b></p>
                          <p class="text-center">
                              <img src="<?php echo $this->webroot; ?>images/img-contact_0305.jpg" alt="" />
                          </p>
                      </div>
                      <div class="small-4 medium-2 large-2 columns mb-10">
                          <p>
                              <img src="<?php echo $this->webroot; ?>images/logo-acc-footer.jpg" alt="" />
                          </p>
                      </div>
                  </div>
              </div>
              <div class="text-center">&copy; <?php echo date("Y")?> Astra Credit Companies</div>
      </footer>
      <!--/footer-->
      <!--scroll to top-->
      <div class="scroll-top-wrapper ">
        <span class="scroll-top-inner">
          <i class="fa fa-2x fa-chevron-up"></i>
        </span>
      </div>

      <!--/scroll to top-->

    <script src="<?php echo $this->webroot; ?>js/vendor/what-input.js"></script>
    <script src="<?php echo $this->webroot; ?>js/vendor/foundation.min.js"></script>
    <script src="<?php echo $this->webroot; ?>js/vendor/jquery.bxslider.min.js"></script>
	 <script src="<?php echo $this->webroot; ?>js/vendor/magnificpopup.js"></script>
    <script src="<?php echo $this->webroot; ?>js/app.js?ver=2.8"></script>
	<?php
		echo $this->Blocks->get('scriptBottom');
		echo $this->Js->writeBuffer();
	?>
  </body>
</html>
