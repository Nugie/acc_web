<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title_for_layout; ?> &raquo; <?php echo Configure::read('Site.title'); ?></title>
		<?php
		echo $this->Meta->meta();
		echo $this->Layout->feed();
		echo $this->Html->css(array(
			'/croogo/css/foundation.min.css',
			'/croogo/css/jquery.bxslider.css',
			'/croogo/css/app.css?ver=2.7',
		));
		echo $this->Layout->js();
		/*
		echo $this->Html->script(array(
			'/croogo/js/jquery/jquery.min',
			'/croogo/js/jquery/jquery.hoverIntent.minified',
			'/croogo/js/jquery/superfish',
			'/croogo/js/jquery/supersubs',
			'/croogo/js/theme',
		));*/
		echo $this->Blocks->get('css');
		echo $this->Blocks->get('script');
	?>
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  </head>
  <body>
    <header>
       <!--top header-->
      <div id="topheader" class="row">
          <div class="large-4 small-3 columns text-center"><a href="index.html"><img src="images/logo-acc.jpg" alt="Astra Credit Companies" /></a></div>
          <div class="large-5 small-9 columns topheader-menu">
              <div><a href="#" class="active">INDONESIA</a> | <a href="#">ENGLISH</a></div>
              <div><a href="#">PAKET KREDIT</a> | <a href="hargamobil.html">HARGA MOBIL</a></div>
              <div><a href="member-register.html">DAFTAR</a> | <a href="member-login.html">LOGIN</a></div>
          </div>
          <div class="large-2 columns end text-center">
              <form>
                <input type="text" placeholder="cari" class="topsearch" /><input type="image" src="images/button-search.jpg" class="middle" />
              </form>
          </div>
      </div>
      <!--/top header-->
      <!--menu-->
      <div id="menutop">
        <div class="title-bar" data-responsive-toggle="top-bar" data-hide-for="medium">
          <button class="menu-icon" type="button" data-toggle></button>
          <div class="title-bar-title">Main Menu</div>
        </div>

        <div class="row">
          <div class="top-bar menu-centered" id="top-bar">
                <ul class="vertical medium-horizontal menu" data-responsive-menu="drilldown medium-dropdown">
                  <li>
                    <a href="#">Tentang Kami</a>
					 
                    <ul class="menu arrow_box">
					   	<?php //echo $this->Menus->menu('main'); ?>
                      <li><a href="about-sejarah.html">Riwayat Singkat Perusahaan</a></li>
                      <li><a href="about-visi.html">Visi, Misi dan Nilai</a></li>
                      <li><a href="about-strukturgrup.html">Struktur Grup</a></li>
                      <li><a href="about-strukturkepemilikan.html">Struktur Kepemilikan</a></li>
                      <li><a href="about-strukturorganisasi.html">Struktur Organisasi</a></li>
                      <li><a href="about-boc.html">Dewan Komisaris</a></li>
                      <li><a href="about-syariah.html">Dewan Pengawas Syariah</a></li>
                      <li><a href="about-direksi.html">Direksi</a></li>
                      <li><a href="about-secretary.html">Sekretaris Perusahaan</a></li>
                      <li><a href="about-komite.html">Komite</a></li>
                      <li><a href="about-ekuitas.html">Perusahaan-Perusahaan Dimana Dimiliki Ekuitas</a></li>
                      <li><a href="about-anggaran.html">Anggaran Dasar Perusahaan</a></li>
                      <li><a href="about-penghargaan.html">Penghargaan dan Pencapaian</a></li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">Hubungan Investor</a>
                    <ul class="menu arrow_box">
                      <li><a href="investor-rups.html">Informasi RUPS</a></li>
                      <li><a href="investor-laporan.html">Laporan Tahunan</a></li>
                      <li><a href="investor-info.html">Informasi Keuangan</a></li>
                      <li><a href="investor-surathutang.html">Informasi Surat Hutang/Obligasi</a></li>
                      <li><a href="investor-lembaga.html">Lembaga dan Profesi Penunjang</a></li>
                      <li><a href="investor-keterbukaan.html">Keterbukaan Informasi</a></li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">Tata Kelola Perusahaan</a>
                    <ul class="menu arrow_box">
                      <li><a href="tatakelola-dokumen.html">Dokumen Tata Kelola</a></li>
                      <li><a href="tatakelola-kepengurusan.html">Pedoman Kepengurusan</a></li>
                      <li><a href="tatakelola-kebijakankomite.html">Pedoman dan Kebijakan Komite</a></li>
                      <li><a href="tatakelola-piagam.html">Piagam Unit Audit Internal</a></li>
                      <li><a href="tatakelola-manajemenresiko.html">Kebijakan Manajemen Risiko</a></li>
                      <li><a href="tatakelola-pelanggaran.html">Kebijakan Mekanisme Sistem Pelaporan Pelanggaran</a></li>
                      <li><a href="tatakelola-antikorupsi.html">Kebijakan Anti Korupsi </a></li>
                      <li><a href="tatakelola-csr.html">Tanggung Jawab Sosial Perusahaan</a></li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">Produk &amp; Layanan</a>
                    <ul class="menu arrow_box">
                      <li><a href="produk-ringkasan.html">Ringkasan Informasi Produk</a></li>
                      <li><a href="produk-jenis.html">Jenis dan Produk </a></li>
                      <li><a href="#">Paket Kredit</a></li>
                      <li><a href="#">Simulasi Kredit</a></li>
                      <li><a href="hargamobil.html">Harga Mobil</a></li>
                      <li><a href="#">Promo</a></li>
                      <li><a href="produk-carapengajuan.html">Cara Pengajuan Kredit</a></li>
                      <li><a href="produk-carapembayaran.html">Cara Pembayaran</a></li>
                      <li><a href="produk-cekkontrak.html">Cek Kontrak</a></li>
                      <li><a href="produk-layanan.html">Layanan Asuransi, Astra Credit Protection (ACP), STNK dan BPKB</a></li>
                      <li><a href="produk-testimoni.html">Testimoni</a></li>
                    </ul>
                  </li>
                  <li><a href="jaringan.html">Jaringan ACC</a></li>
                  <li>
                    <a href="#">Berita</a>
                    <ul class="menu arrow_box">
                      <li><a href="berita.html">Berita Otomotif</a></li>
                      <li><a href="berita.html">Berita Keuangan</a></li>
                      <li><a href="berita.html">Berita Gaya Hidup</a></li>
                      <li><a href="berita.html">Siaran Pers</a></li>
                      <li><a href="berita.html">Agenda Kegiatan Perusahaan</a></li>
                    </ul>
                  </li>
                </ul>
          </div>
        </div>
      </div>
      <!--/menu-->
    </header>
      <!--main slider-->
      <section id="mainslider">
        <ul class="bxslider">
          <li><img src="images/img-slide1.jpg" /></li>
          <li><img src="images/img-slide2.jpg" /></li>
          <li><img src="images/img-slide3.jpg" /></li>
        </ul>
      </section>
      <!--/main slider-->

      <!--simulasi & paket-->
      <section id="tab-simulasipaket">
        <div class="section-title">
          <ul class="tabs" data-tabs id="simulasi-paket">
            <li class="tabs-title is-active"><a href="#paketkredit" aria-selected="true">PAKET KREDIT</a></li>
            <li class="tabs-title"><a href="#simulasikredit">SIMULASI KREDIT</a></li>
          </ul>
        </div>
        <!--content tab-->
        <div class="tabs-content" data-tabs-content="simulasi-paket">
            <div class="tabs-panel is-active" id="paketkredit">
              <div class="expanded row small-collapse">
                <div class="large-4 columns"><a href="#"><img src="images/img-banner1.jpg" alt="banner 1" class="imgbanner" /></a></div>
                <div class="large-4 columns"><a href="#"><img src="images/img-banner2.jpg" alt="banner 2" class="imgbanner" /></a></div>
                <div class="large-4 columns"><a href="#"><img src="images/img-banner3.jpg" alt="banner 3" class="imgbanner" /></a></div>
              </div>
            </div>
            <div class="tabs-panel" id="simulasikredit">
              <!--tabsimulasi-->
              <div class="row columns">
              <form>
                <div class="formholder">
                    <label>Pilihan Mobil</label>
                    <select class="simulasi">
                        <option value="Baru">Baru</option>
                        <option value="Lama">Lama</option>
                    </select>
                </div>
                <div class="formholder">
                    <label>Merk</label>
                    <select class="simulasi">
                        <option value="">Daihatsu</option>
                        <option value="">Honda</option>
                        <option value="">Mitsubishi</option>
                        <option value="">Toyota</option>
                    </select>
                </div>
                <div class="formholder">
                    <label>Type</label>
                    <select class="simulasi">
                        <option value="">Avanza Veloz</option>
                        <option value="">Avanza 1300</option>
                        <option value="">Avanza M/T</option>
                        <option value="">Avanza A/T</option>
                    </select>
                </div>
                <div class="formholder">
                    <label>Harga Mobil</label>
                    <input type="text" class="simulasi" id="middle-label" placeholder="Masukkan harga">
                </div>
                <div class="formholder">
                    <label>DP</label>
                    <input type="text" class="simulasi" id="middle-label" placeholder="Minimal 20%">
                </div>
                <div class="formholder button-holder">
                    <input type="submit" value="Simulasikan" class="button" />
                </div>
              </form>
              </div>
              <div class="clearfix"></div>
              <!--/tabsimulasi-->
            </div>
          </div>
      </section>
      <!--/content tab-->
      <!--/simulasi & paket-->
      <!--produk & layanan-->
      <section id="produklayanan">
        <div class="section-title mb-20">
          Produk &amp; Layanan
        </div>
        <!--content tab-->
        <div class="expanded row" id="produklayanan-content">
          <div class="slider1">
            <div>
                <a href="#"><img src="images/img-feature1.jpg" alt="" width="425" /></a>
                <h2 class="product-caption"><a href="#">Pembiayaan Kendaraan Bermotor</a></h2>
                <p>ACC menawarkan kemudahan memiliki kendaraan baru bagi pelanggan dari berbagai jenis kendaraan (pick up, truck, sedan, minibus dan jeep), dengan syarat kredit mudah dan fleksibel, uang muka ringan, serta jangka waktu kredit yang dapat disesuaikan dengan kemampuan pelanggan.</p>
                <p class="link-more"><a href="#">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>
            <div>
                <a href="#"><img src="images/img-feature2.jpg" alt="" width="425" /></a>
                <h2 class="product-caption"><a href="#">Pembiayaan Alat Berat, Truk &amp; Mesin-Mesin</a></h2>
                <p>Untuk memenuhi kebutuhan pelanggan akan pembiayaan komersial dan bisnis, di tahun 1995 ACC membentuk divisi FLEET COMMERCIAL & BUSINESS. Fleet mendukung industri seperti <em>agribusiness, construction, distribution, transporter, mining, rental, services, manufacturing, logging dan public transportation</em>.</p>
                <p class="link-more"><a href="#">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>
            <div>
                <img src="images/img-feature3.jpg" alt="" width="425" />
                <h2 class="product-caption"><a href="#">Pembiayaan Kendaraan Bermotor Customer</a></h2>
                <p>C2C Financing adalah fasilitas dari ACC, untuk pembiayaan kredit mobil semua merk dari teman, kenalan atau perseorangan lain. Syarat dan kemudahannya menjadikan ACC terdepan dalam menawarkan produk dan service-nya, untuk pembiayaan kredit mobil bekas.</p>
                <p class="link-more"><a href="#">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>
            <div>
                <a href="#"><img src="images/img-feature1.jpg" alt="" width="425" /></a>
                <h2 class="product-caption"><a href="#">Paket Kredit</a></h2>
                <p>ACC bekerjasama dengan perusahaan otomotif untuk mengeluarkan paket-paket kredit yang menarik dan menguntungkan bagi pelanggan. </p>
                <p class="link-more"><a href="#">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>
            <div>
                <a href="#"><img src="images/img-feature2.jpg" alt="" width="425" /></a>
                <h2 class="product-caption"><a href="#">Simulasi Kredit</a></h2>
                <p>Ingin mengetahui perkiraan cicilan mobil incaran Anda? Gunakan fasilitas simulasi kredit yang telah kami sediakan! Selanjutnya Anda akan dihubungi lebih lanjut oleh team sales kami.  </p>
                <p class="link-more"><a href="#">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>
          </div>
        </div>
      </section>
      <!--/produk & layanan-->
      <!--info promo-->
      <section id="infopromo">
        <div class="section-title mb-20">
          INFO &amp;PROMO ACC
        </div>
        <div class="expanded row">
          <div class="slider1">
            <div><a href="#"><img src="images/img-promo1.jpg" alt="" /></a></div>
            <div><a href="#"><img src="images/img-promo2.jpg" alt="" /></a></div>
            <div><a href="#"><img src="images/img-promo3.jpg" alt="" /></a></div>
            <div><a href="#"><img src="images/img-promo1.jpg" alt="" /></a></div>
            <div><a href="#"><img src="images/img-promo2.jpg" alt="" /></a></div>
          </div>
        </div>
      </section>
      <!--/info promo-->
      <!--info promo-->
      <section id="jaringanacc" class="mb-20">
        <div class="section-title mb-20">
          JARINGAN ACC
        </div>
        <div class="expanded row">
          <div class="large-4 small-12 columns mb-20">
              <h2>LEBIH MUDAH DENGAN ACC YES!</h2>
              <p class="text-14">Unduh ACC YES! sekarang juga!</p>
              <div><a href="#"><img src="images/img-accyes.jpg" alt="ACC YES" /></a></div>
          </div>
          <div class="large-8 small-12 columns">
              <h2>JARINGAN ACC DI SELURUH INDONESIA</h2>
              <p>ACC mendukung penjualan mobil melalui jaringan dealer, showroom maupun perseorangan di seluruh wilayah Indonesia. Jaringan ACC tersebar di hampir seluruh kota besar di Indonesia. Saat ini ACC memiliki 73 kantor cabang yang tersebar di 59 kota di Indonesia, dan akan terus bertambah.  </p>
              <div>
                  <img src="images/img-network.jpg" alt="Peta Indonesia" border="0" />
              </div>
          </div>
        </div>
      </section>
      <!--/info promo-->
      <!--berita-->
      <section id="berita">
        <div class="section-title mb-20">
            BERITA
        </div>
        <div class="expanded row" id="berita-content">
          <div class="slider2">
            <div>
                <a href="berita-detail.html"><img src="images/img-news1.jpg" alt="" width="400" /></a>
                <div class="article-tags">AUTOMOTIVE - 22 Agustus 2016</div>
                <h2><a href="berita-detail.html">BMW Konfirmasikan Kehadiran i3 Generasi Kedua</a></h2>
                <p class="mb-5">Untuk melanjutkan mobil listrik generasi sebelumnya, BMW mengkonfirmasikan akan peluncuran i3 generasi kedua, namun belum ada informasi lebih lanjut mengenai waktu peluncurannya..</p>
                <p class="link-more"><a href="berita-detail.html">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>
            <div>
                <a href="berita-detail.html"><img src="images/img-news2.jpg" alt="" width="400" /></a>
                <div class="article-tags">AUTOMOTIVE - 21 Agustus 2016</div>
                <h2><a href="berita-detail.html">Toyota Luncurkan Roomy dan Tank di Jepang</a></h2>
                <p class="mb-5">Toyota baru saja meluncurkan Roomy dan Tank baru yang tersedia secara eksklusif di pasar Jepang. Kedua model baru ini ditawarkan dengan mesin kecil tiga silinder berkapasitas 1.000 cc..<br /><br /></p>
                <p class="link-more"><a href="berita-detail.html">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>
            <div>
                <a href="berita-detail.html"><img src="images/img-news3.jpg" alt="" width="400" /></a>
                <div class="article-tags">AUTOMOTIVE - 20 Agustus 2016</div>
                <h2><a href="berita-detail.html">Toyota Persiapkan Produksi Masal Kendaraan Listrik Tahun 2020</a></h2>
                <p class="mb-5">Setelah dipamerkan secara perdana tahun lalu di Tokyo Auto Salon, jubah versi petualang Daihatsu Copen yang mengubah tampilan Daihatsu Copen standar menjadi Daihatsu Copen Adventurer.. </p>
                <p class="link-more"><a href="berita-detail.html">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>
            <div>
                <a href="berita-detail.html"><img src="images/img-news1.jpg" alt="" width="400" /></a>
                <div class="article-tags">AUTOMOTIVE - 19 Agustus 2016</div>
                <h2><a href="berita-detail.html">Tips Amankan diri dan Kendaraan di Area Parkir</a></h2>
                <p class="mb-5">Sangat disayangkan bahwa belakangan ini kriminalitas terkait pencurian atau perampokan baik isi maupun kendaraannya cenderung meningkat. Oleh karena itu, maka wajar jika pengendara merasa..</p>
                <p class="link-more"><a href="berita-detail.html">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>
            <div>
                <a href="berita-detail.html"><img src="images/img-news2.jpg" alt="" width="400" /></a>
                <div class="article-tags">AUTOMOTIVE - 17 Agustus 2016</div>
                <h2><a href="berita-detail.html">Rolls Royce Ungkapkan Mahakarya Otomotif yang Terinspirasi dari Dunia Fashion</a></h2>
                <p class="mb-5">Model Dawn terbaru dari Rolls Royce ini merupakan edisi khusus yang terinspirasi dari Fashion. Direktur desain, Giles Taylor, mengumpulkan tim desainer dari dunia fashion..</p>
                <p class="link-more"><a href="berita-detail.html">selengkapnya &nbsp; <i class="fa fa-play"></i></a></p>
            </div>

          </div>
        </div>
      </section>
      <!--/berita-->

      <!--shopping tools-->
      <div id="shoppingtools">
        <div class="shoppingtools shoppingtools-short">
            <div class="float-left">
              <a href="javascript:void(0)" class="linkshoppingtools"><img src="images/img-shoppingtool.png" width="81" height="150" /></a>
            </div>
            <div>
                <ul id="shoppingtools-menu">
                    <li><a href="#">Simulasi Kredit</a></li>
                    <li><a href="hargamobil.html">Harga Mobil</a></li>
                    <li><a href="#">Paket Kredit</a></li>
                    <li><a href="#">Cara Ajukan Kredit</a></li>
                </ul>
            </div>
        </div>
      </div>
      <!--/shopping tools-->
      <!--footer-->
      <footer>
      <!--peta situs-->
      <div id="petasitus">
        <div id="menubottom">PETA SITUS</div>
        <div class="row petasitus-content columns">
              <div class="large-9 large-centered columns">
                <div class="row">
                <!--menu kolom 1-->
                  <div class="large-4 small-12 columns">
                    <h4><a href="#">TENTANG KAMI</a></h4>
                    <ul>
                      <li><a href="about-sejarah.html">Riwayat Singkat Perusahaan</a></li>
                      <li><a href="about-visi.html">Visi, Misi dan Nilai</a></li>
                      <li><a href="about-strukturgrup.html">Struktur Grup</a></li>
                      <li><a href="about-strukturkepemilikan.html">Struktur Kepemilikan</a></li>
                      <li><a href="about-strukturorganisasi.html">Struktur Organisasi</a></li>
                      <li><a href="about-boc.html">Dewan Komisaris</a></li>
                      <li><a href="about-syariah.html">Dewan Pengawas Syariah</a></li>
                      <li><a href="about-direksi.html">Direksi</a></li>
                      <li><a href="about-secretary.html">Sekretaris Perusahaan</a></li>
                      <li><a href="about-komite.html">Komite</a></li>
                      <li><a href="about-ekuitas.html">Perusahaan-Perusahaan Dimana Dimiliki Ekuitas</a></li>
                      <li><a href="about-anggaran.html">Anggaran Dasar Perusahaan</a></li>
                      <li><a href="about-penghargaan.html">Penghargaan dan Pencapaian</a></li>
                    </ul>
                    <h4><a href="#">HUBUNGAN INVESTOR</a></h4>
                    <ul>
                      <li><a href="investor-rups.html">Informasi RUPS</a></li>
                      <li><a href="investor-laporan.html">Laporan Tahunan</a></li>
                      <li><a href="investor-info.html">Informasi Keuangan</a></li>
                      <li><a href="investor-surathutang.html">Informasi Surat Hutang/Obligasi</a></li>
                      <li><a href="investor-lembaga.html">Lembaga dan Profesi Penunjang</a></li>
                      <li><a href="investor-keterbukaan.html">Keterbukaan Informasi</a></li>
                    </ul>
                  </div>
                  <!--/menu kolom 1-->
                  <!--menu kolom 2-->
                <div class="large-4 small-12 columns">
                  <h4><a href="#">Tata Kelola Perusahaan</a></h4>
                  <ul>
                    <li><a href="tatakelola-dokumen.html">Dokumen Tata Kelola</a></li>
                      <li><a href="tatakelola-kepengurusan.html">Pedoman Kepengurusan</a></li>
                      <li><a href="tatakelola-kebijakankomite.html">Pedoman dan Kebijakan Komite</a></li>
                      <li><a href="tatakelola-piagam.html">Piagam Unit Audit Internal</a></li>
                      <li><a href="tatakelola-manajemenresiko.html">Kebijakan Manajemen Risiko</a></li>
                      <li><a href="tatakelola-pelanggaran.html">Kebijakan Mekanisme Sistem Pelaporan Pelanggaran</a></li>
                      <li><a href="tatakelola-antikorupsi.html">Kebijakan Anti Korupsi </a></li>
                      <li><a href="tatakelola-csr.html">Tanggung Jawab Sosial Perusahaan</a></li>
                  </ul>
                  <h4><a href="#">Berita</a></h4>
                  <ul>
                     <li><a href="berita.html">Berita Otomotif</a></li>
                      <li><a href="berita.html">Berita Keuangan</a></li>
                      <li><a href="berita.html">Berita Gaya Hidup</a></li>
                      <li><a href="berita.html">Siaran Pers</a></li>
                      <li><a href="berita.html">Agenda Kegiatan Perusahaan</a></li>
                  </ul>
                  <h4><a href="#">BAHASA</a></h4>
                  <ul>
                    <li><a href="#">Inggris</a></li>
                    <li><a href="#">Indonesia</a></li>
                  </ul>
                </div>
                <!--/menu kolom 2-->
                <!--menu kolom 3-->
                <div class="large-4 small-12 columns">
                  <h4><a href="#">Produk &amp; Layanan</a></h4>
                  <ul>
                    <li><a href="produk-ringkasan.html">Ringkasan Informasi Produk</a></li>
                      <li><a href="produk-jenis.html">Jenis dan Produk </a></li>
                      <li><a href="#">Paket Kredit</a></li>
                      <li><a href="#">Simulasi Kredit</a></li>
                      <li><a href="hargamobil.html">Harga Mobil</a></li>
                      <li><a href="#">Promo</a></li>
                      <li><a href="produk-carapengajuan.html">Cara Pengajuan Kredit</a></li>
                      <li><a href="produk-carapembayaran.html">Cara Pembayaran</a></li>
                      <li><a href="produk-cekkontrak.html">Cek Kontrak</a></li>
                      <li><a href="produk-layanan.html">Layanan Asuransi, Astra Credit Protection (ACP), STNK dan BPKB</a></li>
                      <li><a href="produk-testimoni.html">Testimoni</a></li>
                  </ul>
                  <h4><a href="jaringan.html">Jaringan ACC</a></h4>
                  <h4><a href="hubungikami.html">Hubungi Kami</a></h4>
                  <h4><a href="faq.html">FAQ</a></h4>
                  <h4><a href="kamuspembiayaan.html">KAMUS PEMBIAYAAN</a></h4>
                  <h4><a href="kebijakanwebsite.html">KEBIJAKAN WEBSITE</a></h4>
                </div>
                <!--/menu kolom 3-->
              </div>
          </div>
        </div>
      </div>
      <!--end peta situs-->

        <div class="row columns" id="footer-bottom">
              <div class="large-7 large-centered columns">
                  <div class="row small-collapse">
                      <div class="large-3 small-6 columns mb-10">
                          <p class="text-center mb-10"><b>Ikuti kami di media sosial</b></p>
                          <p class="text-center">
                              <a href="#"><img src="images/icon-fb.jpg" alt="" /></a>
                              <a href="#"><img src="images/icon-twitter.jpg" alt="" /></a>
                              <a href="#"><img src="images/icon-instagram.jpg" alt="" /></a>
                              <a href="#"><img src="images/icon-youtube.jpg" alt="" /></a>
                          </p>
                      </div>
                      <div class="large-4 small-6 columns mb-10">
                          <p class="text-center mb-5"><b>ACC terdaftar &amp; diawasi oleh</b></p>
                          <p class="text-center">
                              <img src="images/img-ojk.jpg" alt="" />
                          </p>
                      </div>
                      <div class="large-3 small-8 columns mb-10">
                          <p class="text-center mb-5"><b>Hubungi kami</b></p>
                          <p class="text-center">
                              <img src="images/img-contact.jpg" alt="" />
                          </p>
                      </div>
                      <div class="large-2 small-4 columns mb-10">
                          <p>
                              <img src="images/logo-acc-footer.jpg" alt="" />
                          </p>
                      </div>
                  </div>
              </div>
              <div class="text-center">&copy; 2016 Astra Credit Companies</div>
      </footer>
      <!--/footer-->
      <!--scroll to top-->
      <div class="scroll-top-wrapper ">
        <span class="scroll-top-inner">
          <i class="fa fa-2x fa-chevron-up"></i>
        </span>
      </div>
      <!--/scroll to top-->
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.min.js"></script>
    <script src="js/vendor/jquery.bxslider.min.js"></script>
    <script src="js/app.js?ver=2.2"></script>
  </body>
</html>
