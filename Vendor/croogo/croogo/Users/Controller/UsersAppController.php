<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Users Application controller
 *
 * @category Controllers
 * @package  Croogo.Users.Controller
 * @since    1.5
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class UsersAppController extends AppController {
	function _send_registration_email($data)
	{
		if ($this->data)
		{
			$site_name = Configure::read("Site.title");
			$domain = preg_replace("/(http:\/\/|https:\/\/)*(www\.)*/", "", Configure::read("Site.home_url"));
			$domain = preg_replace("/(\:[0-9]+)*(\/)*/", "", $domain);
			
			$email = new CakeEmail();
			//$email->from(Configure::read("Site.title") . " <no_reply@{$domain}>");
			$email->from("yfuadi@cakradigital.com");
			//$email->replyTo(Configure::read("Site.title") . " <no_reply@{$domain}>");
			$email->to($this->data["Member"]["email"]);
			//$this->Email->cc = 
			$email->bcc(explode(",", Configure::read("Site.admin_emails")));
			$email->subject('Please verify your ACC account');
			$email->emailFormat('html');
			
			// $this->Email->from = Configure::read("Site.title") . " <no_reply@{$domain}>";
			// $this->Email->replyTo = Configure::read("Site.title") . " <no_reply@{$domain}>";
			// $this->Email->to = $this->data["Member"]["email"];
			// //$this->Email->cc = 
			// $this->Email->bcc = explode(",", Configure::read("Site.admin_emails"));
			// $this->Email->subject = 'Please verify your ACC account';
			// $this->Email->sendAs = 'html';
			
			$message = <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en">
<head>
<title>New Account</title>
</head>

<body>
<style type="text/css">
.text
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	text-decoration: none;
	color: #000000;
}
</style>
<div class="text">
Dear <b>{$data["Member"]["nama"]}</b>,
<br /><br />
Do not reply to this message. If you did not create an account, please ignore this email.
<br /><br />
Please click on the following link to verify your account.
<br />
<a href="http://www.acc.co.id/members/verify_account/{$data["Member"]["verification_code"]}" title="Verify account">http://www.acc.co.id/members/verify_account/{$data["Member"]["verification_code"]}</a>
<br /><br /><br />
This link is valid for 3 days from the time of account creation.
<br /><br />
Your ACC Username: <b>{$data["Member"]["username"]}</b>
<br /><br />
E-mail verification helps protect your identity and allows you to register and access ACC applications and web sites that require verification.
<br /><br /><br />

Best regards,
<br /><br /><br /><br />
<b><u>Your automated system</u></b>
<br />
{$site_name}
</div>
</body>
</html>
END;
			
			//$this->Email->send($message);
			$email->send($message);
		}
	}
	
	function _send_change_password_email($account_data, $new_password)
	{
		$site_name = Configure::read("Site.title");
		$domain = preg_replace("/(http:\/\/|https:\/\/)*(www\.)*/", "", Configure::read("Site.home_url"));
		$domain = preg_replace("/(\:[0-9]+)*(\/)*/", "", $domain);
		
		$this->Email->from = Configure::read("Site.title") . " <no_reply@{$domain}>";
		$this->Email->replyTo = Configure::read("Site.title") . " <no_reply@{$domain}>";
		$this->Email->to = $account_data["Member"]["email"];
		//$this->Email->cc = 
		$this->Email->bcc = explode(",", Configure::read("Site.admin_emails"));
		$this->Email->subject = 'New Password for your ACC Account';
		$this->Email->sendAs = 'html';
			
		$message = <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en">
<head>
<title>Change Password</title>
</head>

<body>
<style type="text/css">
.text
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	text-decoration: none;
	color: #000000;
}
</style>
<div class="text">
You recently requested to reset the password for your ACC.CO.ID (ACC) account.
<br /><br />
Your account username is : <b>{$account_data["Member"]["username"]}</b>
<br />
The new password for your account is: <b>{$new_password}</b>
<br /><br />
Please login with this temporary password to access your account and services.  Once you have done so, please change your password immediately by clicking on the Change Password link on your ACC Member's page.

<br /><br /><br />

Best regards,
<br /><br /><br /><br />
<b><u>Your automated system</u></b>
<br />
{$site_name}
</div>
</body>
</html>
END;
			
		$this->Email->send($message);
	}
}
