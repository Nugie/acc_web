<?php

App::uses('CakeEmail', 'Network/Email');
App::uses('UsersAppController', 'Users.Controller');
App::uses('CakeText', 'Utility');

/**
 * Users Controller
 *
 * @category Controller
 * @package  Croogo.Users.Controller
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class UsersController extends UsersAppController {

/**
 * Components
 *
 * @var array
 * @access public
 */
	public $components = array(
		'Members.WebService',
		'Search.Prg' => array(
			'presetForm' => array(
				'paramType' => 'querystring',
			),
			'commonProcess' => array(
				'paramType' => 'querystring',
				'filterEmpty' => true,
			),
		),
	);


/**
 * Preset Variables Search
 *
 * @var array
 * @access public
 */
	public $presetVars = true;

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Users.User','Members.Member');

/**
 * implementedEvents
 *
 * @return array
 */
	public function implementedEvents() {
		return parent::implementedEvents() + array(
			'Controller.Users.beforeAdminLogin' => 'onBeforeAdminLogin',
			'Controller.Users.adminLoginFailure' => 'onAdminLoginFailure',
		);
	}

/**
 * Notify user when failed_login_limit hash been hit
 *
 * @return bool
 */
	public function onBeforeAdminLogin() {
		$field = $this->Auth->authenticate['all']['fields']['username'];
		if (empty($this->request->data)) {
			return true;
		}
		$cacheName = 'auth_failed_' . $this->request->data['User'][$field];
		$cacheValue = Cache::read($cacheName, 'users_login');
		if ($cacheValue >= Configure::read('User.failed_login_limit')) {
			$this->Session->setFlash(__d('croogo', 'You have reached maximum limit for failed login attempts. Please try again after a few minutes.'), 'flash', array('class' => 'warning callout'));
			return $this->redirect(array('action' => $this->request->params['action']));
		}
		return true;
	}

/**
 * Record the number of times a user has failed authentication in cache
 *
 * @return bool
 * @access public
 */
	public function onAdminLoginFailure() {
		$field = $this->Auth->authenticate['all']['fields']['username'];
		if (empty($this->request->data)) {
			return true;
		}
		$cacheName = 'auth_failed_' . $this->request->data['User'][$field];
		$cacheValue = Cache::read($cacheName, 'users_login');
		Cache::write($cacheName, (int)$cacheValue + 1, 'users_login');
		return true;
	}

/**
 * Admin index
 *
 * @return void
 * @access public
 * $searchField : Identify fields for search
 */
	public function admin_index() {
		$this->set('title_for_layout', __d('croogo', 'Users'));
		$this->Prg->commonProcess();
		$searchFields = array('role_id', 'name');

		$this->User->recursive = 0;
		$criteria = $this->User->parseCriteria($this->Prg->parsedParams());
		$this->paginate['conditions'] = $criteria;

		$this->set('users', $this->paginate());
		$this->set('roles', $this->User->Role->find('list'));
		$this->set('displayFields', $this->User->displayFields());
		$this->set('searchFields', $searchFields);

		if (isset($this->request->query['chooser'])) {
			$this->layout = 'admin_popup';
		}
	}

/**
 * Send activation email
 */
	private function __sendActivationEmail() {
		if (empty($this->request->data['User']['notification'])) {
			return;
		}

		$user = $this->request->data['User'];
		$activationUrl = Router::url(array(
			'admin' => false,
			'plugin' => 'users',
			'controller' => 'users',
			'action' => 'activate',
			$user['username'],
			$user['activation_key'],
		), true);
		$this->_sendEmail(
			array(Configure::read('Site.title'), $this->_getSenderEmail()),
			$user['email'],
			__d('croogo', '[%s] Please activate your account', Configure::read('Site.title')),
			'Users.register',
			'user activation',
			$this->theme,
			array(
				'user' => $this->request->data,
				'url' => $activationUrl,
			)
		);
	}

/**
 * Admin add
 *
 * @return void
 * @access public
 */
	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->User->create();
			$this->request->data['User']['activation_key'] = md5(uniqid());
			if ($this->User->saveAssociated($this->request->data)) {
				$this->request->data['User']['id'] = $this->User->id;
				$this->__sendActivationEmail();

				$this->Session->setFlash(__d('croogo', 'The User has been saved'), 'flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__d('croogo', 'The User could not be saved. Please, try again.'), 'flash', array('class' => 'warning callout'));
				unset($this->request->data['User']['password']);
			}
		} else {
			$this->request->data['User']['role_id'] = 2; // default Role: Registered
		}
		$roles = $this->User->Role->find('list');
		$this->set(compact('roles'));
	}

/**
 * Admin edit
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_edit($id = null) {
		if (!empty($this->request->data)) {
			if ($this->User->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', 'The User has been saved'), 'flash', array('class' => 'success'));
				return $this->Croogo->redirect(array('action' => 'edit', $this->User->id));
			} else {
				$this->Session->setFlash(__d('croogo', 'The User could not be saved. Please, try again.'), 'flash', array('class' => 'warning callout'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
		$roles = $this->User->Role->find('list');
		$this->set(compact('roles'));
		$this->set('editFields', $this->User->editFields());
	}

/**
 * Admin reset password
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_reset_password($id = null) {
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid User'), 'flash', array('class' => 'warning callout'));
			return $this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', 'Password has been reset.'), 'flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__d('croogo', 'Password could not be reset. Please, try again.'), 'flash', array('class' => 'warning callout'));
			}
		}
		$this->request->data = $this->User->findById($id);
	}

/**
 * Admin delete
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__d('croogo', 'Invalid id for User'), 'flash', array('class' => 'warning callout'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__d('croogo', 'User deleted'), 'flash', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__d('croogo', 'User cannot be deleted'), 'flash', array('class' => 'warning callout'));
			return $this->redirect(array('action' => 'index'));
		}
	}

/**
 * Admin login
 *
 * @return void
 * @access public
 */
	public function admin_login() {
		$this->set('title_for_layout', __d('croogo', 'Admin Login'));
		$this->layout = "admin_login";
		if ($this->Auth->user('id')) {
			if (!$this->Session->check('Message.auth')) {
				$this->Session->setFlash(
					__d('croogo', 'You are already logged in'), 'flash',
					array('class' => 'alert'), 'auth'
				);
			}
			return $this->redirect($this->Auth->redirect());
		}
		if ($this->request->is('post')) {
/*
			$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			if ($response['success'] == true) {
				Croogo::dispatchEvent('Controller.Users.beforeAdminLogin', $this);
				if ($this->Auth->login()) {
					Croogo::dispatchEvent('Controller.Users.adminLoginSuccessful', $this);
					return $this->redirect($this->Auth->redirect());
				} else {
					Croogo::dispatchEvent('Controller.Users.adminLoginFailure', $this);
					$this->Auth->authError = __d('croogo', 'Incorrect username or password');
					$this->Session->setFlash($this->Auth->authError, 'flash', array('class' => 'warning callout'), 'auth');
					return $this->redirect($this->Auth->loginAction);
				}
			}else{
				Croogo::dispatchEvent('Controller.Users.adminLoginFailure', $this);
				$this->Session->setFlash("Silahkan cek kembali kode keamanan Anda!", 'flash', array('class' => 'error'), 'auth');
				return $this->redirect($this->Auth->loginAction);
			}
*/
Croogo::dispatchEvent('Controller.Users.beforeAdminLogin', $this);
if ($this->Auth->login()) {
	Croogo::dispatchEvent('Controller.Users.adminLoginSuccessful', $this);
	return $this->redirect($this->Auth->redirect());
} else {
	Croogo::dispatchEvent('Controller.Users.adminLoginFailure', $this);
	$this->Auth->authError = __d('croogo', 'Incorrect username or password');
	$this->Session->setFlash($this->Auth->authError, 'flash', array('class' => 'warning callout'), 'auth');
	return $this->redirect($this->Auth->loginAction);
}
		}
	}

/**
 * Admin logout
 *
 * @return void
 * @access public
 */
	public function admin_logout() {
		Croogo::dispatchEvent('Controller.Users.adminLogoutSuccessful', $this);
		$this->Session->setFlash(__d('croogo', 'Log out successful.'), 'flash', array('class' => 'warning callout'));
		return $this->redirect($this->Auth->logout());
	}

/**
 * Index
 *
 * @return void
 * @access public
 */
	public function index() {
		$this->set('title_for_layout', __d('croogo', 'Users'));
	}

/**
 * Convenience method to send email
 *
 * @param string $from Sender email
 * @param string $to Receiver email
 * @param string $subject Subject
 * @param string $template Template to use
 * @param string $theme Theme to use
 * @param array  $viewVars Vars to use inside template
 * @param string $emailType user activation, reset password, used in log message when failing.
 * @return boolean True if email was sent, False otherwise.
 */
	protected function _sendEmail($from, $to, $subject, $template, $emailType, $theme = null, $viewVars = null) {
		if (is_null($theme)) {
			$theme = $this->theme;
		}
		$success = false;

		try {
			$email = new CakeEmail();
			$email->from($from[1], $from[0]);
			$email->to($to);
			$email->subject($subject);
			$email->template($template);
			$email->viewVars($viewVars);
			$email->theme($theme);
			$success = $email->send();
		} catch (SocketException $e) {
			$this->log(sprintf('Error sending %s notification : %s', $emailType, $e->getMessage()));
		}

		return $success;
	}

/**
 * Add
 *
 * @return void
 * @access public
 */
	public function register() {
		$this->set('title_for_layout', __d('croogo', 'Register'));
		if (!empty($this->request->data)) {
			$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			if ($response['success'] == true) {
				$this->User->create();
				$this->request->data['User']['role_id'] = 2; // Registered
				$this->request->data['User']['activation_key'] = md5(uniqid());
				$this->request->data['User']['status'] = 0;
				$this->request->data['User']['username'] = htmlspecialchars($this->request->data['User']['username']);
				//$this->request->data['User']['website'] = htmlspecialchars($this->request->data['User']['website']);
				//$this->request->data['User']['name'] = htmlspecialchars($this->request->data['User']['name']);
				$customer_name = "";
				//$customer_name = $this->request->data['User']['username'];
				if (isset($this->request->data["User"]["customer_type"]) && ($this->request->data["User"]["customer_type"] == "P"))
				{
					$result = $this->WebService->_get_web_service_data(
						"validate_customer_personal",
						array(
							"cd_customer" => $this->request->data["User"]["cd_customer"],
							//"place_birth" => strtoupper($this->data["User"]["placeofbirth"]),
							"dt_birth" => $this->request->data["User"]["dateofbirth"]["year"] . "-" . $this->request->data["User"]["dateofbirth"]["month"] . "-" . $this->request->data["User"]["dateofbirth"]["day"]
						)
					);

					if (isset($result["error"]) && !empty($result["error"]))
						$webservice_error = $result["error"];
					elseif (isset($result["recordCount"]) && ($result["recordCount"] == 0))
						$webservice_error = "Invalid customer verification. Please provide a valid customer code and birth date.";

					if (isset($result["rowsData"][0]["name"]) && !empty($result["rowsData"][0]["name"]))
						$customer_name = trim($result["rowsData"][0]["name"]);
				}
				else
				{
					$result = $this->WebService->_get_web_service_data(
						"validate_customer_company",
						array(
							"cd_customer" => $this->request->data["User"]["cd_customer"],
							"no_aggr" => $this->request->data["User"]["code_agreement"]
						)
					);

					if (isset($result["error"]) && !empty($result["error"]))
						$webservice_error = $result["error"];
					elseif (isset($result["recordCount"]) && ($result["recordCount"] == 0))
						$webservice_error = "Invalid customer verification. Please provide a valid customer code, and agreement code.";

					if (isset($result["rowsData"][0]["name_co"]) && !empty($result["rowsData"][0]["name_co"]))
						$customer_name = trim($result["rowsData"][0]["name_co"]);
				}
				if ($customer_name==''){
					Croogo::dispatchEvent('Controller.Users.registrationFailure', $this);
					$this->Session->setFlash(__d('croogo', 'Nomor pelanggan tidak ditemukan / sudah terdaftar'), 'flash', array('class' => 'warning callout'));
					return;
					//exit;
				}else{
					$this->request->data['User']['name'] = $customer_name;
				}
				if ($this->User->save($this->request->data ) ) {
					Croogo::dispatchEvent('Controller.Users.registrationSuccessful', $this);
					$this->Session->setFlash(__d('croogo', 'Pendaftaran berhasil. Silahkan cek email Anda untuk melakukan aktivasi!'), 'flash', array('class' => 'warning callout'));
					$this->request->data['User']['password'] = null;

					$this->_sendEmail(
						array(Configure::read('Site.title'), $this->_getSenderEmail()),
						$this->request->data['User']['email'],
						__d('croogo', '[%s] Please activate your account', Configure::read('Site.title')),
						'Users.register',
						'user activation',
						$this->theme,
						array('user' => $this->request->data)
					);

					$this->Session->setFlash(__d('croogo', 'You have successfully registered an account. An email has been sent with further instructions.'), 'flash', array('class' => 'success'));
					return $this->redirect(array('action' => 'login'));
				} else {
					Croogo::dispatchEvent('Controller.Users.registrationFailure', $this);
					$this->Session->setFlash(__d('croogo', 'The User could not be saved. Please, try again.'), 'flash', array('class' => 'warning callout'));
				}
			}else{
				$this->Session->setFlash("Silahkan cek kembali kode keamanan Anda!", 'flash', array('class' => 'warning callout'), 'auth');
			}
		}
	}
	/**
 * Login
 *
 * @return boolean
 * @access public
 */
	public function register_() {
		$this->set('title_for_layout', __d('croogo', 'Register'));
		$status = 0;
		if ($this->data){
			$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			if ($response['success'] == true) {
				//$this->data = Sanitize::clean($this->data);
				$this->loadModel('Member');
				$data = $this->data;
				$this->Member->set($this->data);

				//print_r($this->Member);
				 if ( $this->Member->validates())
				 {
					$webservice_error = "";
					$customer_name = "";

					if (isset($this->data["Member"]["member_status"]) && ($this->data["Member"]["member_status"] == "Customer"))
					{
						if (isset($this->data["Member"]["customer_type"]) && ($this->data["Member"]["customer_type"] == "P"))
						{
							$result = $this->WebService->_get_web_service_data(
								"validate_customer_personal",
								array(
									"cd_customer" => $this->data["Member"]["cd_customer"],
									//"place_birth" => strtoupper($this->data["Member"]["placeofbirth"]),
									"dt_birth" => $this->data["Member"]["dateofbirth"]["year"] . "-" . $this->data["Member"]["dateofbirth"]["month"] . "-" . $this->data["Member"]["dateofbirth"]["day"]
								)
							);

							if (isset($result["error"]) && !empty($result["error"]))
								$webservice_error = $result["error"];
							elseif (isset($result["recordCount"]) && ($result["recordCount"] == 0))
								$webservice_error = "Invalid customer verification. Please provide a valid customer code and birth date.";

							if (isset($result["rowsData"][0]["name"]) && !empty($result["rowsData"][0]["name"]))
								$customer_name = trim($result["rowsData"][0]["name"]);
						}
						else
						{
							$result = $this->WebService->_get_web_service_data(
								"validate_customer_company",
								array(
									"cd_customer" => $this->data["Member"]["cd_customer"],
									"no_aggr" => $this->data["Member"]["code_agreement"]
								)
							);

							if (isset($result["error"]) && !empty($result["error"]))
								$webservice_error = $result["error"];
							elseif (isset($result["recordCount"]) && ($result["recordCount"] == 0))
								$webservice_error = "Invalid customer verification. Please provide a valid customer code, and agreement code.";

							if (isset($result["rowsData"][0]["name_co"]) && !empty($result["rowsData"][0]["name_co"]))
								$customer_name = trim($result["rowsData"][0]["name_co"]);
						}
					 }
					//echo "AAA". $webservice_error;
					$webservice_error = false;
					 if (!$webservice_error)
					{
						$this->set("form_info", "Terima kasih, data registrasi anda sudah kami proses. Silahkan periksa email anda untuk melanjutkan ke tahap registrasi selanjutnya. Mohon periksa folder spam / junk mail anda, apabila anda tidak dapat menemukan email dari kami dalam folder inbox anda.");

						if ($customer_name)
							$data["Member"]["nama"] = $customer_name;
						else
							$customer_name = $this->data["Member"]["username"];
						$data["Member"]["nama"] = $customer_name;
						$data["Member"]["password"] = md5($this->data["Member"]["password"]);
						$data["Member"]["password_confirm"] = md5($this->data["Member"]["password_confirm"]);
						$data["Member"]["verified"] = "No";
						$data["Member"]["verification_code"] = CakeText::uuid();

						$this->Member->create($data);
						$this->Member->set($data);
						$this->Member->save($data);

						$this->_send_registration_email($data);

						$this->data = array();
					}
					else
						$this->set("form_error", $webservice_error);
				 }
				 else
				 {
					 $form_error = array_values($this->Member->invalidFields());
					 $this->set("form_error", $form_error[0]);
				 }

			}else{
				$this->set('form_error',"Silahkan cek kembali kode keamanan Anda!");

			}
		}


	}
/**
 * Activate
 *
 * @param string $username
 * @param string $key
 * @return void
 * @access public
 */
	public function activate($username = null, $key = null) {
		if ($username == null || $key == null) {
			return $this->redirect(array('action' => 'login'));
		}

		if ($this->Auth->user('id')) {
			$this->Session->setFlash(
				__d('croogo', 'You are currently logged in as:') . ' ' .
				$this->Auth->user('username')
			);
			return $this->redirect($this->referer());
		}

		$redirect = array('action' => 'login');
		if (
			$this->User->hasAny(array(
				'User.username' => $username,
				'User.activation_key' => $key,
				'User.status' => 0,
			))
		) {
			$user = $this->User->findByUsername($username);
			$this->User->id = $user['User']['id'];

			$db = $this->User->getDataSource();
			$key = md5(uniqid());
			$this->User->updateAll(array(
				$this->User->escapeField('status') => $db->value(1),
				$this->User->escapeField('activation_key') => $db->value($key),
			), array(
				$this->User->escapeField('id') => $this->User->id
			));

			if (isset($user) && empty($user['User']['password'])) {
				$redirect = array('action' => 'reset', $username, $key);
			}

			Croogo::dispatchEvent('Controller.Users.activationSuccessful', $this);
			$this->Session->setFlash(__d('croogo', 'Account activated successfully.'), 'flash', array('class' => 'success'));
		} else {
			Croogo::dispatchEvent('Controller.Users.activationFailure', $this);
			$this->Session->setFlash(__d('croogo', 'An error occurred.'), 'flash', array('class' => 'warning callout'));
		}

		if ($redirect) {
			return $this->redirect($redirect);
		}
	}
	public function agreement_history() {
		$this->set('title_for_layout', __d('croogo', 'Agreement History'));
		$loguser = $this->Auth->User();
		$this->set('loguser', $loguser);
		if ($loguser['member_status'] != "Customer")
			$this->redirect("/member");

		//print_r($loguser);
		$contracts = $this->WebService->_get_web_service_data(
			"get_contract_list",
			array(
				"cd_customer" => $loguser['cd_customer'],//$this->Session->read("Member.cd_customer"),
				"flag_per_co" => $loguser['customer_type']//$this->Session->read("Member.customer_type")
			)
		);
		$this->set("contracts", $contracts);


		//print_r($contracts);
	}

		function agreement_detail($no_aggr = null)
	{


		$loguser = $this->Auth->User();
		$this->set('loguser', $loguser);
		if ($loguser['member_status'] != "Customer")
			$this->redirect("/member/");


		if (!ctype_digit($no_aggr))
			$this->redirect("/member/");



		$contract_detail = $this->WebService->_get_web_service_data(
			"get_contract_detail",
			array(
				"cd_customer" => $loguser['cd_customer'],
				"flag_per_co" => $loguser['customer_type'],
				"no_aggr" => $no_aggr
			)
		);


		$this->set("contract_detail", $contract_detail);

		$payments_new = $this->WebService->_get_web_service_data(
			"get_contract_payments_new",
			array(
				"no_aggr" => $no_aggr
			)
		);
                // print_r($no_aggr);
                // print_r($payments_new);

		$this->set("payments_new", $payments_new);


		$this->set("title_for_layout", "Agreement Detail");
		$this->set("layout_description", "ACC Member - Agreement Detail");
		$this->set("layout_keywords", "Astra Credit Companies, ACC, Member, Agreement, Detail");
	}
/**
 * Edit
 *
 * @return void
 * @access public
 */


	public function profile() {
		$this->set('title_for_layout', __d('croogo', 'User Profile'));
		$data_user = $this->Auth->User();
		if (!empty($this->request->data)) {
			//print_r($this->request->data);
			//$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			//if ($response['success'] == true) {
				$user = $this->User->findByUsername($data_user['username']);
				if (!isset($user['User']['id'])) {
					$this->Session->setFlash(__d('croogo', 'Invalid username.'), 'flash', array('class' => 'warning callout'));
					return $this->redirect(array('action' => 'login'));
				}

				$data = $this->request->data;

				$date =  $data['User']['dateofbirth']['year'] ."-". $data['User']['dateofbirth']['month'] ."-". $data['User']['dateofbirth']['day'];
				$data['User']['dateofbirth'] = date('Y-m-d', strtotime($date));//date($data['User']['dateofbirth']['year'] ."-". $data['User']['dateofbirth']['month'] ."-". $data['User']['dateofbirth']['day']);
				$options = array('fieldList' => array('cd_customer', 'customer_type', 'placeofbirth','dateofbirth'));
				//$result = $this->User->query("UPDATE User Set placeofbirth='". $data['User']['placeofbirth'] ."' WHERE User");
				$this->User->id = $user['User']['id'];

				//print_r($data);
				if ($this->User->save($data['User'],$options)) {
					$this->Session->setFlash(__d('croogo', 'Terima kasih, seluruh perubahan pada data pribadi anda sudah kami simpan dalam database kami'), 'flash', array('class' => 'warning callout'));
					//return $this->Croogo->redirect(array('action' => 'edit', $this->User->id));
				} else {
					$this->Session->setFlash(__d('croogo', 'Maaf, perubahan tidak bisa dilakukan. Silahkan cek kembali.'), 'flash', array('class' => 'warning callout'));
				}
			//}else{
			//	$this->Session->setFlash("Silahkan cek kembali kode keamanan Anda!", 'flash', array('class' => 'warning callout'));
			//}
		}

		$this->set('loguser', $data_user);
		$user = $this->User->findByUsername($data_user['username']);


		//$User = $this->User->get($id);
		//print_r($user);
		//exit;

		$this->set('user', $user);
	}
/**
 * Edit
 *
 * @return void
 * @access public
 */
	public function edit() {

	}

/**
 * Forgot
 *
 * @return void
 * @access public
 */
	public function forgot() {
		$this->set('title_for_layout', __d('croogo', 'Forgot Password'));

		if (!empty($this->request->data) && isset($this->request->data['User']['username'])) {
			$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			if ($response['success'] == true) {

				$user = $this->User->findByUsername($this->request->data['User']['username']);
				if (!isset($user['User']['id'])) {
					$this->Session->setFlash(__d('croogo', 'Invalid username.'), 'flash', array('class' => 'warning callout'));
					return $this->redirect(array('action' => 'login'));
				}

				$this->User->id = $user['User']['id'];
				$activationKey = md5(uniqid());
				$this->User->saveField('activation_key', $activationKey);
				$this->set(compact('user', 'activationKey'));

				$emailSent = $this->_sendEmail(
					array(Configure::read('Site.title'), $this->_getSenderEmail()),
					$user['User']['email'],
					__d('croogo', '[%s] Reset Password', Configure::read('Site.title')),
					'Users.forgot_password',
					'reset password',
					$this->theme,
					compact('user', 'activationKey')
				);

				if ($emailSent) {
					$this->Session->setFlash(__d('croogo', 'An email has been sent with instructions for resetting your password.'), 'flash', array('class' => 'warning callout'));
					return $this->redirect(array('action' => 'login'));
				} else {
					$this->Session->setFlash(__d('croogo', 'An error occurred. Please try again.'), 'flash', array('class' => 'warning callout'));
				}
			}else{
				$this->Session->setFlash("Silahkan cek kembali kode keamanan Anda!", 'flash', array('class' => 'warning callout'));
			}
		}
	}

/**
 * Reset
 *
 * @param string $username
 * @param string $key
 * @return void
 * @access public
 */
	public function reset($username = null, $key = null) {
		$this->set('title_for_layout', __d('croogo', 'Reset Password'));

		if ($username == null || $key == null) {
			$this->Session->setFlash(__d('croogo', 'An error occurred.'), 'flash', array('class' => 'warning callout'));
			return $this->redirect(array('action' => 'login'));
		}

		$user = $this->User->find('first', array(
			'conditions' => array(
				'User.username' => $username,
				'User.activation_key' => $key,
			),
		));
		//print_r($user);
		if (!isset($user['User']['id'])) {
			$this->Session->setFlash(__d('croogo', 'An error occurred.'), 'flash', array('class' => 'warning callout'));
			return $this->redirect(array('action' => 'login'));
		}

		if (!empty($this->request->data) && isset($this->request->data['User']['password'])) {
			$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			if ($response['success'] == true) {
				$this->User->id = $user['User']['id'];
				$user['User']['activation_key'] = md5(uniqid());
				$user['User']['password'] = $this->request->data['User']['password'];
				$user['User']['verify_password'] = $this->request->data['User']['verify_password'];
				$options = array('fieldList' => array('password', 'verify_password', 'activation_key'));
				if ($this->User->save($user['User'], $options)) {
					$this->Session->setFlash(__d('croogo', 'Your password has been reset successfully.'), 'flash', array('class' => 'warning callout'));
					return $this->redirect(array('action' => 'login'));
				} else {
					$this->Session->setFlash(__d('croogo', 'An error occurred. Please try again.'), 'flash', array('class' => 'warning callout'));
				}
			}else{
				$this->Session->setFlash("Silahkan cek kembali kode keamanan Anda!", 'flash', array('class' => 'warning callout'));
			}

		}

		$this->set(compact('user', 'username', 'key'));
	}
	public function change_password() {
		$this->set('title_for_layout', __d('croogo', 'Change Password'));
		$loguser = $this->Auth->user();
				$this->set('loguser', $loguser);
		if (!empty($this->request->data) && isset($this->request->data['User']['old_password']) && isset($this->request->data['User']['password'])) {
			//$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			//if ($response['success'] == true) {
				$password_lama1 = AuthComponent::password($this->data['User']['old_password']);
				$password_lama2 =  md5($this->data['User']['old_password']);

				$user = $this->User->find('first', array(
				'conditions' => array(
					'User.username' => $loguser['username'],
					)
				));
				//print_r($user);
				if (!isset($user['User']['id'])) {
					$this->Session->setFlash(__d('croogo', 'An error occurred.'), 'flash', array('class' => 'warning callout'));
					return $this->redirect(array('action' => 'login'));
				}
				if ($user['User']['password'] == $password_lama1  || $user['User']['old_password'] == $password_lama2){
					$this->User->id = $user['User']['id'];
					$user['User']['activation_key'] = md5(uniqid());
					$user['User']['password'] = $this->request->data['User']['password'];
					$user['User']['verify_password'] = $this->request->data['User']['verify_password'];
					$options = array('fieldList' => array('password', 'verify_password', 'activation_key'));
					if ($this->User->save($user['User'], $options)) {
						$this->Session->setFlash(__d('croogo', 'Your password has been reset successfully.'), 'flash', array('class' => 'warning callout'));
						return $this->redirect(array('action' => 'login'));
					} else {
						$this->Session->setFlash(__d('croogo', 'An error occurred. Please try again.'), 'flash', array('class' => 'warning callout'));
					}
				}else{
					$this->Session->setFlash("Password Lama Anda salah!", 'flash', array('class' => 'warning callout'));
				}
			//}else{
			//	$this->Session->setFlash("Silahkan cek kembali kode keamanan Anda!", 'flash', array('class' => 'warning callout'));
			//}
		}
	}
/**
 * Login
 *
 * @return boolean
 * @access public
 */
	public function login() {
		$this->set('title_for_layout', __d('croogo', 'Log in'));
		if ($this->request->is('post')) {
			 $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			 if ($response['success'] == true) {
				Croogo::dispatchEvent('Controller.Users.beforeLogin', $this);
				//echo $this->Auth->login();
				if ($this->Auth->login()) {

					Croogo::dispatchEvent('Controller.Users.loginSuccessful', $this);
					if ($this->Auth->redirect()=="/"){
						return $this->redirect("/member");
					}else{
						return $this->redirect($this->Auth->redirect());
					}

					//echo "Sukses";
					//echo $this->Auth->redirect();
					//exit;
				} else {
					Croogo::dispatchEvent('Controller.Users.loginFailure', $this);
					$this->Session->setFlash($this->Auth->authError, 'flash', array('class' => 'warning callout'), 'auth');
					return $this->redirect($this->Auth->loginAction);
				}
			 }else{
				 $this->Session->setFlash("Silahkan cek kembali kode keamanan Anda!", 'flash', array('class' => 'warning callout'), 'auth');
			 }

		}
	}



	function verify_account($verification_code = null)
	{
		if ($this->_check_login())
			$this->redirect("/members");


		$temp = $this->Member->query("SELECT DATE_SUB('" . date("Y-m-d H:i:s") . "', INTERVAL 3 DAY) AS `minimal_date`");
		$minimal_date = $temp[0][0]["minimal_date"];

		$data = $this->Member->find(
			"first",
			array(
				"conditions" => array("Member.verified" => "No", "Member.verification_code" => $verification_code, "Member.modified >=" => $minimal_date),
				"order" => "Member.member_id ASC",
				"limit" => 1
			)
		);

		if ($data)
		{
			$data["Member"]["verified"] = "Yes";
			$data["Member"]["verification_code"] = "";

			unset($data["Member"]["created"]);
			unset($data["Member"]["modified"]);

			$this->Member->create($data);
			$this->Member->set($data);
			$this->Member->save($data);
		}
		else
			$this->redirect("/");

		$this->set("data", $data);
		$this->set("url", "/users/users/login/");


		$this->set("title_for_layout", "Member Verification");

	}
/**
 * Logout
 *
 * @return void
 * @access public
 */
	public function logout() {
		Croogo::dispatchEvent('Controller.Users.beforeLogout', $this);
		$this->Session->setFlash(__d('croogo', 'Log out successful.'), 'flash', array('class' => 'warning callout'));
		$redirect = $this->Auth->logout();
		Croogo::dispatchEvent('Controller.Users.afterLogout', $this);
		return $this->redirect($redirect);
	}
	/**
 * View
 *
 * @param string $username
 * @return void
 * @access public
 */
	public function member() {
		$this->set('title_for_layout', 'Member');
		$user = $this->Auth->user();
		$this->set('loguser', $user);
		$this->set(compact('user'));
	}
/**
 * View
 *
 * @param string $username
 * @return void
 * @access public
 */
	public function view($username = null) {
		if ($username == null) {
			$username = $this->Auth->user('username');
		}
		$user = $this->User->findByUsername($username);
		if (!isset($user['User']['id'])) {
			$this->Session->setFlash(__d('croogo', 'Invalid User.'), 'flash', array('class' => 'warning callout'));
			return $this->redirect('/');
		}

		$this->set('title_for_layout', $user['User']['name']);
		$this->set(compact('user'));
	}

	protected function _getSenderEmail() {
		return 'noreply@' . preg_replace('#^www\.#', '', strtolower($_SERVER['SERVER_NAME']));
	}

	function rebuildARO() {
		// Build the groups.
		$this->loadModel('Role');
		$groups = $this->Role->find('all');
		$this->loadModel('Aro');
		 $aro = new Aro();
		// /*
		// foreach($groups as $group) {
			// $aro->create();
			// $aro->save(array(
			// //	'alias'=>$group['Group']['name'],
				// 'foreign_key' => $group['Group']['id'],
				// 'model'=>'Role',
				// 'parent_id' => null
			// ));
		// }*/

		 $this->loadModel('User');
		 // Build the users.
		 $users = $this->User->find('all',array(
			'conditions'=>'User.id >9500 AND User.id <=10000 ',
			'order'=>'User.id asc',
		 ));
		 $i=0;
		 foreach($users as $user) {
			 $aroList[$i++]= array(
			 	 'alias' => $user['User']['username'],
				 'foreign_key' => $user['User']['id'],
				 'model' => 'User',
				 'parent_id' => $user['User']['role_id']
			 );
		 }
		 foreach($aroList as $data) {
			 $aro->create();
			 $aro->save($data);
		 }

		echo "AROs rebuilt!";
		exit;
	}
	public function beforeFilter() {

		$this->Security->unlockedActions = array('register','login','forgot','profile','change_password');

		parent::beforeFilter();
	}

}
