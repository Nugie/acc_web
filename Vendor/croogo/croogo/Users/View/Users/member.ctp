<?php
//print_r($loguser);
?>
<!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">DEPAN</a></li>
              <li class="disabled">MEMBER</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
		  <?php //echo $this->here?>
          <div class="large-4 columns">
                <ul id="sidebar-menu" class="mb-20">
                 <li <?php if ($this->here=='/change_password') {?>class="active" <?php } ?>><a href="<?php echo $this->Html->url('/change_password', true);?>">Change Password</a></li>
                  <li <?php if ($this->here=='/profile') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/profile', true);?>">Update Profile</a></li>
                   <?php if ($loguser['member_status']=='Customer') { ?>
				  <li <?php if ($this->here=='/agreement_history') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/agreement_history', true);?>">Agreement History</a></li>
				  <?php } ?>
                  <li <?php if ($this->here=='/logout') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/logout', true);?>">Logout</a></li>
                  
                </ul>
          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="large-8 columns" id="main-content">
            <h1>MEMBER</h1>
            <!--content detail-->
				<div style="width: 95%; margin-left: 16px;">
					Selamat datang, <b><?php echo $user['name'] ?></b>
					<br>
					Untuk menjaga keamanan akun anda, silahkan mengganti password secara berkala.
					<br><br>

					Dengan fitur terbaru dalam website ini, anda sekarang dapat mengakses data agreement atau kontrak anda dengan Astra Credit Companies dan data pembayaran anda secara online.
					<br><br>
					Dan dengan menjadi member Astra Credit Companies anda dapat mengirimkan data simulasi kredit atau paket kredit yang sesuai dengan kebutuhan anda untuk kami proses.
					<br><br>
					Nikmati kemudahan dalam melakukan kredit, pilihan kredit terjangkau hanya di Astra Credit Companies.
					<br><br>
					</div>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->