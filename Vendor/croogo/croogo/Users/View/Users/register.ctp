<script src='https://www.google.com/recaptcha/api.js?hl=id'></script>

<!--content area-->
      <div class="mb-20 mt-30 text-center" id="main-content">
            <h1>PENDAFTARAN</h1>
            <p class="text-center warning-text">Pendaftaran khusus untuk customer ACC</p>
            <?php
				echo $this->Form->create(
					"User",
					array(
						"name" => "member-register-form",
						"id" => "member-register-form",
						"method" => "post"
					)
				);
				echo $this->Form->input(
						"member_status",
						array(
							"type" => "hidden",
							"id" => "member_status",
							"value" => "Customer",
						)
					);
			?>
              <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
                  <label for="username" class="text-right middle">Username</label>
                </div>
                <div class="large-4 small-9 columns end">

				  <?php
					echo $this->Form->input(
						"username",
						array(
							"type" => "text",
							"id" => "username",
							"size" => "40",
							"div" => false,
							"label" => false,
							"maxlength" => 16,
							"required"=>true
						)
					);
				?>
                </div>
              </div>
              <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
                  <label for="password" class="text-right middle">Password</label>
                </div>
                <div class="large-4 small-9 columns end">
                  <?php
					echo $this->Form->input(
						"password",
						array(
							"type" => "password",
							"id" => "password",
							"size" => "40",
							"div" => false,
							"label" => false,
							"required"=>true
						)
					);
				?>
                </div>
              </div>
              <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
                  <label for="password" class="text-right middle">Konfirmasi Password</label>
                </div>
                <div class="large-4 small-9 columns end">
                  <?php
					echo $this->Form->input(
						"password_confirm",
						array(
							"type" => "password",
							"id" => "password_confirm",
							"size" => "40",
							"class" => "text-field ui-widget-content ui-corner-all",
							"div" => false,
							"label" => false,
							"required"=>true
						)
					);
				?>
                </div>
              </div>
              <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
                  <label for="username" class="text-right middle">Email</label>
                </div>
                <div class="large-4 small-9 columns end">
					<?php
					echo $this->Form->input(
						"email",
						array(
							"type" => "email",
							"id" => "email",
							"size" => "40",
							"div" => false,
							"label" => false,
							"maxlength" => 50,
							"required"=>true
						)
					);
				?>
                </div>
              </div>

              <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
                  <label for="username" class="text-right middle">Nomor Pelanggan</label>
                </div>
                <div class="large-4 small-9 columns end">
                  <?php
					echo $this->Form->input(
						"cd_customer",
						array(
							"type" => "text",
							"id" => "cd_customer",
							"size" => "40",
							"class" => "text-field ui-widget-content ui-corner-all",
							"div" => false,
							"label" => false,
							"maxlength" => 17,
							"required"=>true
						)
					);
					?>
                  <p class="help-text text-left" id="passwordHelpText">Masukkan 12 digit nomor pelanggan yang tertera di kanan atas Surat Perjanjian Kontrak anda. Klik <a href="/images/contoh_surat_kontrak.jpg" class="image-link">disini</a> untuk melihat contoh nomor pelanggan pada surat perjanjian kontrak.</p>
                </div>
              </div>
              <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
                  <label for="customertype" class="text-right middle">Tipe Pelanggan</label>
                </div>
                <div class="large-4 small-9 columns end">
                 <?php
					echo $this->Form->input(
						"customer_type",
						array(
							"id" => "customer_type",
							"div" => false,
							"label" => false,
							"required"=>true,
							"options" => array("P" => "Personal", "C" => "Company")
						)
					);
				?>
                </div>
              </div>
			   <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
                  <label for="customertype" class="text-right middle">Tempat Lahir</label>
                </div>
                <div class="large-4 small-9 columns end">
                 <?php
						echo $this->Form->input(
							"placeofbirth",
							array(
								"type" => "text",
								"id" => "placeofbirth",
								"size" => "40",
								"class" => "text-field ui-widget-content ui-corner-all",
								"div" => false,
								"label" => false,
								"maxlength" => 20,
								"required"=>true
							)
						);
					?>
                </div>
              </div>
              <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
                  <label for="customertype" class="text-right middle">Tanggal Lahir</label>
                </div>
                <div class="text-left large-5 small-2 columns end">
					<?php
						echo $this->Form->input(
							"dateofbirth",
							array(
								"type" => "date",
								"id" => "dateofbirth",
								//"size" => "40",
								//"class" => "text-field ui-widget-content ui-corner-all",
								"div" => false,
								"label" => false,
								"required"=>true,
								"dateFormat" => "DMY",
								"minYear" => date("Y") - 80,
								"maxYear" => date("Y") - 16
							)
						);
					?>

                </div>
              </div>
			   <div class="row">
                <div class="text-right large-3 small-3 columns large-offset-2">
                  <label>&nbsp;</label>
				  <label for="captcha">&nbsp;</label>
                </div>
                <div class="large-3 small-9 columns end">
					<div class="g-recaptcha" data-sitekey="<?php echo Configure::read('Service.recaptcha_public_key') ?>"></div>
                </div>
              </div>
              <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
                  <label for="password" class="text-right middle">&nbsp;</label>
                </div>
                <div class="large-3 small-9 columns end">
								  <?php
						echo $this->Form->submit(
							"Submit",
							array(
								"id" => "form-submit",
								"class" => "button float-left",
								"div" => false
							)
						);
					?>
                </div>
              </div>

             <?php echo $this->Form->end(); ?>

      </div>
      <!--/content area-->
