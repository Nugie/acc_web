
	<!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">DEPAN</a></li>
              <li class="disabled">MEMBER</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
	    <div class="row mb-20">
          <!--sidebar-->
		  <?php //echo $this->here?>
          <div class="large-4 columns">
                <ul id="sidebar-menu" class="mb-20">
                 <li <?php if ($this->here=='/change_password') {?>class="active" <?php } ?>><a href="<?php echo $this->Html->url('/change_password', true);?>">Change Password</a></li>
                  <li <?php if ($this->here=='/profile') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/profile', true);?>">Update Profile</a></li>
                   <?php if ($loguser['member_status']=='Customer') { ?>
				  <li <?php if ($this->here=='/agreement_history') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/agreement_history', true);?>">Agreement History</a></li>
				  <?php } ?>
                  <li <?php if ($this->here=='/logout') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/logout', true);?>">Logout</a></li>

                </ul>
          </div>
          <!--/sidebar-->
          <!--main content-->
       <div class="large-8 columns" id="main-content">
            <h1><?php echo $title_for_layout; ?></h1>
			<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'change_password')));?>
               <div class="row">
				<div class="large-3 small-3 columns large-offset-2">
                  <label for="username" class="text-right middle">Password Sekarang</label>
                </div>
                <div class="large-3 small-9 columns end">
                 <?php echo $this->Form->input('old_password', array('label' =>false,'type'=>'password')); ?>
                </div>
              </div>
			  <div class="row">
				<div class="large-3 small-3 columns large-offset-2">
                  <label for="username" class="text-right middle">Password Baru</label>
                </div>
                <div class="large-3 small-9 columns end">
                 <?php echo $this->Form->input('password', array('label' =>false,'type'=>'password')); ?>
                </div>
              </div>
			   <div class="row">
				<div class="large-3 small-3 columns large-offset-2">
                  <label for="username" class="text-right middle">Konfirmasi Password</label>
                </div>
                <div class="large-3 small-9 columns end">
                <?php echo $this->Form->input('verify_password', array('label' =>false,'type'=>'password')); ?>
                </div>
              </div>


              <div class="row">
				<div class="large-3 small-3 columns large-offset-2">
                  &nbsp;
                </div>
                <div class="large-4 small-9 columns end text-left">
                   <?php echo $this->Form->button('Change',['class'=>'button float-left']); ?>
                </div>

              </div>

            <?php echo $this->Form->end();?>
      </div>
      <!--/content area-->
  </div>
      <!--/content area-->
