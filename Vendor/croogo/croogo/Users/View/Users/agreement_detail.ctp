

<!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">DEPAN</a></li>
              <li class="disabled">AGREEMENT HISTORY</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
		  <?php //echo $this->here?>
          <div class="medium-4 large-4 columns">
                <ul id="sidebar-menu" class="mb-20">
                 <li <?php if ($this->here=='/change_password') {?>class="active" <?php } ?>><a href="<?php echo $this->Html->url('/change_password', true);?>">Change Password</a></li>
                  <li <?php if ($this->here=='/profile') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/profile', true);?>">Update Profile</a></li>
                   <?php if ($loguser['member_status']=='Customer') { ?>
				  <li  class="active" ><a href="<?php echo $this->Html->url('/agreement_history', true);?>">Agreement History</a></li>
				  <?php } ?>
                  <li <?php if ($this->here=='/logout') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/logout', true);?>">Logout</a></li>

                </ul>
          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1>AGREEMENT HISTORY</h1>
            <!--content detail-->
				<?php if (isset($contract_detail["rowsData"]) && (count($contract_detail["rowsData"]) > 0)) : ?>

				<!-- CONTRACT DETAIL BEGIN -->
				<table border="0" cellspacing="2" cellpadding="6">
				<tbody>
				<tr>
					<td><b>No. Perjanjian</b></td>
					<td>:</td>
					<td><?php echo $contract_detail["rowsData"][0]["no_aggr"]; ?></td>
				</tr>

				<!--
				<tr>
					<td><b>Tanggal</b></td>
					<td>:</td>
					<td><?php echo $contract_detail["rowsData"][0]["aggr_date"]; ?></td>
				</tr>
				-->
				<tr>
					<td><b>No. Kendaraan</b></td>
					<td>:</td>
					<td><?php echo $contract_detail["rowsData"][0]["no_car_police"]; ?></td>
				</tr>
				<tr>
					<td><b>Tipe Kendaraan</b></td>
					<td>:</td>
					<td><?php echo $contract_detail["rowsData"][0]["desc_vehicle_brand"] . " " . $contract_detail["rowsData"][0]["desc_vehicle_type"] . " " . $contract_detail["rowsData"][0]["desc_vehicle_model"]; ?></td>
				</tr>
				</tbody>
				</table>
				<br />
				<div style="font-size: 11px;">
				*) Nilai total bayar belum termasuk nilai denda jika terjadi keterlambatan bayar. Nilai denda ini dapat diinformasikan melalui Customer Service di kantor layanan ACC atau melalui Call Center ACC.
				</div>
				<br />
				<!-- CONTRACT DETAIL ENDS -->

				<!-- PAYMENT HISTORY BEGIN -->
				<?php if (isset($payments_new["rowsData"]) && (count($payments_new["rowsData"]) > 0)) : ?>
				<?php
				$idx = 0;
				?>
				<table width="99%" border="0" cellspacing="0" cellpadding="5" class="member_agreement">
				<tbody>
				<tr>
					<th align="left">No.</th>
					<th align="center">Tanggal</th>
					<th align="right">Angsuran</th>
					<th align="right">Late Charge</th>
					<th align="right">Total Bayar</th>
					<th align="center">Status</th>
				</tr>
				<?php foreach ($payments_new["rowsData"] as $data) : ?>
				<?php
				$class = (($idx % 2) == 0) ? " class=\"odd\"" : " class=\"event\"";
				$idx++;

				if ($data["st_inst"] == "CLR")
					$data["st_inst"] = "Clear";
				elseif ($data["st_inst"] == "UNP")
					$data["st_inst"] = "Unpaid";
				elseif (($data["st_inst"] == "OVR") || ($data["st_inst"] == "OVD") || ($data["st_inst"] == "OVRD"))
					$data["st_inst"] = "Overdue";
				
				//$date = new Cake\I18n\Date($data["dt_due"]);
				?>
				<tr>
					<td<?php echo $class; ?> align="left"><?php echo $data["no_inst"]; ?></td>
					<td<?php echo $class; ?> align="center"><?php echo date("d-m-Y", strtotime($data["dt_due"]));  ?></td>
					<td<?php echo $class; ?> align="right"><?php echo number_format($data["angsuran"],0); ?></td>
					<td<?php echo $class; ?> align="right"><?php echo number_format($data["amount_charge"],0); ?></td>
					<td<?php echo $class; ?> align="right"><?php echo number_format($data["total_bayar"],0); ?></td>
					<td<?php echo $class; ?> align="center"><?php echo $data["status"]; ?></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
				</table>
				<?php elseif (isset($payments_new["error"]) && !empty($payments_new["error"])) : ?>
				<div class="ui-state-error ui-corner-all" style="margin-bottom: 16px; padding: 8px;">
				<b>Fatal Error :</b>
				<br /><br />
				<?php echo $payments_new["error"]; ?>
				</div>
				<?php else : ?>
				<div class="ui-state-error ui-corner-all" style="margin-bottom: 16px; padding: 8px;">
				<b>Error :</b>
				<br /><br />
				System can not find your payment data.
				</div>
				<?php endif; ?>
				<!-- PAYMENT HISTORY ENDS -->



				<?php elseif (isset($contract_detail_new["error"]) && !empty($contract_detail_new["error"])) : ?>
				<div class="ui-state-error ui-corner-all" style="margin-bottom: 16px; padding: 8px;">
				<b>Fatal Error :</b>
				<br /><br />
				<?php echo $contract_detail_new["error"]; ?>
				</div>
				<?php else : ?>
				<div class="ui-state-error ui-corner-all" style="margin-bottom: 16px; padding: 8px;">
				<b>Error :</b>
				<br /><br />
				System can not find your agreement data.
				</div>
				<?php endif; ?>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
