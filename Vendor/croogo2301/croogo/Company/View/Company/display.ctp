 <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="#">DEPAN</a></li>
              <li><a href="#">TATA KELOLA PERUSAHAAN</a></li>
              <li class="disabled">Dokumen Tata Kelola</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
              <li class="active"><a href="tatakelola-dokumen.html">Dokumen Tata Kelola</a></li>
              <li><a href="tatakelola-kepengurusan.html">Pedoman Kepengurusan</a></li>
              <li><a href="tatakelola-kebijakankomite.html">Pedoman dan Kebijakan Komite</a></li>
              <li><a href="tatakelola-piagam.html">Piagam Unit Audit Internal</a></li>
              <li><a href="tatakelola-manajemenresiko.html">Kebijakan Manajemen Risiko</a></li>
              <li><a href="tatakelola-pelanggaran.html">Kebijakan Mekanisme Sistem Pelaporan Pelanggaran</a></li>
              <li><a href="tatakelola-antikorupsi.html">Kebijakan Anti Korupsi </a></li>
              <li><a href="tatakelola-csr.html">Tanggung Jawab Sosial Perusahaan</a></li>
            </ul>

          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="large-8 columns" id="main-content">
            <h1>Dokumen Tata Kelola</h1>
            <!--content detail-->
            <p>Perseroan meyakini  bahwa penerapan Tata Kelola Perusahaan yang Baik (Good Corporate Governance-GCG)  merupakan kunci sukses perusahaan untuk tumbuh dan berkembang serta memberikan  manfaat jangka panjang, sekaligus memenangkan persaingan bisnis. Kegagalan  dalam penerapan GCG dapat menjadi salah satu penyebab utama kegagalan  perusahaan untuk bertahan dalam persaingan bisnis, yang bila terjadi secara  masif dapat menjebabkan terjadinya krisis ekonomi yang bersifat sistemik. Untuk  itu, kebijakan dalam bentuk Pedoman Tata Kelola Perusahaan dan prosedur yang  berlaku di perusahaan selalu menjadi dasar dan pedoman manajemen dalam  mengarahkan dan menentukan strategi perusahaan serta pengembangan dan penerapan  semua keputusan manajemen&nbsp; pada setiap  kegiatan operasional perusahaan.</p>
              <p>Sebagai perusahaan bisnis yang  besar, Perseroan menyadari perannya dalam industri pembiayaan untuk mematuhi  prosedur Tata Kelola Perusahaan yang baik sehingga dapat memenuhi harapan dan  tuntutan para stakeholder dalam mencapai tingkat keuntungan dan manfaat bisnis  yang sebesar-besarnya sesuai dengan peraturan perundang-undangan dan nilai-nilai  etika serta prinsip-prinsip perjanjian dan praktik usaha pembiayaan yang sehat.</p>
              <p>Dalam kaitannya dengan itu,  Perusahaan menyadari aspek-aspek penting Tata Kelola Perusahaan akan:</p>
              <ul>
              <li>Adanya keseimbangan hubungan antara organ-organ  perusahaan yaitu Rapat Umum Pemegang Saham (RUPS), Dewan Komisaris, dan Direksi  yang mencakup hal-hal yang berkaitan dengan tingkat kedudukan dan mekanisme  operasional ketiga organ tersebut.</li>
              <li>Adanya pemenuhan tanggung jawab Perusahaan sebagai  entitas bisnis dalam masyarakat kepada seluruh stakeholder yang meliputi  tanggung jawab pengelolaan perusahaan, pengawasan, serta pertanggungjawaban  kepada para pemegam saham dan stakeholders lainnya.</li>
              <li>Adanya hak-hak pemegang saham untuk mendapatkan informasi  yang tepat dan benar mengenai Perusahaan pada waktu yang diperlukan serta hak  berperan serta dalam pengambilan keputusan mengenai perkembangan strategis dan  perubahan mendasar perusahaan serta dalam hal menikmati keuntungan yang  diperoleh Perusahaan dalam pertumbuhannya.</li>
              <li>Adanya perlakukan yang sama terhadap para pemegang saham,  terutama seluruh pemegang saham melalui keterbukaan informasi yang material dan  relevan.</li>
              </ul>
              <p>Sebagai pelaku usaha dalam  industri pembiayaan, Perusahaan terus berusaha untuk turut memajukan industri  pembiayaan yang lebih kompetitif dan sehat dengan mendukung terciptanya Tata  Kelola Perusahaan industri pembiayaan melalui usaha-usaha:</p>
              <ul>
              <li>Menerapkan etika bisnis secara konsisten sehingga dapat  terwujud iklim usaha yang sehat, efisien dan transparan;</li>
              <li>Bersikap dan berperilaku yang memperlihatkan kepatuhan  dunia usaha dalam melaksanakan peraturan perundang-undangan;</li>
              <li>Meningkatkan kualitas struktur pengelolaan dan pola kerja  Perusahaan yang didasarkan pada asas Tata Kelola Perusahaan secara  berkesinambungan; dan</li>
              <li>Melaksanakan fungsi pemantauan dan pengawasan untuk dapat  menampung informasi tentang penyimpangan-penyimpangan yang terjadi dalam  Perusahaan. </li>
              </ul>
              <p>Untuk itu, Perusahaan membuat Pedoman Tata  Kelola Perusahaan yang merupakan suatu proses dan struktur yang digunakan oleh  Perusahaan untuk mendorong pengembangan usaha Perusahaan, pengelolaan sumber  daya dan risiko secara efisien dan efektif, serta pertanggungjawaban seluruh  organ Perusahaan dan pemangku kepentingan lainnya berdasarkan prinsip-prinsip  Tata Kelola Perusahaan yang meliputi :</p>
              <ol>
              <li><strong>Keterbukaan (transparency), </strong>yaitu keterbukaan dalam proses  pengambilan keputusan dan keterbukaan dalam pengungkapan dan penyediaan  informasi yang relevan mengenai perusahaan, yang mudah diakses oleh Pemangku  Kepentingan sesuai dengan peraturan perundang-undangan di bidang pembiayaan serta  standar, prinsip, dan praktik penyelenggaraan usaha pembiayaan yang sehat; </li>
              <li><strong>Akuntabilitas  (accountability), </strong>yaitu kejelasan fungsi dan pelaksanaan pertanggungjawaban  Organ Perusahaan sehingga kinerja perusahaan dapat berjalan secara transparan,  wajar, efektif, dan efisien;</li>
              <li><strong>Pertanggungjawaban  (responsibility) </strong>yaitu kesesuaian pengelolaan Perusahaan dengan peraturan  perundang-undangan di bidang pembiayaan dan nilai-nilai etika serta standar,  prinsip, dan praktik penyelenggaraan usaha pembiayaan yang sehat;</li>
              <li><strong>Kemandirian (independency), </strong>yaitu keadaan Perusahaan yang  dikelola secara mandiri dan profesional serta bebas dari Benturan Kepentingan  dan pengaruh atau tekanan dari pihak manapun yang tidak sesuai dengan peraturan  perundang-undangan di bidang pembiayaan dan nilai-nilai etika serta standar,  prinsip, dan praktik penyelenggaraan usaha pembiayaan yang sehat; dan </li>
              <li><strong>Kesetaraan dan kewajaran (fairness), </strong>yaitu kesetaraan,  keseimbangan, dan keadilan di dalam memenuhi hak-hak Pemangku Kepentingan yang  timbul berdasarkan perjanjian, peraturan perundang-undangan, dan nilai-nilai  etika serta standar, prinsip, dan praktik penyelenggaraan usaha pembiayaan yang  sehat.</li>
              </ol>
              <p><strong>Pedoman  Tata Kelola Perusahaan <a href="http://www.acc.co.id/files/page_media/2015/12/ojk/file-tata_kelola_perusahaan.pdf">unduh disini</a>.</strong></p>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->