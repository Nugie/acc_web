<?php
$this->viewVars['title_for_layout'] = __d('network', 'Networks');
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('network', 'Networks'), array('action' => 'index'));

if ($this->action == 'admin_edit') {
	$this->Html->addCrumb($this->request->data['Network']['title'], '/' . $this->request->url);
	$this->viewVars['title_for_layout'] = __d('network', 'Networks') . ': ' . $this->request->data['Network']['title'];
} else {
	$this->Html->addCrumb(__d('croogo', 'Add'), '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('Network'));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('network', 'Network'), '#network');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('network');

		echo $this->Form->input('network_id');

		echo $this->Form->input('location', array(
			'label' =>  __d('network', 'Location'),
		));
		echo $this->Form->input('x_axis', array(
			'label' =>  __d('network', 'X Axis'),
		));
		echo $this->Form->input('y_axis', array(
			'label' =>  __d('network', 'Y Axis'),
		));
		echo $this->Form->input('title', array(
			'label' =>  __d('network', 'Title'),
		));
		echo $this->Form->input('subtitle', array(
			'label' =>  __d('network', 'Subtitle'),
		));
		echo $this->Form->input('description', array(
			'label' =>  __d('network', 'Description'),
		));
		echo $this->Form->input('content', array(
			'label' =>  __d('network', 'Content'),
		));
		echo $this->Form->input('tags', array(
			'label' =>  __d('network', 'Tags'),
		));
		echo $this->Form->input('read_count', array(
			'label' =>  __d('network', 'Read Count'),
		));
		echo $this->Form->input('review_count', array(
			'label' =>  __d('network', 'Review Count'),
		));
		echo $this->Form->input('review_rates', array(
			'label' =>  __d('network', 'Review Rates'),
		));
		echo $this->Form->input('published', array(
			'label' =>  __d('network', 'Published'),
		));
		echo $this->Form->input('active', array(
			'label' =>  __d('network', 'Active'),
		));

	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'primary')) .
		$this->Form->button(__d('croogo', 'Save & New'), array('button' => 'success', 'name' => 'save_and_new')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('button' => 'danger'));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
