<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Networks'), h($network['Network']['title']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Networks'), array('action' => 'index'));

if (isset($network['Network']['title'])):
	$this->Html->addCrumb($network['Network']['title'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Network'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Network'), array(
		'action' => 'edit',
		$network['Network']['network_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Network'), array(
		'action' => 'delete', $network['Network']['network_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $network['Network']['network_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Networks'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Network'), array('action' => 'add'), array('button' => 'success'));
$this->end();

$this->append('main');
?>
<div class="networks view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Network Id'); ?></dt>
		<dd>
			<?php echo h($network['Network']['network_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Location'); ?></dt>
		<dd>
			<?php echo h($network['Network']['location']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'X Axis'); ?></dt>
		<dd>
			<?php echo h($network['Network']['x_axis']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Y Axis'); ?></dt>
		<dd>
			<?php echo h($network['Network']['y_axis']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Title'); ?></dt>
		<dd>
			<?php echo h($network['Network']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Subtitle'); ?></dt>
		<dd>
			<?php echo h($network['Network']['subtitle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Description'); ?></dt>
		<dd>
			<?php echo h($network['Network']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Content'); ?></dt>
		<dd>
			<?php echo h($network['Network']['content']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Tags'); ?></dt>
		<dd>
			<?php echo h($network['Network']['tags']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Read Count'); ?></dt>
		<dd>
			<?php echo h($network['Network']['read_count']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Review Count'); ?></dt>
		<dd>
			<?php echo h($network['Network']['review_count']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Review Rates'); ?></dt>
		<dd>
			<?php echo h($network['Network']['review_rates']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Published'); ?></dt>
		<dd>
			<?php echo h($network['Network']['published']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Active'); ?></dt>
		<dd>
			<?php echo h($network['Network']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($network['Network']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($network['Network']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>