<!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="#">DEPAN</a></li>
              <li class="disabled">JARINGAN ACC</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="medium-4 large-4 columns">
                <ul id="sidebar-menu" class="mb-20">
					
                  <li <?php if ($this->params['controller']=='network') {?>class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>network">Jaringan ACC</a></li>
                  <li <?php if ($this->here=='/contact') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>contact">Hubungi Kami</a></li>
                  <li <?php if ($this->here=='/faq') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>faq">FAQ</a></li>
                  <li <?php if ($this->here=='/kamus-pembiayaan') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>kamus-pembiayaan">Kamus Pembiayaan</a></li>
                  <li <?php if ($this->here=='/kebijakan-website') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>kebijakan-website">Kebijakan Website</a></li>
                </ul>
          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1>JARINGAN ACC</h1>
            <!--content detail-->
            <div id="network-map-holder"><div id="network-map">
			<img src="<?php echo $this->webroot; ?>files/template_images/map_indonesia.jpg" alt="Peta Indonesia" border="0" width="700" height="332" class="image" />
			<?php foreach ($pointer_data as $tmp) : ?>
			<a href="<?php echo $this->webroot; ?>network/view/<?php echo $tmp["Network"]["network_id"]; ?>/<?php echo Inflector::slug(strtolower($tmp["Network"]["location"])); ?>" title="<?php echo $tmp["Network"]["location"]; ?>"><span class="pointer" style="left: <?php echo $tmp["Network"]["x_axis"]; ?>px; top: <?php echo $tmp["Network"]["y_axis"]; ?>px;"></span></a>
			<?php endforeach; ?>
			</div>
</div>
			<div id="content-body">
			<?php

			echo $data["Network"]["content"];

			?>
			</div>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->