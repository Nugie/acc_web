<?php

CroogoNav::add('sidebar', 'Network', array(
	'title' => 'Network',
	'icon' => 'credit-card',
	'url' => '#',
	'weight' => 50,
	'children' => array(
		'Networks' => array(
			'title' => 'Networks',
			'url' => array(
				'admin' => true,
				'plugin' => 'network',
				'controller' => 'networks',
				'action' => 'index',
			),
		),
		
	),
));