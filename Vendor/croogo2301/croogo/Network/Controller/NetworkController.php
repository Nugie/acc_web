<?php
App::uses('NetworkAppController', 'Network.Controller');
/**
 * Acps Controller
 *
 * @property Acp $Acp
 * @property PaginatorComponent $Paginator
 */
class NetworkController extends NetworkAppController {


/**
 * index method
 *
 * @return void
 */
	public function display() {
		$this->set('title_for_layout', __d('croogo', 'Jaringan ACC'));
		$pointer_data = $this->Network->find(
			"all",
			array(
				"conditions" => array("Network.active" => "Yes", "Network.published" => "1"),
				"order" => "Network.network_id DESC",
				"recursive" => 2
			)
		);

		$this->set("pointer_data", $pointer_data);

		//print_r($pointer_data);
	}

	public function view($network_id = null) {
		$this->set('title_for_layout', __d('croogo', 'Jaringan ACC'));

		if (!ctype_digit($network_id)) $this->cakeError('error404');

		$data = $this->Network->find(
			"first",
			array(
				"conditions" => array("Network.network_id" => $network_id, "Network.published" => 1),
				"limit" => "1"
			)
		);

		if (empty($data)) $this->cakeError('error404');

		$pointer_data = $this->Network->find(
			"all",
			array(
				"conditions" => array("Network.active" => "Yes", "Network.published" => "1"),
				"order" => "Network.network_id DESC",
				"recursive" => 2
			)
		);
		$this->set("data", $data);
		$this->set("pointer_data", $pointer_data);

		//print_r($pointer_data);
	}
}
