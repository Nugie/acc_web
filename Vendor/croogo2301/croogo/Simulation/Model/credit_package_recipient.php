<?php

class CreditPackageRecipient extends CreditPackagesAppModel
{
	var $name = "CreditPackageRecipient";
	var $primaryKey = "recipient_id";
	var $useTable = "credit_package_recipients";
	
	var $validate = array(
		"name" => array(
			"minLength" => array(
				"rule" => array("minLength", 3),
				"message" => "Recipient name must contain at least 3 characters."
			),
			"isUnique" => array(
				"rule" => "isUnique",
				"message" => "This recipient name has already been taken."
			)
		),
		"email" => array(
			"minLength" => array(
				"rule" => array("minLength", 3),
				"message" => "Email address must contain at least 3 characters."
			),
			"email" => array(
				"rule" => array("email", true),
				"message" => "Please provide a valid email address."
			)
		)
	);
}

?>