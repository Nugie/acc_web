<?php
App::uses('SimulationAppModel', 'Simulation.Model');
/**
 * VehicleModel Model
 *
 * @property Brand $Brand
 * @property Type $Type
 * @property Category $Category
 * @property Kind $Kind
 */
class VehicleModel extends SimulationAppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'model_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'code_group' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code_model' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Brand' => array(
			'className' => 'Brand',
			'foreignKey' => false,
			'conditions' => array(
				'`Brand`.`brand_id`=`VehicleModel`.`brand_id`',
			),
			'fields' => '',
			'order' => ''
		),
		'VehicleType' => array(
			'className' => 'VehicleType',
			'foreignKey' => false,
			'conditions' => array(
				'`VehicleType`.`type_id`=`VehicleModel`.`type_id`',
			),
			'fields' => '',
			'order' => ''
		),
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => false,
			'conditions' => array(
				'`Category`.`category_id`=`VehicleModel`.`category_id`',
			),
			'fields' => '',
			'order' => ''
		),
		'Kind' => array(
			'className' => 'Kind',
			'foreignKey' => false,
			'conditions' => array(
				'`Kind`.`kind_id`=`VehicleModel`.`kind_id`',
			),
			'fields' => '',
			'order' => ''
		)
	);
}
