<?php
App::uses('SimulationAppModel', 'Simulation.Model');
/**
 * Branch Model
 *
 * @property Area $Area
 */
class Branch extends SimulationAppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'branches';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'branch_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'code_branch' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Area' => array(
			'className' => 'Area',
			'foreignKey' => 'area_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
