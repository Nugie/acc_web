<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Kinds'), h($kind['Kind']['kind_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Kinds'), array('action' => 'index'));

if (isset($kind['Kind']['kind_id'])):
	$this->Html->addCrumb($kind['Kind']['kind_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Kind'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Kind'), array(
		'action' => 'edit',
		$kind['Kind']['kind_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Kind'), array(
		'action' => 'delete', $kind['Kind']['kind_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $kind['Kind']['kind_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Kinds'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Kind'), array('action' => 'add'), array('button' => 'success'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Categories'), array('controller' => 'categories', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Category'), array('controller' => 'categories', 'action' => 'add'));
$this->end();

$this->append('main');
?>
<div class="kinds view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Kind Id'); ?></dt>
		<dd>
			<?php echo h($kind['Kind']['kind_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($kind['Category'][''], array('controller' => 'categories', 'action' => 'view', $kind['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Code Kind'); ?></dt>
		<dd>
			<?php echo h($kind['Kind']['code_kind']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Description'); ?></dt>
		<dd>
			<?php echo h($kind['Kind']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($kind['Kind']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($kind['Kind']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>