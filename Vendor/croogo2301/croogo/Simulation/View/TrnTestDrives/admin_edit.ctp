<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Trn Test Drives');
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Trn Test Drives'), array('action' => 'index'));

if ($this->action == 'admin_edit') {
	$this->Html->addCrumb($this->request->data['TrnTestDrive']['trn_id'], '/' . $this->request->url);
	$this->viewVars['title_for_layout'] = __d('simulation', 'Trn Test Drives') . ': ' . $this->request->data['TrnTestDrive']['trn_id'];
} else {
	$this->Html->addCrumb(__d('croogo', 'Add'), '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('TrnTestDrive'));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('simulation', 'Trn Test Drive'), '#trn-test-drive');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('trn-test-drive');

		echo $this->Form->input('trn_id');

		echo $this->Form->input('trn_date', array(
			'label' =>  __d('simulation', 'Trn Date'),
		));
		echo $this->Form->input('personal_name', array(
			'label' =>  __d('simulation', 'Personal Name'),
		));
		echo $this->Form->input('personal_phone', array(
			'label' =>  __d('simulation', 'Personal Phone'),
		));
		echo $this->Form->input('personal_email', array(
			'label' =>  __d('simulation', 'Personal Email'),
		));
		echo $this->Form->input('personal_area', array(
			'label' =>  __d('simulation', 'Personal Area'),
		));
		echo $this->Form->input('personal_branch', array(
			'label' =>  __d('simulation', 'Personal Branch'),
		));

	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'primary')) .
		$this->Form->button(__d('croogo', 'Save & New'), array('button' => 'success', 'name' => 'save_and_new')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('button' => 'danger'));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
