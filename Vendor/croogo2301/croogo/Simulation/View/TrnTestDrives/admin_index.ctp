<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Trn Test Drives');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Trn Test Drives'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('trn_id'),
		$this->Paginator->sort('trn_date'),
		$this->Paginator->sort('personal_name'),
		$this->Paginator->sort('personal_phone'),
		$this->Paginator->sort('personal_email'),
		$this->Paginator->sort('personal_area'),
		$this->Paginator->sort('personal_branch'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($trnTestDrives as $trnTestDrive):
		$row = array();
		$row[] = h($trnTestDrive['TrnTestDrive']['trn_id']);
		$row[] = $this->Time->format($trnTestDrive['TrnTestDrive']['trn_date'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = h($trnTestDrive['TrnTestDrive']['personal_name']);
		$row[] = h($trnTestDrive['TrnTestDrive']['personal_phone']);
		$row[] = h($trnTestDrive['TrnTestDrive']['personal_email']);
		$row[] = h($trnTestDrive['TrnTestDrive']['personal_area']);
		$row[] = h($trnTestDrive['TrnTestDrive']['personal_branch']);
		$actions = array($this->Croogo->adminRowActions($trnTestDrive['TrnTestDrive']['trn_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $trnTestDrive['TrnTestDrive']['trn_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$trnTestDrive['TrnTestDrive']['trn_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$trnTestDrive['TrnTestDrive']['trn_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $trnTestDrive['TrnTestDrive']['trn_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
