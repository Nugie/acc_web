<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Vehicle Models');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Vehicle Models'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('model_id'),
		$this->Paginator->sort('brand_id'),
		$this->Paginator->sort('type_id'),
		$this->Paginator->sort('category_id'),
		$this->Paginator->sort('kind_id'),
		$this->Paginator->sort('code_group'),
		$this->Paginator->sort('code_model'),
		$this->Paginator->sort('description'),
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($vehicleModels as $vehicleModel):
		$row = array();
		$row[] = h($vehicleModel['VehicleModel']['model_id']);
		$row[] = $this->Html->link($vehicleModel['Brand']['description'], array(
			'controller' => 'brands',
		'action' => 'view',
			$vehicleModel['Brand']['brand_id'],
	));
		$row[] = $this->Html->link($vehicleModel['VehicleType']['description'], array(
			'controller' => 'VehicleType',
		'action' => 'view',
			$vehicleModel['VehicleType']['type_id'],
	));
		$row[] = $this->Html->link($vehicleModel['Category']['description'], array(
			'controller' => 'categories',
		'action' => 'view',
			$vehicleModel['Category']['category_id'],
	));
		$row[] = $this->Html->link($vehicleModel['Kind']['description'], array(
			'controller' => 'kinds',
		'action' => 'view',
			$vehicleModel['Kind']['kind_id'],
	));
		$row[] = h($vehicleModel['VehicleModel']['code_group']);
		$row[] = h($vehicleModel['VehicleModel']['code_model']);
		$row[] = h($vehicleModel['VehicleModel']['description']);
		$row[] = $this->Time->format($vehicleModel['VehicleModel']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = $this->Time->format($vehicleModel['VehicleModel']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($vehicleModel['VehicleModel']['model_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $vehicleModel['VehicleModel']['model_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$vehicleModel['VehicleModel']['model_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$vehicleModel['VehicleModel']['model_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $vehicleModel['VehicleModel']['model_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
