<div class="administrations index">
	<h2><?php echo __('Administrations'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('adm_id'); ?></th>
			<th><?php echo $this->Paginator->sort('tenor'); ?></th>
			<th><?php echo $this->Paginator->sort('vehicle_status'); ?></th>
			<th><?php echo $this->Paginator->sort('polis'); ?></th>
			<th><?php echo $this->Paginator->sort('fiducia'); ?></th>
			<th><?php echo $this->Paginator->sort('administration'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($administrations as $administration): ?>
	<tr>
		<td><?php echo h($administration['Administration']['adm_id']); ?>&nbsp;</td>
		<td><?php echo h($administration['Administration']['tenor']); ?>&nbsp;</td>
		<td><?php echo h($administration['Administration']['vehicle_status']); ?>&nbsp;</td>
		<td><?php echo h($administration['Administration']['polis']); ?>&nbsp;</td>
		<td><?php echo h($administration['Administration']['fiducia']); ?>&nbsp;</td>
		<td><?php echo h($administration['Administration']['administration']); ?>&nbsp;</td>
		<td><?php echo h($administration['Administration']['created']); ?>&nbsp;</td>
		<td><?php echo h($administration['Administration']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $administration['Administration']['adm_id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $administration['Administration']['adm_id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $administration['Administration']['adm_id']), array('confirm' => __('Are you sure you want to delete # %s?', $administration['Administration']['adm_id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Administration'), array('action' => 'add')); ?></li>
	</ul>
</div>
