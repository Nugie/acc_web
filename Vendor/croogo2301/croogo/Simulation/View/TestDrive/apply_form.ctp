<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ACC</title>
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/foundation.min.css">
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/app.css?v=3.0">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	 <script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
  </head>
  <body>
    <div class="alertbox">
        <?php echo $msg; ?>
    </div>
	<?php if ($status==0) { ?>	
	<?php
								echo $this->Form->create(
									"Personal",
									array(
										"name" => "frmPersonal",
										"id" => "frmPersonal",
										"enctype"=>"multipart/form-data",
										"method" => "post",
										"class"=>"popup-form mt-20",
									)
								);
	   ?>
      <div class="row">
        <div class="medium-12 columns">
          <label><strong>NAMA</strong></label>
             <?php
									echo $this->Form->input(
										"personal_name",
										array(
											"id" => "personal_name",
											"type" => "text",
											"required" => true,
											"placeholder" => "Masukkan nama lengkap Anda",
											"maxlength" => 50,
											"label" => false
										)
									);
								?>
         
        </div>
        <div class="medium-12 columns">
          <label><strong>NOMOR TELEPON</strong></label>
		   <?php
									echo $this->Form->input(
										"personal_phone",
										array(
											"id" => "personal_phone",
											"type" => "tel",
											"required" => true,
											"placeholder" => "Masukkan nomer telepon Anda",
											"maxlength" => 50,
											"label" => false
										)
									);
								?>
        </div>
        <div class="medium-12 columns">
          <label><strong>EMAIL</strong></label>
		     <?php
									echo $this->Form->input(
										"personal_email",
										array(
											"id" => "personal_email",
											"type" => "email",
											"required" => true,
											"placeholder" => "Masukkan alamat email Anda",
											"maxlength" => 50,
											"label" => false
										)
									);
								?>
        </div>
        <div class="medium-12 columns">
          <label><strong>KOTA</strong></label>
		  <?php
				echo $this->Form->input(
					"personal_area",
					array(
						"id" => "personal_area",
						"class" => "input-baru",
						"empty" => "Pilih Kota",
						"required" => true,
						"div" => false,
						"label" => false,
						"options" => $area_list
					)
				);
			?>
        </div>
        <div class="medium-12 columns">
          <label><strong>KANTOR CABANG ACC</strong></label>
		   <?php
				echo $this->Form->input(
				"personal_branch",
				array(
					"id" => "personal_branch",
					"class" => "input-baru",
					"empty" => "Pilih Cabang",
					"required" => true,
					"div" => false,
					"label" => false,
					"options" => $cabang_list
				)
			);
			?> <img id="vehicle-group-loading" src="<?php echo $this->webroot; ?>img/ajax-loader.gif" alt="loading" style="display: none;">
        </div>
		
        <div class="medium-12 columns">
          <label><strong>KODE KEAMANAN</strong><br /></label>
		   <label for="captcha">&nbsp;</label>
           <div class="g-recaptcha" data-sitekey="<?php echo Configure::read('Service.recaptcha_public_key') ?>"></div>
        </div>
        <div class="medium-12 columns mt-20">
          <em>Saya mengizinkan ACC dan mitranya untuk menghubungi saya dalam membantu proses Test Drive. Dengan memberikan nomor telepon dan email, saya telah menyetujui untuk menerima semua pemberitahuan melalui ACC.</em>
        </div>
        <div class="medium-12 columns mt-20">
            <button type="submit" class="button" id="btnKirim">Kirim</button>
        </div>
      </div>
    <?php echo $this->Form->end(); ?>

  <script src="<?php echo $this->webroot; ?>js/vendor/jquery.js"></script>
  <script src="<?php echo $this->webroot; ?>js/vendor/foundation.min.js"></script>
  
  <script>
	$(document).ready(function(){
			function load_vehicle_group()
			{
				$("#vehicle-group-loading").show();

				var brand_id = $("#personal_area").val();
				$.get("/simulation/credit/query_branch/" + brand_id, function(data) {
					data = $.trim(data);
					$("#personal_branch").empty().html(data);
					$("#vehicle-group-loading").hide();

					//load_vehicle_model2();
				});
			}
			$("#personal_area").change(function(){
				load_vehicle_group();
			});
			load_vehicle_group();
	});
  </script>
  <?php } ?>
  </body>
  </html>
