<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Areas'), h($area['Area']['id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Areas'), array('action' => 'index'));

if (isset($area['Area']['id'])):
	$this->Html->addCrumb($area['Area']['id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Area'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Area'), array(
		'action' => 'edit',
		$area['Area']['id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Area'), array(
		'action' => 'delete', $area['Area']['id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $area['Area']['id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Areas'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Area'), array('action' => 'add'), array('button' => 'success'));
$this->end();

$this->append('main');
?>
<div class="areas view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Id'); ?></dt>
		<dd>
			<?php echo h($area['Area']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Code Area'); ?></dt>
		<dd>
			<?php echo h($area['Area']['code_area']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Description'); ?></dt>
		<dd>
			<?php echo h($area['Area']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($area['Area']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($area['Area']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>