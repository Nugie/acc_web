<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Acps'), h($acp['Acp']['acp_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Acps'), array('action' => 'index'));

if (isset($acp['Acp']['acp_id'])):
	$this->Html->addCrumb($acp['Acp']['acp_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Acp'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Acp'), array(
		'action' => 'edit',
		$acp['Acp']['acp_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Acp'), array(
		'action' => 'delete', $acp['Acp']['acp_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $acp['Acp']['acp_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Acps'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Acp'), array('action' => 'add'), array('button' => 'success'));
$this->end();

$this->append('main');
?>
<div class="acps view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Acp Id'); ?></dt>
		<dd>
			<?php echo h($acp['Acp']['acp_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Tenor'); ?></dt>
		<dd>
			<?php echo h($acp['Acp']['tenor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Acp Rate'); ?></dt>
		<dd>
			<?php echo h($acp['Acp']['acp_rate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($acp['Acp']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($acp['Acp']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>