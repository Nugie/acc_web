<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Acps');
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Acps'), array('action' => 'index'));

if ($this->action == 'admin_edit') {
	$this->Html->addCrumb($this->request->data['Acp']['acp_id'], '/' . $this->request->url);
	$this->viewVars['title_for_layout'] = __d('simulation', 'Acps') . ': ' . $this->request->data['Acp']['acp_id'];
} else {
	$this->Html->addCrumb(__d('croogo', 'Add'), '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('Acp'));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('simulation', 'Acp'), '#acp');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('acp');

		echo $this->Form->input('acp_id');

		echo $this->Form->input('tenor', array(
			'label' =>  __d('simulation', 'Tenor'),
		));
		echo $this->Form->input('acp_rate', array(
			'label' =>  __d('simulation', 'Acp Rate'),
		));

	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'primary')) .
		$this->Form->button(__d('croogo', 'Save & New'), array('button' => 'success', 'name' => 'save_and_new')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('button' => 'danger'));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
