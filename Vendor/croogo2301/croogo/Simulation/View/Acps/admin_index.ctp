<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Acps');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Acps'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('acp_id'),
		$this->Paginator->sort('tenor'),
		$this->Paginator->sort('acp_rate'),
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($acps as $acp):
		$row = array();
		$row[] = h($acp['Acp']['acp_id']);
		$row[] = h($acp['Acp']['tenor']);
		$row[] = h($acp['Acp']['acp_rate']);
		$row[] = $this->Time->format($acp['Acp']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = $this->Time->format($acp['Acp']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($acp['Acp']['acp_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $acp['Acp']['acp_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$acp['Acp']['acp_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$acp['Acp']['acp_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $acp['Acp']['acp_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
