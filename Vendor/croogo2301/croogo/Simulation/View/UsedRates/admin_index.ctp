<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Used Rates');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Used Rates'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('ur_id'),
		$this->Paginator->sort('area_id'),
		$this->Paginator->sort('branch_id'),
		$this->Paginator->sort('brand_id'),
		$this->Paginator->sort('type_id'),
		$this->Paginator->sort('category_id'),
		$this->Paginator->sort('kind_id'),
		$this->Paginator->sort('year_from'),
		$this->Paginator->sort('year_to'),
		$this->Paginator->sort('tenor'),
		$this->Paginator->sort('otr_start'),
		$this->Paginator->sort('otr_end'),
		$this->Paginator->sort('from_dp'),
		$this->Paginator->sort('to_dp'),
		$this->Paginator->sort('flat_rate_percentage'),
		/*
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		*/
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($usedRates as $usedRate):
		$row = array();
		$row[] = h($usedRate['UsedRate']['ur_id']);
		$row[] = $this->Html->link($usedRate['Area']['id'], array(
			'controller' => 'areas',
		'action' => 'view',
			$usedRate['Area']['id'],
	));
		$row[] = $this->Html->link($usedRate['Branch']['description'], array(
			'controller' => 'branches',
		'action' => 'view',
			$usedRate['Branch']['branch_id'],
	));
		$row[] = $this->Html->link($usedRate['Brand']['description'], array(
			'controller' => 'brands',
		'action' => 'view',
			$usedRate['Brand']['brand_id'],
	));
		$row[] = $this->Html->link($usedRate['Type']['description'], array(
			'controller' => 'types',
		'action' => 'view',
			$usedRate['Type']['type_id'],
	));
		$row[] = $this->Html->link($usedRate['Category']['description'], array(
			'controller' => 'categories',
		'action' => 'view',
			$usedRate['Category']['category_id'],
	));
		$row[] = $this->Html->link($usedRate['Kind']['description'], array(
			'controller' => 'kinds',
		'action' => 'view',
			$usedRate['Kind']['kind_id'],
	));
		$row[] = h($usedRate['UsedRate']['year_from']);
		$row[] = h($usedRate['UsedRate']['year_to']);
		$row[] = h($usedRate['UsedRate']['tenor']);
		$row[] = h($usedRate['UsedRate']['otr_start']);
		$row[] = h($usedRate['UsedRate']['otr_end']);
		$row[] = h($usedRate['UsedRate']['from_dp']);
		$row[] = h($usedRate['UsedRate']['to_dp']);
		$row[] = h($usedRate['UsedRate']['flat_rate_percentage']);
		//$row[] = $this->Time->format($usedRate['UsedRate']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		//$row[] = $this->Time->format($usedRate['UsedRate']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($usedRate['UsedRate']['ur_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $usedRate['UsedRate']['ur_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$usedRate['UsedRate']['ur_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$usedRate['UsedRate']['ur_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $usedRate['UsedRate']['ur_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
