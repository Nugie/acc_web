<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Used Rates'), h($usedRate['UsedRate']['ur_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Used Rates'), array('action' => 'index'));

if (isset($usedRate['UsedRate']['ur_id'])):
	$this->Html->addCrumb($usedRate['UsedRate']['ur_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Used Rate'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Used Rate'), array(
		'action' => 'edit',
		$usedRate['UsedRate']['ur_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Used Rate'), array(
		'action' => 'delete', $usedRate['UsedRate']['ur_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $usedRate['UsedRate']['ur_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Used Rates'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Used Rate'), array('action' => 'add'), array('button' => 'success'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Areas'), array('controller' => 'areas', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Area'), array('controller' => 'areas', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Branches'), array('controller' => 'branches', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Branch'), array('controller' => 'branches', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Brands'), array('controller' => 'brands', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Brand'), array('controller' => 'brands', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Types'), array('controller' => 'types', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Type'), array('controller' => 'types', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Categories'), array('controller' => 'categories', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Category'), array('controller' => 'categories', 'action' => 'add'));
	echo $this->Croogo->adminAction(__d('croogo', 'List Kinds'), array('controller' => 'kinds', 'action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Kind'), array('controller' => 'kinds', 'action' => 'add'));
$this->end();

$this->append('main');
?>
<div class="usedRates view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Ur Id'); ?></dt>
		<dd>
			<?php echo h($usedRate['UsedRate']['ur_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Area'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usedRate['Area']['id'], array('controller' => 'areas', 'action' => 'view', $usedRate['Area']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Branch'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usedRate['Branch'][''], array('controller' => 'branches', 'action' => 'view', $usedRate['Branch']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Brand'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usedRate['Brand'][''], array('controller' => 'brands', 'action' => 'view', $usedRate['Brand']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usedRate['Type']['title'], array('controller' => 'types', 'action' => 'view', $usedRate['Type']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usedRate['Category'][''], array('controller' => 'categories', 'action' => 'view', $usedRate['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Kind'); ?></dt>
		<dd>
			<?php echo $this->Html->link($usedRate['Kind'][''], array('controller' => 'kinds', 'action' => 'view', $usedRate['Kind']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Year From'); ?></dt>
		<dd>
			<?php echo h($usedRate['UsedRate']['year_from']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Year To'); ?></dt>
		<dd>
			<?php echo h($usedRate['UsedRate']['year_to']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Tenor'); ?></dt>
		<dd>
			<?php echo h($usedRate['UsedRate']['tenor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Otr Start'); ?></dt>
		<dd>
			<?php echo h($usedRate['UsedRate']['otr_start']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Otr End'); ?></dt>
		<dd>
			<?php echo h($usedRate['UsedRate']['otr_end']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'From Dp'); ?></dt>
		<dd>
			<?php echo h($usedRate['UsedRate']['from_dp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'To Dp'); ?></dt>
		<dd>
			<?php echo h($usedRate['UsedRate']['to_dp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Flat Rate Percentage'); ?></dt>
		<dd>
			<?php echo h($usedRate['UsedRate']['flat_rate_percentage']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($usedRate['UsedRate']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($usedRate['UsedRate']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>