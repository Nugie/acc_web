<div class="vehicleTypes view">
<h2><?php echo __('Vehicle Type'); ?></h2>
	<dl>
		<dt><?php echo __('Type Id'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['type_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Brand'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vehicleType['Brand'][''], array('controller' => 'brands', 'action' => 'view', $vehicleType['Brand']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Group'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['code_group']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Desc Group'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['desc_group']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Type'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['code_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vehicle Type'), array('action' => 'edit', $vehicleType['VehicleType']['type_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vehicle Type'), array('action' => 'delete', $vehicleType['VehicleType']['type_id']), array('confirm' => __('Are you sure you want to delete # %s?', $vehicleType['VehicleType']['type_id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicle Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Brands'), array('controller' => 'brands', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Brand'), array('controller' => 'brands', 'action' => 'add')); ?> </li>
	</ul>
</div>
