<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Vehicle Types');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Vehicle Types'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('type_id'),
		$this->Paginator->sort('brand_id'),
		$this->Paginator->sort('code_group'),
		$this->Paginator->sort('desc_group'),
		$this->Paginator->sort('code_type'),
		$this->Paginator->sort('description'),
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($vehicleTypesNew as $vehicleType):
		$row = array();
		$row[] = h($vehicleType['VehicleTypeNew']['type_id']);
		$row[] = $this->Html->link($vehicleType['Brand']['description'], array(
			'controller' => 'brands',
		'action' => 'view',
			$vehicleType['Brand']['description'],
	));
		$row[] = h($vehicleType['VehicleTypeNewNew']['code_group']);
		$row[] = h($vehicleType['VehicleTypeNew']['desc_group']);
		$row[] = h($vehicleType['VehicleTypeNew']['code_type']);
		$row[] = h($vehicleType['VehicleTypeNew']['description']);
		$row[] = $this->Time->format($vehicleType['VehicleTypeNew']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = $this->Time->format($vehicleType['VehicleTypeNew']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($vehicleType['VehicleTypeNew']['type_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $vehicleType['VehicleTypeNew']['type_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$vehicleType['VehicleTypeNew']['type_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$vehicleType['VehicleTypeNew']['type_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $vehicleType['VehicleTypeNew']['type_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
