<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Branchs');
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Branchs'), array('action' => 'index'));

if ($this->action == 'admin_edit') {
	$this->Html->addCrumb($this->request->data['Branch']['branch_id'], '/' . $this->request->url);
	$this->viewVars['title_for_layout'] = __d('simulation', 'Branchs') . ': ' . $this->request->data['Branch']['branch_id'];
} else {
	$this->Html->addCrumb(__d('croogo', 'Add'), '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('Branch'));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('simulation', 'Branch'), '#branch');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('branch');

		echo $this->Form->input('branch_id');

		echo $this->Form->input('area_id', array(
			'label' =>  __d('simulation', 'Area'),
		));
		echo $this->Form->input('code_branch', array(
			'label' =>  __d('simulation', 'Code Branch'),
		));
		echo $this->Form->input('description', array(
			'label' =>  __d('simulation', 'Description'),
		));

	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'primary')) .
		$this->Form->button(__d('croogo', 'Save & New'), array('button' => 'success', 'name' => 'save_and_new')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('button' => 'danger'));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
