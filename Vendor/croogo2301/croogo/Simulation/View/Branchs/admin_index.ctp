<?php
$this->viewVars['title_for_layout'] = __d('simulation', 'Branchs');
$this->extend('/Common/admin_index');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('simulation', 'Branchs'), array('action' => 'index'));

$this->set('tableClass', 'table table-striped');

$this->append('table-heading');
	$tableHeaders = $this->Html->tableHeaders(array(
		$this->Paginator->sort('branch_id'),
		$this->Paginator->sort('area_id'),
		$this->Paginator->sort('code_branch'),
		$this->Paginator->sort('description'),
		$this->Paginator->sort('created'),
		$this->Paginator->sort('modified'),
		array(__d('croogo', 'Actions') => array('class' => 'actions')),
	));
	echo $this->Html->tag('thead', $tableHeaders);
$this->end();

$this->append('table-body');
	$rows = array();
	foreach ($branchs as $branch):
		$row = array();
		$row[] = h($branch['Branch']['branch_id']);
		$row[] = $this->Html->link($branch['Area']['id'], array(
			'controller' => 'areas',
		'action' => 'view',
			$branch['Area']['id'],
	));
		$row[] = h($branch['Branch']['code_branch']);
		$row[] = h($branch['Branch']['description']);
		$row[] = $this->Time->format($branch['Branch']['created'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$row[] = $this->Time->format($branch['Branch']['modified'], '%Y-%m-%d %H:%M', __d('croogo', 'Invalid datetime'));
		$actions = array($this->Croogo->adminRowActions($branch['Branch']['branch_id']));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'view', $branch['Branch']['branch_id']
	), array(
			'icon' => 'eye-open',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'edit',
			$branch['Branch']['branch_id'],
		), array(
			'icon' => 'pencil',
		));
		$actions[] = $this->Croogo->adminRowAction('', array(
			'action' => 'delete',
			$branch['Branch']['branch_id'],
		), array(
			'icon' => 'trash',
			'escape' => true,
		),
		__d('croogo', 'Are you sure you want to delete # %s?', $branch['Branch']['branch_id'])
		);
		$row[] = $this->Html->div('item-actions', implode(' ', $actions));
		$rows[] = $this->Html->tableCells($row);
	endforeach;
	echo $this->Html->tag('tbody', implode('', $rows));
$this->end();
