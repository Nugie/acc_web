<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Categories'), h($category['Category']['category_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Categories'), array('action' => 'index'));

if (isset($category['Category']['category_id'])):
	$this->Html->addCrumb($category['Category']['category_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Category'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Category'), array(
		'action' => 'edit',
		$category['Category']['category_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Category'), array(
		'action' => 'delete', $category['Category']['category_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $category['Category']['category_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Categories'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Category'), array('action' => 'add'), array('button' => 'success'));
$this->end();

$this->append('main');
?>
<div class="categories view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Category Id'); ?></dt>
		<dd>
			<?php echo h($category['Category']['category_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Code Category'); ?></dt>
		<dd>
			<?php echo h($category['Category']['code_category']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Description'); ?></dt>
		<dd>
			<?php echo h($category['Category']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($category['Category']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($category['Category']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>