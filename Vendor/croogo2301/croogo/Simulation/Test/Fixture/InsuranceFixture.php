<?php
/**
 * Insurance Fixture
 */
class InsuranceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'insurance_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'category_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'tenor' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => true, 'key' => 'index'),
		'ar_gross_rate' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => true),
		'otr_start' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'otr_end' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'insurance_id', 'unique' => 1),
			'category_id' => array('column' => 'category_id', 'unique' => 0),
			'tenor' => array('column' => 'tenor', 'unique' => 0),
			'vehicle_status' => array('column' => 'vehicle_status', 'unique' => 0),
			'otr_start' => array('column' => 'otr_start', 'unique' => 0),
			'otr_end' => array('column' => 'otr_end', 'unique' => 0),
			'category_id_2' => array('column' => array('category_id', 'tenor', 'vehicle_status', 'otr_start', 'otr_end'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'insurance_id' => '',
			'category_id' => '',
			'tenor' => 1,
			'ar_gross_rate' => 1,
			'otr_start' => '',
			'otr_end' => '',
			'created' => '2016-11-05 02:57:38',
			'modified' => '2016-11-05 02:57:38'
		),
	);

}
