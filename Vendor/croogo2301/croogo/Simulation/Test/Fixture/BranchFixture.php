<?php
/**
 * Branch Fixture
 */
class BranchFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'branchs';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'branch_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'area_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'code_branch' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 32, 'key' => 'index', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'description' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 64, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'branch_id', 'unique' => 1),
			'area_id' => array('column' => 'area_id', 'unique' => 0),
			'code_branch' => array('column' => 'code_branch', 'unique' => 0),
			'area_id_2' => array('column' => array('area_id', 'code_branch'), 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'branch_id' => '',
			'area_id' => '',
			'code_branch' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet',
			'created' => '2016-11-05 01:23:43',
			'modified' => '2016-11-05 01:23:43'
		),
	);

}
