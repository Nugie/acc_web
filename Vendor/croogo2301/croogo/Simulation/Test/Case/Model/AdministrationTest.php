<?php
App::uses('Administration', 'Simulation.Model');

/**
 * Administration Test Case
 */
class AdministrationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.simulation.administration'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Administration = ClassRegistry::init('Simulation.Administration');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Administration);

		parent::tearDown();
	}

}
