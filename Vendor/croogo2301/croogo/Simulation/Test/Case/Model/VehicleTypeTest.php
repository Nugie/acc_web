<?php
App::uses('VehicleType', 'Simulation.Model');

/**
 * VehicleType Test Case
 */
class VehicleTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.simulation.vehicle_type',
		'plugin.simulation.brand'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VehicleType = ClassRegistry::init('Simulation.VehicleType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VehicleType);

		parent::tearDown();
	}

}
