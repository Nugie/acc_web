<?php
App::uses('Kind', 'Simulation.Model');

/**
 * Kind Test Case
 */
class KindTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.simulation.kind',
		'plugin.simulation.category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Kind = ClassRegistry::init('Simulation.Kind');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Kind);

		parent::tearDown();
	}

}
