<?php
App::uses('Insurance', 'Simulation.Model');

/**
 * Insurance Test Case
 */
class InsuranceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.simulation.insurance',
		'plugin.simulation.category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Insurance = ClassRegistry::init('Simulation.Insurance');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Insurance);

		parent::tearDown();
	}

}
