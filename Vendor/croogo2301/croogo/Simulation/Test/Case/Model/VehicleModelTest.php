<?php
App::uses('VehicleModel', 'Simulation.Model');

/**
 * VehicleModel Test Case
 */
class VehicleModelTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.simulation.vehicle_model',
		'plugin.simulation.brand',
		'plugin.simulation.type',
		'plugin.simulation.category',
		'plugin.simulation.kind'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VehicleModel = ClassRegistry::init('Simulation.VehicleModel');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VehicleModel);

		parent::tearDown();
	}

}
