<?php
App::uses('Acp', 'Simulation.Model');

/**
 * Acp Test Case
 */
class AcpTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.simulation.acp'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Acp = ClassRegistry::init('Simulation.Acp');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Acp);

		parent::tearDown();
	}

}
