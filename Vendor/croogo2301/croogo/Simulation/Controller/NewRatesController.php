<?php
App::uses('SimulationAppController', 'Simulation.Controller');
/**
 * NewRates Controller
 *
 * @property NewRate $NewRate
 * @property PaginatorComponent $Paginator
 */
class NewRatesController extends SimulationAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->NewRate->recursive = 0;
		$this->set('newRates', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->NewRate->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'new rate')));
		}
		$options = array('conditions' => array('NewRate.' . $this->NewRate->primaryKey => $id));
		$this->set('newRate', $this->NewRate->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->NewRate->create();
			if ($this->NewRate->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'new rate')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->NewRate->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'new rate')), 'default', array('class' => 'error'));
			}
		}
		$areas = $this->NewRate->Area->find('list');
		$branches = $this->NewRate->Branch->find('list');
		$brands = $this->NewRate->Brand->find('list');
		$types = $this->NewRate->Type->find('list');
		$categories = $this->NewRate->Category->find('list');
		$kinds = $this->NewRate->Kind->find('list');
		$this->set(compact('areas', 'branches', 'brands', 'types', 'categories', 'kinds'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->NewRate->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'new rate')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->NewRate->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'new rate')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'new rate')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('NewRate.' . $this->NewRate->primaryKey => $id));
			$this->request->data = $this->NewRate->find('first', $options);
		}
		$areas = $this->NewRate->Area->find('list');
		$branches = $this->NewRate->Branch->find('list');
		$brands = $this->NewRate->Brand->find('list');
		$types = $this->NewRate->Type->find('list');
		$categories = $this->NewRate->Category->find('list');
		$kinds = $this->NewRate->Kind->find('list');
		$this->set(compact('areas', 'branches', 'brands', 'types', 'categories', 'kinds'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->NewRate->id = $id;
		if (!$this->NewRate->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'new rate')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->NewRate->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'New rate')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'New rate')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}
}
