<?php
App::uses('SimulationAppController', 'Simulation.Controller');
/**
 * UsedRates Controller
 *
 * @property UsedRate $UsedRate
 * @property PaginatorComponent $Paginator
 */
class UsedRatesController extends SimulationAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->UsedRate->recursive = 0;
		$this->set('usedRates', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->UsedRate->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'used rate')));
		}
		$options = array('conditions' => array('UsedRate.' . $this->UsedRate->primaryKey => $id));
		$this->set('usedRate', $this->UsedRate->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->UsedRate->create();
			if ($this->UsedRate->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'used rate')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->UsedRate->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'used rate')), 'default', array('class' => 'error'));
			}
		}
		$areas = $this->UsedRate->Area->find('list');
		$branches = $this->UsedRate->Branch->find('list');
		$brands = $this->UsedRate->Brand->find('list');
		$types = $this->UsedRate->Type->find('list');
		$categories = $this->UsedRate->Category->find('list');
		$kinds = $this->UsedRate->Kind->find('list');
		$this->set(compact('areas', 'branches', 'brands', 'types', 'categories', 'kinds'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->UsedRate->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'used rate')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->UsedRate->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'used rate')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'used rate')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('UsedRate.' . $this->UsedRate->primaryKey => $id));
			$this->request->data = $this->UsedRate->find('first', $options);
		}
		$areas = $this->UsedRate->Area->find('list');
		$branches = $this->UsedRate->Branch->find('list');
		$brands = $this->UsedRate->Brand->find('list');
		$types = $this->UsedRate->Type->find('list');
		$categories = $this->UsedRate->Category->find('list');
		$kinds = $this->UsedRate->Kind->find('list');
		$this->set(compact('areas', 'branches', 'brands', 'types', 'categories', 'kinds'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->UsedRate->id = $id;
		if (!$this->UsedRate->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'used rate')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->UsedRate->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Used rate')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Used rate')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}
}
