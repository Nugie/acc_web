<?php
App::uses('SimulationAppController', 'Simulation.Controller');
/**
 * VehicleTypes Controller
 *
 * @property VehicleType $VehicleType
 * @property PaginatorComponent $Paginator
 */
class VehicleTypesController extends SimulationAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->VehicleType->recursive = 0;
		$this->set('vehicleTypes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->VehicleType->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle type')));
		}
		$options = array('conditions' => array('VehicleType.' . $this->VehicleType->primaryKey => $id));
		$this->set('vehicleType', $this->VehicleType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->VehicleType->create();
			if ($this->VehicleType->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'vehicle type')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->VehicleType->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'vehicle type')), 'default', array('class' => 'error'));
			}
		}
		$brands = $this->VehicleType->Brand->find('list');
		$this->set(compact('brands'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->VehicleType->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle type')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VehicleType->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'vehicle type')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'vehicle type')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('VehicleType.' . $this->VehicleType->primaryKey => $id));
			$this->request->data = $this->VehicleType->find('first', $options);
		}
		$brands = $this->VehicleType->Brand->find('list');
		$this->set(compact('brands'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->VehicleType->id = $id;
		if (!$this->VehicleType->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle type')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->VehicleType->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Vehicle type')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Vehicle type')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->VehicleType->recursive = 0;
		$this->set('vehicleTypes', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->VehicleType->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle type')));
		}
		$options = array('conditions' => array('VehicleType.' . $this->VehicleType->primaryKey => $id));
		$this->set('vehicleType', $this->VehicleType->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->VehicleType->create();
			if ($this->VehicleType->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'vehicle type')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->VehicleType->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'vehicle type')), 'default', array('class' => 'error'));
			}
		}
		$brands = $this->VehicleType->Brand->find('list');
		$this->set(compact('brands'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->VehicleType->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle type')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VehicleType->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'vehicle type')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'vehicle type')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('VehicleType.' . $this->VehicleType->primaryKey => $id));
			$this->request->data = $this->VehicleType->find('first', $options);
		}
		$brands = $this->VehicleType->Brand->find('list');
		$this->set(compact('brands'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->VehicleType->id = $id;
		if (!$this->VehicleType->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle type')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->VehicleType->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Vehicle type')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Vehicle type')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}
}
