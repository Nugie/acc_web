<?php
App::uses('SimulationAppController', 'Simulation.Controller');
/**
 * TrnTestDrives Controller
 *
 * @property TrnTestDrive $TrnTestDrive
 * @property PaginatorComponent $Paginator
 */
class TrnTestDrivesController extends SimulationAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TrnTestDrive->recursive = 0;
		$this->set('trnTestDrives', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TrnTestDrive->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'trn test drive')));
		}
		$options = array('conditions' => array('TrnTestDrive.' . $this->TrnTestDrive->primaryKey => $id));
		$this->set('trnTestDrive', $this->TrnTestDrive->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TrnTestDrive->create();
			if ($this->TrnTestDrive->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'trn test drive')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->TrnTestDrive->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'trn test drive')), 'default', array('class' => 'error'));
			}
		}
		$personals = $this->TrnTestDrive->Personal->find('list');
		$this->set(compact('personals'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TrnTestDrive->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'trn test drive')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TrnTestDrive->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'trn test drive')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'trn test drive')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('TrnTestDrive.' . $this->TrnTestDrive->primaryKey => $id));
			$this->request->data = $this->TrnTestDrive->find('first', $options);
		}
		$personals = $this->TrnTestDrive->Personal->find('list');
		$this->set(compact('personals'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TrnTestDrive->id = $id;
		if (!$this->TrnTestDrive->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'trn test drive')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TrnTestDrive->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Trn test drive')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Trn test drive')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}
}
