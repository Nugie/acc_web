<?php
App::uses('SimulationAppController', 'Simulation.Controller');
/**
 * VehicleModels Controller
 *
 * @property VehicleModel $VehicleModel
 * @property PaginatorComponent $Paginator
 */
class VehicleModelsController extends SimulationAppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->VehicleModel->recursive = 0;
		$this->set('vehicleModels', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->VehicleModel->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle model')));
		}
		$options = array('conditions' => array('VehicleModel.' . $this->VehicleModel->primaryKey => $id));
		$this->set('vehicleModel', $this->VehicleModel->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->VehicleModel->create();
			if ($this->VehicleModel->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'vehicle model')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->VehicleModel->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'vehicle model')), 'default', array('class' => 'error'));
			}
		}
		$brands = $this->VehicleModel->Brand->find('list');
		$types = $this->VehicleModel->Type->find('list');
		$categories = $this->VehicleModel->Category->find('list');
		$kinds = $this->VehicleModel->Kind->find('list');
		$this->set(compact('brands', 'types', 'categories', 'kinds'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->VehicleModel->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle model')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VehicleModel->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'vehicle model')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'vehicle model')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('VehicleModel.' . $this->VehicleModel->primaryKey => $id));
			$this->request->data = $this->VehicleModel->find('first', $options);
		}
		$brands = $this->VehicleModel->Brand->find('list');
		$types = $this->VehicleModel->Type->find('list');
		$categories = $this->VehicleModel->Category->find('list');
		$kinds = $this->VehicleModel->Kind->find('list');
		$this->set(compact('brands', 'types', 'categories', 'kinds'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->VehicleModel->id = $id;
		if (!$this->VehicleModel->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle model')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->VehicleModel->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Vehicle model')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Vehicle model')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->VehicleModel->recursive = 0;
		$this->set('vehicleModels', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->VehicleModel->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle model')));
		}
		$options = array('conditions' => array('VehicleModel.' . $this->VehicleModel->primaryKey => $id));
		$this->set('vehicleModel', $this->VehicleModel->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->VehicleModel->create();
			if ($this->VehicleModel->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'vehicle model')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $this->VehicleModel->id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'vehicle model')), 'default', array('class' => 'error'));
			}
		}
		$brands = $this->VehicleModel->Brand->find('list');
		$types = $this->VehicleModel->Type->find('list');
		$categories = $this->VehicleModel->Category->find('list');
		$kinds = $this->VehicleModel->Kind->find('list');
		$this->set(compact('brands', 'types', 'categories', 'kinds'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->VehicleModel->exists($id)) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle model')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->VehicleModel->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__d('croogo', '%s has been saved', __d('simulation', 'vehicle model')), 'default', array('class' => 'success'));
				$redirectTo = array('action' => 'index');
				if (isset($this->request->data['apply'])) {
					$redirectTo = array('action' => 'edit', $id);
				}
				if (isset($this->request->data['save_and_new'])) {
					$redirectTo = array('action' => 'add');
				}
				return $this->redirect($redirectTo);
			} else {
				$this->Session->setFlash(__d('croogo', '%s could not be saved. Please, try again.', __d('simulation', 'vehicle model')), 'default', array('class' => 'error'));
			}
		} else {
			$options = array('conditions' => array('VehicleModel.' . $this->VehicleModel->primaryKey => $id));
			$this->request->data = $this->VehicleModel->find('first', $options);
		}
		$brands = $this->VehicleModel->Brand->find('list');
		$types = $this->VehicleModel->Type->find('list');
		$categories = $this->VehicleModel->Category->find('list');
		$kinds = $this->VehicleModel->Kind->find('list');
		$this->set(compact('brands', 'types', 'categories', 'kinds'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->VehicleModel->id = $id;
		if (!$this->VehicleModel->exists()) {
			throw new NotFoundException(__d('croogo', 'Invalid %s', __d('simulation', 'vehicle model')));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->VehicleModel->delete()) {
			$this->Session->setFlash(__d('croogo', '%s deleted', __d('simulation', 'Vehicle model')), 'default', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__d('croogo', '%s was not deleted', __d('simulation', 'Vehicle model')), 'default', array('class' => 'error'));
		return $this->redirect(array('action' => 'index'));
	}
}
