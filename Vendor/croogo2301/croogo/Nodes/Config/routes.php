<?php



CroogoRouter::mapResources('Nodes.Nodes', array(
	'prefix' => '/:api/:prefix/',
));

Router::connect('/:api/:prefix/nodes/lookup', array(
	'plugin' => 'nodes',
	'controller' => 'nodes',
	'action' => 'lookup',
), array(
	'routeClass' => 'ApiRoute',
));

//OK
// Front
// Network

CroogoRouter::connect('/xx_login', array(
	 'admin'=>true,'plugin' => 'users','controller' => 'users', 'action' => 'admin_login'
));

CroogoRouter::connect('/network', array(
	 'plugin' => 'network','controller' => 'network', 'action' => 'display'
));

CroogoRouter::connect('/network/view/*', array(
	 'plugin' => 'network','controller' => 'network', 'action' => 'view'
));

CroogoRouter::connect('/harga-mobil/test-drive/*', array(
	'plugin' => 'simulation', 'controller' => 'TestDrive', 'action' => 'apply_form'
));
CroogoRouter::connect('/harga-mobil/test_drive/*', array(
	'plugin' => 'simulation', 'controller' => 'TestDrive', 'action' => 'apply_form'
));
CroogoRouter::connect('/harga-mobil/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_harga',
	'type' => 'page', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/harga-mobil', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_harga',
	'type' => 'page', 'slug' => 'toyota'
));

CroogoRouter::connect('/simulasi-kredit-mobil/*', array(
	'plugin' => 'simulation', 'controller' => 'credit', 'action' => 'index'
));
CroogoRouter::connect('/simulasi-kredit-mobil2', array(
	'plugin' => 'simulation', 'controller' => 'credit', 'action' => 'index2'
));
CroogoRouter::connect('/apply_form/:id', array(
	'plugin' => 'simulation', 'controller' => 'credit', 'action' => 'apply_form','id'=>'^[0-9]*$'
));

CroogoRouter::connect('/agreement_detail/*', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'agreement_detail'
));
CroogoRouter::connect('/agreement_history', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'agreement_history'
));
CroogoRouter::connect('/profile', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'profile'
));
CroogoRouter::connect('/forgot', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'forgot'
));
CroogoRouter::connect('/member', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'member'
));

CroogoRouter::connect('/change_password', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'change_password'
));

CroogoRouter::connect('/login', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'login'
));
CroogoRouter::connect('/logout', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'logout'
));
CroogoRouter::connect('/register', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'register'
));
CroogoRouter::connect('/activate/*', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'activate'
));
CroogoRouter::connect('/reset/*', array(
	 'plugin' => 'users','controller' => 'users', 'action' => 'reset'
));


CroogoRouter::connect('/testimonials', array(
	 'plugin' => 'testimonial','controller' => 'testimonial', 'action' => 'display'
));
CroogoRouter::connect('/testimonials/add', array(
	 'plugin' => 'testimonial','controller' => 'testimonial', 'action' => 'add'
));
CroogoRouter::connect('/testimonials/page/:page', array(
	'plugin' => 'testimonial', 'controller' => 'testimonial','action' => 'display','slug' =>'','page'=>'^[0-9]*$'
));

CroogoRouter::connect('/faq', array(
	 'plugin' => 'nodes','controller' => 'nodes', 'action' => 'view_network','type' => 'page', 'slug' => 'faq'
));

CroogoRouter::connect('/kamus-pembiayaan', array(
	 'plugin' => 'nodes','controller' => 'nodes', 'action' => 'view_network','type' => 'page', 'slug' => 'kamus-pembiayaan'
));
CroogoRouter::connect('/kebijakan-website', array(
	 'plugin' => 'nodes','controller' => 'nodes', 'action' => 'view_network','type' => 'page', 'slug' => 'kebijakan-website'
));
CroogoRouter::connect('/jalur-pelayanan-penyelesaian-pengaduan', array(
	 'plugin' => 'nodes','controller' => 'nodes', 'action' => 'view_network','type' => 'page', 'slug' => 'jalur-pelayanan-penyelesaian-pengaduan'
));

CroogoRouter::connect('/tentang-kami/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_about',
	'type' => 'page', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/informasi-investor/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_investor',
	'type' => 'page', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/tata-kelola-perusahaan/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_company',
	'type' => 'page', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/produk-layanan/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_product',
	'type' => 'page', 'slug' => '^[a-zA-Z0-9_]*$'
));
//NEWS
CroogoRouter::connect('/news', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'news'
));
CroogoRouter::connect('/news/page/:page', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'news','page'=>'^[0-9]*$'
));
CroogoRouter::connect('/news/read/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_news',
	'type' => 'news', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/news/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'news', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/news/:slug/page/:page', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'news', 'slug' => '^[a-zA-Z0-9_]*$','page'=>'^[0-9]*$'
));
//END NEWS
//Corporate News
CroogoRouter::connect('/corporatenews', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'corporatenews'
));
CroogoRouter::connect('/corporatenews/page/:page', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'corporatenews','page'=>'^[0-9]*$'
));
CroogoRouter::connect('/corporatenews/read/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_news',
	'type' => 'corporatenews', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/corporatenews/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'corporatenews', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/corporatenews/:slug/page/:page', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'corporatenews', 'slug' => '^[a-zA-Z0-9_]*$','page'=>'^[0-9]*$'
));
//END corporatenews
//Corporate EVENTS
CroogoRouter::connect('/events', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'events'
));
CroogoRouter::connect('/events/page/:page', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'events','page'=>'^[0-9]*$'
));
CroogoRouter::connect('/events/read/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_news',
	'type' => 'events', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/events/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'events', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/events/:slug/page/:page', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'events', 'slug' => '^[a-zA-Z0-9_]*$','page'=>'^[0-9]*$'
));
//END EVENTS
//Corporate promo
CroogoRouter::connect('/promo', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'promo'
));
CroogoRouter::connect('/promo/page/:page', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'promo','page'=>'^[0-9]*$'
));
CroogoRouter::connect('/promo/read/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_news',
	'type' => 'promo', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/promo/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'promo', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/promo/:slug/page/:page', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'promo', 'slug' => '^[a-zA-Z0-9_]*$','page'=>'^[0-9]*$'
));
//END promo

/*
CroogoRouter::connect('/news', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'news'
));
CroogoRouter::connect('/news/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'news', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/news/read/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_news',
	'type' => 'news', 'slug' => '^[a-zA-Z0-9_]*$'
));

CroogoRouter::connect('/corporatenews/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'corporatenews', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/corporatenews/read/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_news',
	'type' => 'corporatenews', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/corporatenews/*', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'corporatenews'
));
CroogoRouter::connect('/events/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'term_news',
	'type' => 'events', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/events/read/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view_news',
	'type' => 'events', 'slug' => '^[a-zA-Z0-9_]*$'
));
CroogoRouter::connect('/events/*', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'index_news',
	'type' => 'events'
));
*/

CroogoRouter::connect('/search/*', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'search'
));

CroogoRouter::connect('/', array(
	 'plugin' => 'front','controller' => 'site', 'action' => 'index'
));


//OK

// About

CroogoRouter::connect('/tentang-kami/*', array(
	 'plugin' => 'about','controller' => 'about', 'action' => 'display'
));



// Investor
CroogoRouter::connect('/informasi-investor/*', array(
	 'plugin' => 'investor','controller' => 'investor', 'action' => 'display'
));

// Company
CroogoRouter::connect('/tata-kelola-perusahaan/*', array(
	 'plugin' => 'company','controller' => 'company', 'action' => 'display'
));

// Product
CroogoRouter::connect('/produk-layanan/*', array(
	 'plugin' => 'product','controller' => 'product', 'action' => 'display'
));



// Basic
CroogoRouter::connect('/', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'promoted'
));

CroogoRouter::connect('/promoted/*', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'promoted'
));



// Content types
CroogoRouter::contentType('blog');
CroogoRouter::contentType('node');
if (Configure::read('Croogo.installed')) {
	CroogoRouter::routableContentTypes();
}

// Page
CroogoRouter::connect('/about', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view',
	'type' => 'page', 'slug' => 'about'
));
CroogoRouter::connect('/page/:slug', array(
	'plugin' => 'nodes', 'controller' => 'nodes', 'action' => 'view',
	'type' => 'page'
));
