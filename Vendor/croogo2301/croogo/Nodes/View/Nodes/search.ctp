<?php

$title_for_layout = __d('croogo', 'Search Results: %s', h($q));

?>
  <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">DEPAN</a></li>
              <li class="disabled">SEARCH</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
           <!--sidebar-->
          <div class="medium-4 large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
                <li><a href="<?php echo  $this->Html->url('/news', true) ?>">Berita Terbaru</a></li>
                <li><a href="<?php echo  $this->Html->url('/corporatenews', true) ?>">Siaran Pers</a></li>
                <li><a href="<?php echo  $this->Html->url('/events', true) ?>">Agenda Kegiatan Perusahaan</a></li>
            </ul>
            
          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1><?php echo $title_for_layout; ?></h1>
            <!--news list-->
			<?php
				if (count($nodes) == 0) {
					echo __d('croogo', 'No items found.');
				}
			?>
			<?php
				foreach ($nodes as $node):
					$this->Nodes->set($node);
			?>
            <div class="row mb-10">
				 <div class="medium-5 large-5 columns mb-5">
						<?php 
						$type = $node['Node']['type'];
						//print_r();
						if (isset($node['Multiattach'][0])){ ?>
							<?php
							
//echo $node['Multiattach'][0]['Multiattach']['filename'];
							if ($node['Multiattach'][0]['Multiattach']['mime']=="application/json"){ 
								
							?>
								<a href="<?php echo $this->Html->url('/'. $type .'/read/', true) .$this->Nodes->field('slug') ?>"><img src="<?php echo $node['Multiattach'][0]['Multiattach']['filename'] ?>" alt="<?php if (isset($node['Multiattach'][0]['Multiattach']['metaDisplay'])) echo $node['Multiattach'][0]['Multiattach']['metaDisplay']?>" width="400" /></a>
							<?php } else { ?>
									<?php $str = $this->Html->url('/', true) . str_replace("webroot","",$node['Multiattach'][0]['Multiattach']['real_filename']); 
										//echo $str;
									if (@getimagesize($str)){ ?>
										<a href="<?php echo  $this->Html->url('/'. $type.'/read/', true) .$this->Nodes->field('slug') ?>"><img src="<?php echo $str ?>" alt="<?php if (isset($node['Multiattach'][0]['Multiattach']['metaDisplay'])) echo $node['Multiattach'][0]['Multiattach']['metaDisplay']?>" width="400" /></a>
									<?php } ?>
							<?php }  ?>
						<?php } ?>
				</div>
                <div class="medium-7 large-7 columns">
					<?php // print_r($node) ?>
                    <h2><a href="<?php echo $this->Html->url('/'. $type .'/read/', true) .$this->Nodes->field('slug') ?>"><?php echo $this->Nodes->field('title') ?></a></h2>
                    <p class="mb-5"><?php echo $this->Nodes->field('excerpt'); ?></p>
                    <p class="link-more"><a href="<?php echo $this->Html->url('/'. $type .'/read/', true) .$this->Nodes->field('slug') ?>">Selengkapnya</a>&nbsp;&nbsp;<i class="fa fa-play"></i></a></p>
                </div>
            </div>
			<?php
				endforeach;
			?>
           
            <!--/news list-->
            <ul class="pagination text-center mt-20" role="navigation" aria-label="Pagination">
               <?php echo $this->Paginator->numbers(array('tag'=>'li')); ?>
            </ul>
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->