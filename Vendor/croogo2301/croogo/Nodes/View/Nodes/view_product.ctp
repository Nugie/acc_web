 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58508076c43393da"></script>
 <?php $this->Nodes->set($node); ?>
 <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->webroot; ?>">Home</a></li>
                <li class="disabled">PRODUK &amp; LAYANAN</li>
              <li class="disabled"><?php echo $this->Nodes->field('title'); ?></li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->

          <div class="medium-4 large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
			  <?php
			  //print_r($this->request->params['action']);
			  // print_r($this->request->params['named']['type']);
			  foreach ($menus as $menu) {
					//print_r($menu['Link']['link']);
					if (isset($menu['Link']['link']['slug'])){
			   ?>
					<li <?php if ($menu['Link']['link']['slug']==$this->request->params['named']['slug']) { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url($menu['Link']['link']) ?>"><?php echo $menu['Link']['title'] ?></a></li>
			   <?php } else {?>
						<li <?php if ($menu['Link']['link']['controller']==$this->request->params['controller'] && $menu['Link']['link']['type']==$this->request->params['named']['type']) { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url($menu['Link']['link']) ?>"><?php echo $menu['Link']['title'] ?></a></li>
			   <?php
					}
			   } ?>

            </ul>

          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1><?php echo $this->Nodes->field('title'); ?></h1>
			<div class="mb-20">
				  <!-- AddThis Button BEGIN -->
                  <div id="share-article" class="float-left">Bagikan :</div><div class="float-left"><div class="addthis_inline_share_toolbox"></div></div><div class="clearfix"></div>
              </div>
            <!--content detail-->
				<?php

					 //echo $this->Nodes->field('body');
					 echo $this->Nodes->info();
					 echo $this->Nodes->body();
					 echo $this->Nodes->moreInfo();
				?>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
