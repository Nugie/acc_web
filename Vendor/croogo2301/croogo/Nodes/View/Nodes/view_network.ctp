 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58508076c43393da"></script>
 <?php $this->Nodes->set($node); ?>
 <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->webroot; ?>">Home</a></li>
              <li class="disabled"><?php echo $this->Nodes->field('title'); ?></li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->

          <div class="medium-4 large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
				<li <?php if ($this->params['controller']=='network') {?>class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>network">Jaringan ACC</a></li>
                  <li <?php if ($this->here=='/contact') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>contact">Hubungi Kami</a></li>
                  <li <?php if ($this->here=='/faq') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>faq">FAQ</a></li>
                  <li <?php if ($this->here=='/kamus-pembiayaan') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>kamus-pembiayaan">Kamus Pembiayaan</a></li>
                  <li <?php if ($this->here=='/kebijakan-website') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>kebijakan-website">Kebijakan Website</a></li>
                  <li <?php if ($this->here=='/jalur-pelayanan-penyelesaian-pengaduan') { ?> class="active" <?php } ?>><a href="<?php echo $this->webroot; ?>jalur-pelayanan-penyelesaian-pengaduan">JALUR PELAYANAN & PENYELESAIAN PENGADUAN</a></li>	
            </ul>

          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1><?php echo $this->Nodes->field('title'); ?></h1>
			<div class="mb-20">
				  <!-- AddThis Button BEGIN -->
                  <div id="share-article" class="float-left">Bagikan :</div><div class="float-left"><div class="addthis_inline_share_toolbox"></div></div><div class="clearfix"></div>
              </div>
            <!--content detail-->
				<?php

					 //echo $this->Nodes->field('body');
					 echo $this->Nodes->info();
					 echo $this->Nodes->body();
					 echo $this->Nodes->moreInfo();
				?>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
