<?php

CroogoRouter::connect('/admin', array(
	 'plugin' => 'front','controller' => 'site', 'action' => 'index'
));
CroogoRouter::connect('/admin/secret/xx_login', array(
	 'admin'=>true,'plugin' => 'users','controller' => 'users', 'action' => 'login'
));

CroogoRouter::connect('/4cc_admin', Configure::read('Croogo.dashboardUrl'));
