<?php $title_for_layout = __d('croogo', 'Page not found'); ?>
<div class="row mb-20 mt-20" id="main-content">
		<div class="large-12 columns">
				<h1><?php echo __d('croogo', 'Halaman Tidak Ditemukan'); ?></h1>
					<p>Mohon maaf, artikel yang Anda cari tidak ditemukan. Silahkan klik pada menu diatas, atau gunakan kotak pencarian.</p>
		</div>
		<!--/main content-->
</div>
<?php Configure::write('debug', 0); ?>
