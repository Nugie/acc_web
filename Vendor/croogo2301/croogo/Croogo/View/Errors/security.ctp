<?php $title_for_layout = __d('croogo', 'Page not found'); ?>
<div class="row mb-20 mt-20" id="main-content">
		<div class="large-12 columns">
				<h1><?php echo __d('croogo', 'Halaman Tidak Ditemukan'); ?></h1>
					<p>Mohon maaf, artikel yang Anda cari tidak ditemukan. Silahkan klik pada menu diatas, atau gunakan kotak pencarian.</p>
					<?php if (Configure::read('debug') > 0): ?>
					<p class="notice">
						<?php echo __d('croogo', 'Request blackholed due to "%s" violation.', $type); ?>
					</p>
					<?php endif; ?>
		</div>
</div>


<h2><?php echo __d('croogo', 'Security Error'); ?></h2>
<p class="error">
	<?php echo __d('croogo', 'The requested address was not found on this server.'); ?>
</p>
<?php if (Configure::read('debug') > 0): ?>
<p class="notice">
	<?php echo __d('croogo', 'Request blackholed due to "%s" violation.', $type); ?>
</p>
<?php endif; ?>
<?php Configure::write('debug', 0); ?>
