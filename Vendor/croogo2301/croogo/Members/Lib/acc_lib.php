<?php

$local_dir = dirname(__FILE__);
require_once($local_dir . DIRECTORY_SEPARATOR . "php_aes.php");

class AccLib
{
	var $str_hash;
	var $cipher_key;
	var $cipher_key_hash;
	
	var $aes;
	
	function AccLib()
	{
		// gjdfgjkdfjkgdf
		// -------------------------------------------------------------------------------------------------------------------------------------------------------
		//  Calculate String Hash Seed
		// -------------------------------------------------------------------------------------------------------------------------------------------------------
		$tmp = sha1(
			$this->combineString(
				date("Y-m-d") . date("F dS Y, l"),
				"Astra Credit Companies Astra Credit Companies Astra Credit Companies Astra Credit Companies Astra Credit Companies Astra Credit Companies"
			)
		);
		$tmp .= $tmp;
		$this->str_hash = md5(
			$this->combineString(
				date("Y-m-d") . date("F dS Y, l"),
				$tmp
			)
		);
		$this->str_hash .= sha1(
			$this->combineString(
				date("Y-m-d") . date("F dS Y, l"),
				$tmp
			)
		);
		
		// -------------------------------------------------------------------------------------------------------------------------------------------------------
		//  Calculate Cipher Key Hash Seed
		// -------------------------------------------------------------------------------------------------------------------------------------------------------
		$tmp = sha1(
			$this->combineString(
				date("Y-m-d") . date("F dS Y, l"),
				"Astra Credit Companies Astra Credit Companies Astra Credit Companies Astra Credit Companies Astra Credit Companies Astra Credit Companies"
			)
		);
		$tmp .= $tmp;
		$this->cipher_key_hash = md5(
			$this->combineString(
				date("F dS Y, l") . date("Y-m-d"),
				$tmp
			)
		);
		
		// -------------------------------------------------------------------------------------------------------------------------------------------------------
		//  Calculate Cipher Key
		// -------------------------------------------------------------------------------------------------------------------------------------------------------
		$this->cipher_key = md5(
			$this->combineString(
				$this->getSession(),
				$this->cipher_key_hash
			)
		);
		$this->aes = new AES($this->cipher_key);
	}
	
	function getSession()
	{
		$cur_date = date("FzSdmYlDt");
		
		return md5($this->combineString($cur_date));
	}
	
	function combineString($str, $hash = null)
	{
		if (!$hash)
			$hash = $this->str_hash;
		
		$result = "";
		
		for ($i=0,$c=strlen($str); $i<$c; $i++)
			$result .= $hash{$i} . $str{$i};
		
		$c--;
		$result .= $hash{$c};
		
		return $result;
	}
	
	function validateSession($session_id)
	{
		return ($session_id == $this->getSession()) ? true : false;
	}
	
	function encrypt($data)
	{
		return base64_encode(
			$this->aes->encrypt($data)
		);
	}
	
	function decrypt($data)
	{
			return $this->aes->decrypt(
				base64_decode($data)
			);
	}
	
	function generateXML($data, $model)
	{
		$result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		$result .= "<dataset>\n";
		$result .= "<modelName>{$model}</modelName>\n";
		$result .= "<recordCount>" . count($data) . "</recordCount>\n";
		
		foreach ($data as $tmp)
		{
			$result .= "<row>\n";
			foreach ($tmp[$model] as $field => $value)
				$result .= "<{$field}><![CDATA[{$value}]]></{$field}>\n";
			$result .= "</row>\n";
		}
		
		$result .= "</dataset>";
		
		return $result;
	}
	
	function generateErrorXML($error_message)
	{
		$result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		$result .= "<dataset>\n";
		$result .= "<error><![CDATA[{$error_message}]]></error>\n";
		$result .= "</dataset>";
		
		echo $result;
		die();
	}
}

?>