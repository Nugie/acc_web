<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Members'), h($member['Member']['member_id']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Members'), array('action' => 'index'));

if (isset($member['Member']['member_id'])):
	$this->Html->addCrumb($member['Member']['member_id'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Member'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Member'), array(
		'action' => 'edit',
		$member['Member']['member_id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Member'), array(
		'action' => 'delete', $member['Member']['member_id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $member['Member']['member_id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Members'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Member'), array('action' => 'add'), array('button' => 'success'));
$this->end();

$this->append('main');
?>
<div class="members view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Member Id'); ?></dt>
		<dd>
			<?php echo h($member['Member']['member_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Username'); ?></dt>
		<dd>
			<?php echo h($member['Member']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Password'); ?></dt>
		<dd>
			<?php echo h($member['Member']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Member Status'); ?></dt>
		<dd>
			<?php echo h($member['Member']['member_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Cd Customer'); ?></dt>
		<dd>
			<?php echo h($member['Member']['cd_customer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Customer Type'); ?></dt>
		<dd>
			<?php echo h($member['Member']['customer_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'No Ktp'); ?></dt>
		<dd>
			<?php echo h($member['Member']['no_ktp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Nama'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nama']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Alamat'); ?></dt>
		<dd>
			<?php echo h($member['Member']['alamat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Rt'); ?></dt>
		<dd>
			<?php echo h($member['Member']['rt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Rw'); ?></dt>
		<dd>
			<?php echo h($member['Member']['rw']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Kelurahan'); ?></dt>
		<dd>
			<?php echo h($member['Member']['kelurahan']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Kecamatan'); ?></dt>
		<dd>
			<?php echo h($member['Member']['kecamatan']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'City'); ?></dt>
		<dd>
			<?php echo h($member['Member']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Postcode'); ?></dt>
		<dd>
			<?php echo h($member['Member']['postcode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Telephone'); ?></dt>
		<dd>
			<?php echo h($member['Member']['telephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Fax'); ?></dt>
		<dd>
			<?php echo h($member['Member']['fax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Npwp'); ?></dt>
		<dd>
			<?php echo h($member['Member']['npwp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Mtr Maiden'); ?></dt>
		<dd>
			<?php echo h($member['Member']['mtr_maiden']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Placeofbirth'); ?></dt>
		<dd>
			<?php echo h($member['Member']['placeofbirth']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Dateofbirth'); ?></dt>
		<dd>
			<?php echo h($member['Member']['dateofbirth']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Religion'); ?></dt>
		<dd>
			<?php echo h($member['Member']['religion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Nationality'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nationality']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Marital'); ?></dt>
		<dd>
			<?php echo h($member['Member']['marital']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Jml Tanggungan'); ?></dt>
		<dd>
			<?php echo h($member['Member']['jml_tanggungan']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Education'); ?></dt>
		<dd>
			<?php echo h($member['Member']['education']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Job'); ?></dt>
		<dd>
			<?php echo h($member['Member']['job']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Residence'); ?></dt>
		<dd>
			<?php echo h($member['Member']['residence']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Email'); ?></dt>
		<dd>
			<?php echo h($member['Member']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Verified'); ?></dt>
		<dd>
			<?php echo h($member['Member']['verified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Verification Code'); ?></dt>
		<dd>
			<?php echo h($member['Member']['verification_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'No Aggr'); ?></dt>
		<dd>
			<?php echo h($member['Member']['no_aggr']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Nopol'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nopol']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($member['Member']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($member['Member']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>