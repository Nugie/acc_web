<?php
$this->viewVars['title_for_layout'] = __d('testimonial', 'Testimonials');
$this->extend('/Common/admin_edit');

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('testimonial', 'Testimonials'), array('action' => 'index'));

if ($this->action == 'admin_edit') {
	$this->Html->addCrumb($this->request->data['Testimonial']['name'], '/' . $this->request->url);
	$this->viewVars['title_for_layout'] = __d('testimonial', 'Testimonials') . ': ' . $this->request->data['Testimonial']['name'];
} else {
	$this->Html->addCrumb(__d('croogo', 'Add'), '/' . $this->request->url);
}

$this->append('form-start', $this->Form->create('Testimonial'));

$this->append('tab-heading');
	echo $this->Croogo->adminTab(__d('testimonial', 'Testimonial'), '#testimonial');
	echo $this->Croogo->adminTabs();
$this->end();

$this->append('tab-content');

	echo $this->Html->tabStart('testimonial');

		echo $this->Form->input('id');

		echo $this->Form->input('email_address', array(
			'label' =>  __d('testimonial', 'Email Address'),
		));
		echo $this->Form->input('name', array(
			'label' =>  __d('testimonial', 'Name'),
		));
		echo $this->Form->input('content', array(
			'label' =>  __d('testimonial', 'Content'),
		));
		echo $this->Form->input('published', array(
			'label' =>  __d('testimonial', 'Published'),
		));

	echo $this->Html->tabEnd();

	echo $this->Croogo->adminTabs();

$this->end();

$this->append('panels');
	echo $this->Html->beginBox(__d('croogo', 'Publishing')) .
		$this->Form->button(__d('croogo', 'Apply'), array('name' => 'apply')) .
		$this->Form->button(__d('croogo', 'Save'), array('button' => 'primary')) .
		$this->Form->button(__d('croogo', 'Save & New'), array('button' => 'success', 'name' => 'save_and_new')) .
		$this->Html->link(__d('croogo', 'Cancel'), array('action' => 'index'), array('button' => 'danger'));
	echo $this->Html->endBox();

	echo $this->Croogo->adminBoxes();
$this->end();

$this->append('form-end', $this->Form->end());
