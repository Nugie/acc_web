<?php

$this->extend('/Common/admin_view');
$this->viewVars['title_for_layout'] = sprintf('%s: %s', __d('croogo', 'Testimonials'), h($testimonial['Testimonial']['name']));

$this->Html
	->addCrumb('', '/admin', array('icon' => 'home'))
	->addCrumb(__d('croogo', 'Testimonials'), array('action' => 'index'));

if (isset($testimonial['Testimonial']['name'])):
	$this->Html->addCrumb($testimonial['Testimonial']['name'], '/' . $this->request->url);
endif;

$this->set('title', __d('croogo', 'Testimonial'));

$this->append('actions');
	echo $this->Croogo->adminAction(__d('croogo', 'Edit Testimonial'), array(
		'action' => 'edit',
		$testimonial['Testimonial']['id'],
	), array(
		'button' => 'default',
	));
	echo $this->Croogo->adminAction(__d('croogo', 'Delete Testimonial'), array(
		'action' => 'delete', $testimonial['Testimonial']['id'],
	), array(
		'method' => 'post',
		'button' => 'danger',
		'escapeTitle' => true,
		'escape' => false,
	),
	__d('croogo', 'Are you sure you want to delete # %s?', $testimonial['Testimonial']['id'])
	);
	echo $this->Croogo->adminAction(__d('croogo', 'List Testimonials'), array('action' => 'index'));
	echo $this->Croogo->adminAction(__d('croogo', 'New Testimonial'), array('action' => 'add'), array('button' => 'success'));
$this->end();

$this->append('main');
?>
<div class="testimonials view">
	<dl class="inline">
		<dt><?php echo __d('croogo', 'Id'); ?></dt>
		<dd>
			<?php echo h($testimonial['Testimonial']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Email Address'); ?></dt>
		<dd>
			<?php echo h($testimonial['Testimonial']['email_address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Name'); ?></dt>
		<dd>
			<?php echo h($testimonial['Testimonial']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Content'); ?></dt>
		<dd>
			<?php echo h($testimonial['Testimonial']['content']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Published'); ?></dt>
		<dd>
			<?php echo h($testimonial['Testimonial']['published']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Created'); ?></dt>
		<dd>
			<?php echo $this->Time->format($testimonial['Testimonial']['created'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
		<dt><?php echo __d('croogo', 'Modified'); ?></dt>
		<dd>
			<?php echo $this->Time->format($testimonial['Testimonial']['modified'], '%Y-%m-%d %H:%M:%S', __d('croogo', 'Invalid datetime')); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php $this->end(); ?>