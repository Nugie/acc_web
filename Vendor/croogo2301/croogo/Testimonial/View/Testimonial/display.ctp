 <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">DEPAN</a></li>
              <li class="disabled">Testimoni</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="medium-4 large-4 columns">
           <ul id="sidebar-menu" class="mb-20">
			  <?php 
			  //print_r($this->request->params['action']);
			  // print_r($this->request->params['named']['type']);
			  if (!isset($this->request->params['named']['type'])){
				$this->request->params['named']['type'] = "";
			  }
			  foreach ($menus as $menu) {
					//print_r($menu['Link']['link']);
					if (!isset($menu['Link']['link']['type'])){
						$menu['Link']['link']['type'] =  "";
					}
					if (isset($menu['Link']['link']['slug'])){
			   ?>		
					<li <?php if ($menu['Link']['link']['slug']==$this->request->params['named']['slug']) { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url($menu['Link']['link']) ?>"><?php echo $menu['Link']['title'] ?></a></li>
			   <?php } else {?>
						<li <?php if ($menu['Link']['link']['controller']==$this->request->params['controller'] && $menu['Link']['link']['type']==$this->request->params['named']['type']) { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url($menu['Link']['link']) ?>"><?php echo $menu['Link']['title'] ?></a></li>
			   <?php 		
					}
			   } ?>		
			 
            </ul>

          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="medium-8 large-8 columns" id="main-content">
            <h1 class="float-left">Testimoni</h1>
            <div class="float-right"><a href="<?php echo $this->Html->url('/testimonials/add', true);?>" class="button-general" title="add testimony">Add Testimony</a></div>
            <div class="clearfix"></div>
            <!--content detail-->
            <div id="content-disclaimer" class="mb-20">
              Kami ucapkan terima kasih kepada semua pelanggan yang telah meluangkan waktu untuk memberikan testimonial kepada kami. Pujian dari anda adalah penghargaan tak ternilai bagi kami untuk terus berusaha memberikan yang terbaik.
            </div>
			<?php 
			//print_r($data);
			foreach($data as $row) { ?>
            <div class="testimonial-content">
              <div class="blue-title-noborder"><?php echo $row['Testimonial']['name'] ?> | <?php echo $row['Testimonial']['created'] ?></div>
              <p><?php echo substr($row['Testimonial']['email_address'],0,strpos($row['Testimonial']['email_address'], '@')-3)?>***@**<?php echo substr($row['Testimonial']['email_address'],strpos($row['Testimonial']['email_address'], '.',strpos($row['Testimonial']['email_address'], '@')),strlen($row['Testimonial']['email_address'])-1) ?></p>
              <p><?php echo $this->Text->stripLinks($row['Testimonial']['content']) ?> </p>
            </div>
			<?php } ?>
           
            <!--/content detail-->
			 <!--/news list-->
            <ul class="pagination text-center mt-20" role="navigation" aria-label="Pagination">
              <?php echo $this->Paginator->numbers(array('tag'=>'li')); ?>
			  
            </ul>
			
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->