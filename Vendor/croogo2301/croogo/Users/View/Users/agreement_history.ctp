<!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">DEPAN</a></li>
              <li class="disabled">AGREEMENT HISTORY</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
		  <?php //echo $this->here?>
          <div class="large-4 columns">
                <ul id="sidebar-menu" class="mb-20">
                 <li <?php if ($this->here=='/change_password') {?>class="active" <?php } ?>><a href="<?php echo $this->Html->url('/change_password', true);?>">Change Password</a></li>
                  <li <?php if ($this->here=='/profile') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/profile', true);?>">Update Profile</a></li>
                   <?php if ($loguser['member_status']=='Customer') { ?>
				  <li <?php if ($this->here=='/agreement_history') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/agreement_history', true);?>">Agreement History</a></li>
				  <?php } ?>
                  <li <?php if ($this->here=='/logout') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/logout', true);?>">Logout</a></li>
                  
                </ul>
          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="large-8 columns" id="main-content">
            <h1>AGREEMENT HISTORY</h1>
            <!--content detail-->
				<?php if (isset($contracts["rowsData"]) && (count($contracts["rowsData"]) > 0)) : ?>
				<div class="sub-header-orange4" style="margin-bottom: 8px;">Your agreement history</div>
				<?php
				$idx = 0;
				?>
				<table width="99%" border="0" cellspacing="0" cellpadding="5" class="member_agreement">
				<tbody>
				<tr>
					<th align="left">No. Perjanjian</th>
					<th align="left">Tanggal</th>
					<th align="left">No. Kendaraan</th>
					<th align="left">Tipe Kendaraan</th>
					<th align="center">&nbsp;</th>
				</tr>

				<?php foreach ($contracts["rowsData"] as $data) : ?>
				<?php
				$class = (($idx % 2) == 0) ? " class=\"odd\"" : " class=\"event\"";
				$idx++;
				?>
				<tr>
					<td<?php echo $class; ?> align="left"><?php echo $data["no_aggr"]; ?></td>
					<td<?php echo $class; ?> align="left"><?php echo get_simple_indonesian_date($data["aggr_date"]); ?></td>
					<td<?php echo $class; ?> align="left"><?php echo $data["no_car_police"]; ?></td>
					<td<?php echo $class; ?> align="left"><?php echo $data["desc_vehicle_brand"] . " " . $data["desc_vehicle_type"]; ?></td>
					<td<?php echo $class; ?> align="center"><a href="/members/agreement_detail/<?php echo $data["no_aggr"]; ?>" title="View Detail">View detail</a></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
				</table>
				<?php elseif (isset($contracts["error"]) && !empty($contracts["error"])) : ?>
				<div class="ui-state-error ui-corner-all" style="margin-bottom: 16px; padding: 8px;">
				<b>Fatal Error :</b>
				<br /><br />
				<?php echo $contracts["error"]; ?>
				</div>
				<?php else : ?>
				<div class="ui-state-error ui-corner-all" style="margin-bottom: 16px; padding: 8px;">
				<b>Error :</b>
				<br /><br />
				System can not find your agreement history.
				</div>
				<?php endif; ?>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->