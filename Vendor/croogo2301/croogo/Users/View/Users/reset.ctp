<script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
  <!--content area-->
      <div class="mb-20 mt-30 text-center" id="main-content">
            <h1><?php echo $title_for_layout; ?></h1>
			<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'reset', $username, $key)));?>
              <div class="row">
				<div class="large-3 small-3 columns large-offset-2">
                  <label for="username" class="text-right middle">New password</label>
                </div>
                <div class="large-3 small-9 columns end">
                 <?php echo $this->Form->input('password', array('label' =>false,'type'=>'password')); ?>
                </div>
              </div>
			   <div class="row">
				<div class="large-3 small-3 columns large-offset-2">
                  <label for="username" class="text-right middle">Verify Password</label>
                </div>
                <div class="large-3 small-9 columns end">
                <?php echo $this->Form->input('verify_password', array('label' =>false,'type'=>'password')); ?>
                </div>
              </div>
            
			  <div class="row">
                <div class="large-3 small-3 columns large-offset-2">
					&nbsp;
                </div>
                <div class="large-3 small-9 columns end">
					<div class="g-recaptcha" data-sitekey="<?php echo Configure::read('Service.recaptcha_public_key') ?>"></div>
                </div>
              </div>
              <div class="row">
				<div class="large-3 small-3 columns large-offset-2">
                  &nbsp;
                </div>
                <div class="large-4 small-9 columns end text-left">
                   <?php echo $this->Form->button('Reset',['class'=>'button float-left']); ?>
                </div>
				
              </div>
             
            <?php echo $this->Form->end();?>
      </div>
      <!--/content area-->

