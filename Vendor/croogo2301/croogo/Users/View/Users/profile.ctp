<script src='https://www.google.com/recaptcha/api.js?hl=id'></script>
<!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="<?php echo $this->Html->url('/', true);?>">DEPAN</a></li>
              <li class="disabled">UPDATE PROFILE</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
		  <?php //echo $this->here?>
          <div class="large-4 columns">
                <ul id="sidebar-menu" class="mb-20">
                 <li <?php if ($this->here=='/change_password') {?>class="active" <?php } ?>><a href="<?php echo $this->Html->url('/change_password', true);?>">Change Password</a></li>
                  <li <?php if ($this->here=='/profile') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/profile', true);?>">Update Profile</a></li>
                  <?php if ($loguser['member_status']=='Customer') { ?>
				  <li <?php if ($this->here=='/agreement_history') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/agreement_history', true);?>">Agreement History</a></li>
				  <?php } ?>
                  <li <?php if ($this->here=='/logout') { ?> class="active" <?php } ?>><a href="<?php echo $this->Html->url('/logout', true);?>">Logout</a></li>

                </ul>
          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="large-8 columns" id="main-content" style='text-align:left;'>
            <h1>UPDATE PROFILE</h1>

            <?php
				echo $this->Form->create(
					"User",
					array(
						"name" => "member-register-form",
						"id" => "member-register-form",
						"method" => "post"
					)
				);
				echo $this->Form->input(
						"member_status",
						array(
							"type" => "hidden",
							"id" => "member_status",
							"value" => "Customer",
						)
					);
			?>
              <div class="row">
                <div class="large-3 small-3 columns">
                  <label for="username" class="text-right middle">Username</label>
                </div>
                <div class="large-4 small-9 columns end">

				  <?php
					echo $this->Form->input(
						"username",
						array(
							"type" => "text",
							"id" => "username",
							"size" => "40",
							"div" => false,
							"label" => false,
							"maxlength" => 16,
							"required"=>true,
							"value"=>$user['User']['username'],
							"disabled"=>true
						)
					);
				?>
                </div>
              </div>

              <div class="row">
                <div class="large-3 small-3 columns">
                  <label for="username" class="text-right middle">Email</label>
                </div>
                <div class="large-4 small-9 columns end">
					<?php
					echo $this->Form->input(
						"email",
						array(
							"type" => "email",
							"id" => "email",
							"size" => "40",
							"div" => false,
							"label" => false,
							"maxlength" => 50,
							"required"=>true,
							"value"=>$user['User']['email'],
							"disabled"=>true
						)
					);
				?>
                </div>
              </div>

              <div class="row">
                <div class="large-3 small-3 columns">
                  <label for="username" class="text-right middle">Nomor Pelanggan</label>
                </div>
                <div class="large-4 small-9 columns end">
                  <?php
					echo $this->Form->input(
						"cd_customer",
						array(
							"type" => "text",
							"id" => "cd_customer",
							"size" => "40",
							"class" => "text-field ui-widget-content ui-corner-all",
							"div" => false,
							"label" => false,
							"maxlength" => 16,
							"required"=>true,
							"value"=>$user['User']['cd_customer']
						)
					);
					?>
                  <p class="help-text text-left" id="passwordHelpText">Masukkan 12 digit nomor pelanggan yang tertera di kanan atas Surat Perjanjian Kontrak anda. Klik <a href="#">disini</a> untuk melihat contoh nomor pelanggan pada surat perjanjian kontrak.</p>
                </div>
              </div>
              <div class="row">
                <div class="large-3 small-3 columns">
                  <label for="customertype" class="text-right middle">Tipe Pelanggan</label>
                </div>
                <div class="large-4 small-9 columns end">
                 <?php
					echo $this->Form->input(
						"customer_type",
						array(
							"id" => "customer_type",
							"div" => false,
							"label" => false,
							"required"=>true,
							"options" => array("P" => "Personal", "C" => "Company"),
							"value"=>$user['User']['customer_type']
						)
					);
				?>
                </div>
              </div>
			   <div class="row">
                <div class="large-3 small-3 columns">
                  <label for="customertype" class="text-right middle">Tempat Lahir</label>
                </div>
                <div class="large-4 small-9 columns end">
                 <?php
						echo $this->Form->input(
							"placeofbirth",
							array(
								"type" => "text",
								"id" => "placeofbirth",
								"size" => "40",
								"class" => "text-field ui-widget-content ui-corner-all",
								"div" => false,
								"label" => false,
								"maxlength" => 20,
								"required"=>true,
								"value"=>$user['User']['placeofbirth']
							)
						);
					?>
                </div>
              </div>
              <div class="row">
                <div class="large-3 small-3 columns">
                  <label for="customertype" class="text-right middle">Tanggal Lahir</label>
                </div>
                <div class="large-5 small-2 columns end">
					<?php
						echo $this->Form->input(
							"dateofbirth",
							array(
								"type" => "date",
								"id" => "dateofbirth",
								//"size" => "40",
								//"class" => "text-field ui-widget-content ui-corner-all",
								"div" => false,
								"label" => false,
								"required"=>true,
								"dateFormat" => "DMY",
								"minYear" => date("Y") - 80,
								"maxYear" => date("Y") - 16,
								"value"=>$user['User']['dateofbirth']
							)
						);
					?>

                </div>
              </div>
			   <div class="row">
                <div class="large-3 small-3 columns">
                  <label><strong>&nbsp;</strong><br /></label>
				  <label for="captcha">&nbsp;</label>
                </div>
                <div class="large-3 small-9 columns end">
					<div class="g-recaptcha" data-sitekey="<?php echo Configure::read('Service.recaptcha_public_key') ?>"></div>
                </div>
              </div>
              <div class="row">
                <div class="large-3 small-3 columns">
                  <label for="password" class="text-right middle">&nbsp;</label>
                </div>
                <div class="large-3 small-9 columns end">
								  <?php
						echo $this->Form->submit(
							"Submit",
							array(
								"id" => "form-submit",
								"class" => "button",
								"div" => false
							)
						);
					?>
                </div>
              </div>

             <?php echo $this->Form->end(); ?>
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
