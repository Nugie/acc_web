  <!--content area-->
      <!--breadcrumb-->
      <div class="row columns mt-20">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
              <li><a href="#">DEPAN</a></li>
              <li><a href="#">HUBUNGAN INVESTOR</a></li>
              <li class="disabled">Informasi RUPS</li>
            </ul>
          </nav>
      </div>
      <!--/breadcrumb-->
      <div class="row mb-20">
          <!--sidebar-->
          <div class="large-4 columns">
            <ul id="sidebar-menu" class="mb-20">
              <li class="active"><a href="investor-rups.html">Informasi RUPS</a></li>
              <li><a href="investor-laporan.html">Laporan Tahunan</a></li>
              <li><a href="investor-info.html">Informasi Keuangan</a></li>
              <li><a href="investor-surathutang.html">Informasi Surat Hutang/Obligasi</a></li>
              <li><a href="investor-lembaga.html">Lembaga dan Profesi Penunjang</a></li>
              <li><a href="investor-keterbukaan.html">Keterbukaan Informasi</a></li>
            </ul>

          </div>
          <!--/sidebar-->
          <!--main content-->
          <div class="large-8 columns" id="main-content">
            <h1>Informasi RUPS</h1>
            <!--content detail-->
            <p>Content Here</p>
            <!--/content detail-->
          </div>
          <!--/main content-->
      </div>
      <!--/content area-->
	  
   