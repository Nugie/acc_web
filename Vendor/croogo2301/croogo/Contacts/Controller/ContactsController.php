<?php

App::uses('CakeEmail', 'Network/Email');
App::uses('ContactsAppController', 'Contacts.Controller');

/**
 * Contacts Controller
 *
 * @category Controller
 * @package  Croogo.Contacts.Controller
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class ContactsController extends ContactsAppController {

/**
 * Controller name
 *
 * @var string
 * @access public
 */
	public $name = 'Contacts';

/**
 * Components
 *
 * @var array
 * @access public
 */
	public $components = array(
		'Croogo.Akismet',
		'Croogo.Recaptcha',
	);

/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
	public $uses = array('Contacts.Contact', 'Contacts.Message');

/**
 * Admin index
 *
 * @return void
 * @access public
 */
	public function admin_index() {
		$this->set('title_for_layout', __d('croogo', 'Hubungi Kami'));

		$this->Contact->recursive = 0;
		$this->paginate['Contact']['order'] = 'Contact.title ASC';
		$this->set('contacts', $this->paginate());
		$this->set('displayFields', $this->Contact->displayFields());
	}

/**
 * Admin add
 *
 * @return void
 * @access public
 */
	public function admin_add() {
		$this->set('title_for_layout', __d('croogo', 'Add Contact'));

		if (!empty($this->request->data)) {
			$this->Contact->create();
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', 'The Contact has been saved'), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $this->Contact->id));
			} else {
				$this->Session->setFlash(__d('croogo', 'The Contact could not be saved. Please, try again.'), 'flash', array('class' => 'error'));
			}
		}
	}

/**
 * Admin edit
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_edit($id = null) {
		$this->set('title_for_layout', __d('croogo', 'Edit Contact'));

		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid Contact'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}
		if (!empty($this->request->data)) {
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__d('croogo', 'The Contact has been saved'), 'flash', array('class' => 'success'));
				$this->Croogo->redirect(array('action' => 'edit', $this->Contact->id));
			} else {
				$this->Session->setFlash(__d('croogo', 'The Contact could not be saved. Please, try again.'), 'flash', array('class' => 'error'));
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Contact->read(null, $id);
		}
	}

/**
 * Admin delete
 *
 * @param integer $id
 * @return void
 * @access public
 */
	public function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__d('croogo', 'Invalid id for Contact'), 'flash', array('class' => 'error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->Contact->delete($id)) {
			$this->Session->setFlash(__d('croogo', 'Contact deleted'), 'flash', array('class' => 'success'));
			return $this->redirect(array('action' => 'index'));
		}
	}

/**
 * View
 *
 * @param string $alias
 * @return void
 * @access public
 * @throws NotFoundException
 */
	public function view($alias = null) {
	    //echo "BBB";
		if ($this->request->data){

			// $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". Configure::read('Service.recaptcha_private_key') ."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

			// if ($response['success'] == true) {
				//$this->data = Sanitize::clean($this->data);
				$this->Contact->set($this->data);

				if ( $this->Contact->validates() )
				{
					$arrDataContact = $this->Contact->data['Contact'];
                                        unset($arrDataContact['NAME']);
                                        $result = $this->_submitToCCRP($arrDataContact);
                                        if($result == 'Berhasil Tersimpan'){
                                            //$this->set("form_info", "Terima kasih, pesan anda telah terkirim.<br />Kami akan memberikan respon kepada anda secepatnya.");
                                            $this->Session->setFlash("Terima kasih, pesan anda telah terkirim.<br />Kami akan memberikan respon kepada anda secepatnya.", 'flash', array('class' => 'warning callout'));
											$this->data=null;
                                        }else{
                                           // $this->set("form_info", $result."<br/> Silahkan hubungi kami melalui media lain.");
											$this->Session->setFlash($result."<br/> Silahkan hubungi kami melalui media lain.", 'flash', array('class' => 'warning callout'));
                                        }

				}
				else
				{
					$form_error = array_values($this->Contact->invalidFields());
					//$this->set("form_error", $form_error[0]);
					//print_r($form_error);
					$this->Session->setFlash($form_error[0][0], 'flash', array('class' => 'warning callout'));
				}

			// } else {
				// $this->Session->setFlash("Silahkan cek kembali kode keamanan Anda!", 'flash', array('class' => 'warning callout'));
				// //$this->set("form_error","Silahkan cek kembali kode keamanan Anda!");

			// }
		}

		$this->set(
			"recipient_list",
			array('complaint'=>'Keluhan','saran'=>'Saran','pertanyaan'=>'Pertanyaan')
		);

                $this->set(
			"identity_list",
			array('Pelanggan'=>'Pelanggan','Non Pelanggan'=>'Non Pelanggan')
		);

		$this->set(
				'relation',
				$this->_retrieveFromCCRP('/?r=service/getRelation')
		);

		$this->set(
				'complaint_category_id',
				$this->_retrieveFromCCRP('/?r=service/getComplaintType')
		);

		$this->set(
				'subject',
				 $this->_retrieveFromCCRP('/?r=service/getSubject')
		);


		$this->set(
				'cd_sp_handler',
			     $this->_retrieveFromCCRP('/?r=service/getSP')
		);

		$this->set(
				'jenis_pelanggan',
				array('C'=>'Perusahaan','P'=>'Personal')
		);

		$this->set('title_for_layout', 'Contact');
	}

/**
 * Validation
 *
 * @param boolean $continue
 * @param array $contact
 * @return boolean
 * @access protected
 */
	protected function _validation($continue, $contact) {
		if ($this->Contact->Message->set($this->request->data) &&
			$this->Contact->Message->validates() &&
			$continue === true) {
			if ($contact['Contact']['message_archive'] &&
				!$this->Contact->Message->save($this->request->data['Message'])) {
				$continue = false;
			}
		} else {
			$continue = false;
		}

		return $continue;
	}

/**
 * Spam protection
 *
 * @param boolean $continue
 * @param array $contact
 * @return boolean
 * @access protected
 */
	protected function _spam_protection($continue, $contact) {
		if (!empty($this->request->data) &&
			$contact['Contact']['message_spam_protection'] &&
			$continue === true) {
			$this->Akismet->setCommentAuthor($this->request->data['Message']['name']);
			$this->Akismet->setCommentAuthorEmail($this->request->data['Message']['email']);
			$this->Akismet->setCommentContent($this->request->data['Message']['body']);
			if ($this->Akismet->isCommentSpam()) {
				$continue = false;
				$this->Session->setFlash(__d('croogo', 'Sorry, the message appears to be spam.'), 'flash', array('class' => 'error'));
			}
		}

		return $continue;
	}

/**
 * Captcha
 *
 * @param boolean $continue
 * @param array $contact
 * @return boolean
 * @access protected
 */
	protected function _captcha($continue, $contact) {
		if (!empty($this->request->data) &&
			$contact['Contact']['message_captcha'] &&
			$continue === true &&
			!$this->Recaptcha->valid($this->request)) {
			$continue = false;
			$this->Session->setFlash(__d('croogo', 'Invalid captcha entry'), 'flash', array('class' => 'error'));
		}

		return $continue;
	}

/**
 * Send Email
 *
 * @param boolean $continue
 * @param array $contact
 * @return boolean
 * @access protected
 */
	protected function _send_email($continue, $contact) {
		$email = new CakeEmail();
		if (!$contact['Contact']['message_notify'] || $continue !== true) {
			return $continue;
		}

		$siteTitle = Configure::read('Site.title');
		try {
			$email->from($this->request->data['Message']['email'])
				->to($contact['Contact']['email'])
				->subject(__d('croogo', '[%s] %s', $siteTitle, $contact['Contact']['title']))
				->template('Contacts.contact')
				->viewVars(array(
					'contact' => $contact,
					'message' => $this->request->data,
				));
			if ($this->theme) {
				$email->theme($this->theme);
			}

			if (!$email->send()) {
				$continue = false;
			}
		} catch (SocketException $e) {
			$this->log(sprintf('Error sending contact notification: %s', $e->getMessage()));
			$continue = false;
		}

		return $continue;
	}

	public function customer()
	{
		$this->layout = "blank";
		//echo "AAA";
		$id = $_GET['id'];
                $t = $_GET['t'];
		$url = Configure::read("CCRP.url")."/?r=service/customer&no=$id&t=$t";
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, true);

		$customer = curl_exec($curl);
		curl_close($curl);
		echo $customer;

		die();
	}

	function _retrieveFromCCRP($url)
	{
		//$url = Configure::read("CCRP.url").$url;
		$url = "http://172.16.5.36".$url;
		$curl = curl_init($url);
                curl_setopt($curl, CURLOPT_PORT, 80);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, true);
                curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,10);
                curl_setopt($curl, CURLOPT_TIMEOUT, 10);


		$json_response = curl_exec($curl);
		curl_close($curl);

		//mencegah error apabila satu hal dan lain hal curl tidak muncul data
		if(empty($json_response) || $json_response==false){
			$json_response = '[]';
		}

		return json_decode($json_response, true);
	}

        function _submitToCCRP($arrDataContact)
        {
            if($arrDataContact['ATTACHMENT']['tmp_name']){
                $attachmentRaw = file_get_contents($arrDataContact['ATTACHMENT']['tmp_name']);
                $attachmentBase64 = base64_encode($attachmentRaw);
                $arrDataContact['ATTACHMENT']['base64'] = $attachmentBase64 ;
                $ext = explode('.',$arrDataContact['ATTACHMENT']['name']);
                $random = md5(date('Y-m-d H:i:s'));
                $arrDataContact['ATTACHMENT']['filename'] = $random.end($ext);
            }

            $arrDataContact = array('ccrp'=>$arrDataContact);
            $content = http_build_query($arrDataContact);

            //$url = Configure::read("CCRP.url")."/?r=service/createComplaint";
			$url = "http://172.16.5.36/?r=service/createComplaint";
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, array('ccrp'=>$content));
            curl_setopt($curl, CURLOPT_VERBOSE, true);

            $response = curl_exec($curl);

            //$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);

            return $response;
        }
		public function beforeFilter() {

			//$this->Security->csrfCheck = false;
			//$this->Security->validatePost = false;
			$this->Security->unlockedActions = array('view');

			parent::beforeFilter();
		}

}
