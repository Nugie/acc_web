<?php
App::uses('ContactsAppModel', 'Contacts.Model');

class Contact extends ContactsAppModel {

	var $name = "Contact";
	var $useTable = false;
	var $validate = array(
		"RECIPIENT_ID" => array(
			"notBlank" => array(
				"rule" => "notBlank",
				"message" => "Jenis pesan harus diisi."
			)
		),
		"IDENTITY" => array(
			"notBlank" => array(
				"rule" => "notBlank",
				"message" => "Identitas harus diisi."
			),
		),
		"NO_AGGR" => array(
			"minLength" => array(
				"rule" => array("minLength", 17),
				"message" => "Panjang nomor kontrak adalah 17 karakter."
			),
          "numeric" => array(
              "rule" => array("numeric"),
              "message" => "Nomor kontrak harus angka."
          )//YAR - 20150407 - add rule numeric
		),
		"CALLER_NAME_NONCUST" => array(
			"notBlank" => array(
				"rule" => "notBlank",
				"message" => "Nama harus diisi."
			),
			"minLength" => array(
				"rule" => array("minLength", 3),
				"message" => "Panjang nama pelapor minimal 3 karakter."
			)
		),
		"RELATION" => array(
			"notBlank" => array(
				"rule" => "notBlank",
				"message" => "Hubungan pelapor dengan pelanggan harus diisi."
			),
		),
		"SUBJECT" => array(
			"notBlank" => array(
				"rule" => "notBlank",
				"message" => "Kategori pesan harus diisi."
			),
		),
		"COMPLAINT_CATEGORY_ID" => array(
			"notBlank" => array(
				"rule" => "notBlank",
				"message" => "Kategori pesan harus diisi."
			),
		),
		"ADDRESS" => array(
			"minLength" => array(
				"rule" => array("minLength", 4),
				"message" => "Panjang alamat minimal 4 karakter."
			)
		),
		"TELEPHONE" => array(
			"minLength" => array(
				"rule" => array("minLength", 4),
				"message" => "Panjang telepon minimal 4 karakter."
			)
		),
		"EMAIL" => array(
			"notBlank" => array(
				"rule" => "notBlank",
				"message" => "Harap masukkan alamat email."
			),
			"email" => array(
				"rule" => array("email", true),
				"message" => "Harap masukkan alamat email dengan benar."
			)
		),
		"MESSAGE_CONTENT" => array(
			"minLength" => array(
				"rule" => array("minLength", 4),
				"message" => "Panjang pesan minimal 4 karakter."
			)
		),
		"ATTACHMENT" => array(
			'rule' => array('extension', array('jpeg', 'png', 'jpg')),
			'message' => 'Harap upload gambar dengan format PNG, JPG atau JPEG.',
			'allowEmpty' => true
		)
	);

	public function beforeValidate($options = array()) {
		if (empty($this->data[$this->alias]['ATTACHMENT']['name'])) {
			unset($this->validate['ATTACHMENT']);
		}

		if ($this->data[$this->alias]['IDENTITY'] == 'Non Pelanggan') {
			unset($this->validate['NO_AGGR']);
		}

		if ($this->data[$this->alias]['RECIPIENT_ID'] != 'complaint') {
			unset($this->validate['COMPLAINT_CATEGORY_ID']);
		} else {
			unset($this->validate['SUBJECT']);
		}

		return true;
	}

}

?>