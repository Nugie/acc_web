<?php
require_once 'medoo.php';

// Initialize

/*$database = new medoo([
    'database_type' => 'mysql',
    'database_name' => 'acc_new',
    'server' => 'localhost',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8'
]);*/

$database = new medoo([
    'database_type' => 'mysql',
    'database_name' => 'acc_web_jan2017',
    'server' => 'localhost',
    'username' => 'acc_samdesign',
    'password' => 'sam_tebet',
    'charset' => 'utf8'
]);

function cleanInput($input) {

  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );

    $output = preg_replace($search, '', $input);
    return $output;
  }

 function sanitize($input) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val);
        }
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        $output = $input;
    }
    return $output;
}
?>
