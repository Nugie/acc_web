<?php
error_reporting(0);
session_start();
require_once 'includes/config.php';
require_once 'includes/PHPMailer/PHPMailerAutoload.php';
//require_once 'includes/autoload.php';
$pid = sanitize($_POST['paket']);
$pkv = sanitize($_POST['vtype']);
$nama = sanitize($_POST['nama']);
$notelp = sanitize($_POST['notelp']);
$emailaddr = sanitize($_POST['emailaddr']);
$kota = sanitize($_POST['kota']);
$cabang = sanitize($_POST['cabang']);
$paketkreditnya = "KARYAWAN";
$extrainfo = sanitize($_POST['extrainfo']);

$areas = $database->select("areas", [
	"id",
	"description"
], [
	"id" => $kota
]);
$namakota = $areas[0]["description"];

$category = $database->select("paketkredit_vehicle_types", [
"[>]paketkredit_categories" => ["pkc_id" => "pkc_id"],
	], [
	"paketkredit_categories.pkc_name",
	"paketkredit_vehicle_types.pkv_vehicletype"
], [
	"paketkredit_vehicle_types.pkv_id" => $pkv,
	"LIMIT" => 1
]);

$packages = $database->select("paketkredit_packages", [
	"pkp_id",
	"pkv_id",
	"pkp_variant",
	"pkp_price",
	"pkp_term",
	"pkp_pdp",
	"pkp_dp",
	"pkp_installment"
], [
	"pkp_id" => $pid
]);

$jenispaket = $category[0]["pkc_name"];
$tipemobil = $category[0]["pkv_vehicletype"];
$jenismobil = $packages[0]["pkp_variant"];
$hargamobil = $packages[0]["pkp_price"];
$termmobil = $packages[0]["pkp_term"];
$persendp = $packages[0]["pkp_pdp"];
$dpmobil = $packages[0]["pkp_dp"];
$angsuranmobil = $packages[0]["pkp_installment"];

$categories = $database->select("paketkredit_categories", [
	"pkc_id",
	"pkc_name",
	"pkc_order"
], [
  "paketkredit_categories.pkc_id[<=]" => 2,
	"ORDER" => "pkc_order"
]);

$siteKey = '6LdpsxETAAAAAOlPjhE-uQbe-8Y3Bs60vgy9axSJ';
$secret = '6LdpsxETAAAAAGB9VYRuOIZKOvVpAIEZnGs3vKsi';
$vendor = "toyota";
?>

<?php include_once('header.php');?>
<div id="maincontent">
	<div class="row">
		<div class="col-md-12">
    	<?php
            if (!isset($_POST['g-recaptcha-response'], $nama, $extrainfo, $notelp, $emailaddr, $kota, $cabang)) {
			?>
         <div class="box-orange2 text-center">
			   Mohon lengkapi data diri Anda!
			    </div>
          <?php } else {
						$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
						if ($response['success'] == '1') {
				//proses

      	$pkentry = $database->insert("paketkredit_applicants", [
				"pkp_id" => $pid,
				"pka_name" => $nama,
				"pka_phone" => $notelp,
				"pka_email" => $emailaddr,
				"pka_city" => $namakota,
				"pka_branch" => $cabang,
				"pka_category" => $jenispaket,
				"pka_type" => $tipemobil,
				"pka_variant" => $jenismobil,
				"pka_price" => $hargamobil,
				"pka_term" => $termmobil,
				"pka_pdp" => $persendp,
				"pka_dp" => $angsuranmobil,
				"pka_installment" => $dpmobil,
				"pka_created" => date('Y-m-d H:i:s'),
				"pka_referral" => $_SESSION["referral"],
				"pka_packages" => $paketkreditnya,
				"pka_extrainfo" => $extrainfo,
			]);

				//send email

				$recipients = $database->select("paketkredit_recipients", [
				"name",
				"email"
				], [
				"active" => 'Yes'
				]);

				//$to="";
				//foreach($recipients as $rcpt_to){
				//	$to .= $rcpt_to["email"]."; ";
				 //}


				/*$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 $headers = 'From: '.$nama.' <'.$emailaddr.'>'."\r\n" .
				 'Bcc: benyfr@gmail.com\r\n' .
				'Reply-To: '.$emailaddr. "\r\n" .
				'X-Mailer: PHP/' . phpversion();*/
				$pesanh = "Nama : $nama <br />";
				$pesanh .= "Email : $emailaddr <br />";
				$pesanh .= "No. Telepon : $notelp <br />";
				$pesanh .= "NPK : $extrainfo <br />";
				$pesanh .= "Kota : $namakota <br />";
				$pesanh .= "Cabang Terdekat : $cabang <br />";
				$pesanh .= "Brand : $jenispaket <br />";
				$pesanh .= "Tipe Mobil : $tipemobil <br />";
				$pesanh .= "Jenis Mobil : $jenismobil <br />";
				$pesanh .= "Tenor : $termmobil bulan<br />";
				$pesanh .= "Persentase DP : ".$persendp."% <br />";
				$pesanh .= "Harga Mobil : ".number_format($hargamobil,0,',','.')." <br />";
				$pesanh .= "Angsuran : ".number_format($angsuranmobil,0,',','.')." <br />";
				$pesanh .= "Uang Muka : ".number_format($dpmobil,0,',','.')." <br />";
				$pesanh .= "Referral : ".$_SESSION["referral"]." <br />";
				$pesanh .= "Paket Kredit : ".$paketkreditnya." <br />";

				$pesan = "Nama : $nama \r\n";
				$pesan .= "Email : $emailaddr \r\n";
				$pesan .= "No. Telepon : $notelp \r\n";
				$pesan .= "NPK : $extrainfo \r\n";
				$pesan .= "Kota : $namakota \r\n";
				$pesan .= "Cabang Terdekat : $cabang \r\n";
				$pesan .= "Brand : $jenispaket \r\n";
				$pesan .= "Tipe Mobil : $tipemobil \r\n";
				$pesan .= "Jenis Mobil : $jenismobil \r\n";
				$pesan .= "Tenor : $termmobil bulan\r\n";
				$pesan .= "Persentase DP : ".$persendp."% \r\n";
				$pesan .= "Harga Mobil : ".number_format($hargamobil,0,',','.')." \r\n";
				$pesan .= "Angsuran : ".number_format($angsuranmobil,0,',','.')." \r\n";
				$pesan .= "Uang Muka : ".number_format($dpmobil,0,',','.')." \r\n";
				$pesan .= "Referral : ".$_SESSION["referral"]." \r\n";
				$pesan .= "Paket Kredit : ".$paketkreditnya." \r\n";

				//send mail
				$mail = new PHPMailer;
				$mail->isSendmail();
				$mail->setFrom("info@acc.co.id", "ACC Website");
				$mail->addReplyTo($emailaddr, $nama);
				foreach($recipients as $rcpt_to){
					$mail->addAddress($rcpt_to["email"],$rcpt_to["name"]);
				}
				$mail->isHTML(true);
				$mail->Subject = '[Paket Kredit KARYAWAN] Customer Apply';
				$mail->Body    = $pesanh;
				$mail->AltBody = $pesan;

				$pesannya = "Terima kasih, aplikasi kredit Anda akan segera kami proses. \r\n\r\n\r\n Astra Credit Companies";
				$pesannyah = "Terima kasih, aplikasi kredit Anda akan segera kami proses. <br><br> Astra Credit Companies";

				$mail2 = new PHPMailer;
				$mail2->isSendmail();
				$mail2->setFrom('info@acc.co.id','Astra Credit Companies');
				$mail2->addReplyTo('info@acc.co.id','Astra Credit Companies');
				$mail2->addAddress($emailaddr, $nama);
				$mail2->isHTML(true);
				$mail2->Subject = '[Paket Kredit ACC] Thank You';
				$mail2->Body    = $pesannyah;
				$mail2->AltBody = $pesannya;

				if($mail->send()||$mail2->send()) {
		?>
    		  	<div class="box-orange2 text-center" style="margin-top:20px;">
			    Terima kasih, aplikasi Paket Kredit ACC Anda telah terkirim. Kami akan memberikan respon kepada Anda secepatnya!
			    </div>
              <!--end proc-->
          <?php } else {?>
					<div id="maincontent">
				  <div class="box-orange2 text-center" style="margin-top:20px;">
			    Terima kasih, aplikasi Paket Kredit ACC Anda telah terkirim. Kami akan memberikan respon kepada Anda secepatnya!
			    </div>
					</div>
			<?php	}
			  } else { ?>
					<div id="maincontent">
               <div class="box-orange2 text-center">
			    	 			Silahkan cek kembali kode keamanan Anda!
			    	</div>
					</div>
              <?php } }?>
 			</div>
		</div></div>
<?php include_once('footer.php');?>
