<?php
include('class/database.class.php');
error_reporting(0);

	session_start();

    $query_branch = "SELECT branch_id, description FROM cso_branches WHERE area_id = 1 ORDER BY description";
	$branch_list = database::getData($query_branch);

	$query_type = "SELECT type_id, description FROM cso_vehicle_types WHERE brand_id = 1 ORDER BY description";
	$type_list = database::getData($query_type);

	$tenor_list = array(
		"1" => "1 Tahun",
		"2" => "2 Tahun",
		"3" => "3 Tahun",
		"4" => "4 Tahun",
	);
	//print_r($_REQUEST);
	if ($_REQUEST['model_id']!=""){

		$idModel	= $_REQUEST['model_id'];
		$idType = ($_SESSION['type']);
		$query_otr = "SELECT * FROM cso_daihatsu WHERE a_variantid =  $idModel";
		$tmp_otr = database::getData($query_otr);
		$otr = $tmp_otr[0]['a_price'];
		$otr = number_format($otr,0,'','.')
		?>
		<!--<input name="otrd" type="text" id="otrd" size="23" class="teksnya" value="<?php echo $otr?>" disabled="disabled"/>-->

        <input name="otr" type="text" id="otr" size="23" class="otr teksnya" value="<?php echo $otr?>" />
       <?php
	}

function hitung_simulasi($area, $branch, $type, $model, $acp_status, $otr, $dp, $tenor)
	{
		$otr = (int)str_replace('.', '', $otr);
		$downpayment = $dp * $otr;
		$percent_dp = ($dp * 100);
		if ($acp_status == "Yes")
		{
			if ($tenor>1) {
				$acp_rate = 1.69;
			} else {
				$acp_rate = 0.69;
			}
		}
		else
			$acp_rate = 0;

		$query_kind = "SELECT a_type FROM cso_daihatsu WHERE a_variantid=".$model." limit 1";
		$tmp_kind = database::getData($query_kind);
		$kind = $tmp_kind[0]['a_type'];

		$query_insc1 = "SELECT * FROM cso_insurances WHERE category_id = 1 AND tenor = ".$tenor." AND vehicle_status = 'New' AND otr_start <= ".$otr." AND otr_end >= ".$otr." limit 1";
		$query_insc2 = "SELECT * FROM cso_insurances WHERE category_id = 1 AND tenor = 1 AND vehicle_status = 'New' AND otr_start <= ".$otr." AND otr_end >= ".$otr." limit 1";
		$tmp_insc1 = database::getData($query_insc1);
		$tmp_insc2 = database::getData($query_insc2);
		$insurance1 = $tmp_insc1[0]['ar_gross_rate'];
		$insurance2 = $tmp_insc2[0]['ar_gross_rate'];

		$query_flatrate = "SELECT * FROM cso_new_rates WHERE kind_id = ".$kind." AND tenor = ".$tenor." AND from_dp <= ".$percent_dp." AND to_dp >= ".$percent_dp." ORDER BY nr_id DESC limit 1";
		$tmp_flatrate = database::getData($query_flatrate);
		$flat_rate = $tmp_flatrate[0]['flat_rate_percentage'];


		/*$query_adm = "SELECT * FROM cso_administrations WHERE tenor = ".$tenor." AND vehicle_status = 'New' ORDER BY adm_id DESC limit 1";
		$tmp_adm = database::getData($query_adm);
		$administration_polis = $tmp_adm[0]['polis'];
		$administration_adm = $tmp_adm[0]['administration'];
		$administration_fiducia = $tmp_adm[0]['fiducia'];
		*/
		// -------------------------------------------------------------------------------------------------------------------------------
		// Hitung Pokok Hutang
		// -------------------------------------------------------------------------------------------------------------------------------

		$asuransikredit = ($otr * $insurance1) / 100;
		$HargaOTR = ($otr - $downpayment); /*OTR setelah dikurangi DP*/
		$Bunga = (($HargaOTR* $flat_rate) / 100) * $tenor; /*bunga*/
		$TotalHutang0 = $HargaOTR + $Bunga + $asuransikredit; /*Total hutang*/
		$ACP0 = ($TotalHutang0 * $acp_rate) / 100; /*ACP*/
		$ACP = ceil($ACP0 / 1000) * 1000;
		$TotalHutang = $HargaOTR + $Bunga + $asuransikredit + $ACP;
		$BungaBaru = ($HargaOTR+$asuransikredit+$ACP)*($flat_rate/100)*$tenor;
		$PHBaru = $HargaOTR+$asuransikredit+$ACP;
		$ARBaru = $BungaBaru + $PHBaru;
		$H37 = ceil($ARBaru / ($tenor * 12) / 1000) * 1000;

		//$D35 = $downpayment + $H37 + $F18 + $administration_polis + $administration_adm + $administration_fiducia;
		//echo $downpayment."-".$H37."-".$ACP."<br />";
		$D35 = $downpayment + $H37 + 1500000;
		$uang_muka = $D35;
		$angsuran =  ceil($H37 / 10000) * 10000;
		//$result = "H20 : {$H20}\nH22 : {$H22}\nH23 : {$H23}\n";
		$result = array('uang_muka' => $uang_muka,'angsuran' => $angsuran);
		//$rate = $model;

		return $result;
	}
?>
