<? 
session_cache_expire(10);
session_start();?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Simulasi Kredit ACC</title>

<?php 
include 'simulation.php';
?>
<link type="text/css" href="ui/demos.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="jquery.selectbox.css" />
<link rel="stylesheet" type="text/css" href="jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" href="jquery-ui-1.9.1.custom.min.css" />
<script type="text/javascript" src="js/jquery-1.8.2.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.9.1.custom.min.js"></script>
<script type="text/javascript" src="js/cufon-yui.js" ></script>
<script type="text/javascript" src="js/Myriad_Pro_400-Myriad_Pro_700.font.js"></script>
<script type="text/javascript" src="js/jquery.selectbox-0.2.min.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="js/jquery.maskMoney.min.js"></script>


<script type="text/javascript">
Cufon.replace('.block_acc', { fontFamily: 'Myriad Pro' });
Cufon.replace('.company_title', { fontFamily: 'Myriad Pro' });

	$(function() {
		$(".otr").maskMoney({thousands:'.', decimal:'', allowZero:false, suffix: '', precision:0});
		$("#loader").hide();
	
		$("#area").selectbox({
			onChange: function (val, inst) {
				$("#branch_loading").html('<img src="images/ajax-loader.gif" />');
				$.ajax({
					type: "GET",
					data: {area: val},
					url: "load_query.php",
					success: function (data) {
						$("#branch_loading").html(data);
						$("#branch").selectbox({
							onChange: function (val, inst) {
								
								$("#applybutton").html('');
							}	
							});
					}
				});
			},
			effect: "slide"
			});	
		$("#vehicle_model").selectbox({
			width:300,
			onChange: function (val, inst) {
				$("#uang-muka").html('');
				$("#angsuran").html('');
				$("#applybutton").html('');
						$('#otr_loading').fadeOut();
						$('#loader').show();
						$.post("simulation.php", {
							model_id: $('#vehicle_model').val(),
						}, function(response){
							
							setTimeout("finishAjax('otr_loading', '"+escape(response)+"')", 400);
						});
						return false;
			}	
			});
		
		$("#dp_id").selectbox({
			onChange: function (val, inst) {
				$("#uang-muka").html('');
				$("#angsuran").html('');
				$("#applybutton").html('');
			}	
			});
		$("#branch").selectbox({
			onChange: function (val, inst) {
				$("#applybutton").html('');
			}	
			});
		$("#tenor_id").selectbox({
			onChange: function (val, inst) {
				$("#uang-muka").html('');
				$("#angsuran").html('');
				$("#applybutton").html('');
			}	
			});
			
		$("#vehicle_type").selectbox({
			onChange: function (val, inst) {
				$("#otr").val('');
				$("#uang-muka").html('');
				$("#angsuran").html('');
				$("#applybutton").html('');
				$("#model_loading").html('<img src="images/ajax-loader.gif" />');
				$.ajax({
					type: "GET",
					data: {type_id: val},
					url: "load_query.php",
					success: function (data) {
						$("#model_loading").html(data);
						$("#vehicle_model").selectbox();
					}
				});
			},
			effect: "slide"
			});		
			$('#otr').keyup(function() {
 				$("#uang-muka").html('');
				$("#angsuran").html('');
				$("#applybutton").html('');
			});
			$( "#AcpStatusYes, #AcpStatusNo" ).change(function () {
  				  $("#uang-muka").html('');
				$("#angsuran").html('');
				$("#applybutton").html('');
			});
			
	});
	
	function finishAjax(id, response){
	$('#loader').hide();
	$('#'+id).html(unescape(response));
	$('#'+id).fadeIn();
	} 
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48042405-1', 'acc.co.id');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
	<div id="header-center">
    	<div class="blueblock gradien1">
					<div class="top_logo">
						<a href="http://www.astracreditcompanies.com/" target="_blank">
							<img src="images/logo.jpg" border="0" />
						</a>
					</div>
					<div class="company_title"><a href="http://www.astracreditcompanies.com/" class="putih" target="_blank">Astra Credit Companies</a>
					</div>
					<a href="http://www.astracreditcompanies.com/" target="_blank">
						<div class="block_acc">
							<div class="ic_acc">
							<b>ACC</b><br/>
							Credit Simulation</div>
						</div>
					</a>
              </div>
	</div>
	<div id="content">
	
		<form name="credit-simulations-form" id="credit-simulations-form" method="post" action="index.php" accept-charset="utf-8">
			<table width="100%" border="0" cellspacing="0" cellpadding="6">
				<tbody>
				<tr>
					<td valign="top" width="140">Area / Daerah</td>
					<td valign="top">:</td>
					<td valign="top"><?php $idArea = $_REQUEST['area'];?>
					<select name="area" id="area" class="styled-combo">
						<?
								$query_area = "SELECT area_id, description FROM cso_areas ORDER BY description";
								$area_list = database::getData($query_area);?>
							<option value="">Select Area</option>
						<?php foreach($area_list as $area){
								if($area['area_id'] == $idArea){
						?>
							<option value="<?= $area['area_id'] ?>" selected><?= $area['description'] ?></option>
							<?php } else { ?>
							<option value="<?= $area['area_id'] ?>" <? if($area['area_id'] == 1) {?>selected="selected"<? }?>><?= $area['description'] ?></option>
							<?php } } ?>
					</select>
					<td>
				</tr>
				<tr>
					<td valign="top" width="140">Cabang ACC terdekat</td>
					<td valign="top">:</td>
					<td valign="top"><?php $idBranch = $_REQUEST['branch'];
					?>
					<div id="branch_loading">
					<select name="branch" id="branch" class="styled-combo" required="required">
                    <option value="">Select Branch</option>
							<?
									if ($idArea=="") {
									$query_branch = "SELECT branch_id, description FROM cso_branches WHERE area_id = 1 ORDER BY description";
									}else {
									$query_branch = "SELECT branch_id, description FROM cso_branches WHERE area_id = ".$idArea." ORDER BY description";
									}
									$branch_list = database::getData($query_branch);?>
									
								<?php foreach($branch_list as $branch){
										if($branch['branch_id']==$idBranch){?>
											<option value="<?php echo $branch['branch_id']?>" selected><?= $branch['description']?></option>
										<?php } else {?>
											<option value="<?php echo $branch['branch_id']?>" <? if($branch['branch_id'] == 11&&$idBranch=="") {?>selected="selected"<? }?>><?= $branch['description']?></option>
										<?php } } ?>

					</select></div></td>
				</tr>
				<tr>
					<td valign="top" width="140">Model</td>
					<td valign="top">:</td>
					<td valign="top"><?php $idType = $_REQUEST['vehicle_type'];?>
					<select name="vehicle_type" id="vehicle_type" class="styled-combo">
                    <option value="">Select Model</option>
					<?php
						if(!empty($_REQUEST['variant_id'])){
							$id = $_REQUEST['variant_id'];
							$query_type = "SELECT a_variantid, model_id, a_variant, a_price FROM cso_daihatsu WHERE a_variantid = ".$id." limit 1";
							$type_list = database::getData($query_type);
							$price = $type_list[0]['a_price'];
							$modelnya = $type_list[0]['model_id'];
							$query_type1 = "SELECT * FROM cso_daihatsu_model order by model_name";
							$type_list1 = database::getData($query_type1);
							foreach($type_list1 as $tipe1){
							?>
                            	<option value="<?php echo $tipe1['model_id']?>" <? if($tipe1['model_id'] == $modelnya) {?>selected="selected"<? }?>><?= $tipe1['model_name']?></option>
                             <? }?>
						<?	
						} else {
							$query_type = "SELECT * FROM cso_daihatsu_model ORDER BY model_name";
							$type_list = database::getData($query_type);?>
								<?php foreach($type_list as $tipe){?>
                                <?php if($tipe['model_id']==$idType){?>
										<option value="<?php echo $tipe['model_id']?>" selected="selected"><?php echo $tipe['model_name']?></option>
									<?php } else{ ?>
										<option value="<?php echo $tipe['model_id']?>"><?php echo $tipe['model_name']?></option>
								<?php } } }?>
						
					</select></td>
				</tr>
				<tr>
					<td valign="top" width="140">Variant</td>
					<td valign="top">:</td>
					<td valign="top">
					<?php
					$idType = $_REQUEST['vehicle_type'];
					$idModel = $_REQUEST['vehicle_model'];
					?>
					<div id="model_loading">
                    
					<select name="vehicle_model" id="vehicle_model" class="styled-combo">
					
					<?php
					if(!empty($_REQUEST['variant_id'])){
						$query_m = "SELECT a_variantid,a_variant FROM cso_daihatsu where model_id=$modelnya";
						$m_list = database::getData($query_m);
						foreach($m_list as $m){?>
							<option value="<?php echo $m['a_variantid']?>" <? if($m['a_variantid'] == $id) {?>selected="selected"<? }?>><? echo $m['a_variant']?></option>
							<?php }
					} else {
						$query_model = "SELECT a_variantid,a_variant FROM cso_daihatsu WHERE model_id ='". $idType ."' ";
						
						$model_list = database::getData($query_model);
						foreach($model_list as $model){
							if($model['a_variantid']==$idModel){?>
								<option value="<?php echo $model['a_variantid']?>" selected><?= $model['a_variant']?></option>
							<?php } else { ?>
								<option value="<?php echo $model['a_variantid']?>"><?= $model['a_variant']?></option>
					<?php } } }	?>
					</select></div>
					</td>
				</tr>
				<!--<tr>
					<td valign="top" width="140">Menggunakan ACP</td>
					<td valign="top">:</td>
					<td valign="top"><?php $idStatus = $_REQUEST['acp_status'];?>
					<?php if(!empty($_REQUEST['variant_id'])){ ?>
					<ul id="acp_status">
						<li><input type="radio" class="acp_statuss"  name="acp_status" id="AcpStatusYes" value="Yes" checked/> &nbsp;Ya &nbsp;&nbsp;&nbsp;</li>
						<li><input type="radio" class="acp_statuss" name="acp_status" id="AcpStatusNo" value="No"  /> &nbsp;Tidak</li>
					</ul>
					<?php } else { 
					if($idStatus == "Yes"){?>
					<ul id="acp_status">
						<li><input type="radio" class="acp_statuss" name="acp_status" id="AcpStatusYes" value="Yes" checked/> &nbsp;Ya &nbsp;&nbsp;&nbsp;</li>
						<li><input type="radio" class="acp_statuss" name="acp_status" id="AcpStatusNo" value="No"  /> &nbsp;Tidak</li>
					</ul>
					<?php } else { 
					if($idStatus == "No"){?>
					<ul id="acp_status">
						<li><input type="radio" class="acp_statuss" name="acp_status" id="AcpStatusYes" value="Yes"/> &nbsp;Ya &nbsp;&nbsp;&nbsp;</li>
						<li><input type="radio" class="acp_statuss" name="acp_status" id="AcpStatusNo" value="No" checked  /> &nbsp;Tidak</li>
					</ul>
					<?php } else { ?>
					<ul id="acp_status">					
						<li><input type="radio" class="acp_statuss" name="acp_status" id="AcpStatusYes" value="Yes" checked/> &nbsp;Ya &nbsp;&nbsp;&nbsp;</li>
						<li><input type="radio" class="acp_statuss" name="acp_status" id="AcpStatusNo" value="No"/> &nbsp;Tidak</li>
					</ul>
					<?php } } }?>
					</td>
				</tr>-->
                <input type="hidden" name="acp_status" value="Yes" />
				<tr>
					<td valign="top" width="140">Harga Kendaraan</td>
					<td valign="top">:</td>
					<td valign="top"><?php $val_otr = $_REQUEST['otr'];?>
					<div id="otr_loading">
					<?php if(!empty($_REQUEST['variant_id'])){ ?>
						<!--<input name="otrd" type="text" id="otrd" size="23" class="teksnya" value="<?php echo $price?>" disabled="disabled"/>-->
                        <input name="otr" type="text" id="otr" size="23" class="otr teksnya" value="<?php echo $price?>" />
					<?php } else { ?>
						<!--<input name="otrd" type="text" id="otrd" size="23" class="teksnya" value="<?php echo $val_otr?>" disabled="disabled"/>-->
                        <input name="otr" type="text" id="otr" size="23" class="otr teksnya" value="<?php echo $val_otr?>" />
					<?php } ?></div><div id="loader"></div>
					<em>(harga kendaraan dapat diubah secara manual)</em>
					</td>
				</tr>
				<tr>
					<td valign="top" width="140">Down Payment (DP)</td>
					<td valign="top">:</td>
					<td valign="top"><?php $dp = $_REQUEST['dp'];?>
					<select name="dp" id="dp_id" class="styled-combo">
						<?php if(!empty($_REQUEST['variant_id'])){ ?>
	                        <option value="0.25" selected="selected">25%</option>
							<option value="0.3">30%</option>
							<option value="0.4">40%</option>
							<option value="0.5">50%</option>
                            <option value="0.6">60%</option>
						<?php } else {
								if($dp == 0.25){?>
                                <option value="0.25" selected="selected">25%</option>
									<option value="0.3">30%</option>
									<option value="0.4">40%</option>
									<option value="0.5">50%</option>
                                     <option value="0.6">60%</option>
						<?php } elseif($dp == 0.3){ ?>
                        <option value="0.25">25%</option>
									<option value="0.3" selected>30%</option>
									<option value="0.4">40%</option>
									<option value="0.5">50%</option>
                                    <option value="0.6">60%</option>
                                    <?php } elseif($dp == 0.4){ ?>
                        			<option value="0.25">25%</option>
									<option value="0.3">30%</option>
									<option value="0.4" selected>40%</option>
									<option value="0.5">50%</option>
                                    <option value="0.6">60%</option>
                                     <?php } elseif($dp == 0.5){ ?>
                        			<option value="0.25">25%</option>
									<option value="0.3">30%</option>
									<option value="0.4">40%</option>
									<option value="0.5" selected="selected">50%</option>
                                    <option value="0.6">60%</option>
									  <?php } elseif($dp == 0.6){ ?>
                        			<option value="0.25">25%</option>
									<option value="0.3">30%</option>
									<option value="0.4">40%</option>
									<option value="0.5">50%</option>
                                    <option value="0.6" selected="selected">60%</option>
									<?php } else { ?>
                       				 <option value="0.25" selected="selected">25%</option>
									<option value="0.3">30%</option>
									<option value="0.4">40%</option>
									<option value="0.5">50%</option>
                                    <option value="0.6">60%</option>
						<?php } } ?> 
					</select>
					</td>
				</tr>
				<tr>
					<td valign="top" width="140">Term / Tenor</td>
					<td valign="top">:</td>
					<td valign="top"><?php $tenor = $_REQUEST['tenor'];?>
					<select name="tenor" id="tenor_id" class="styled-combo">
					<?php if(!empty($_REQUEST['variant_id'])){
						for($i=1;$i<=4;$i++){?>
							<option value="<?=$i?>"><?= $tenor_list[$i]?></option>
						<?php } 
						} else {
						for($i=1;$i<=4;$i++){
							if($i == $tenor){ ?>
								<option value="<?=$i?>" selected><?= $tenor_list[$i]?></option>
						<?php } else { ?>
								<option value="<?=$i?>"><?= $tenor_list[$i]?></option>
						<?php } } } ?>
					</select>
					</td>
				</tr>
		
			<tr>
				<td valign="top" width="140">Uang Muka</td>
				<td valign="top">:</td>
				<td valign="top"><strong><div id="uang-muka"></div></strong></td>
			</tr>
			<tr>
				<td valign="top" width="140">Angsuran</td>
				<td valign="top">:</td>
				<td valign="top"><strong><div id="angsuran"></div></strong></td>
			</tr>
			<?
if(!empty($_REQUEST['variant_id'])){ 
	$idmodela = $_REQUEST['variant_id'];
} else {
	$idmodela = $idModel;
}
$query_model = "SELECT a_variant, b.model_name FROM cso_daihatsu a left join cso_daihatsu_model b on a.model_id=b.model_id WHERE a.a_variantid ='". $idmodela ."' ";
	
	$model_list = database::getData($query_model);
	foreach($model_list as $model){
		$pilihan = $model["model_name"]." - ".$model["a_variant"];
	} 	

	?>
			<tr>
				<td valign="top" width="140">&nbsp;</td>
				<td valign="top">&nbsp;</td>
				<td valign="top" style="padding-top:30px;">
				<ul id="button_calc">
					<li><a id="form-submit" class="tombol ui-corner-all" href="#" onclick= document.forms["credit-simulations-form"].submit();>Calculate</a></li>
					<? if (!empty($pilihan)&&!empty($idBranch)) {
                    	$_SESSION["pilihan"] = $pilihan;
						$_SESSION["idBranch"] = $idBranch;
					?>
					<li></li>
					<li><div id="applybutton">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a id="applys" class="tombol ui-corner-all fancybox fancybox.iframe" href="apply.php">Apply</a></div></li>
					<? }?>
				</ul>
				</td>
			</tr>
	</tbody>
</table>
</form>
	  
    </div>
    <div id="term">
		<ul id="toc">
	        <li>&nbsp;* OTR DKI Jakarta</li>
			<li>&nbsp;* Harga sudah termasuk asuransi All-Risk</li>
			<li>&nbsp;** Rincian kredit diatas tidak mengikat dan dapat berubah sewaktu-waktu. Untuk penawaran menarik di bulan ini, Anda dapat menghubungi dealer Daihatsu terdekat.</li>
		</ul>
	  </div>
       <div id="footer"><img src="images/footer1.jpg" alt="Footer" border="0" usemap="#Map" />
        <map name="Map">
          <area shape="rect" coords="5,3,215,49" href="http://www.acc.co.id" alt="ACC Website" target="_blank" title="ACC Website">
          <area shape="rect" coords="266,3,439,51" href="https://www.facebook.com/acckreditmobil" alt="Facebook ACC" target="_blank" title="Facebook ACC">
        </map>
      </div>
      <div id="footer2"><img src="images/footer2.jpg" alt="Footer" border="0" usemap="#Map2" />
        <map name="Map2">
          <area shape="rect" coords="5,3,215,49" href="https://www.twitter.com/acckreditmobil" alt="Twitter ACC" target="_blank" title="Twitter ACC">
        </map>
  </div>
</div>
<?php 

if($_POST){
	$area = $_POST['area'];
	$branch = $_POST['branch'];
	$v_type = $_POST['vehicle_type'];
	$v_model = $_POST['vehicle_model'];
	$acp = $_POST['acp_status'];
	$otr = $_POST['otr'];
	$dp = $_POST['dp'];
	$tenor = $_POST['tenor'];
	
	$hit = hitung_simulasi($area,$branch,$v_type,$v_model,$acp,$otr,$dp,$tenor);
	if ($area==''||$branch==''||$v_type==''||$v_model==''){
	$uang_muka = "<span style=\"color:red;\">Mohon lengkapi data Area, Cabang, Model dan Variant terlebih dahulu</span>";
	$angsuran = "";
	} else {
	$uang_muka = number_format($hit['uang_muka'],0,',','.');
	$angsuran = number_format($hit['angsuran'],0,',','.');
	}
	
}
 ?>
<script>
	//document.getElementById('vehicle_type').selectedIndex = '<?php echo $_POST['vehicle_type']?>';
	//document.getElementById('vehicle_model').innerHTML = '<?php echo $_POST['vehicle_model']?>';
	//document.getElementById('acp_status').innerHTML = '<?php echo $_POST['acp_status']?>';
	//document.getElementById('otr').innerHTML = '<?php echo $_POST['otr']?>';
	//document.getElementById('dp').selectedIndex = '<?php echo $_POST['dp']?>';
	//document.getElementById('tenor').selectedIndex = '<?php echo $_POST['tenor']?>';
	document.getElementById('uang-muka').innerHTML = '<?php echo $uang_muka?>';
	document.getElementById('angsuran').innerHTML = '<?php echo $angsuran?>';

	$(document).ready(function() {
		$('.fancybox').fancybox(
		{
			maxWidth	: 350,
			maxHeight	: 500,
			fitToView	: false,
			margin		: 0,
			padding		: 5,
			width		: '330px',
			height		: '540px',
			autoSize	: false,
			closeBtn	: false,
			topRatio : 0.6,
			closeClick	: false	
		}
		);
	})
</script>
</body></html>