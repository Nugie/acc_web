<? 
ini_set('display_errors', 0);
session_start();
include ('class/database.class.php');
$pilihan = $_SESSION["pilihan"];
$idBranch = $_SESSION["idBranch"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apply Credit</title>
<link type="text/css" href="ui/demos.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="jquery-ui-1.9.1.custom.min.css" />
<script type="text/javascript" src="js/jquery-1.8.2.js"></script>
<script src="js/jquery.tools.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.1.custom.min.js"></script>
  <script>
   $(function() {
		var currentyear=(new Date).getFullYear();
		maxyear = currentyear-10;
		$("#datebirth" ).datepicker({ maxDate: maxyear, dateFormat:"d MM yy", changeMonth: true, changeYear: true,showButtonPanel: true,yearRange: "-100:-10"  });   
  	 }
   )
   </script>
</head>

<body class="bodydialog">
<div class="demo" style="margin:0;padding:0;">
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-dialog-buttons" tabindex="-1" role="dialog" aria-labelledby="ui-id-1" style="outline: 0px;height: auto; width: 325px; display: block;margin:0;padding:0;">
<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span id="ui-id-1" class="ui-dialog-title">Apply Credit</span><a href="javascript:void(0)" onclick="javascript:parent.jQuery.fancybox.close();" class="ui-dialog-titlebar-close ui-corner-all" role="button"><span class="ui-icon ui-icon-closethick">close</span></a></div>
	<form action="sendapps.php" method="post" enctype="multipart/form-data" id="applyform" class="cols">
	<fieldset>
		<label for="name">Nama
		<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" required="required" /></label>
		<label for="email">Email
		<input type="email" name="email" id="email" value="" class="text ui-widget-content ui-corner-all" required="required" /></label>
		<label for="placebirth">Tempat, Tanggal Lahir
		<input type="text" name="placebirth" id="placebirth" value="" class="text ui-widget-content ui-corner-all" required="required" placeholder="Tempat Lahir" /><input type="text" name="datebirth" id="datebirth" value="" placeholder="Tanggal Lahir" class="text ui-widget-content ui-corner-all" required="required" size="10" readonly="readonly" /></label>
		<label for="phone">No. Telp
		<input type="text" name="phone" id="phone" value="" class="text ui-widget-content ui-corner-all" required="required" /></label>
		<label for="address">Alamat
		<input type="text" name="address" id="address" value="" class="text ui-widget-content ui-corner-all" required="required" /></label>
		<label for="scanid">File Scan KTP (format JPG/GIF/PNG)
        <input type="file" name="scanid" class="text ui-widget-content ui-corner-all" required="required" id="scanid" /></label>
	<?
            $query_branch = "SELECT branch_id, description FROM cso_branches WHERE branch_id = ".$idBranch." limit 1";
            $branch_list = database::getData($query_branch);?>
            
        <?php foreach($branch_list as $branch){
            $branchname = $branch['description'];
         }  ?>
		<input type="hidden" name="dbranch" id="dbranch" value="<? echo $branchname;?>" />
        <input type="hidden" name="model" id="model" value="<? echo $pilihan;?>" />
        <div style="text-align:right;"><button type="submit" value="Apply" />Apply</button><button href="javascript:void();" onclick="javascript:parent.jQuery.fancybox.close();">Cancel</button></div>
	</fieldset>
	</form>
   </div>
 </div>
 <script type="text/javascript">
$.tools.validator.localize("id", {
	'*'			: 'Harap isi kolom ini',
	':email'  	: 'Isi dengan alamat email',
	':number' 	: 'Isi dengan angka',
	':url' 		: 'Isi dengan URL',
	'[max]'	 	: 'Maksimal karakter $1',
	'[min]'		: 'Minimal karakter $1',
	'[required]'	: 'Harap isi kolom ini'
});
 
$("#applyform").validator(
	{ lang: 'id'}
);
</script>
</body>
</html>